Представлена демо-версия программы

- работает только один маршрут с первого на третий путь;

- ограничен выбор неисправностей;

- не все схемы представлены в списке;

Для запуска необходимо установленная jre1.8

Для управления станцией в нормальном режиме(без неисправностей)

необходимо выполнить вход в панели идентификации (логин "1", пароль "1")

   Программа предназначена для отработки поиска неисправностей в устройствах СЦБ.  Программа имитирует работу реальных устройств в схемах управления станцией, путём создания и запуска  виртуальных электрических цепей. Текущее состояние устройств отображается на панели управления станцией. (вкладка «Арм  ДСЦП»).  Действия по управлению станцией (Задание и отмена маршрутов, перевод стрелок  и т.д.), также осуществляются на этой вкладке.  Изменение состояния устройств, можно посмотреть  на вкладке «схемы» (рис.2.) на которой,  также осуществляется поиск неисправностей путем измерения параметров и осмотра элементов схем. Для начала поиска неисправности необходимо  после загрузки программы на вкладке «Выбор задания» нажать кнопку «старт». После получения информации, необходимо попробовать выполнить указанное действие, найти и устранить неисправность во вкладке «схемы». Кроме того существует возможность посмотреть схемы и потренироваться в управлении станции в нормальном режиме, установить неисправность вручную и т.д. Для создания новых схем присутствует редактор который позволяет нарисовать и сохранить схемы в файлах формата “xml”, а также создать из этих рисунков виртуальные электрические цепи.

