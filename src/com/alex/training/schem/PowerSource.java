﻿package com.alex.training.schem;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;

import com.alex.training.schemElement.PowerSourceTerminal;
import com.alex.training.schemElement.SchemElement;

/**
 * @author Alex
 *
 */
public class PowerSource implements Source {

	protected String varName;
	protected List<PowerSourceTerminal> terminals = new ArrayList<PowerSourceTerminal>();
	protected List<PowerSourceTerminal> minusTerminals = new ArrayList<PowerSourceTerminal>();
	// пороговое значение сопротивления цепи после которого
	// падение напряжения на других участках = 0
	protected double rPorog;

	public PowerSource() {
		setIsConst(false);
		setEds(0);
		setInR(0);
	}

	public PowerSource(double u) {
		this();
		setEds(u);
	}

	// свойство тип тока постоянный - true
	// ------------------------------------------------
	public BooleanProperty isConst = new SimpleBooleanProperty();

	public BooleanProperty isConstProperty() {
		return isConst;
	}

	public void setIsConst(boolean newValue) {
		isConst.set(newValue);
	}

	public boolean getIsConst() {
		return isConst.get();
	}

	// ЭДС источника
	public DoubleProperty eds = new SimpleDoubleProperty();

	public DoubleProperty edsProperty() {
		return eds;
	}

	public void setEds(double newValue) {
		eds.set(newValue);
	}

	public double getEds() {
		return eds.get();
	}

	// внутреннее сопротивление источника
	public DoubleProperty inR = new SimpleDoubleProperty();

	public DoubleProperty inRProperty() {
		return inR;
	}

	public void setInR(double newValue) {
		inR.set(newValue);
	}

	public double getInR() {
		return inR.get();
	}

	public PowerSourceTerminal getTerminal(int i) {
		return terminals.get(i);
	}

	public List<PowerSourceTerminal> getMinusTerminals() {
		return minusTerminals;
	}

	public void addTerminal(PowerSourceTerminal t) {
		if (t != null
				&& terminals.stream().allMatch((l) -> !l.equals(t))
				&& terminals.stream().allMatch(
						(te) -> !te.getVarName().equals(t.getVarName())))
			terminals.add(t);

	}

	public void addMinusTerminal(PowerSourceTerminal t) {
		if (t != null
				&& minusTerminals.stream().allMatch((l) -> !l.equals(t))
				&& minusTerminals.stream().allMatch(
						(te) -> !te.getVarName().equals(t.getVarName())))
			minusTerminals.add(t);
	}

	public List<PowerSourceTerminal> getTerminals() {
		return terminals;
	}

	public boolean isTerminalConteined(String varName) {
		return (getTerminal(varName) != null) ? true : false;
	}

	public SchemElement getTerminal(String varName) {
		SchemElement temp = null;
		for (SchemElement sc : terminals) {
			if (sc.getVarName().equals(varName))
				temp = sc;
		}
		if (temp == null)
			temp = getMinusTerminal(varName);

		return temp;
	}

	public SchemElement getMinusTerminal(String newValue) {
		SchemElement temp = null;
		for (SchemElement sc : minusTerminals) {
			if (sc.getVarName().equals(newValue))
				temp = sc;
		}
		return temp;
	}

	public boolean isTerminalConteined(PowerSourceTerminal term) {
		return terminals.stream().anyMatch((t) -> t.equals(term))
				|| minusTerminals.stream().anyMatch((t) -> t.equals(term));
	}


	public int getIndexTerminal(PowerSourceTerminal term) {
		return (isTerminalConteined(term)) ? (terminals.stream()
				.anyMatch((t) -> t.equals(term))) ? terminals.indexOf(term)
				: minusTerminals.indexOf(term) : -1;
	}

	public int getIndexTerminal(String varName) {
		int i = -1;
		if (getTerminals().stream().anyMatch(
				(t) -> t.getVarName().equals(varName)))
			i = terminals.indexOf(this.getTerminal(varName));
		return i;
	}

	public int getIndexMinusTerminal(String varName) {
		int i = -1;
		if (getMinusTerminals().stream().anyMatch(
				(t) -> t.getVarName().equals(varName)))
			i = minusTerminals.indexOf(this.getTerminal(varName));
		return i;
	}

	public PowerSourceTerminal getMinusTerminal(int i) {
		return minusTerminals.get(i);
	}

	public double getRPorog() {
		return rPorog;
	}

	public void setRPorog(double rPorog) {
		this.rPorog = rPorog;
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	@Override
	public String toString() {
		return varName;

	}

}