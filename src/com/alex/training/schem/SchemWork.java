﻿/**
 * 
 */
package com.alex.training.schem;

import java.util.List;
import java.util.stream.Collectors;

import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;

import com.alex.training.sceneElement.SchemContextMenu;
import com.alex.training.schemElement.Clamp;
import com.alex.training.schemElement.Contact;
import com.alex.training.schemElement.PowerSourceTerminal;
import com.alex.training.schemElement.PowerSourceTerminalDCM;
import com.alex.training.schemElement.SLine;
import com.alex.training.schemElement.SchemElement;
import com.alex.training.schemElement.Tester;
import com.alex.training.schemElement.TroynicInterface;
import com.alex.training.schemElement.TylContact;

/**
 * @author Alex - класс отвечающий за работу схемы в процессе поиска
 *         неисправностей (подключение тестера, обработка событий мыши, и.т.д)
 *
 */
public class SchemWork {

	private Tester tester;
	private AnchorPane field;
	private List<SchemElement> list;
	private List<Loop> loops;
	private List<ElCircuit> circuits;
	private Clamp clPlus;
	private Clamp clMinus;
	private SchemContextMenu sContextMenu;
	private boolean curClConnectPlus = false;
	private boolean isTesterVisible;
	private double xPosition;
	private double yPosition;
	private double calculateValue = 0;
	// private double plusRes, minusRes;
	private int znak;
	private BoxAs boxAs;
	private Tooltip tooltipReleInfo;
	private SchemElement currentElement;
	private Tooltip tooltipContactInfo;
	private Tooltip tooltipMontagInfo;
	private String montagInfo;
	private Alert alert;

	public SchemWork(AnchorPane f, List<SchemElement> l, List<ElCircuit> cr,
			BoxAs ba) {
		field = f;
		list = l;
		circuits = cr;
		boxAs = ba;

		// сообщение об обнаружении неисправности
		alert = new Alert(AlertType.ERROR);
		alert.setHeaderText("ОБНАРУЖЕНА НЕИСПРАВНОСТЬ!");
		alert.setTitle("Неисправность");

		// создание тестера и загрузка в поле схемы
		tester = new Tester();
		tester.drowElement(800, 400);
		tester.setVisible(false);
		clPlus = new Clamp(true);
		clPlus.setVisible(false);
		isTesterVisible = false;
		clMinus = new Clamp(false);
		clMinus.setVisible(false);
		field.getChildren().addAll(tester, clPlus, clMinus);

		// контекстное меню поля схемы----------------------
		sContextMenu = new SchemContextMenu();

		field.setOnMouseClicked((e) -> {
			if ((e.getClickCount() == 1)
					&& (e.getButton().equals(MouseButton.SECONDARY))) {
				xPosition = e.getX();
				yPosition = e.getY();
				sContextMenu.showMenu(field, field, e.getSceneX(),
						e.getSceneY());
			}
			if ((e.getClickCount() == 1)
					&& (e.getButton().equals(MouseButton.PRIMARY)))
				sContextMenu.hide();

		});

		// нажатие на кнопку мыши------------------------------------
		field.setOnMousePressed((e) -> {
			if (currentElement != null) {
				tooltipReleInfo.hide();
				tooltipContactInfo.hide();
				tooltipMontagInfo.hide();
			}
		});

		// установка действий при выборе кнопок
		// меню---------------------------------------------------------------

		// показ тестера и точек подключения при выборе
		// меню--------------------------
		sContextMenu.getPlusCl().setOnAction((ae) -> {
			tester.setPlusMultConnect(false);
			curClConnectPlus = true;
			setVisibleCheckPointSchem(true);
			if (!isTesterVisible) {
				tester.setVisible(true);
				tester.setLayoutX(xPosition);
				tester.setLayoutY(yPosition);
				isTesterVisible = true;
			}
		});
		sContextMenu.getMinusCl().setOnAction((ae) -> {
			tester.setMinusMultConnect(false);
			curClConnectPlus = false;
			setVisibleCheckPointSchem(true);
			if (!isTesterVisible) {
				tester.setVisible(true);
				tester.setLayoutX(xPosition);
				tester.setLayoutY(yPosition);
				isTesterVisible = true;
			}
		});

		// сброс подключения мультиметра
		sContextMenu.getHideMult().setOnAction((ae) -> {
			setVisibleCheckPointSchem(false);
			tester.setVisible(false);
			isTesterVisible = false;
			list.stream().distinct().forEach((sc) -> {
				((SchemElement) sc).setPlusMultConnect(false);
				((SchemElement) sc).setMinusMultConnect(false);
			});
			tester.setPlusMultConnect(false);
			tester.setMinusMultConnect(false);
			clPlus.setVisible(false);
			clMinus.setVisible(false);
		});

		// показ информации о реле
		tooltipReleInfo = new Tooltip();

		sContextMenu.getMenuItemReleInfo().setOnAction(
				(ae) -> {
					tooltipReleInfo.setText(currentElement.toString());
					tooltipReleInfo.setFont(Font.font("sans", 30));
					tooltipReleInfo.show(currentElement,
							currentElement.getLayoutX(),
							currentElement.getLayoutY());
				});

		// показ информации о состоянии монтажа
		tooltipMontagInfo = new Tooltip();
		sContextMenu.getMenuViewMontag().setOnAction(
				(ae) -> {
					if (currentElement.getIsError()
							&& currentElement.getErrorMod() == 1) {
						viewErrorMessage(1, "Обнаружена плохая пайка.");
						currentElement.setIsError(false);
					} else {
						montagInfo = "Монтаж в порядке";
						tooltipMontagInfo.setText(montagInfo);
						tooltipMontagInfo.setFont(Font.font("sans", 30));
						tooltipMontagInfo.show(currentElement,
								currentElement.getLayoutX(),
								currentElement.getLayoutY());
					}
				});

		// показ информации о контакте
		tooltipContactInfo = new Tooltip();

		sContextMenu.getMenuContactResistance().setOnAction(
				(ae) -> {
					tooltipContactInfo.setText("Сопротивление контакта - "
							+ currentElement.getResistance() + " Ом");
					tooltipContactInfo.setFont(Font.font("sans", 30));
					tooltipContactInfo.show(currentElement,
							currentElement.getLayoutX(),
							currentElement.getLayoutY());
				});

		// показ информации о тыловом контакте тройника
		sContextMenu.getMenuTroynicTylResistance().setOnAction(
				(ae) -> {
					TylContact tc = ((TroynicInterface) currentElement)
							.getTylTr();
					String info = String.valueOf(tc.getContNum() + 1) + "-"
							+ String.valueOf(tc.getContNum() + 3) + " "
							+ tc.getTxName().getText();
					tooltipContactInfo.setText("Контакт " + info
							+ " Сопротивление контакта - " + tc.getResistance()
							+ " Ом");
					tooltipContactInfo.setFont(Font.font("sans", 30));
					tooltipContactInfo.show(currentElement,
							currentElement.getLayoutX(),
							currentElement.getLayoutY());
				});

		// установка ошибки тыловом контакте тройника
		sContextMenu
				.getMenuTroynicTylSetupResistance()
				.selectedProperty()
				.addListener(
						(property, oldValue, newValue) -> {
							TylContact tc = ((TroynicInterface) currentElement)
									.getTylTr();
							tc.setIsError(newValue.booleanValue());
						});
		// установка ошибки фронтовом контакте тройника
		sContextMenu.getMenuTroynicFrontSetupResistance().selectedProperty()
				.addListener((property, oldValue, newValue) -> {
					currentElement.setIsError(newValue.booleanValue());
				});
		// установка повышенного переходного сопротивления на контакте
		sContextMenu.getMenuContactSetupResistance().selectedProperty()
				.addListener((property, oldValue, newValue) -> {
					currentElement.setErrorMod(0);
					currentElement.setIsError(newValue.booleanValue());
				});
		// установка холодной пайки на контакте
		sContextMenu.getMenuContactSetupSoldering().selectedProperty()
				.addListener((property, oldValue, newValue) -> {
					currentElement.setErrorMod(1);
					currentElement.setIsError(newValue.booleanValue());
				});

		// установка перемычки на контакте
		sContextMenu
				.getMenuContactSetPerem()
				.selectedProperty()
				.addListener(
						(property, oldValue, newValue) -> {
							if (currentElement.getIsError()
									&& ((Contact) currentElement).getErrorMod() == 0)
								viewErrorMessage(0,
										"Обнаружено повышенное сопротивление контакта.");
							((Contact) currentElement).setupJumper(newValue
									.booleanValue());

						});
		// замена реле на контакте
		sContextMenu
				.getMenuContactZamenaRele()
				.setOnAction(
						(ae) -> {
							if (currentElement.getIsError()
									&& ((Contact) currentElement).getErrorMod() == 0) {
								viewErrorMessage(0,
										"Обнаружено повышенное сопротивление контакта.");

								currentElement.setIsError(false);
							}
						});
		// установка перемычки на фронтовом контакте тройника
		sContextMenu
				.getMenuTroynicFrontSetPerem()
				.selectedProperty()
				.addListener(
						(property, oldValue, newValue) -> {
							((Contact) currentElement).setupJumper(newValue
									.booleanValue());
						});
		// установка перемычки на тыловом контакте тройника
		sContextMenu
				.getMenuTroynicTylSetPerem()
				.selectedProperty()
				.addListener(
						(property, oldValue, newValue) -> {
							((TroynicInterface) currentElement)
									.setupJumperTyl(newValue.booleanValue());
						});
		// подача плюса в полюс ДЦМ
		sContextMenu
				.getMenuAddPlusDCM()
				.selectedProperty()
				.addListener(
						(property, oldValue, newValue) -> {
							currentElement.setResistance((newValue
									.booleanValue()) ? 0 : currentElement
									.getObryv());
						});

		// -----------------------------меню поля схемы----------------------

		// обработка событий мыши для всех элементов ---------------
		list.stream().forEach((el) -> {
			initMouseSchemElement(el);
		});

		// привязка изменения подключения клемм тестера к измерению
		tester.isConnectProperty().addListener(
				(property, oldValue, newValue) -> {
					if (newValue && tester.getBtnV().isSelected()) {
						calculateVolt();
					} else
						tester.getTxTablo().setText("0.");
				});

		// привязка изменения выбора режима тестера к измерению
		tester.getBtnV().selectedProperty()
				.addListener((property, oldValue, newValue) -> {
					if (newValue && tester.getIsConnect()) {
						calculateVolt();
					} else
						tester.getTxTablo().setText("0.");
				});

	} // ------------конструктор

	private void viewErrorMessage(int mod, String info) {
		if (currentElement.getIsError() && currentElement.getErrorMod() == mod) {
			montagInfo = info + " Неисправность устранена.";
			alert.setContentText(montagInfo);
			alert.showAndWait();
		}
	}

	public SchemContextMenu getsContextMenu() {
		return sContextMenu;
	}

	// показ точек подключения тестера к схеме
	private void setVisibleCheckPointSchem(boolean flag) {
		list.stream()
				.distinct()
				.forEach((sc) -> ((SchemElement) sc).setVisibleCheckPoint(flag));
	}

	private void setVisibleClamp(Clamp cl, double x, double y) {
		cl.setVisible(true);
		cl.setLayoutX(x);
		cl.setLayoutY(y);
	}

	/**
	 * для теста !!! Вывод в консоль точек подключения клемм тестера
	 */
	private void printNameCirClampConnected(ElCircuit cir, ElCircuit cirMinus) {
		ParUnion un = null;
		ParUnion unMinus = null;
		Loop lp = null;
		Loop lpMinus = null;
		// тестер подключен в одну цепь
		if (cir != null && cirMinus == null) {
			System.out.println("Обе клеммы подключены к цепи - "
					+ cir.getVarName() + "; Сопротивление цепи = "
					+ cir.getCircuitR() + " Ом");
			cir.printUnionList();
			// тестер подключен к одному соединению
			if (cir.isAllClampUnionConnect()) {
				un = cir.getUnionByConnectedAllClamp();
				System.out.println("Обе клеммы подключены к соединению - "
						+ un.getVarName() + "; Сопротивление соединения = "
						+ un.getUnionR() + " Ом");
				un.printLoopList();
				lp = un.getLoopByConnectedAllClamp();
				// тестер подключен к одному контуру
				if (lp != null) {
					System.out.println("Обе клеммы подключены к контуру - "
							+ lp.getVarName() + "; Сопротивление контура = "
							+ lp.getLoopR() + " Ом");
					// тестер подключен к разным контурам
				} else {
					lp = un.getLoopByConnectedClamp(true);
					lpMinus = un.getLoopByConnectedClamp(false);
					System.out
							.println("Плюсовая клемма подключена к контуру - "
									+ lp.getVarName()
									+ "; Сопротивление контура = "
									+ lp.getLoopR() + " Ом");
					System.out
							.println("Минусовая клемма подключена к контуру - "
									+ lpMinus.getVarName()
									+ "; Сопротивление контура = "
									+ lpMinus.getLoopR() + " Ом");
				}
				// тестер подключен к разным соединениям
			} else {
				un = cir.getUnionByConnectedPlusClamp();
				unMinus = cir.getUnionByConnectedMinusClamp();
				System.out.println("Плюсовая клемма подключена к соединению - "
						+ un.getVarName() + "; Сопротивление соединения = "
						+ un.getUnionR() + " Ом");
				System.out
						.println("Минусовая клемма подключена к соединению - "
								+ unMinus.getVarName()
								+ "; Сопротивление соединения = "
								+ unMinus.getUnionR() + " Ом");
				lp = un.getLoopByConnectedClamp(true);
				lpMinus = unMinus.getLoopByConnectedClamp(false);
				System.out.println("Плюсовая клемма подключена к контуру - "
						+ lp.getVarName() + "; Сопротивление контура = "
						+ lp.getLoopR() + " Ом");
				System.out.println("Минусовая клемма подключена к контуру - "
						+ lpMinus.getVarName() + "; Сопротивление контура = "
						+ lpMinus.getLoopR() + " Ом");
			}

		}
		// тестер подключен в разные цепи
		if (cir != null && cirMinus != null) {
			System.out.println("Плюсовая клемма подключена к цепи - "
					+ cir.getVarName() + "; Сопротивление цепи = "
					+ cir.getCircuitR() + " Ом");
			System.out.println("Минусовая клемма подключена к цепи - "
					+ cirMinus.getVarName() + "; Сопротивление цепи = "
					+ cirMinus.getCircuitR() + " Ом");

			un = cir.getUnionByConnectedPlusClamp();
			unMinus = cirMinus.getUnionByConnectedMinusClamp();
			System.out.println("Плюсовая клемма подключена к соединению - "
					+ un.getVarName() + "; Сопротивление соединения = "
					+ un.getUnionR() + " Ом");
			System.out.println("Минусовая клемма подключена к соединению - "
					+ unMinus.getVarName() + "; Сопротивление соединения = "
					+ unMinus.getUnionR() + " Ом");

			lp = un.getLoopByConnectedClamp(true);
			lpMinus = unMinus.getLoopByConnectedClamp(false);
			System.out.println("Плюсовая клемма подключена к контуру -  "
					+ lp.getVarName() + "; Сопротивление контура = "
					+ lp.getLoopR() + " Ом");
			lp.printLoopList();
			System.out.println("Минусовая клемма подключена к контуру - "
					+ lpMinus.getVarName() + "; Сопротивление контура = "
					+ lpMinus.getLoopR() + " Ом");
			lpMinus.printLoopList();
		}

	}

	/**
	 * определение знака при подключении клемм
	 */
	private int getZnak(ElCircuit cir, ElCircuit cirMinus) {
		PowerSourceTerminal pst;
		// если тестер подключен в одну цепь
		if (cir != null && cirMinus == null) {
			if (cir.isPlusClampFirst()) {
				pst = cir.getTerminalBeforeClamp();
			} else {
				pst = cir.getTerminalPostClamp();
			}

			if (pst == null)
				return 1;

			return (pst.isMinus()) ? -1 : 1;
		}
		// если тестер подключен в разные цепи
		if (cir != null && cirMinus != null) {
			pst = getPlusClampTerminal(cir);
			if (pst == null)
				return 1;

			return (pst.isMinus()) ? -1 : 1;
		}

		return 1;
	}

	/**
	 * расчет напряжения при поключении обеих клемм тестера
	 */
	private void calculateVolt() {
		ElCircuit cir = getMinResCircuitByConnectedAllClamp();
		ElCircuit cirMinus = null;
		PowerSourceTerminal pst = null;
		PowerSourceTerminal pstMinus = null;
		double tok, u, res;
		calculateValue = 0;
		znak = 1;
		if (cir != null) { // 1 тестер подключен к одной цепи-----------

			znak = getZnak(cir, null);
			printNameCirClampConnected(cir, null);
			// 1.1 через цепь протекает ток
			// if (cir.IsConducted()) {
			// res = (cir.getCircuitR() - cir.getResByConnectedAllClamp());
			// System.out.println("ток цепи  " + cir.getVarName() + " - "
			// + cir.getCirTok());
			//
			// calculateValue = (res > 0) ? (znak * res * cir.getCirTok()) : 0;
			// }
			// 1.2 цепь разомкнута
			// else {
			if (cir.getResByConnectedAllClamp() <= 0) {
				calculateValue = znak * cir.getSource().getEds();
			} else {
				tok = (cir.getSource().getEds() / (cir
						.getResByConnectedAllClamp() + cir.getSource().getInR()));
				u = tok * cir.getResByConnectedAllClamp();
				calculateValue = znak * (cir.getSource().getEds() - u);
			}

			// }
		} else {
			// 2 тестер подключен к разным цепям-----------
			cir = getMinResCircuitByConnectedClamp(true);
			cirMinus = getMinResCircuitByConnectedClamp(false);
			znak = getZnak(cir, cirMinus);

			// для отладки
			printNameCirClampConnected(cir, cirMinus);

			pst = getPlusClampTerminal(cir);
			pstMinus = getMinusClampTerminal(cirMinus);

			if (isAllClampSourceConnect(pst, pstMinus)) {
				System.out.println("Плюсовая клемма подключена к полюсу -"
						+ pst.getVarName() + " ");
				System.out.println("Минусовая клемма подключена к полюсу -"
						+ pstMinus.getVarName() + " ");
				System.out.println("Клеммы разнополюсные? -"
						+ isOpposite(pst, pstMinus));
				double rCir;
				// если источник постоянного тока
				if (cir.getSource().getIsConst()) {
					rCir = cir.getMinResistancePlusClamp()
							+ ((isOpposite(pst, pstMinus)) ? cirMinus
									.getMinResistanceMinusClamp() : cirMinus
									.getCircuitR()
									- cirMinus.getMinResistanceMinusClamp());
				}
				// если источник переменного тока
				else {
					rCir = cir.getMinResistancePlusClamp()
							+ ((isOpposite(pst, pstMinus)) ? cirMinus
									.getMinResistanceMinusClamp() : 1000000);
				}
				tok = (cir.getSource().getEds() / (rCir + cir.getSource()
						.getInR()));
				u = tok * rCir;
				calculateValue = znak * (cir.getSource().getEds() - u);

			}

		}
		tester.showCalculateValue(calculateValue, cir.getSource().getIsConst());
	}

	/**
	 * Проверяет подключены ли обе клеммы к одному источнику
	 */
	public boolean isAllClampSourceConnect(PowerSourceTerminal pst,
			PowerSourceTerminal pstMinus) {
		if (pst != null && pstMinus != null)
			return boxAs.getSource(pst).equals(boxAs.getSource(pstMinus));
		else
			return false;
	}

	/**
	 * @param term
	 * @return являются ли два полюса противоположными
	 */
	public boolean isOpposite(PowerSourceTerminal term,
			PowerSourceTerminal term2) {
		return (term.isMinus() && !term2.isMinus())
				|| (!term.isMinus() && term2.isMinus()
				// для полюсов переменного тока разные фазы
				|| (term.getFaza() != 0 && term2.getFaza() != 0 && term
						.getFaza() != term2.getFaza()));
	}

	/**
	 * Проверяет есть ли цепь к которой подключены обе клеммы
	 */
	public boolean isAllClampElcircuitConnect() {
		return circuits.stream().anyMatch(
				(cir) -> cir.isMinusClampCircuitConnect()
						&& cir.isPlusClampCircuitConnect());
	}

	/**
	 * @return возвращает цепь с минимальным сопротивлением к которой подключены
	 *         обе клеммы
	 */
	private ElCircuit getMinResCircuitByConnectedAllClamp() {
		List<ElCircuit> cirs;
		ElCircuit cir = null;
		double res = 0;
		if (isAllClampElcircuitConnect()) {
			cirs = circuits
					.stream()
					.filter((c) -> c.isMinusClampCircuitConnect()
							&& c.isPlusClampCircuitConnect())
					.collect(Collectors.toList());
			if (!cirs.isEmpty()) {
				res = cirs.get(0).getCircuitR();
				cir = cirs.get(0);
				for (ElCircuit elCir : cirs) {
					if (elCir.getCircuitR() < res) {
						cir = elCir;
						res = elCir.getCircuitR();
					}
				}
			}
		}
		return cir;
	}

	/**
	 * @return возвращает цепь с минимальным сопротивлением к которой подключена
	 *         плюсовая или минусовая клемма
	 */
	private ElCircuit getMinResCircuitByConnectedClamp(boolean plusClamp) {
		List<ElCircuit> cirs;
		ElCircuit cir = null;
		double res = 0;
		cirs = getCircuitsByConnectedClamp(plusClamp);
		if (!cirs.isEmpty()) {
			res = cirs.get(0).getCircuitR();
			cir = cirs.get(0);
			for (ElCircuit elCir : cirs) {
				if (elCir.getCircuitR() < res) {
					cir = elCir;
					res = elCir.getCircuitR();
				}
			}
		}

		return cir;
	}

	/**
	 * @return список эл. цепей к которым подключена плюсовая или минусовая
	 *         клемма
	 */
	private List<ElCircuit> getCircuitsByConnectedClamp(boolean plusClamp) {
		return circuits
				.stream()
				.filter((c) -> (plusClamp) ? c.isPlusClampCircuitConnect() : c
						.isMinusClampCircuitConnect())
				.collect(Collectors.toList());
	}

	/**
	 * @return возвращает ближайший по сопротивлению полюс к подключенной
	 *         плюсовой клемме в конкретной цепи
	 */
	public PowerSourceTerminal getPlusClampTerminal(ElCircuit cir) {
		PowerSourceTerminal pst = null;
		if (cir != null)
			pst = cir.getPlusClampMinRTerminal();
		return pst;
	}

	/**
	 * @return возвращает ближайший по сопротивлению полюс к подключенной
	 *         минусовой клемме
	 */
	public PowerSourceTerminal getMinusClampTerminal(ElCircuit cir) {
		PowerSourceTerminal pst = null;
		pst = cir.getMinusClampMinRTerminal();
		return pst;
	}

	private void initMouseSchemElement(SchemElement sc) {
		if (sc != null) {
			// установка обработки нажатия на точку подключения тестера к схеме
			// для линии---------------
			if (sc.getClass().equals(SLine.class)) {
				sc.getStartRect()
						.setOnMouseClicked(
								(e) -> {
									if ((e.getClickCount() == 2)
											&& (e.getButton()
													.equals(MouseButton.PRIMARY))) {
										setVisibleCheckPointSchem(false);
										if (curClConnectPlus)
											setVisibleClamp(clPlus, e.getX(),
													e.getY());
										else
											setVisibleClamp(clMinus, e.getX(),
													e.getY());
										clickCheckPointSchem(sc,
												curClConnectPlus);
									}
								});
				sc.getStartRect1()
						.setOnMouseClicked(
								(e) -> {
									if ((e.getClickCount() == 2)
											&& (e.getButton()
													.equals(MouseButton.PRIMARY))) {
										setVisibleCheckPointSchem(false);
										if (curClConnectPlus)
											setVisibleClamp(clPlus, e.getX(),
													e.getY());
										else
											setVisibleClamp(clMinus, e.getX(),
													e.getY());
										clickCheckPointSchem(sc,
												curClConnectPlus);
									}
								});
			} // для линии---------------

			// для всех элементов кроме линии--------
			if (!sc.getClass().equals(SLine.class)) {

				sc.setOnMouseClicked((e) -> {
					if ((e.getClickCount() == 1)
							&& (e.getButton().equals(MouseButton.SECONDARY))) {
						// Установка текущего элемента
						currentElement = sc;
						xPosition = e.getX();
						yPosition = e.getY();
						sContextMenu.showMenu(sc, field, e.getSceneX(),
								e.getSceneY());
					}
					if ((e.getClickCount() == 1)
							&& (e.getButton().equals(MouseButton.PRIMARY)))
						sContextMenu.hide();
					tooltipReleInfo.hide();
					Tooltip.uninstall(sc, tooltipReleInfo);
					tooltipContactInfo.hide();
					Tooltip.uninstall(sc, tooltipContactInfo);
				});

			} // contact class

		} // if null

	}

	// установка свойств подключения клемм к схемноу элементу при нажатии
	// на точку подключения тестера к схеме
	private void clickCheckPointSchem(SchemElement sc, boolean isPlus) {
		if (isPlus) {
			sc.setPlusMultConnect(true);
			list.stream()
					.filter((s) -> !s.equals(sc))
					.forEach(
							(s) -> ((SchemElement) s).setPlusMultConnect(false));
			tester.setPlusMultConnect(true);
		} else {
			sc.setMinusMultConnect(true);
			list.stream()
					.filter((s) -> !s.equals(sc))
					.forEach(
							(s) -> ((SchemElement) s)
									.setMinusMultConnect(false));
			tester.setMinusMultConnect(true);
		}
	}

}
