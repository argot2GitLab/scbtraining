﻿/**
 * 
 */
package com.alex.training.schem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.alex.training.schemElement.AutostopMotor;
import com.alex.training.schemElement.PowerSourceTerminal;
import com.alex.training.schemElement.PowerSourceTerminalDCM;
import com.alex.training.schemElement.Rele;
import com.alex.training.schemElement.ReleANVSH;
import com.alex.training.schemElement.ReleASH2_110_220;
import com.alex.training.schemElement.ReleDSH2;
import com.alex.training.schemElement.ReleNMPSH03_90;
import com.alex.training.schemElement.ReleNMPSH2_2500;
import com.alex.training.schemElement.ReleNMSH;
import com.alex.training.schemElement.ReleNMSH2_4000;
import com.alex.training.schemElement.ReleNMSHM1_1120;
import com.alex.training.schemElement.ReleNMSHM1_560;
import com.alex.training.schemElement.ReleNMSHM1_700;
import com.alex.training.schemElement.ReleNMSH_per3_4;
import com.alex.training.schemElement.ReleOMSHM;
import com.alex.training.schemElement.RelePMPUSH;
import com.alex.training.schemElement.SchemElement;
import com.alex.training.schemElement.blocks.StrMotor;

/**
 * @author Alex класс для связи индикации АРМ с релейными схемами
 *
 */
public class BoxAs {

	private List<Rele> reles;
	private List<Source> sources = new ArrayList<Source>();
	private List<StrMotor> strMotors = new ArrayList<StrMotor>();
	private Source sourceTsz;
	private Source sourceVSP;

	public BoxAs() {

		int numRel = 0;
		reles = new ArrayList<Rele>();
		// index rele + 37 - stroka rele
		reles.add(numRel++, new ReleOMSHM("ДБО", "18-21", false));
		reles.add(numRel++, new ReleOMSHM("413БО", "11-21", false));
		reles.add(numRel++, new ReleOMSHM("ДКО", "18-22", true));
		reles.add(numRel++, new ReleNMSH("ДСУ", "18-25", false));
		reles.add(numRel++, new ReleNMSH("п414П", "2-101", true));
		reles.add(numRel++, new ReleNMSH("413зС", "3-77", false));
		reles.add(numRel++, new ReleNMSHM1_700("п413УС", "3-42", false));
		reles.add(numRel++, new ReleNMSHM1_560("413-3/4УС", "3-28", false));
		reles.add(numRel++, new ReleNMSH("пГОМ", "3-85", false));
		reles.add(numRel++, new ReleNMSHM1_560("ГОМ", "3-86", false));
		reles.add(numRel++, new ReleNMSH("30зМ", "5-51", false));
		reles.add(numRel++, new ReleNMSH("п15зМ", "5-25", false));
		reles.add(numRel++, new ReleNMSH("1113зМ", "5-94", false));
		reles.add(numRel++, new ReleNMSH("32зМ", "5-53", false));
		reles.add(numRel++, new ReleNMSH("31зМ", "5-91", false));
		reles.add(numRel++, new ReleNMSH("40зМ", "5-46", false));
		reles.add(numRel++, new ReleNMSH("п13зМ", "5-27", false));
		reles.add(numRel++, new ReleNMSH("п97зМ", "5-56", false));
		reles.add(numRel++, new ReleNMSH("п42зМ", "5-95", false));
		reles.add(numRel++, new ReleNMSH("41зМ", "5-92", false));
		reles.add(numRel++, new ReleNMSH("42зМ", "5-48", false));
		reles.add(numRel++, new ReleNMSH("20зМ", "5-57", false));
		reles.add(numRel++, new ReleNMSH("23зМ", "5-61", false));
		reles.add(numRel++, new ReleNMSH("24зМ", "5-63", false));
		reles.add(numRel++, new ReleNMSH("97зМ", "5-55", false));
		reles.add(numRel++, new ReleNMSH("90зМ", "5-54", false));
		reles.add(numRel++, new ReleNMSH("п413РО", "11-31", false));
		reles.add(numRel++, new ReleNMSH("10зМ", "5-22", false));
		reles.add(numRel++, new ReleNMSH("13зМ", "5-26", false));
		reles.add(numRel++, new ReleNMSH("14зМ", "5-41", false));
		reles.add(numRel++, new ReleNMSH("15зМ", "5-24", false));
		reles.add(numRel++, new ReleNMSHM1_560("3бУС", "3-113", false));
		reles.add(numRel++, new ReleNMSHM1_700("п3УС", "3-32", false));
		reles.add(numRel++, new ReleNMSHM1_700("п413/4УС", "3-46", false));
		reles.add(numRel++, new ReleNMSHM1_700("2п3УС", "3-33", false));
		reles.add(numRel++, new ReleNMSH("3зС", "3-73", false));
		reles.add(numRel++, new ReleNMSH("60зМ", "5-44", false));
		reles.add(numRel++, new ReleNMSH("62зМ", "5-45", false));
		reles.add(numRel++, new ReleNMSH("п24зМ", "5-64", false));
		reles.add(numRel++, new ReleNMSH("70зМ", "5-42", false));
		reles.add(numRel++, new ReleNMSH("110зМ", "5-93", false));
		reles.add(numRel++, new ReleNMSH("п3БО", "18-33", false));
		reles.add(numRel++, new ReleNMSHM1_700("п4УС", "3-35", false));
		reles.add(numRel++, new ReleNMSHM1_560("4бУС", "3-115", false));
		reles.add(numRel++, new ReleNMSH("4зС", "3-74", false));
		reles.add(numRel++, new ReleNMSH("п4БО", "17-23", false));
		reles.add(numRel++, new ReleNMSHM1_700("п7УС", "3-38", false));
		reles.add(numRel++, new ReleNMSH("7зС", "3-75", false));
		reles.add(numRel++, new ReleNMSH("п7БО", "13-21", false));
		reles.add(numRel++, new ReleNMSH("п23зМ", "5-62", false));
		reles.add(numRel++, new ReleNMSH("п30зМ", "5-52", false));
		reles.add(numRel++, new ReleNMSH("ОН", "5-21", false));
		reles.add(numRel++, new ReleNMSHM1_700("п9УС", "3-48", false));
		reles.add(numRel++, new ReleNMSH("9зС", "3-76", false));
		reles.add(numRel++, new ReleNMSH("79зМ", "5-43", false));
		reles.add(numRel++, new ReleNMSH("п9БО", "13-23", false));
		reles.add(numRel++, new ReleNMSH("ДзС", "3-72", false));
		reles.add(numRel++, new ReleNMSH("416/410зС", "3-71", false));
		reles.add(numRel++, new ReleNMSH("п416РО", "14-51", false));
		reles.add(numRel++, new ReleNMSH("п414РО", "14-54", false));
		reles.add(numRel++, new ReleNMSH("п412РО", "14-57", false));
		reles.add(numRel++, new ReleNMSH("п410РО", "14-44", false));
		reles.add(numRel++, new ReleNMSH("407-411зС", "3-118", false));
		reles.add(numRel++, new ReleNMSH("п10зМ", "5-23", false));
		reles.add(numRel++, new ReleNMSH("п40зМ", "5-47", false));
		reles.add(numRel++, new ReleNMSHM1_700("п416/410УС", "3-22", false));
		reles.add(numRel++, new ReleNMSHM1_560("413УС", "3-41", false));
		reles.add(numRel++, new ReleNMSHM1_560("413/3УС", "3-43", false));
		reles.add(numRel++, new ReleNMSHM1_560("413/4УС", "3-45", false));
		reles.add(numRel++, new ReleNMSHM1_560("4УС", "3-34", false));
		reles.add(numRel++, new ReleNMSHM1_560("3УС", "3-31", false));
		reles.add(numRel++, new ReleNMSHM1_560("416/410УС", "3-21", false));
		reles.add(numRel++, new ReleNMSHM1_560("9УС", "3-47", false));
		reles.add(numRel++, new ReleNMSHM1_560("7УС", "3-37", false));
		reles.add(numRel++, new ReleNMSHM1_560("Д/3УС", "3-23", false));
		reles.add(numRel++, new ReleNMSHM1_560("Д/4УС", "3-25", false));
		reles.add(numRel++, new ReleNMSHM1_560("407-411УС", "3-117", false));
		reles.add(numRel++, new ReleNMSH("О1АС", "3-51", false));
		reles.add(numRel++, new ReleNMSH("1АС", "3-52", false));
		reles.add(numRel++, new ReleNMSH("О2АС", "3-54", false));
		reles.add(numRel++, new ReleNMSH("2АС", "3-55", false));
		reles.add(numRel++, new ReleNMSH("О3АС", "7-28", false));
		reles.add(numRel++, new ReleNMSH("3АС", "7-38", false));
		reles.add(numRel++, new ReleNMSH("О3АД", "3-61", false));
		reles.add(numRel++, new ReleNMSH("3АД", "3-62", false));
		reles.add(numRel++, new ReleNMSH("О4АД", "3-64", false));
		reles.add(numRel++, new ReleNMSH("4АД", "3-65", false));
		reles.add(numRel++, new ReleNMSH("3АСД", "7-66", false));
		reles.add(numRel++, new ReleNMSHM1_560("1ПП", "2-55", false));
		reles.add(numRel++, new ReleNMSHM1_560("1ГС", "2-21", false));
		reles.add(numRel++, new ReleNMSHM1_560("1бГС", "7-77", false));
		reles.add(numRel++, new ReleNMSHM1_560("1ИР", "2-38", false));
		reles.add(numRel++, new ReleNMSH("1з", "2-22", false));
		reles.add(numRel++, new ReleNMSH("1бз", "7-56", false));
		reles.add(numRel++, new ReleNMSH("1М-1", "2-27", false));
		reles.add(numRel++, new ReleNMSH("1М-2", "2-28", false));
		reles.add(numRel++, new ReleNMSH("1Рз", "2-33", false));
		reles.add(numRel++, new ReleNMSH_per3_4("1ВРз", "2-36", false));
		reles.add(numRel++, new ReleNMSH_per3_4("1РВ", "2-34", false));
		reles.add(numRel++, new ReleNMSHM1_560("2ПП", "2-56", false));
		reles.add(numRel++, new ReleNMSHM1_560("2ГС", "2-41", false));
		reles.add(numRel++, new ReleNMSHM1_560("ДГС", "2-51", false));
		reles.add(numRel++, new ReleNMSHM1_560("2ИР", "2-31", false));
		reles.add(numRel++, new ReleNMSH("2з", "2-42", false));
		reles.add(numRel++, new ReleNMSH("2дз", "2-45", false));
		reles.add(numRel++, new ReleNMSH("Дз", "2-52", false));
		reles.add(numRel++, new ReleNMSH("2М-1", "2-47", false));
		reles.add(numRel++, new ReleNMSH("2М-2", "2-48", false));
		reles.add(numRel++, new ReleNMSHM1_560("3ПП", "3-96", false));
		reles.add(numRel++, new ReleNMSHM1_560("7ГС", "3-91", false));
		reles.add(numRel++, new ReleNMSHM1_560("9ГС", "3-101", false));
		reles.add(numRel++, new ReleNMSHM1_560("3ИР", "3-97", false));
		reles.add(numRel++, new ReleNMSH("7з", "3-92", false));
		reles.add(numRel++, new ReleNMSH("9з", "3-102", false));
		reles.add(numRel++, new ReleNMSH("3М-1", "3-94", false));
		reles.add(numRel++, new ReleNMSH("3М-2", "3-95", false));
		reles.add(numRel++, new ReleNMSHM1_560("11ПП", "7-52", false));
		reles.add(numRel++, new ReleNMSHM1_560("11ГС", "7-54", false));
		reles.add(numRel++, new ReleNMSHM1_560("11ИР", "7-51", false));
		reles.add(numRel++, new ReleNMSH("11з", "7-53", false));
		reles.add(numRel++, new ReleNMSH("11М-1", "7-88", false));
		reles.add(numRel++, new ReleNMSH("11М-2", "7-87", false));
		reles.add(numRel++, new ReleNMSH("413СУ", "11-33", false));
		reles.add(numRel++, new ReleNMSH("Вс2АС", "7-68", false));
		reles.add(numRel++, new ReleNMSH("Вс1-3АС", "7-67", false));
		reles.add(numRel++, new ReleNMSHM1_700("п3АД", "3-63", false));
		reles.add(numRel++, new ReleNMSHM1_700("п4АД", "3-66", false));
		reles.add(numRel++, new ReleNMSHM1_700("пД/3УС", "3-24", false));
		reles.add(numRel++, new ReleNMSHM1_700("пД/4УС", "3-26", false));
		reles.add(numRel++, new ReleNMSHM1_700("п1АС", "3-53", false));
		reles.add(numRel++, new ReleNMSHM1_700("п413/3УС", "3-44", false));
		reles.add(numRel++, new ReleNMSHM1_700("п3бУС", "3-114", false));
		reles.add(numRel++, new ReleNMSHM1_700("п4бУС", "3-116", false));
		reles.add(numRel++, new ReleNMSHM1_700("п2АС", "3-56", false));
		reles.add(numRel++, new ReleNMSHM1_700("п3АС", "7-38", false));
		reles.add(numRel++, new ReleNMSHM1_700("3/4АД", "3-67", false));
		reles.add(numRel++, new ReleNMSH("2п3П", "3-104", false));
		reles.add(numRel++, new ReleNMSH("п4П", "2-104", false));
		reles.add(numRel++, new ReleNMSHM1_700("Д-3/4УС", "3-27", false));
		reles.add(numRel++, new ReleNMSH("2п2з", "2-44", false));
		reles.add(numRel++, new ReleNMSH("п20зМ", "5-58", false));
		reles.add(numRel++, new ReleNMSHM1_700("2п4бУС", "3-112", false));
		reles.add(numRel++, new ReleNMSHM1_700("2п4УС", "3-36", false));
		reles.add(numRel++, new ReleNMSH("п31зМ", "5-98", false));
		reles.add(numRel++, new ReleNMSH("пДБО", "18-23", false));
		reles.add(numRel++, new ReleNMSH("407РО", "7-84", false));
		reles.add(numRel++, new ReleNMSH("409РО", "7-74", false));
		reles.add(numRel++, new ReleNMSH("2п411РО", "7-64", false));
		reles.add(numRel++, new ReleNMSH_per3_4("п1ПК", "2-61", false));
		reles.add(numRel++, new ReleNMSH("2п1ПК", "2-62", false));
		reles.add(numRel++, new ReleNMSH_per3_4("п1МК", "2-63", false));
		reles.add(numRel++, new ReleNMSH("2п1МК", "2-64", false));
		reles.add(numRel++, new ReleNMSH_per3_4("п2ПК", "2-65", false));
		reles.add(numRel++, new ReleNMSH("2п2ПК", "2-66", false));
		reles.add(numRel++, new ReleNMSH_per3_4("п4ПК", "2-73", false));
		reles.add(numRel++, new ReleNMSH("2п4ПК", "2-74", false));
		reles.add(numRel++, new ReleNMSH_per3_4("п4МК", "2-75", false));
		reles.add(numRel++, new ReleNMSH("2п4МК", "2-76", false));
		reles.add(numRel++, new ReleNMSH_per3_4("3ПК", "1-47", false));
		reles.add(numRel++, new ReleNMSH("2п3ПК", "1-101", false));
		reles.add(numRel++, new ReleNMSH_per3_4("5ПК", "1-67", false));
		reles.add(numRel++, new ReleNMSH("2п5ПК", "1-104", false));
		reles.add(numRel++, new ReleNMSH_per3_4("6ПК", "1-77", false));
		reles.add(numRel++, new ReleNMSH("2п6ПК", "1-106", false));
		reles.add(numRel++, new ReleNMSH("п1з", "2-23", false));
		reles.add(numRel++, new ReleNMSH("2п1з", "2-24", false));
		reles.add(numRel++, new ReleNMSH("п2ГС", "2-25", false));
		reles.add(numRel++, new ReleNMSH("п2з", "2-43", false));
		reles.add(numRel++, new ReleNMSH("п1ГС", "2-44", false));
		reles.add(numRel++, new ReleNMSH("п2дз", "2-46", false));
		reles.add(numRel++, new ReleNMSH("пДз", "2-53", false));
		reles.add(numRel++, new ReleNMSH("3пДз", "2-112", false));
		reles.add(numRel++, new ReleNMSH("п7з", "3-93", false));
		reles.add(numRel++, new ReleNMSH("п9з", "3-103", false));
		reles.add(numRel++, new ReleNMSH("2пГОМ", "3-84", false));
		reles.add(numRel++, new ReleNMSH("КГ", "22-48", false));
		reles.add(numRel++, new ReleNMSH("1КГ", "16-51", false));
		reles.add(numRel++, new ReleNMSH("3КГ", "16-41", false));
		reles.add(numRel++, new ReleNMSH("2КГ", "22-51", false));
		reles.add(numRel++, new ReleNMSH("4КГ", "22-41", false));
		reles.add(numRel++, new ReleNMSH("5КГ", "22-24", false));
		reles.add(numRel++, new ReleNMSH("1А", "10-21", false));
		reles.add(numRel++, new ReleNMSH("3А", "10-23", false));
		reles.add(numRel++, new ReleNMSH("п1-3А", "10-24", false));
		reles.add(numRel++, new ReleNMSH("2п414П", "2-102", false));
		reles.add(numRel++, new ReleNMSH("3п414П", "1-111", false));
		reles.add(numRel++, new ReleOMSHM("408КО", "25-33", false));
		reles.add(numRel++, new ReleOMSHM("408КЖО", "25-21", false));
		reles.add(numRel++, new ReleOMSHM("408ЖО", "25-31", false));
		reles.add(numRel++, new ReleOMSHM("408ЗО", "25-32", false));
		reles.add(numRel++, new ReleNMSH("п408РО", "25-24", false));
		reles.add(numRel++, new ReleNMSH("п408КЖО", "25-22", false));
		reles.add(numRel++, new ReleNMSH("п408КО", "25-23", false));
		reles.add(numRel++, new ReleNMSH("пДКО", "18-24", false));
		reles.add(numRel++, new ReleOMSHM("413КО", "11-23", false));
		reles.add(numRel++, new ReleOMSHM("413КЖО", "11-25", false));
		reles.add(numRel++, new ReleOMSHM("413ЗО", "11-22", false));
		reles.add(numRel++, new ReleNMSH("п413КО", "12-31", false));
		reles.add(numRel++, new ReleNMSH("п413КЖО", "12-32", false));
		reles.add(numRel++, new ReleNMSH("2п413РО", "2-26", false));
		reles.add(numRel++, new ReleOMSHM("3КО", "18-32", false));
		reles.add(numRel++, new ReleOMSHM("3БО", "18-31", false));
		reles.add(numRel++, new ReleOMSHM("4КО", "17-22", false));
		reles.add(numRel++, new ReleOMSHM("4БО", "17-21", false));
		reles.add(numRel++, new ReleNMSH("п3КО", "18-34", false));
		reles.add(numRel++, new ReleNMSH("2п3БО", "3-57", false));
		reles.add(numRel++, new ReleNMSH("п4КО", "17-24", false));
		reles.add(numRel++, new ReleNMSH("2п4БО", "3-111", false));
		reles.add(numRel++, new ReleNMSH("п407П", "15-25", false));
		reles.add(numRel++, new ReleNMSH("2п407П", "2-81", false));
		reles.add(numRel++, new ReleNMSH("п409П", "13-35", false));
		reles.add(numRel++, new ReleNMSH("2п409П", "2-82", false));
		reles.add(numRel++, new ReleNMSH("п409сП", "13-34", false));
		reles.add(numRel++, new ReleNMSH("2п409сП", "2-83", false));
		reles.add(numRel++, new ReleNMSH("3п409сП", "2-113", false));
		reles.add(numRel++, new ReleNMSH("п411П", "11-32", false));
		reles.add(numRel++, new ReleNMSH("2п411П", "2-84", false));
		reles.add(numRel++, new ReleNMSH("3п411П", "2-114", false));
		reles.add(numRel++, new ReleNMSH("п411сП", "11-36", false));
		reles.add(numRel++, new ReleNMSH("2п411сП", "2-85", false));
		reles.add(numRel++, new ReleNMSH("3п411сП", "2-86", false));
		reles.add(numRel++, new ReleNMSH("п413сП", "11-35", false));
		reles.add(numRel++, new ReleNMSH("2п413сП", "2-81", false));
		reles.add(numRel++, new ReleNMSH("п408сП", "21-36", false));
		reles.add(numRel++, new ReleNMSH("2п408сП", "2-91", false));
		reles.add(numRel++, new ReleNMSH("п408П", "21-34", false));
		reles.add(numRel++, new ReleNMSH("2п408П", "2-92", false));
		reles.add(numRel++, new ReleNMSH("п410сП", "23-36", false));
		reles.add(numRel++, new ReleNMSH("2п410сП", "2-93", false));
		reles.add(numRel++, new ReleNMSH("п412сП", "23-35", false));
		reles.add(numRel++, new ReleNMSH("2п412сП", "2-94", false));
		reles.add(numRel++, new ReleNMSH("3п412сП", "26-26", false));
		reles.add(numRel++, new ReleNMSH("п412П", "25-38", false));
		reles.add(numRel++, new ReleNMSH("2п412П", "2-95", false));
		reles.add(numRel++, new ReleNMSH("3п412П", "1-112", false));
		reles.add(numRel++, new ReleNMSH("3аП", "18-27", false));
		reles.add(numRel++, new ReleNMSH("п3аП", "2-96", false));
		reles.add(numRel++, new ReleNMSH("2п3аП", "2-97", false));
		reles.add(numRel++, new ReleNMSH("3п3аП", "1-117", false));
		reles.add(numRel++, new ReleNMSH("4аП", "17-27", false));
		reles.add(numRel++, new ReleNMSH("п4аП", "2-98", false));
		reles.add(numRel++, new ReleNMSH("2п4аП", "2-88", false));
		reles.add(numRel++, new ReleNMSH("3п4аП", "1-114", false));
		reles.add(numRel++, new ReleDSH2("3П-1", "18-41", false));
		reles.add(numRel++, new ReleDSH2("3П-2", "18-42", false));
		reles.add(numRel++, new ReleNMSH("п3П", "2-103", false));
		reles.add(numRel++, new ReleDSH2("4П-1", "18-43", false));
		reles.add(numRel++, new ReleDSH2("4П-2", "18-44", false));
		reles.add(numRel++, new ReleNMSH("2п4П", "2-105", false));
		reles.add(numRel++, new ReleDSH2("5П-1", "17-41", false));
		reles.add(numRel++, new ReleDSH2("5П-2", "17-42", false));
		reles.add(numRel++, new ReleNMSH("п5П", "2-106", false));
		reles.add(numRel++, new ReleNMSH("2п5П", "2-107", false));
		reles.add(numRel++, new ReleDSH2("413сП-1", "15-43", false));
		reles.add(numRel++, new ReleDSH2("413сП-2", "15-44", false));
		reles.add(numRel++, new ReleNMSH("п3бП", "3-105", false));
		reles.add(numRel++, new ReleNMSH("2п3бП", "3-106", false));
		reles.add(numRel++, new ReleOMSHM("406КО", "21-23", false));
		reles.add(numRel++, new ReleNMSH("п406КО", "21-21", false));
		reles.add(numRel++, new ReleOMSHM("411ЗО", "15-32", false));
		reles.add(numRel++, new ReleOMSHM("411ЖО", "15-31", false));
		reles.add(numRel++, new ReleNMSH("п411РО", "15-22", false));
		reles.add(numRel++, new ReleNMSH_per3_4("6МК", "1-78", false));
		reles.add(numRel++, new ReleNMSH("2п6МК", "1-107", false));
		reles.add(numRel++, new ReleNMSH("п2МК", "2-67", false));
		reles.add(numRel++, new ReleNMSH("Ц2ИР", "2-32", false));
		reles.add(numRel++, new ReleNMSH("416КА", "24-57", false));
		reles.add(numRel++, new ReleNMSH("п416П", "24-55", false));
		reles.add(numRel++, new ReleNMSH("п418П", "24-54", false));
		reles.add(numRel++, new ReleNMSH("п420бП", "24-53", false));
		reles.add(numRel++, new ReleNMSH("п420аП", "24-52", false));
		reles.add(numRel++, new ReleNMSH_per3_4("п5МК", "2-78", false));
		reles.add(numRel++, new ReleNMSH_per3_4("п6ПК", "2-57", false));
		reles.add(numRel++, new ReleNMSH("7КА", "3-108", false));
		reles.add(numRel++, new ReleDSH2("п420П", "22-56", false));
		reles.add(numRel++, new ReleNMSH("1ВС", "1-26", false));
		reles.add(numRel++, new ReleNMSH("1В", "7-21", false));
		reles.add(numRel++, new ReleNMSH("3п1РК", "6-64", false));
		reles.add(numRel++, new RelePMPUSH("1ПС", "1-24", true));
		reles.add(numRel++, new ReleNMPSH03_90("1НС-1", "1-21", false));
		reles.add(numRel++, new ReleNMPSH03_90("1НС-2", "1-22", false));
		reles.add(numRel++, new ReleNMPSH03_90("1НС-3", "1-23", false));
		reles.add(numRel++, new ReleNMSH("2п1РК", "6-63", false));
		reles.add(numRel++, new ReleNMSH("п1РК", "6-81", false));
		reles.add(numRel++, new ReleNMSH("М1С", "6-23", false));
		reles.add(numRel++, new ReleNMSH("1ВзС", "7-46", false));
		reles.add(numRel++, new ReleNMSH("1РК", "6-62", false));
		reles.add(numRel++, new ReleNMSH_per3_4("1ПК", "1-27", false));
		reles.add(numRel++, new ReleNMSH_per3_4("1МК", "1-28", false));
		reles.add(numRel++, new ReleNMSH_per3_4("п3ПК", "2-71", false));
		reles.add(numRel++, new ReleNMSH_per3_4("4ПК", "1-57", false));
		reles.add(numRel++, new ReleNMSH_per3_4("п5ПК", "2-77", false));
		reles.add(numRel++, new ReleNMSH_per3_4("п3МК", "2-72", false));
		reles.add(numRel++, new ReleNMSH_per3_4("п6МК", "2-58", false));
		reles.add(numRel++, new ReleNMSHM1_700("2п3БУС", "3-83", false));
		reles.add(numRel++, new ReleNMSH("п403П", "7-35", false));
		reles.add(numRel++, new ReleNMSH("п405П", "7-36", false));
		reles.add(numRel++, new ReleNMSH_per3_4("3МК", "1-48", false));
		reles.add(numRel++, new ReleNMSH("п1бз", "7-96", false));
		reles.add(numRel++, new ReleNMSH("Ц1ИР", "2-37", false));
		reles.add(numRel++, new ReleNMSH("1ДИ", "1-87", false));
		reles.add(numRel++, new ReleNMSH("413бСУ", "11-34", false));
		reles.add(numRel++, new ReleNMSHM1_1120("Д-4НУ", "17-38", false));
		reles.add(numRel++, new ReleNMSHM1_1120("Д-3НУ", "17-34", false));
		reles.add(numRel++, new ReleNMSHM1_1120("413НУ", "12-38", false));
		reles.add(numRel++, new ReleNMSHM1_1120("413-3НУ", "17-33", false));
		reles.add(numRel++, new ReleNMSHM1_1120("413-4НУ", "17-37", false));
		reles.add(numRel++, new ReleNMSHM1_1120("410-2ЧУ", "26-37", false));
		reles.add(numRel++, new ReleNMSHM1_1120("4-2ЧУ", "18-38", false));
		reles.add(numRel++, new ReleNMSHM1_1120("3-2ЧУ", "18-37", false));
		reles.add(numRel++, new ReleNMSHM1_1120("411-407НУ", "14-24", false));
		reles.add(numRel++, new ReleNMSH("416/410СУ", "26-38", false));
		reles.add(numRel++, new ReleNMSH("4СУ", "17-26", false));
		reles.add(numRel++, new ReleNMSH("3СУ", "18-26", false));
		reles.add(numRel++, new ReleNMSH("411-407СУ", "15-23", false));
		reles.add(numRel++, new ReleNMSH("п11ГС", "7-86", false));
		reles.add(numRel++, new ReleASH2_110_220("413КОА", "11-24", false));
		reles.add(numRel++, new ReleNMSH("п1АВС", "11-27", false));
		reles.add(numRel++, new ReleDSH2("413Л", "11-45", false));
		reles.add(numRel++, new ReleDSH2("411сП-1", "11-41", false));
		reles.add(numRel++, new ReleANVSH("413УЛС", "11-38", false));
		reles.add(numRel++, new ReleDSH2("411сП-2", "11-42", false));
		reles.add(numRel++, new ReleNMSH("412-40НУ", "26-36", false));
		reles.add(numRel++, new ReleNMSH("414-40НУ", "26-47", false));
		reles.add(numRel++, new ReleNMSH("4а-40НУ", "5-38", false));
		reles.add(numRel++, new ReleNMSH("4-40НУ", "17-35", false));
		reles.add(numRel++, new ReleNMSH("411с-40НУ", "12-37", false));
		reles.add(numRel++, new ReleNMSH("5-40НУ", "22-38", false));
		reles.add(numRel++, new ReleNMSH("413-3а-40НУ", "5-31", false));
		reles.add(numRel++, new ReleNMSH("п413-3а-40НУ", "5-32", false));
		reles.add(numRel++, new ReleNMSH("4п3аП", "1-118", false));
		reles.add(numRel++, new ReleNMSH("Д-4а-40НУ", "5-36", false));
		reles.add(numRel++, new ReleNMSH("пД-4а-40НУ", "5-37", false));
		reles.add(numRel++, new ReleNMSH("3а-40НУ", "5-33", false));
		reles.add(numRel++, new ReleNMSH("3-40НУ", "17-31", false));
		reles.add(numRel++, new ReleNMSH("412с-40ЧУ", "24-45", false));
		reles.add(numRel++, new ReleNMSH("412-40ЧУ", "26-35", false));
		reles.add(numRel++, new ReleNMSH("414-40ЧУ", "26-46", false));
		reles.add(numRel++, new ReleNMSH("4а-40ЧУ", "5-28", false));
		reles.add(numRel++, new ReleNMSH("4-40ЧУ", "17-36", false));
		reles.add(numRel++, new ReleNMSH("3-40ЧУ", "17-32", false));
		reles.add(numRel++, new ReleNMSH("3а-40ЧУ", "5-34", false));
		reles.add(numRel++, new ReleNMSH("п3а-40ЧУ", "5-35", false));
		reles.add(numRel++, new ReleDSH2("3ВА", "18-45", false));
		reles.add(numRel++, new ReleDSH2("4ВА", "17-43", false));
		reles.add(numRel++, new ReleNMSH2_4000("ОЧ-4", "17-44", false));
		reles.add(numRel++, new ReleNMSH2_4000("ОЧ-3", "17-45", false));
		reles.add(numRel++, new ReleNMPSH2_2500("413ВА", "13-25", false));
		reles.add(numRel++, new ReleDSH2("7ВА", "", false));
		reles.add(numRel++, new AutostopMotor("ЛН413", false));
		reles.add(numRel++, new ReleNMSH("412с-40НУ", "24-46", false));
		reles.add(numRel++, new ReleNMSH("4п4аП", "1-115", false));
		reles.add(numRel++, new ReleNMSH("2п5МК", "1-105", false));
		reles.add(numRel++, new ReleNMSH("3пОРЧ", "22-26", false));
		reles.add(numRel++, new ReleNMSH_per3_4("2ПК", "1-37", false));
		reles.add(numRel++, new ReleNMSHM1_560("2ДИ", "4-10", true));
		reles.add(numRel++, new ReleNMSH_per3_4("5МК", "1-68", false));
		reles.add(numRel++, new ReleNMSH_per3_4("4МК", "1-58", false));
		reles.add(numRel++, new ReleNMSH_per3_4("2МК", "1-38", false));
		reles.add(numRel++, new RelePMPUSH("3ПС", "1-44", true));
		reles.add(numRel++, new ReleNMPSH03_90("3НС-1", "1-41", false));
		reles.add(numRel++, new ReleNMPSH03_90("3НС-2", "1-42", false));
		reles.add(numRel++, new ReleNMPSH03_90("3НС-3", "1-43", false));
		reles.add(numRel++, new ReleNMSH("2п3РК", "6-73", false));
		reles.add(numRel++, new ReleNMSH("п3РК", "6-71", false));
		reles.add(numRel++, new ReleNMSH("М3С", "6-25", false));
		reles.add(numRel++, new ReleNMSH("3РК", "6-72", false));
		reles.add(numRel++, new ReleNMSH("3ВС", "1-46", false));
		reles.add(numRel++, new ReleNMSH("3В", "7-23", false));
		reles.add(numRel++, new ReleNMSH("3ВЗС", "7-48", false));
		reles.add(numRel++, new ReleNMSH("3п3РК", "6-74", false));


		reles.stream()
				.filter((r) -> isDuplicate(r))
				.forEach(
						(r) -> {
							System.out.println(r.getTxName().getText() + " - "
									+ reles.indexOf(r) + " - "
									+ r.getTxPlace().getText());
						});
		// -------------------------------------
		// Печать списка реле
		// reles.stream().forEach(
		// (r) -> {
		// System.out.println(r.getTxPlace().getText() + " - "
		// + reles.indexOf(r) + "  -  "
		// + r.getTxName().getText());
		// });

		sourceVSP = new PowerSource();
		sourceVSP.setIsConst(true);
		sourceVSP.setVarName("ВСП-26В");
		sourceVSP.setEds(26);
		sourceVSP.setInR(10);

		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"zm2317Pb9zs"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("19", "", "pdcm19"));
		sourceVSP.addTerminal(new PowerSourceTerminal("5-3ПБ", "",
				"zm253Pb79zm"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"zm2317PbDzs"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("12", "", "pdcm12"));
		sourceVSP.addTerminal(new PowerSourceTerminal("5-3ПБ", "",
				"zm253Pb62zm"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"zm2317Pb416zs"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("16", "", "pdcm16"));
		sourceVSP.addTerminal(new PowerSourceTerminal("5-3ПБ", "",
				"zm253Pb60zm"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("113", "", "pdcm113"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("22", "", "pdcm22"));
		sourceVSP.addTerminal(new PowerSourceTerminal("5-3ПБ", "",
				"zm253Pb1113zm"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"zm2317Pb407zs"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("21", "", "pdcm21"));
		sourceVSP.addTerminal(new PowerSourceTerminal("5-3ПБ", "",
				"zm253Pb110zm"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"zm2Mb416zs"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "zm2Mb9zs"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "zm2MbDzs"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "zm2MbOn"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("4-2МБ", "",
				"zm242Mb"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"zm2Mb407zs"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"zm1317Pb413zs"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("11", "", "pdcm11"));
		sourceVSP.addTerminal(new PowerSourceTerminal("5-3ПБ", "",
				"zm153Pb31zm"));
		sourceVSP.addTerminal(new PowerSourceTerminal("5-3ПБ", "",
				"zm153Pb10zm"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("15", "", "pdcm15"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"zm1317Pb3zs"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("13", "", "pdcm13"));
		sourceVSP.addTerminal(new PowerSourceTerminal("5-3ПБ", "",
				"zm153Pb13zm"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"zm1317Pb4zs"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("14", "", "pdcm14"));
		sourceVSP.addTerminal(new PowerSourceTerminal("5-3ПБ", "",
				"zm153Pb24zm"));
		sourceVSP.addTerminal(new PowerSourceTerminal("5-3ПБ", "",
				"zm153Pb14zm"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"zm1317Pb7zs"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("17", "", "pdcm17"));
		sourceVSP.addTerminal(new PowerSourceTerminal("5-3ПБ", "",
				"zm153Pb97zm"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"zm1Mb413zs"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "zm1Mb3zs"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "zm1Mb4zs"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "zm1Mb7zs"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("4-2МБ", "",
				"zm142Mb"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys1317PbP10zm"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys1317Pb1AS"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys1317Pb413ys"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys1317Pb4133ys"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys1317Pb4134ys"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys1317Pb4ys"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys1317Pb3ys"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys1317Pb416410ys"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys1317Pb90zm"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys1317Pb9ys"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys1317Pb7ys"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys1317PbD3ys"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys1317PbD4ys"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"ys1Mb413zs"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"ys1Mb2p2z"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "ys1Mb3ad"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "ys1MbDzs"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys2317PbP40zm"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys2317PbP3bys"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys2317PbP4bys"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys2317Pb110zm"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-17ПБ", "",
				"ys2317Pb407411ys"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-19ПБ", "",
				"ys2319PbP1AS"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-19ПБ", "",
				"ys2319Pb1AS"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-19ПБ", "",
				"ys2319PbVS13AS12"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-19ПБ", "",
				"ys2319PbP2AS"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-19ПБ", "",
				"ys2319Pb2AS"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-19ПБ", "",
				"ys2319PbVS2AS"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-19ПБ", "",
				"ys2319PbP3AS22"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-19ПБ", "",
				"ys2319Pb3AS32"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-19ПБ", "",
				"ys2319PbVS13AS22"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-19ПБ", "",
				"ys2319PbP3AD"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-19ПБ", "",
				"ys2319Pb3AD"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-19ПБ", "",
				"ys2319PbP4AD"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-19ПБ", "",
				"ys2319Pb4AD"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-19ПБ", "",
				"ys2319PbP1AS31"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "ys2Mb1bz"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "ys2Mb11z"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"ys2Mb413zs53"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"ys2Mb416410zs"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"ys2Mb407411zs"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"ys2Mb11gs"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"ys2Mb413zs43"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("71", "", "pdcm71"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("72", "", "pdcm72"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("73", "", "pdcm73"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("74", "", "pdcm74"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("75", "", "pdcm75"));
		sourceVSP.addTerminal(new PowerSourceTerminal("2-20ПБ", "",
				"pvt220PbP1PK"));
		sourceVSP.addTerminal(new PowerSourceTerminal("1-20ПБ", "",
				"pvt120Pb3PK"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-14ПБ", "",
				"pvt314Pb416ys"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-15ПБ", "",
				"pvt315Pb4AD"));
		sourceVSP.addTerminal(new PowerSourceTerminal("2-16ПБ", "",
				"pvt216Pb1z"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-14ПБ", "",
				"pvt314Pb7z"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-15ПБ", "",
				"pvt315PbGOM"));
		sourceVSP.addTerminal(new PowerSourceTerminal("22-20ПБ", "",
				"pvt2220Pb1kg"));
		sourceVSP.addTerminal(new PowerSourceTerminal("10-16ПБ", "",
				"pvt1016Pb1A"));
		sourceVSP.addTerminal(new PowerSourceTerminal("2-20ПБ", "",
				"pvt220PbP414P"));
		sourceVSP.addTerminal(new PowerSourceTerminal("25-19ПБ", "",
				"pvt2519Pb408GO"));
		sourceVSP.addTerminal(new PowerSourceTerminal("18-12ПБ", "",
				"pvt1812PbDBO"));
		sourceVSP.addTerminal(new PowerSourceTerminal("11-19ПБ", "",
				"pvt1119Pb413BO"));
		sourceVSP.addTerminal(new PowerSourceTerminal("18-12ПБ", "",
				"pvt1812Pb3BO"));
		sourceVSP.addTerminal(new PowerSourceTerminal("17-20ПБ", "",
				"pvt1720Pb4BO"));
		sourceVSP.addTerminal(new PowerSourceTerminal("2-20ПБ", "",
				"pvt220PbP407P"));
		sourceVSP.addTerminal(new PowerSourceTerminal("2-20ПБ", "",
				"pvt220Pb3aP"));
		sourceVSP.addTerminal(new PowerSourceTerminal("3-20ПБ", "",
				"pvt320PbP3bP"));
		sourceVSP.addTerminal(new PowerSourceTerminal("11-19ПБ", "",
				"pvt1119Pb413sP1"));
		sourceVSP.addTerminal(new PowerSourceTerminal("К3-3ПБ", "",
				"pvtK33Pb10zm"));
		sourceVSP.addTerminal(new PowerSourceTerminal("21-19ПБ", "",
				"pvt2119Pb406KO"));
		sourceVSP.addTerminal(new PowerSourceTerminal("ПБ", "", "pvtPb411GO"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"pvtMb2p1PK"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"pvtMbP416ys"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"pvtMb34AD"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "pvtMbP1z"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "pvtMbKG"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"pvtMbP13A"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"pvtMb2p414P"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"pvtMbP408RO"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"pvtMb2p407P"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"pvtMbP3aP"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("К3-4МБ", "",
				"pvtK34Mb"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"pvtMbP406KO"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"pvtMbP411RO"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"ys2MbO1AS"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"ys2MbO2AS"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"ys2MbO3AS"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"ys2MbO3AD"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"ys2MbO4AD"));
		sourceVSP.addTerminal(new PowerSourceTerminal("1-19ПБ", "",
				"str1119Pb41334ys"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("26", "", "pdcm26"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("25", "", "pdcm25"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("23", "", "pdcm23"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("24", "", "pdcm24"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"str1Mb1Vs"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"str1MbP1z"));
		sourceVSP.addTerminal(new PowerSourceTerminal("2-17ПБ", "",
				"gs1217Pb1PP21"));
		sourceVSP.addTerminal(new PowerSourceTerminal("2-17ПБ", "",
				"gs1217Pb1Bgs21"));
		sourceVSP.addTerminal(new PowerSourceTerminal("2-17ПБ", "",
				"gs1217Pb2P413RO31"));
		sourceVSP.addTerminal(new PowerSourceTerminal("2-17ПБ", "",
				"gs1217Pb1RV21"));
		sourceVSP.addTerminal(new PowerSourceTerminal("2-17ПБ", "",
				"gs1217Pb3p411P51"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("106", "", "pdcm106"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "gs1Mb1RV"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"gs1Mb1VRz"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "gs1Mb1Rz"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"gs1Mb413zs21"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"gs1Mb3zs61"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "gs1Mb1z"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"gs1MbC1IR"));
		sourceVSP.addTerminal(new PowerSourceTerminal("2-12ПБ", "",
				"nu1212PbDsu"));
		sourceVSP.addTerminal(new PowerSourceTerminal("2-13ПБ", "",
				"nu1213Pb416su"));
		sourceVSP.addTerminal(new PowerSourceTerminal("ПБ", "", "nu1Pb73Pr"));
		sourceVSP.addTerminal(new PowerSourceTerminal("ПБ", "", "nu1Pb74Pr"));

		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"nu1Mb416su"));
		sourceVSP
				.addMinusTerminal(new PowerSourceTerminal("МБ", "", "nu1MbDsu"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("гом", "", "pdcmGom"));
		sourceVSP.addTerminal(new PowerSourceTerminal("ПБ", "", "su2Pb413Va"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"su2Mb413Va"));
		sourceVSP.addTerminal(new PowerSourceTerminal("4-18ПБ", "", "diPb418"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"diMb2di"));
		sourceVSP.addTerminal(new PowerSourceTerminal("1-20ПБ", "", "diPb120"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"diMb1di"));
		sourceVSP.addTerminal(new PowerSourceTerminal("1-19ПБ", "", "str3Pb119"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"str3MbPS"));
		sourceVSP.addMinusTerminal(new PowerSourceTerminal("МБ", "",
				"str3Mb3Vs"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("35", "", "pdcm35"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("36", "", "pdcm36"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("33", "", "pdcm33"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("34", "", "pdcm34"));
		sourceVSP.addTerminal(new PowerSourceTerminalDCM("78", "", "pdcm78"));


		// источник ТСЗ----------------
		sourceTsz = new PowerSource();
		sourceTsz.setVarName("TСЗ-127В");
		sourceTsz.setEds(130);
		sourceTsz.setInR(10);

		int numTerm = 0;
		
		sourceTsz.addTerminal(new PowerSourceTerminal("С1", "", "str1C1"));
		sourceTsz.getTerminal(numTerm++).setFaza(3);
		sourceTsz.addTerminal(new PowerSourceTerminal("В1", "", "str1B1"));
		sourceTsz.getTerminal(numTerm++).setFaza(2);
		sourceTsz.addTerminal(new PowerSourceTerminal("А1", "", "str1A1"));
		sourceTsz.getTerminal(numTerm++).setFaza(1);
		sourceTsz.addTerminal(new PowerSourceTerminal("СТК", "", "str1Stk"));
		sourceTsz.getTerminal(numTerm++).setFaza(1);
		sourceTsz.addTerminal(new PowerSourceTerminal("ОСТК", "", "str1OStk"));
		sourceTsz.getTerminal(numTerm++).setFaza(3);
		sourceTsz
				.addTerminal(new PowerSourceTerminal("11-4А", "", "sv413114a"));
		sourceTsz.getTerminal(numTerm++).setFaza(1);

		sourceTsz
				.addTerminal(new PowerSourceTerminal("11-5В", "", "sv413115b"));
		sourceTsz.getTerminal(numTerm++).setFaza(2);
		sourceTsz
				.addTerminal(new PowerSourceTerminal("11-1А", "", "sv413111a"));
		sourceTsz.getTerminal(numTerm++).setFaza(1);
		sourceTsz
				.addTerminal(new PowerSourceTerminal("11-2В", "", "sv413112b"));
		sourceTsz.getTerminal(numTerm++).setFaza(2);
		sourceTsz
				.addTerminal(new PowerSourceTerminal("11-3С", "", "sv413113c"));
		sourceTsz.getTerminal(numTerm++).setFaza(3);
		sourceTsz.addTerminal(new PowerSourceTerminal("11-13А", "",
				"sv4131113a"));
		sourceTsz.getTerminal(numTerm++).setFaza(1);

		sourceTsz.addTerminal(new PowerSourceTerminal("11-14С", "",
				"sv4131114c"));
		sourceTsz.getTerminal(numTerm++).setFaza(3);
		// стрелка 3 
		sourceTsz.addTerminal(new PowerSourceTerminal("С1", "", "str3C1"));
		sourceTsz.getTerminal(numTerm++).setFaza(3);
		sourceTsz.addTerminal(new PowerSourceTerminal("В1", "", "str3B1"));
		sourceTsz.getTerminal(numTerm++).setFaza(2);
		sourceTsz.addTerminal(new PowerSourceTerminal("А1", "", "str3A1"));
		sourceTsz.getTerminal(numTerm++).setFaza(1);
		sourceTsz.addTerminal(new PowerSourceTerminal("СТК", "", "str3Stk"));
		sourceTsz.getTerminal(numTerm++).setFaza(1);
		sourceTsz.addTerminal(new PowerSourceTerminal("ОСТК", "", "str3OStk"));
		sourceTsz.getTerminal(numTerm++).setFaza(3);


		getSources().add(0, sourceVSP);
		getSources().add(1, sourceTsz);

		// установка длительности подачи плюса в полюс 4 секунд
		((PowerSourceTerminalDCM) getTerminal("pdcmGom")).setZadergka(240);

		// установка минусового полюса
		sourceVSP.getMinusTerminals().stream().forEach((t) -> {
			t.setMinus(true);
			// System.out.println(t.getVarName()+" - index" +
			// sourceVSP.getMinusTerminals().indexOf(t));
			});
		// sourceVSP.getTerminals().stream().forEach((t)->{
		// System.out.println(t.getVarName()+" - index" +
		// sourceVSP.getTerminals().indexOf(t));
		// });

		strMotors.add(0, new StrMotor("", ""));
		strMotors.add(1, new StrMotor("", ""));
		strMotors.add(2, new StrMotor("", ""));
		strMotors.add(3, new StrMotor("", ""));
		strMotors.add(4, new StrMotor("", ""));
		strMotors.add(5, new StrMotor("", ""));
		strMotors.get(0).setVarName("motorStr1");
		strMotors.get(1).setVarName("motorStr2");
		strMotors.get(2).setVarName("motorStr3");
		strMotors.get(3).setVarName("motorStr4");
		strMotors.get(4).setVarName("motorStr5");
		strMotors.get(5).setVarName("motorStr6");

	}

	private boolean isDuplicate(Rele rel) {
		return reles.stream().anyMatch(
				(r) -> (r.getTxName().getText()
						.equals(rel.getTxName().getText()) || r.getTxPlace()
						.getText().equals(rel.getTxPlace().getText()))
						&& r != rel);

	}

	public void addRele(int i, Rele r) {
		reles.add(i, r);
	}

	public List<StrMotor> getStrMotors() {
		return strMotors;
	}

	public Rele getRele(long l) {
		return reles.get((int) l);
	}

	public Rele getRele(String name) {
		Optional<Rele> rel = null;
		if (name != null)
			rel = reles.stream()
					.filter((r) -> r.getTxName().getText().equals(name))
					.findFirst();
		return rel.get();
	}

	public List<Rele> getReles() {
		return reles;
	}

	public List<Source> getSources() {
		return sources;
	}

	public SchemElement getTerminal(String varName) {
		SchemElement temp = null;
		for (Source s : sources) {
			temp = s.getTerminal(varName);
			if (temp != null)
				break;
		}

		return temp;
	}

	/**
	 * @param pst
	 *            Возвращает источник питания к которому принадлежит полюс
	 */
	public Source getSource(PowerSourceTerminal pst) {
		Source source = null;
		for (Source so : getSources()) {
			if (so.isTerminalConteined(pst))
				source = so;
		}
		return source;
	}

	/**
	 * постановка под ток реле пока не нарисованы все схемы
	 */
	public void setNormalStateRele() {
		// постановка по ток путевых реле
		// п414П
		reles.get(4).setIsCurrent(false);
		reles.get(4).setIsCurrent(true);
		// п403П
		reles.get(295).setIsCurrent(false);
		reles.get(295).setIsCurrent(true);
		// п405П
		reles.get(296).setIsCurrent(false);
		reles.get(296).setIsCurrent(true);
		// п407П
		reles.get(208).setIsCurrent(false);
		reles.get(208).setIsCurrent(true);
		// п409П
		reles.get(210).setIsCurrent(false);
		reles.get(210).setIsCurrent(true);
		// п409сП
		reles.get(212).setIsCurrent(false);
		reles.get(212).setIsCurrent(true);
		// п411П
		reles.get(215).setIsCurrent(false);
		reles.get(215).setIsCurrent(true);
		// // п411сП
		reles.get(218).setIsCurrent(false);
		reles.get(218).setIsCurrent(true);
		// п411сП-1
		reles.get(319).setIsCurrent(false);
		reles.get(319).setIsCurrent(true);
		// п411сП-2
		reles.get(321).setIsCurrent(false);
		reles.get(321).setIsCurrent(true);

		// 413сП-1
		reles.get(253).setIsCurrent(false);
		reles.get(253).setIsCurrent(true);
		// 413сП-2
		reles.get(254).setIsCurrent(false);
		reles.get(254).setIsCurrent(true);

		// п408сП
		reles.get(223).setIsCurrent(false);
		reles.get(223).setIsCurrent(true);
		// п410сП
		reles.get(227).setIsCurrent(false);
		reles.get(227).setIsCurrent(true);
		// п412сП
		reles.get(229).setIsCurrent(false);
		reles.get(229).setIsCurrent(true);
		// п412П
		reles.get(232).setIsCurrent(false);
		reles.get(232).setIsCurrent(true);
		// 3аП
		reles.get(235).setIsCurrent(false);
		reles.get(235).setIsCurrent(true);
		// 4аП
		reles.get(239).setIsCurrent(false);
		reles.get(239).setIsCurrent(true);
		// 3П-1
		reles.get(243).setIsCurrent(false);
		reles.get(243).setIsCurrent(true);
		// 3П-2
		reles.get(244).setIsCurrent(false);
		reles.get(244).setIsCurrent(true);
		// 4П-1
		reles.get(246).setIsCurrent(false);
		reles.get(246).setIsCurrent(true);
		// 4П-2
		reles.get(247).setIsCurrent(false);
		reles.get(247).setIsCurrent(true);
		// 5П-1
		reles.get(249).setIsCurrent(false);
		reles.get(249).setIsCurrent(true);
		// 5П-1
		reles.get(250).setIsCurrent(false);
		reles.get(250).setIsCurrent(true);
		// п3бП
		reles.get(255).setIsCurrent(false);
		reles.get(255).setIsCurrent(true);
		// п416П
		reles.get(267).setIsCurrent(false);
		reles.get(267).setIsCurrent(true);
		// п418П
		reles.get(268).setIsCurrent(false);
		reles.get(268).setIsCurrent(true);
		// п420бП
		reles.get(269).setIsCurrent(false);
		reles.get(269).setIsCurrent(true);
		// п420аП
		reles.get(270).setIsCurrent(false);
		reles.get(270).setIsCurrent(true);
		// п420аП
		reles.get(274).setIsCurrent(false);
		reles.get(274).setIsCurrent(true);

		// установка поляризованных контактов ПС в плюсовое положение
		// 1ПС
		reles.get(278).setIsCurrent(false);
		reles.get(278).setIsCurrent(true);
		// ---------------------------- f
		// установка стрелок в плюсовое положение
		// пока нет схем 2, 3, 4, 5, 6
		// п2ПК
		reles.get(152).setIsCurrent(false);
		reles.get(152).setIsCurrent(true);
		// 2ПК
		reles.get(354).setIsCurrent(false);
		reles.get(354).setIsCurrent(true);
		// п3ПК
		reles.get(289).setIsCurrent(false);
		reles.get(289).setIsCurrent(true);
		// 3ПК
		reles.get(158).setIsCurrent(false);
		reles.get(158).setIsCurrent(true);
		// п4ПК
		reles.get(154).setIsCurrent(false);
		reles.get(154).setIsCurrent(true);
		// 4ПК
		reles.get(290).setIsCurrent(false);
		reles.get(290).setIsCurrent(true);
		// п5ПК
		reles.get(291).setIsCurrent(false);
		reles.get(291).setIsCurrent(true);
		// 5ПК
		reles.get(160).setIsCurrent(false);
		reles.get(160).setIsCurrent(true);
		// п6ПК
		reles.get(272).setIsCurrent(false);
		reles.get(272).setIsCurrent(true);
		// 6ПК
		reles.get(162).setIsCurrent(false);
		reles.get(162).setIsCurrent(true);
		// ---------------------------------------
		// обесточивание 3пОРЧ пока нет схем
		reles.get(353).setIsCurrent(true);
		reles.get(353).setIsCurrent(false);

		// постановка под ток 7КА пока нет схем
		reles.get(273).setIsCurrent(false);
		reles.get(273).setIsCurrent(true);
		// постановка под ток 9з пока нет схем
		reles.get(113).setIsCurrent(false);
		reles.get(113).setIsCurrent(true);

		// постановка под ток линейного реле 413Л
		// и 413УЛС
		// через контакты 413СУ для отладки пока нет схемы линейного
		reles.get(318).setCurrent(true);
		reles.get(318).setCurrent(false);
		reles.get(320).setCurrent(true);
		reles.get(320).setCurrent(false);
		reles.get(122).isCurrentProperty()
				.addListener((property, oldValue, newValue) -> {
					reles.get(318).setCurrent(newValue.booleanValue());
					reles.get(320).setCurrent(newValue.booleanValue());
				});
	}
}
