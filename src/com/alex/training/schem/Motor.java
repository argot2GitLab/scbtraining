﻿package com.alex.training.schem;

import java.util.List;

import com.alex.training.schemElement.ObmotkaMotor;

public interface Motor {

	public void setIsWork(boolean newValue);

	public boolean getIsWork();

	public List<User> getObmotki();

}
