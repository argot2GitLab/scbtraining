﻿package com.alex.training.schem;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;

import com.alex.training.schemElement.PowerSourceTerminal;
import com.alex.training.schemElement.SchemElement;

/**
 * @author Alex интерфейс который будут реализовывать источники кондеры, трансы
 *         итд
 *
 */

public interface Source {

	// свойство тип тока постоянный - true
	// ------------------------------------------------

	public BooleanProperty isConstProperty();

	public void setIsConst(boolean newValue);

	public boolean getIsConst();

	// ЭДС источника
	public DoubleProperty edsProperty();

	public void setEds(double newValue);

	public double getEds();

	// внутреннее сопротивление источника
	public DoubleProperty inRProperty();

	public void setInR(double newValue);

	public double getInR();

	public PowerSourceTerminal getTerminal(int i);

	public List<PowerSourceTerminal> getMinusTerminals();

	public void addTerminal(PowerSourceTerminal t);

	public void addMinusTerminal(PowerSourceTerminal t);

	public List<PowerSourceTerminal> getTerminals();

	public boolean isTerminalConteined(String varName);

	public SchemElement getTerminal(String varName);

	public SchemElement getMinusTerminal(String newValue);

	public boolean isTerminalConteined(PowerSourceTerminal term);

	public int getIndexTerminal(PowerSourceTerminal term);

	public int getIndexTerminal(String varName);

	public int getIndexMinusTerminal(String varName);

	public PowerSourceTerminal getMinusTerminal(int i);

	public void setVarName(String string);

	public String getVarName();

}