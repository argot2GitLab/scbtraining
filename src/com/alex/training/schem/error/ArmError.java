package com.alex.training.schem.error;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javafx.util.Pair;

public class ArmError {

	// описание ошибки на АРМе(не задается маршрут с первого на и.т.д
	private String info;
	private List<Integer> elements;

	// private String answer;

	public ArmError(String str) {
		info = str;
		elements = new ArrayList<Integer>();

	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public List<Integer> getElements() {
		return elements;
	}

	/**
	 * @return список состоящий из пар чисел 1- номер схемы 2- номер элемента в схеме
	 */
	public List<Pair<Integer, Integer>> getRandomPairs(int num) {
		Random rand = new Random(new Date().getTime());
		List<Pair<Integer, Integer>> list = new ArrayList<Pair<Integer, Integer>>();
		int i = 0;
		Pair<Integer, Integer> pair = null;
		if (!elements.isEmpty() && num < elements.size()) {
			for (int j = 0; j < num; j++) {
				i = rand.nextInt(elements.size());
				if (i % 2 == 0)
					pair = new Pair<Integer, Integer>(elements.get(i),
							elements.get(i + 1));
				else
					pair = new Pair<Integer, Integer>(elements.get(i - 1),
							elements.get(i));

				list.add(j, pair);
			}
		}
		return list;
	}

}
