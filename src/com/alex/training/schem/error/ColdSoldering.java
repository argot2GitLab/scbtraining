package com.alex.training.schem.error;

import java.util.Arrays;

import com.alex.training.schemElement.Contact;
import com.alex.training.schemElement.FrontAutostopContact;
import com.alex.training.schemElement.FrontContact;
import com.alex.training.schemElement.FrontRightContact;
import com.alex.training.schemElement.PolarContact;
import com.alex.training.schemElement.Rele;
import com.alex.training.schemElement.SchemElement;
import com.alex.training.schemElement.StrContact;
import com.alex.training.schemElement.TylAutostopContact;
import com.alex.training.schemElement.TylContact;
import com.alex.training.schemElement.TylRightContact;

public class ColdSoldering extends AbstractElementError implements ElementError {

	// номер контакта для описания ошибки
	private String numCont;

	public ColdSoldering() {
		info = "Холодная пайка контакта ";
		classes = Arrays.asList(FrontContact.class, FrontRightContact.class,
				PolarContact.class, TylContact.class, TylRightContact.class,
				StrContact.class, FrontAutostopContact.class,
				TylAutostopContact.class, Rele.class);
	}

	@Override
	public void setupElementErrorInfo(SchemElement sc) {
		if (sc.getClass().equals(FrontContact.class)
				|| sc.getClass().equals(TylContact.class))
			numCont = String.valueOf(((Contact) sc).getContNum() + 1) + "-"
					+ sc.getTxName().getText();
		if (sc.getClass().equals(FrontRightContact.class)
				|| sc.getClass().equals(TylRightContact.class))
			numCont = String.valueOf(((Contact) sc).getContNum() + 2) + "-"
					+ sc.getTxName().getText();
		if (sc.getClass().getSuperclass().equals(Rele.class))
			numCont = ((Rele) sc).getTxV1().getText() + "-"
					+ sc.getTxName().getText();

	}

	@Override
	public String getInfo() {
		return info + numCont;
	}

}
