package com.alex.training.schem.error;

import com.alex.training.schemElement.SchemElement;

public interface ElementError {

	public void setupElementErrorInfo(SchemElement sc);

	public String getInfo();
}
