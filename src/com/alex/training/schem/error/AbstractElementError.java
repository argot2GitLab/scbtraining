package com.alex.training.schem.error;

import java.util.ArrayList;
import java.util.List;

public class AbstractElementError {

	protected String info;
	protected int idError;

	// список классов которые могут реализовывать данную ошибку
	protected List<Class> classes;

	public AbstractElementError() {

		classes = new ArrayList<Class>();
	}

	public boolean isClassConteined(Class cl) {
		return classes.stream().anyMatch((c) -> c.equals(cl));
	}

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

}
