package com.alex.training.schem.error;

import java.util.Arrays;

import com.alex.training.schemElement.Contact;
import com.alex.training.schemElement.FrontAutostopContact;
import com.alex.training.schemElement.FrontContact;
import com.alex.training.schemElement.FrontRightContact;
import com.alex.training.schemElement.PolarContact;
import com.alex.training.schemElement.SchemElement;
import com.alex.training.schemElement.StrContact;
import com.alex.training.schemElement.TylAutostopContact;
import com.alex.training.schemElement.TylContact;
import com.alex.training.schemElement.TylRightContact;

public class PerechodResistence extends AbstractElementError implements
		ElementError {
	private String numCont;

	public PerechodResistence() {
		info = "Повышенное переходное сопротивление контакта ";
		classes = Arrays.asList(FrontContact.class, FrontRightContact.class,
				PolarContact.class, TylContact.class, TylRightContact.class,
				StrContact.class, FrontAutostopContact.class,
				TylAutostopContact.class);
	}

	@Override
	public void setupElementErrorInfo(SchemElement sc) {
		if (sc.getClass().equals(FrontContact.class)
				|| sc.getClass().equals(FrontRightContact.class))
			numCont = String.valueOf(((Contact) sc).getContNum() + 1) + "-"
					+ String.valueOf(((Contact) sc).getContNum() + 2) + " "
					+ sc.getTxName().getText();
		if (sc.getClass().equals(TylContact.class)
				|| sc.getClass().equals(TylRightContact.class))
			numCont = String.valueOf(((Contact) sc).getContNum() + 1) + "-"
					+ String.valueOf(((Contact) sc).getContNum() + 3) + " "
					+ sc.getTxName().getText();
	}

	@Override
	public String getInfo() {
		return info + numCont;
	}
}
