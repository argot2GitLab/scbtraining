﻿package com.alex.training.schem;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.alex.training.schemElement.PowerSourceTerminal;
import com.alex.training.schemElement.PowerSourceTerminalDCM;
import com.alex.training.schemElement.SchemElement;

import javafx.animation.AnimationTimer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;

public class ParUnion {

	private List<Loop> union = new ArrayList<Loop>();
	private String varName;

	// сопротивление параллельного соединения
	// ---------------------------------------------------
	public DoubleProperty unionR = new SimpleDoubleProperty();

	public final DoubleProperty unionRProperty() {
		return unionR;
	}

	public final void setUnionR(double newValue) {
		unionR.set(newValue);
	}

	public final double getUnionR() {
		return unionR.get();
	}

	// свойство замкнутости параллельного соединения
	// ------------------------------------------------
	public BooleanProperty unionConductance = new SimpleBooleanProperty();

	public final BooleanProperty unionConductanceProperty() {
		return unionConductance;
	}

	public final void setUnionConductance(boolean newValue) {
		unionConductance.set(newValue);
	}

	public final boolean getUnionConductance() {
		return unionConductance.get();
	}

	final private AnimationTimer atUnion = new AnimationTimer() {

		long count = 0;
		boolean flag = false;

		@Override
		public void handle(long arg0) {
			// if (count % 6 == 0) {
			flag = union.stream().anyMatch((l) -> l.getLoopConductance());
			if (getUnionConductance() != flag)
				setUnionConductance(flag);
			// }
			count++;
		}

	};
	private double tempResistance;

	public ParUnion() {
		setUnionR(0);
		setUnionConductance(false);
	}

	public ParUnion(List<Loop> union) {
		this();
		this.union = union;
		union.stream().forEach((l) -> {
			l.loopRProperty().addListener((property, oldValue, newValue) -> {
				setUnionResistence();
			});

		});
		setUnionResistence();
	}

	// добавление контура в соединение при условии что его еще нет
	public void addLoop(Loop loop) {
		if (loop != null && union.stream().allMatch((l) -> !l.equals(loop))) {
			union.add(loop);
			setUnionResistence();
		}
		if (!loop.getLoop().isEmpty()) {
			loop.loopRProperty().addListener(
					(property, oldValue, newValue) -> setUnionResistence());
		}
	}

	public void removeLoop(int i) {
		if (union.size() > i)
			union.remove(i);
	}

	public List<Loop> getUnion() {
		return union;
	}

	public void setUnion(List<Loop> union) {
		this.union = union;
	}

	// определение и установка сопротивления соединения
	public void setUnionResistence() {
		Double result;
		if (union.size() > 1) {
			if (union.stream().anyMatch((l) -> l.getLoopR() == 0))
				result = 0.;
			else {
				tempResistance = 0;
				union.forEach((l) -> {
					tempResistance += 1 / l.getLoopR();
				});
				result = 1 / tempResistance;
			}
		} else {
			result = union.get(0).getLoopR();
		}
		if (getUnionR() != result)
			setUnionR(result);
	}

	public void startUnion() {
		union.forEach((l) -> l.startLoop());
		atUnion.start();
	}

	/**
	 * @param flag
	 *            для подсветки соединения в редакторе эл. цепей
	 */
	public void unionPodsvetka(int mod) {
		if (!union.isEmpty())
			union.stream().forEach((l) -> l.loopPodsvetka(mod));
	}

	public void stopUnion() {
		union.stream().forEach((l) -> l.stopLoop());
		atUnion.stop();
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	@Override
	public String toString() {
		return varName;
	}

	public List<Loop> getLoopsNotConducted() {
		if (!union.isEmpty()) {
			return union.stream().filter((l) -> !l.getLoopConductance())
					.collect(Collectors.toList());
		} else
			return null;
	}

	// // определение сопротивления до подключенной плюсовой клеммы
	// тестера
	public double getUnionResistenceBeforePlusClamp() {
		double rN = 0;
		if (isPlusClampUnionConnect()) {
			for (Loop loop : union) {
				if (loop.isPlusClampConnect()) {
					if (union.size() > 1 && getUnionR() == 0) {
						rN = (loop.isPlusClampEnd(true)) ? 0 : loop
								.getResistenceBeforePlusClamp();
					} else
						rN = loop.getResistenceBeforePlusClamp();

				}
			}
		}
		System.out
				.println(rN
						+ "- сопротивление соединения до подключенной плюсовой клеммы тестера");
		return rN;
	}

	// // определение сопротивления после подключенной плюсовой клеммы
	// тестера
	public double getUnionResistencePostPlusClamp() {
		double rN = -1;
		if (isPlusClampUnionConnect()) {
			for (Loop loop : union) {
				if (loop.isPlusClampConnect())
					if (union.size() > 1 && getUnionR() == 0) {
						rN = (loop.isPlusClampEnd(true)) ? 0 : loop
								.getResistencePostPlusClamp();
					} else
						rN = loop.getResistencePostPlusClamp();
			}
		}
		System.out
				.println(rN
						+ "-сопротивление соединения после подключенной плюсовой клеммы тестера");
		return rN;
	}

	// // определение сопротивления контура после подключенной минусовой клеммы
	// тестера
	public double getUnionResistencePostMinusClamp() {
		double rN = -1;
		if (isMinusClampUnionConnect()) {
			for (Loop loop : union) {
				if (loop.isMinusClampConnect())
					if (union.size() > 1 && getUnionR() == 0) {
						rN = (loop.isPlusClampEnd(false)) ? 0 : loop
								.getResistencePostMinusClamp();
					} else
						rN = loop.getResistencePostMinusClamp();
			}
		}
		System.out
				.println(rN
						+ "-сопротивление соединения после подключенной минусовой клеммы тестера");
		return rN;
	}

	/**
	 * возвращает сопротивление соединения до и после подключенных клемм при
	 * подключении обеих клемм к одному соединению
	 */
	public double getResByConnectedAllClamp() {
		double resLoop = 10000000;
		boolean isCount = true;
		// 1 - при подключении обеих клемм к одному контуру
		for (Loop lp : union) {
			if (lp.isAllClampConnect())
				isCount = !isCount;
			if (isCount) {
				resLoop = 0;
				resLoop += lp.getResByConnectedAllClamp();
				// System.out.println(sc.toString() + "-  "
				// + sc.getResistance());
			}
		}

		return resLoop;
	}

	/**
	 * возвращает контур к которому подключены обе клеммы или null
	 */
	public Loop getLoopByConnectedAllClamp() {
		for (Loop lp : union) {
			if (lp.isAllClampConnect())
				return lp;
		}
		return null;
	}

	/**
	 * возвращает контур к которому подключена плюсовая или минусовая клемма или
	 * null
	 */
	public Loop getLoopByConnectedClamp(boolean isPlus) {
		for (Loop lp : union) {
			if ((isPlus) ? lp.isPlusClampConnect() : lp.isMinusClampConnect())
				return lp;
		}
		return null;
	}

	public boolean isPlusClampUnionConnect() {
		return union.stream().anyMatch((l) -> l.isPlusClampConnect());
	}

	/**
	 * Для ТЕСТА !!!! выводит в консоль список контуров соединения
	 */
	public void printLoopList() {
		for (Loop l : union) {
			System.out.println("Контур - " + l.getVarName()
					+ ": Порядковый номер - " + union.indexOf(l)
					+ "; Сопротивление -  " + l.getLoopR());
		}
	}

	// // подключены ли обе клеммы к соединению
	public boolean isAllClampConnect() {
		return (isMinusClampUnionConnect() && isPlusClampUnionConnect());
	}

	public boolean isMinusClampUnionConnect() {
		return union.stream().anyMatch((l) -> l.isMinusClampConnect());
	}

	// возвращает полюс если это первое соединение в цепи или null
	public PowerSourceTerminal getStartTerminal() {
		PowerSourceTerminal pst = null;
		for (Loop loop : union) {
			if (loop.isTerminalConteined())
				pst = loop.getStartTerminal();
		}
		return pst;
	}

	// возвращает полюс если это последнее соединение в цепи или null
	public PowerSourceTerminal getEndTerminal() {
		PowerSourceTerminal pst = null;
		for (Loop loop : union) {
			if (loop.isTerminalConteined())
				pst = loop.getEndTerminal();
		}
		return pst;
	}

	// возвращает полюс если это последнее соединение в цепи и контур
	// перевернутый
	public PowerSourceTerminal getEndReverseTerminal() {
		PowerSourceTerminal pst = null;
		for (Loop loop : union) {
			if (loop.isTerminalConteined()) {
				if (loop.isTerminalFirst())
					pst = loop.getStartTerminal();
			}
		}
		return pst;
	}

	/**
	 * @return проверяет перевернутый ли последний контур
	 */
	public boolean isTerminalsLoopReverse() {
		for (Loop loop : union) {
			if (loop.isTerminalConteined())
				return loop.isTerminalFirst();
		}
		return false;
	}

	// сопротивление соединения до подключенной минусовой клеммы тестера
	public double getUnionResistenceBeforeMinusClamp() {
		double rN = 0;
		if (isMinusClampUnionConnect()) {
			for (Loop loop : union) {
				if (loop.isMinusClampConnect()) {
					if (union.size() > 1 && getUnionR() == 0) {
						rN = (loop.isPlusClampEnd(false)) ? 0 : loop
								.getResistenceBeforeMinusClamp();
					} else
						rN = loop.getResistenceBeforeMinusClamp();

				}
			}
		}
		System.out
				.println(rN
						+ "- сопротивление соединения  до подключенной минусовой клеммы тестера");
		return rN;

	}

}
