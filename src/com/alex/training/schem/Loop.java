﻿package com.alex.training.schem;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javafx.animation.AnimationTimer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;

import com.alex.training.schemElement.Clamp;
import com.alex.training.schemElement.PowerSourceTerminal;
import com.alex.training.schemElement.PowerSourceTerminalDCM;
import com.alex.training.schemElement.SchemElement;

/**
 * @author Alex Класс для проверки контура на целостность проводников loop -
 *         список проводников входящих в контур. loopConductance- свойство
 *         целостности контура контур - не разветвленный участок эл. цепи
 */
public class Loop {

	private List<SchemElement> loop = new ArrayList<SchemElement>();
	private String varName;
	PowerSourceTerminal pst;

	// сопротивление контура ---------------------------------------------------
	public DoubleProperty loopR = new SimpleDoubleProperty();

	public final DoubleProperty loopRProperty() {
		return loopR;
	}

	public final void setLoopR(double newValue) {
		loopR.set(newValue);
	}

	public final double getLoopR() {
		return loopR.get();
	}

	// свойство замкнутости контура
	// ------------------------------------------------
	public BooleanProperty loopConductance = new SimpleBooleanProperty();

	public final BooleanProperty loopConductanceProperty() {
		return loopConductance;
	}

	public final void setLoopConductance(boolean newValue) {
		loopConductance.set(newValue);
	}

	public final boolean getLoopConductance() {
		return loopConductance.get();
	}

	// ----------------------------------------------------------------

	final private AnimationTimer atLoop = new AnimationTimer() {

		long count = 0;
		boolean flag = false;

		@Override
		public void handle(long arg0) {
			// if (count % 6 == 0) {
			flag = loop.stream().allMatch((s) -> s.getIsConductance());
			if (getLoopConductance() != flag)
				setLoopConductance(flag);
			// }
			count++;
		}

	};

	// конструктор
	// -----------------------------------------------------------------

	public Loop() {
		setLoopConductance(false);
		setLoopR(0);
		// привязка изменения сопротивления проводника
		// к запуску проверки сопротивления контура
		loop.stream()
				.forEach(
						(sc) -> {
							sc.resistanceProperty()
									.addListener(
											(property, oldValue, newValue) -> setLoopResistence());
						});
		setLoopResistence();
	}

	public Loop(List<SchemElement> lp) {
		this();
		loop = lp;

		// привязка изменения сопротивления проводника
		// к запуску проверки сопротивления контура
		loop.stream()
				.forEach(
						(sc) -> {
							sc.resistanceProperty()
									.addListener(
											(property, oldValue, newValue) -> setLoopResistence());
						});
		setLoopResistence();
	}

	// -----------------------------------------------------

	public void startLoop() {
		atLoop.start();
	}

	public void stopLoop() {
		atLoop.stop();
	}

	public void addProvod(SchemElement provod) {
		if (provod != null
				&& loop.stream().allMatch((l) -> !l.equals(provod))
				&& loop.stream().allMatch(
						(sc) -> !sc.getVarName().equals(provod.getVarName()))) {
			loop.add(provod);
			setLoopResistence();
			// привязка изменения сопротивления проводника
			// к запуску проверки сопротивления контура
			provod.resistanceProperty().addListener(
					(property, oldValue, newValue) -> setLoopResistence());
		}
	}

	public void removeProvod(int index) {
		if (loop.size() > index && !loop.isEmpty())
			loop.remove(index);
		setLoopResistence();
	}

	public void removeProvod(SchemElement sc) {
		if (loop.contains(sc))
			loop.remove(sc);
		setLoopResistence();
	}

	public SchemElement getProvod(String varName) {
		SchemElement temp = null;
		for (SchemElement sc : loop) {
			if (sc.getVarName().equals(varName))
				temp = sc;
		}
		return temp;
	}

	/**
	 * @param flag
	 *            для подсветки контура в редакторе эл. цепей
	 */
	public void loopPodsvetka(int mod) {
		if (!loop.isEmpty())
			loop.stream().forEach((sc) -> sc.podsvetka(mod));
	}

	public List<SchemElement> getLoop() {
		return loop;
	}

	// определение и установка сопротивления контура
	public void setLoopResistence() {
		Optional<Double> res;
		res = loop.stream().filter((p) -> p.getResistance() > 0)
				.map((p) -> p.getResistance()).reduce((p1, p2) -> p1 + p2);
		if (getLoopR() != res.orElse(0.))
			setLoopR(res.orElse(0.).doubleValue());
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	// // определение сопротивления контура до подключенной плюсовой клеммы
	// тестера
	public double getResistenceBeforePlusClamp() {
		double rN = -1;
		if (isPlusClampConnect()) {
			boolean isCount = true;
			rN = 0;
			for (SchemElement sc : loop) {
				if (sc.getPlusMultConnect())
					isCount = !isCount;
				if (isCount) {
					rN += sc.getResistance();
					// System.out.println(sc.toString() + "-  "
					// + sc.getResistance());
				}
			}
		}
		System.out
				.println(rN
						+ "-  сопротивление контура до подключенной плюсовой клеммы тестера");
		return rN;
	}

	// // определение сопротивления контура после подключенной плюсовой клеммы
	// тестера
	public double getResistencePostPlusClamp() {
		double rN = -1;
		if (isPlusClampConnect()) {
			rN = 0;
			boolean isCount = false;
			for (SchemElement sc : loop) {
				if (sc.getPlusMultConnect())
					isCount = !isCount;

				if (isCount)
					rN += sc.getResistance();
			}
		}
		System.out
				.println(rN
						+ "-  сопротивление контура после подключенной плюсовой клеммы тестера");
		return rN;
	}

	/**
	 * Для ТЕСТА !!!! выводит в консоль список элементов контура
	 */
	public void printLoopList() {
		for (SchemElement sc : loop) {
			System.out.println("Элемент - " + sc.getVarName()
					+ ": Порядковый номер - " + loop.indexOf(sc));
		}
	}

	// // определение сопротивления контура после подключенной минусовой клеммы
	// тестера
	public double getResistencePostMinusClamp() {
		double rN = -1;
		if (isMinusClampConnect()) {
			rN = 0;
			boolean isCount = false;
			for (SchemElement sc : loop) {
				if (sc.getMinusMultConnect())
					isCount = !isCount;

				if (isCount)
					rN += sc.getResistance();

			}
		}
		System.out
				.println(rN
						+ "-  сопротивление контура после подключенной минусовой клеммы тестера");
		return rN;
	}

	public boolean isPlusClampConnect() {
		return loop.stream().anyMatch((sc) -> sc.getPlusMultConnect());
	}

	/**
	 * @return возвращает true если плюсовая клемма перед обрывом контура
	 *         (отсчет назад)
	 */
	public boolean isPlusClampConnectedBeforeObryv() {
		SchemElement sc = null;
		boolean count = true;
		if (isPlusClampConnect() && !getLoopConductance()) {
			for (SchemElement s : loop) {
				if (s.getIsConductance()) {
					sc = s;
					count = false;
				}
			}
			return (loop.indexOf(getElementByConnectedPlusClamp()) <= loop
					.indexOf(sc));
		}
		return false;
	}

	/**
	 * @return возвращает true если плюсовая или минусовая клемма крайняя по
	 *         сопротивлению к точке соединения контуров
	 * 
	 */
	public boolean isPlusClampEnd(boolean isPlusClamp) {
		if ((isPlusClamp) ? isPlusClampConnect() : isMinusClampConnect())
			return (isPlusClamp) ? (getResistenceBeforePlusClamp() == 0 || getResistencePostPlusClamp() == 0)
					: (getResistenceBeforeMinusClamp() == 0 || getResistencePostMinusClamp() == 0);
		else
			return false;
	}

	/**
	 * @return возвращает элемент контура к которому подключена плюсовая клемма
	 */
	public SchemElement getElementByConnectedPlusClamp() {
		SchemElement sc = null;
		if (isPlusClampConnect())
			for (SchemElement s : loop) {
				if (s.getPlusMultConnect()) {
					return s;
				}
			}
		return sc;
	}

	/**
	 * @return возвращает элемент контура к которому подключена минусовая клемма
	 */
	public SchemElement getElementByConnectedMinusClamp() {
		SchemElement sc = null;
		if (isMinusClampConnect())
			for (SchemElement s : loop) {
				if (s.getMinusMultConnect()) {
					return s;
				}
			}
		return sc;
	}

	public boolean isMinusClampConnect() {
		return loop.stream().anyMatch((sc) -> sc.getMinusMultConnect());
	}

	// подключены ли обе клеммы к контуру
	public boolean isAllClampConnect() {
		return (isMinusClampConnect() && isPlusClampConnect());
	}

	// подключена ли какая-нибудь клемма к контуру
	public boolean isAnyClampConnect() {
		return isMinusClampConnect() || isPlusClampConnect();
	}

	/**
	 * возвращает сопротивление контура до и после подключенных клемм при
	 * подключении обеих клемм к контуру
	 */
	public double getResByConnectedAllClamp() {
		double resLoop = 0;
		boolean isCount = true;
		for (SchemElement sc : loop) {
			if (sc.isAnyClampConnect())
				isCount = !isCount;
			if (isCount) {
				resLoop += sc.getResistance();
				// System.out.println(sc.toString() + "-  "
				// + sc.getResistance());
			}
		}

		return resLoop;
	}

	// возвращает null или первый элемент контура если он полюс
	public PowerSourceTerminal getStartTerminal() {
		PowerSourceTerminal pst = null;
		if (loop.get(0).getClass().equals(PowerSourceTerminal.class)
				|| loop.get(0).getClass().equals(PowerSourceTerminalDCM.class))
			return (PowerSourceTerminal) loop.get(0);
		return pst;
	}

	// возвращает последний элемент контура если он полюс или null ???? пока нет
	public PowerSourceTerminal getEndTerminal() {
		PowerSourceTerminal pst = null;
		int i = loop.size() - 1;

		if (loop.get(i).getClass().equals(PowerSourceTerminal.class)
				|| loop.get(i).getClass().equals(PowerSourceTerminalDCM.class))
			return (PowerSourceTerminal) loop.get(i);

		return pst;
	}

	public boolean isTerminalConteined() {
		return loop.stream().anyMatch(
				(sc) -> sc.getClass().equals(PowerSourceTerminal.class)
						|| sc.getClass().equals(PowerSourceTerminalDCM.class));
	}

	public boolean isTerminalFirst() {
		return (loop.get(0).getClass().equals(PowerSourceTerminal.class) || loop
				.get(0).getClass().equals(PowerSourceTerminalDCM.class)) ? true
				: false;
	}

	@Override
	public String toString() {
		return varName;
	}

	// // определение сопротивления до подключенной минусовой клеммы
	// тестера

	public double getResistenceBeforeMinusClamp() {
		double rN = -1;
		if (isMinusClampConnect()) {
			boolean isCount = true;
			rN = 0;
			for (SchemElement sc : loop) {
				if (sc.getMinusMultConnect())
					isCount = !isCount;
				if (isCount) {
					rN += sc.getResistance();
					// System.out.println(sc.toString() + "-  "
					// + sc.getResistance());
				}
			}
		}
		System.out
				.println(rN
						+ "- сопротивление контура до подключенной минусовой клеммы тестера");
		return rN;
	}
}
