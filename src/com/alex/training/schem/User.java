﻿/**
 * 
 */
package com.alex.training.schem;


/**
 * @author Alex
 * интерфейс который будут реализовывать потребители
 * электрической цепи (реле, лампочки, и.т.д.)
 *
 */

public interface User {
	/**
	 * @param flag
	 * установка и снятие реле под ток
	 */
	public void setCurrent(boolean flag);
	public void setTok(double i);


}
