﻿package com.alex.training.schem;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javafx.animation.AnimationTimer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;

import com.alex.training.schemElement.PowerSourceTerminal;
import com.alex.training.schemElement.SchemElement;
import com.sun.javafx.image.impl.ByteIndexed.Getter;

/**
 * @author Alex класс представляющий электрическую цепь circuit - список
 *         последовательных соединений в цепи потребителя (не параллельных!)
 * 
 *         users- потребители в цепи могут быть включены только последовательно
 *         ????
 */
public class ElCircuit {

	private List<ParUnion> circuit = new ArrayList<ParUnion>();
	private List<User> users = new ArrayList<User>();
	// private PowerSource source;
	private Source source;
	private String varName;
	private double minBrTok = 0;
	private double cirTok = 0;
	// альтернативные ветви по которым пользователь может быть поставлен под ток
	// например цепи самоблокировки
	private List<ElCircuit> branches = new ArrayList<ElCircuit>();
	private boolean isBranch;

	private double resistenceBranch;

	// свойство что через всех пользователей протекает ток
	// ------------------------------------------------
	public BooleanProperty userSuccess = new SimpleBooleanProperty();

	public final BooleanProperty userSuccessProperty() {
		return userSuccess;
	}

	public final void setUserSuccess(boolean newValue) {
		userSuccess.set(newValue);
	}

	public final boolean getUserSuccess() {
		return userSuccess.get();
	}

	// сопротивление эл.цепи
	// ---------------------------------------------------
	public DoubleProperty circuitR = new SimpleDoubleProperty();

	public final DoubleProperty circuitRProperty() {
		return circuitR;
	}

	public final void setCircuitR(double newValue) {
		circuitR.set(newValue);
	}

	public final double getCircuitR() {
		return circuitR.get();
	}

	// ----------------------------------------------------------------

	// таймер для проверки состояния цепи
	final private AnimationTimer atCircuit = new AnimationTimer() {

		long count = 0;
		boolean flag = false;

		@Override
		public void handle(long arg0) {
			// if (count % 6 == 0) {
			flag = (IsConducted() || IsBranchesConducted());
			if (flag) {
				cirTok = source.getEds() / (getCircuitR() + source.getInR());
				minBrTok = source.getEds()
						/ (getBranchesMinR() + source.getInR());
			} else {
				cirTok = 0;
				minBrTok = 0;
			}

			if (getUserSuccess() != flag)
				setUserSuccess(flag);

			// }
			count++;
		}

	};

	public ElCircuit() {
		// установка сопротивления эл. цепи
		setElCircuitResistence();
		// цепь не является ветвью
		setBranch(false);

		// постановка и снятие всех пользователей под ток
		userSuccessProperty().addListener((property, oldValue, newValue) -> {
			setCurrentUsers(newValue.booleanValue());
		});

		circuitRProperty().addListener((property, oldValue, newValue) -> {
			setCurrentUsers(getUserSuccess());
		});

	}

	// конструктор
	public ElCircuit(List<ParUnion> cir, List<User> us) {
		this();
		circuit = cir;
		users = us;
	}

	// конструктор
	public ElCircuit(List<ParUnion> cir, List<User> us, Source s) {
		this(cir, us);
		source = s;

	}

	public boolean IsConducted() {
		return circuit.stream().allMatch((u) -> u.getUnionConductance());
	}

	public boolean IsBranchesConducted() {
		if (!branches.isEmpty()) {
			return branches.stream().anyMatch((c) -> c.IsConducted());
		} else
			return false;
	}

	public double getBranchesMinR() {
		resistenceBranch = getCircuitR();
		// if (!IsConducted()) {
		if (!branches.isEmpty()) {
			branches.forEach((br) -> {
				if (br.IsConducted()) {
					if (br.getCircuitR() < resistenceBranch)
						resistenceBranch = br.getCircuitR();
				}
			});
		}
		// }
		return resistenceBranch;
	}

	public void elCircuitStart() {
		if (!circuit.isEmpty()) {
			for (ParUnion union : circuit) {
				union.startUnion();
			}
			atCircuit.start();
		}

	}

	public void elCircuitStop() {
		atCircuit.stop();

		if (!circuit.isEmpty()) {
			for (ParUnion union : circuit) {
				union.stopUnion();
			}
		}
	}

	/**
	 * @return список соединений через которые не протекает ток
	 */
	public List<ParUnion> getUnionsNotConducted() {
		if (!circuit.isEmpty()) {
			return circuit.stream().filter((u) -> !u.getUnionConductance())
					.collect(Collectors.toList());
		} else
			return null;
	}

	public List<ParUnion> getCircuit() {
		return circuit;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public void addUser(User user) {
		if (user != null && users.stream().allMatch((u) -> !u.equals(user)))
			users.add(user);
	}

	public SchemElement getUser(String varName) {
		SchemElement temp = null;
		for (User sc : users) {
			if (((SchemElement) sc).getVarName().equals(varName))
				temp = (SchemElement) sc;
		}
		return temp;
	}

	public Source getSource() {
		return source;
	}

	public void setSource(Source source) {
		this.source = source;
	}

	// постановка и снятие пользователей под ток
	private void setCurrentUsers(boolean f) {
		// расчет тока в эл.цепи
		if (f) {
			cirTok = source.getEds() / (getCircuitR() + source.getInR());
			minBrTok = source.getEds() / (getBranchesMinR() + source.getInR());
		} else {
			cirTok = 0;
			minBrTok = 0;
		}
		if (!users.isEmpty() && !isBranch()) {
			users.forEach((user) -> {
				user.setTok(minBrTok);
				user.setCurrent(f);
			});
		}
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	// добавление соединения в элю цепь при условии что его еще нет
	public void addUnion(ParUnion union) {
		if (union != null && circuit.stream().allMatch((l) -> !l.equals(union))) {
			circuit.add(union);
			setElCircuitResistence();
		}
		if (!union.getUnion().isEmpty()) {
			union.unionRProperty().addListener(
					(property, oldValue, newValue) -> setElCircuitResistence());
		}
	}

	// добпвление ветви в цепь при условии что её еще нет
	public void addBranch(ElCircuit cir) {
		if (cir.isBranch() && branches.stream().allMatch((c) -> !c.equals(cir))) {
			branches.add(cir);
			// постановка пользователей под ток если цепь является ветвью
			cir.circuitRProperty().addListener(
					(property, oldValue, newValue) -> {
						setCurrentUsers(getUserSuccess());
					});

		}

	}

	public boolean isBranch() {
		return isBranch;
	}

	public void setBranch(boolean isBranch) {
		this.isBranch = isBranch;
	}

	public List<ElCircuit> getBranches() {
		return branches;
	}

	public void setElCircuitResistence() {
		Optional<Double> res;
		Double result;
		res = circuit.stream().filter((c) -> c.getUnionR() > 0)
				.map((c) -> c.getUnionR()).reduce((c1, c2) -> c1 + c2);
		result = (source != null) ? source.getInR() + res.orElse(0.) : 0 + res
				.orElse(0.);
		if (getCircuitR() != result)
			setCircuitR(result);
		// System.out.println("R circuit " + getVarName() + "- " + result);
	}

	/**
	 * @param flag
	 *            для подсветки в редакторе эл. цепей
	 */
	public void circuitPodsvetka(int mod) {
		if (!circuit.isEmpty())
			circuit.stream().forEach((u) -> u.unionPodsvetka(mod));
	}

	public boolean isPlusClampCircuitConnect() {
		return circuit.stream().anyMatch((u) -> u.isPlusClampUnionConnect());
	}

	public boolean isMinusClampCircuitConnect() {
		return circuit.stream().anyMatch((u) -> u.isMinusClampUnionConnect());
	}

	// // определение сопротивления цепи до подключенной плюсовой клеммы тестера
	public double getCircuitResistenceBeforePlusClamp() {
		double rN = 0;
		if (isPlusClampCircuitConnect()) {
			rN = 0;
			for (ParUnion union : circuit) {
				if (union.isPlusClampUnionConnect()) {
					// если соединение последнее в цепи
					if (circuit.indexOf(union) == circuit.size() - 1) {

						rN += (isLastLoopReverse()) ? union
								.getUnionResistencePostPlusClamp() : union
								.getUnionResistenceBeforePlusClamp();
						break;

					} else {
						rN += union.getUnionResistenceBeforePlusClamp();
						break;

					}
				} else {
					rN += union.getUnionR();
				}
			}
		}
		System.out
				.println(rN
						+ "- сопротивление цепи до подключенной плюсовой клеммы тестера");
		return rN;
	}

	// // определение сопротивления цепи до подключенной минусовой клеммы
	// тестера
	public double getCircuitResistenceBeforeMinusClamp() {
		double rN = 0;
		if (isMinusClampCircuitConnect()) {
			rN = 0;
			for (ParUnion union : circuit) {
				if (union.isMinusClampUnionConnect()) {
					if (circuit.indexOf(union) == circuit.size() - 1) {

						rN += (isLastLoopReverse()) ? union
								.getUnionResistencePostMinusClamp() : union
								.getUnionResistenceBeforeMinusClamp();
						break;

					} else {
						rN += union.getUnionResistenceBeforeMinusClamp();
						break;

					}
				} else {
					rN += union.getUnionR();
				}
			}
		}
		System.out
				.println(rN
						+ "- сопротивление цепи до подключенной минусовой клеммы тестера");
		return rN;
	}

	/**
	 * Проверяет есть ли соединение к которму подключены обе клеммы
	 */
	public boolean isAllClampUnionConnect() {
		return circuit.stream().anyMatch(
				(u) -> u.isMinusClampUnionConnect()
						&& u.isPlusClampUnionConnect());
	}

	/**
	 * возвращает соединение к которому подключены обе клеммы или null
	 */
	public ParUnion getUnionByConnectedAllClamp() {
		for (ParUnion u : circuit) {
			if (u.isAllClampConnect())
				return u;
		}
		return null;
	}

	/**
	 * возвращает соединение к которому подключена плюсовая клемма или null
	 */
	public ParUnion getUnionByConnectedPlusClamp() {
		for (ParUnion u : circuit) {
			if (u.isPlusClampUnionConnect())
				return u;
		}
		return null;
	}

	/**
	 * определяет взаимное расположение клемм при подключении обеих клемм к цепи
	 * true - плюсовая клемма первая (отсчет назад)
	 */
	public boolean isPlusClampFirst() {
		ParUnion un = null;
		Loop lp = null;
		// при подключении клемм в разные соединения
		if (!isAllClampUnionConnect())
			return (circuit.indexOf(getUnionByConnectedPlusClamp()) < circuit
					.indexOf(getUnionByConnectedMinusClamp()));
		// при подключении клемм в одно соединение
		else {
			un = getUnionByConnectedAllClamp();
			lp = un.getLoopByConnectedAllClamp();
			// если клеммы подключены в один контур
			if (lp != null) {
				return (lp.getLoop().indexOf(
						lp.getElementByConnectedPlusClamp()) < lp.getLoop()
						.indexOf(lp.getElementByConnectedMinusClamp()));

			}
			// если клеммы подключены в разные контура
			else {
				// если через соединение не проходит ток
				if (!un.getUnionConductance()) {
					lp = un.getLoopByConnectedClamp(true);
					return lp.isPlusClampConnectedBeforeObryv();
				} else
					return false;
			}
		}

	}

	/**
	 * возвращает соединение к которому подключена минусовая клемма или null
	 */
	public ParUnion getUnionByConnectedMinusClamp() {
		for (ParUnion u : circuit) {
			if (u.isMinusClampUnionConnect())
				return u;
		}
		return null;
	}

	/**
	 * Для ТЕСТА !!!! выводит в консоль список соединений цепи
	 */
	public void printUnionList() {
		for (ParUnion u : circuit) {
			System.out.println("Соединение - " + u.getVarName()
					+ ": Порядковый номер - " + circuit.indexOf(u)
					+ "; Сопротивление -  " + u.getUnionR());
		}
	}

	/**
	 * возвращает сопротивление цепи до и после подключенных клемм при
	 * подключении обеих клемм к цепи
	 */
	public double getResByConnectedAllClamp() {
		double resCir = 0;

		resCir = (isPlusClampFirst()) ? getCircuitResistenceBeforePlusClamp()
				+ getCircuitResistencePostMinusClamp()
				: getCircuitResistenceBeforeMinusClamp()
						+ getCircuitResistencePostPlusClamp();

		System.out
				.println(" Сопротивление цепи до и после подключенных клемм = "
						+ resCir);

		return resCir;
	}

	// // определение сопротивления цепи после подключенной плюсовой клеммы
	// тестера
	public double getCircuitResistencePostPlusClamp() {
		double rN = 0;
		boolean flagConnect = false;
		if (isPlusClampCircuitConnect()) {
			for (ParUnion union : circuit) {
				if (flagConnect) {
					rN += union.getUnionR();
				}
				if (union.isPlusClampUnionConnect()) {
					if (circuit.indexOf(union) == circuit.size() - 1) {

						rN += (isLastLoopReverse()) ? union
								.getUnionResistenceBeforePlusClamp() : union
								.getUnionResistencePostPlusClamp();
					} else {
						rN += union.getUnionResistencePostPlusClamp();
					}
					flagConnect = true;
				}
			}
		}

		System.out
				.println(rN
						+ " - сопротивление цепи после подключенной плюсовой клеммы тестера - ");
		return rN;
	}

	// // определение сопротивления цепи после подключенной минусовой клеммы
	// тестера
	public double getCircuitResistencePostMinusClamp() {
		double rN = -1;
		boolean flagConnect = false;
		if (isMinusClampCircuitConnect()) {
			rN = 0;
			for (ParUnion union : circuit) {
				if (flagConnect) {
					rN += union.getUnionR();
				}
				if (union.isMinusClampUnionConnect()) {
					if (circuit.indexOf(union) == circuit.size() - 1) {

						rN += (isLastLoopReverse()) ? union
								.getUnionResistenceBeforeMinusClamp() : union
								.getUnionResistencePostMinusClamp();
					} else {
						rN += union.getUnionResistencePostMinusClamp();
					}
					flagConnect = true;
				}

			}
		}

		System.out
				.println(rN
						+ "- сопротивление цепи после подключенной минусовой клеммы тестера");
		return rN;
	}

	/**
	 * @return возвращает минимальное сопротивление от полюса до подключенной
	 *         плюсовой клеммы
	 */
	public double getMinResistancePlusClamp() {
		double res = -1;
		res = Double.min(getCircuitResistenceBeforePlusClamp(),
				getCircuitResistencePostPlusClamp());
		System.out
				.println(res
						+ "- минимальное сопротивление от полюса до подключенной плюсовой клеммы тестера");
		return res;
	}

	/**
	 * @return возвращает минимальное сопротивление от полюса до подключенной
	 *         минусовой клеммы
	 */
	public double getMinResistanceMinusClamp() {
		double res = -1;
		res = Double.min(getCircuitResistenceBeforeMinusClamp(),
				getCircuitResistencePostMinusClamp());
		System.out
				.println(res
						+ "- минимальное сопротивление от полюса до подключенной минусовой клеммы тестера");
		return res;
	}

	/**
	 * @return возвращает ближайший по сопротивлению полюс к подключенной
	 *         плюсовой клемме
	 */
	public PowerSourceTerminal getPlusClampMinRTerminal() {
		if (getMinResistancePlusClamp() == -1)
			return null;
		PowerSourceTerminal pst = getTerminalPostClamp();
		if (getCircuitResistenceBeforePlusClamp() <= getCircuitResistencePostPlusClamp()) {
			pst = getTerminalBeforeClamp();
		}
		return pst;
	}

	/**
	 * @return возвращает ближайший по сопротивлению полюс к подключенной
	 *         минусовой клемме
	 */
	public PowerSourceTerminal getMinusClampMinRTerminal() {
		if (getMinResistanceMinusClamp() == -1)
			return null;
		PowerSourceTerminal pst = getTerminalPostClamp();
		if (getCircuitResistenceBeforeMinusClamp() <= getCircuitResistencePostMinusClamp()) {
			pst = getTerminalBeforeClamp();
		}
		return pst;
	}

	/**
	 * @param us
	 * @return проверяет является ли конкретный пользователь пользователем р.ц
	 */
	public boolean isUserConteined(User us) {
		return getUsers().contains(us);

	}

	/**
	 *  
	 */
	public PowerSourceTerminal getTerminalBeforeClamp() {
		PowerSourceTerminal pst = null;
		pst = circuit.get(0).getStartTerminal();
		return pst;
	}

	/**
	 * @return проверяет перевернутый ли последний контур
	 */
	public boolean isLastLoopReverse() {
		if (circuit.size() > 1)
			return circuit.get(circuit.size() - 1).isTerminalsLoopReverse();
		else
			return false;
	}

	public PowerSourceTerminal getTerminalPostClamp() {
		PowerSourceTerminal pst = null;
		pst = circuit.get(circuit.size() - 1).getEndTerminal();
		if (pst == null)
			pst = circuit.get(circuit.size() - 1).getEndReverseTerminal();

		return pst;
	}

	public double getMinBrTok() {
		return minBrTok;
	}

	public double getCirTok() {
		return cirTok;
	}

	@Override
	public String toString() {
		return varName;

	}

	/**
	 * @return возвращает поный список элементов цепи
	 */
	public List<SchemElement> getAllElements() {
		List<SchemElement> list = new ArrayList<SchemElement>();
		circuit.forEach((un) -> {
			un.getUnion().forEach((lp) -> {
				list.addAll(lp.getLoop());
			});
		});
		return list;
	}

}
