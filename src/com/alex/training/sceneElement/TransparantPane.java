﻿package com.alex.training.sceneElement;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;

import com.alex.training.Main;
import com.alex.training.stationElement.TopTransparant;

public class TransparantPane extends ToolBar {

	private TopTransparant ts;
	private TopTransparant ty;
	private TopTransparant nagr;
	private TopTransparant station;
	private TopTransparant sound;

	public TransparantPane() {
		super();

		// установка размеров панели -------------------------

		setMinWidth(Screen.getPrimary().getBounds().getWidth());
		setOrientation(Orientation.HORIZONTAL);
		setPadding(new Insets(8, 70, 8, 70));

		Region space[] = new Region[5];
		for (int i = 0; i < 5; i++) {
			space[i] = new Region();
			space[i].setMinSize(160, 50);
		}

		Text t;
		ts = new TopTransparant("Тc", true);
		ty = new TopTransparant("Ту", false);

		// транспарант Нагрузка
		nagr = new TopTransparant("Нагрузка", false);
		t = nagr.getTx();
		t.setFont(Font.font("sans", FontWeight.BOLD, 16));
		t.setX(1 + nagr.getOuter().getWidth() / 2
				- t.getBoundsInLocal().getWidth() / 2);

		// транспарант станция
		station = new TopTransparant("пл.Ленина", false);
		t = station.getTx();
		t.setFill(Color.YELLOW);
		t.setFont(Font.font("sans", FontWeight.BOLD, 24));
		station.getInner().setWidth(t.getBoundsInLocal().getWidth() + 10);
		station.getOuter().setWidth(t.getBoundsInLocal().getWidth() + 20);
		t.setX(1 + station.getOuter().getWidth() / 2
				- t.getBoundsInLocal().getWidth() / 2);

		sound = new TopTransparant("Звук выкл.", true);
		t = sound.getTx();
		t.setFont(Font.font("sans", FontWeight.BOLD, 16));
		t.setX(1 + sound.getOuter().getWidth() / 2
				- t.getBoundsInLocal().getWidth() / 2);

		Image icon = new Image(Main.class.getResource("res/img/alexTM.jpg")
				.toExternalForm(), false);

		ImageView im = new ImageView(icon);
		im.setFitWidth(60);
		im.setFitHeight(50);
//		im.setPreserveRatio(true);

		getItems().addAll(ts, space[0], ty, space[1], nagr, space[2], station,
				space[3], sound, space[4], im);

	}

	public TopTransparant getTs() {
		return ts;
	}

	public TopTransparant getTy() {
		return ty;
	}

	public TopTransparant getNagr() {
		return nagr;
	}

	public TopTransparant getSound() {
		return sound;
	}

}
