﻿/**
 * 
 */
package com.alex.training.sceneElement;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Node;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;

import com.alex.training.schemElement.Contact;
import com.alex.training.schemElement.FrontContact;
import com.alex.training.schemElement.FrontRightContact;
import com.alex.training.schemElement.PolarContact;
import com.alex.training.schemElement.PowerSourceTerminalDCM;
import com.alex.training.schemElement.Rele;
import com.alex.training.schemElement.Troynic;
import com.alex.training.schemElement.TroynicAutostop;
import com.alex.training.schemElement.TroynicInterface;
import com.alex.training.schemElement.TroynicRight;
import com.alex.training.schemElement.TylContact;
import com.alex.training.schemElement.TylRightContact;

/**
 * @author Alex
 *
 */
public class SchemContextMenu extends ContextMenu {

	private Menu menuTester;
	private MenuItem minusCl, plusCl, hideMult;
	private MenuItem menuItemReleInfo;
	private MenuItem menuReleCalcVolt;
	private MenuItem menuZamenaRele;
	private MenuItem menuContactCalcVolt;
	private MenuItem menuContactResistance;
	private CheckMenuItem menuContactSetPerem;
	private Menu menuRele;
	private Menu menuContact;
	private Menu menuTroynic;
	private Menu menuTroynicFront;
	private Menu menuTroynicTyl;
	private MenuItem menuTroynicFrontCalcVolt;
	private MenuItem menuTroynicFrontResistance;
	private CheckMenuItem menuTroynicFrontSetPerem;
	private MenuItem menuTroynicTylCalcVolt;
	private MenuItem menuTroynicTylResistance;
	private CheckMenuItem menuTroynicTylSetPerem;
	private CheckMenuItem menuContactSetupResistance;
	private CheckMenuItem menuTroynicFrontSetupResistance;
	private CheckMenuItem menuTroynicTylSetupResistance;
	private MenuItem menuContactZamenaRele;
	private MenuItem menuTroynicZamenaRele;
	private MenuItem menuViewMontag;

	// свойство администратор ли пользователь
	public BooleanProperty isAdmin = new SimpleBooleanProperty();
	private Menu menuTerminalDCM;
	private CheckMenuItem menuAddPlusDCM;
	private Menu menuContactSetupError;
	private CheckMenuItem menuContactSetupSoldering;

	public final BooleanProperty isAdminProperty() {
		return isAdmin;
	}

	public final void setIsAdmin(boolean newValue) {
		isAdmin.set(newValue);
	}

	public final boolean getIsAdmin() {
		return isAdmin.get();
	}

	public SchemContextMenu() {
		super();
		setIsAdmin(false);
		// тестер для поля схемы --------
		menuTester = new Menu("Подключить мультиметр");
		hideMult = new MenuItem("Отключить мультиметр");

		getItems().addAll(menuTester, hideMult);
		plusCl = new MenuItem("Подключить плюсовой провод");
		minusCl = new MenuItem("Подключить минусовой провод");
		menuTester.getItems().addAll(plusCl, minusCl);

		// меню осмотра монтажа для всех элементов кроме поля схемы
		menuViewMontag = new MenuItem("Осмотреть монтаж");
		menuRele = new Menu("Реле");
		menuTerminalDCM = new Menu("Полюс ДЦМ");
		getItems().addAll(menuViewMontag, menuRele, menuTerminalDCM);
		// инфо для реле --------
		menuItemReleInfo = new MenuItem("Осмотреть реле");

		// измерение напряжения на реле
		menuReleCalcVolt = new MenuItem("Измерить напряжение на реле");

		// замена реле
		menuZamenaRele = new MenuItem("Заменить реле");
		// menuRele.getItems().addAll(menuItemReleInfo, menuReleCalcVolt,
		// menuZamenaRele);
		menuRele.getItems().addAll(menuItemReleInfo);

		// ---------------------------------------
		menuContact = new Menu("Контакт");
		getItems().addAll(menuContact);

		// измерение переходного сопротивления контакта
		menuContactCalcVolt = new MenuItem("Измерить напряжение на контакте");

		// сопротивления контакта
		menuContactResistance = new MenuItem("Сопротивление контакта");

		// установка ошибки контакта -----------------------------
		menuContactSetupError = new Menu("Установить ошибку контакта");

		// установка сопротивления контакта
		menuContactSetupResistance = new CheckMenuItem(
				"Установить повышенное сопротивление контакта");
		menuContactSetupResistance.setSelected(false);
		// установка холодной пайки контакта
		menuContactSetupSoldering = new CheckMenuItem(
				"Установить холодную пайку контакта");
		menuContactSetupResistance.setSelected(false);

		menuContactSetupError.getItems().addAll(menuContactSetupResistance,
				menuContactSetupSoldering);
		// -----------------------------------------------------------------
		// установить перемычку контакта
		menuContactSetPerem = new CheckMenuItem(
				"Установить перемычку на контакт");

		// замена реле
		menuContactZamenaRele = new MenuItem("Заменить реле");

		menuContact.getItems().addAll(menuContactCalcVolt,
				menuContactSetupError, menuContactResistance,
				menuContactZamenaRele);
		// ,menuContactSetPerem);

		// menuContact.getItems().addAll(menuContactSetPerem);
		// ----------------------------------

		menuTroynic = new Menu("Тройник");
		getItems().addAll(menuTroynic);

		menuTroynicFront = new Menu("Фронтовой контакт");

		menuTroynicTyl = new Menu("Тыловой контакт");

		// замена реле
		menuTroynicZamenaRele = new MenuItem("Заменить реле");

		// menuTroynic.getItems().addAll(menuTroynicFront, menuTroynicTyl,
		// menuTroynicZamenaRele);
		menuTroynic.getItems().addAll(menuTroynicFront, menuTroynicTyl);

		// измерение переходного сопротивления контакта
		menuTroynicFrontCalcVolt = new MenuItem(
				"Измерить напряжение на контакте");

		// сопротивления контакта
		menuTroynicFrontResistance = new MenuItem("Сопротивление контакта");

		// установка сопротивления контакта
		menuTroynicFrontSetupResistance = new CheckMenuItem(
				"Установить ошибку контакта");

		// установить перемычку контакта
		menuTroynicFrontSetPerem = new CheckMenuItem(
				"Установить перемычку на контакт");

		// menuTroynicFront.getItems().addAll(menuTroynicFrontSetPerem);
		menuTroynicFront.getItems().addAll(menuTroynicFrontCalcVolt,
				menuTroynicFrontSetupResistance, menuTroynicFrontResistance);
		// ,menuTroynicFrontSetPerem);

		// измерение переходного сопротивления контакта
		menuTroynicTylCalcVolt = new MenuItem("Измерить напряжение на контакте");

		// сопротивления контакта
		menuTroynicTylResistance = new MenuItem("Сопротивление контакта");

		// установка сопротивления контакта
		menuTroynicTylSetupResistance = new CheckMenuItem(
				"Установить ошибку контакта");
		// установить перемычку контакта
		menuTroynicTylSetPerem = new CheckMenuItem(
				"Установить перемычку на контакт");
		// для отладки
		menuTroynicTyl.getItems().addAll(menuTroynicTylCalcVolt,
				menuTroynicTylSetupResistance, menuTroynicTylResistance);
		// ,menuTroynicTylSetPerem);
		// menuTroynicTyl.getItems().addAll(menuTroynicTylSetPerem);

		// подача плюса в полюс ДЦМ
		menuAddPlusDCM = new CheckMenuItem("Подать плюс в полюс");

		menuTerminalDCM.getItems().addAll(menuAddPlusDCM);

		// установка пунктов меню в зависимости от роли пользователя
		isAdminProperty().addListener((property, oldValue, newValue) -> {
			setMenuForAdmin(newValue.booleanValue());
		});

		setMenuForAdmin(getIsAdmin());

	} // конструктор

	public Menu getMenuTester() {
		return menuTester;
	}

	public Menu getMenuRele() {
		return menuRele;
	}

	public Menu getMenuContact() {
		return menuContact;
	}

	public Menu getMenuTroynic() {
		return menuTroynic;
	}

	public Menu getMenuTroynicFront() {
		return menuTroynicFront;
	}

	public Menu getMenuTroynicTyl() {
		return menuTroynicTyl;
	}

	public Menu getMenuTerminalDCM() {
		return menuTerminalDCM;
	}

	public Menu getMenuContactSetupError() {
		return menuContactSetupError;
	}

	public void setMenuContactZamenaRele(MenuItem menuContactZamenaRele) {
		this.menuContactZamenaRele = menuContactZamenaRele;
	}

	public void setMenuForAdmin(boolean flag) {
		// тройник тыл
		menuTroynicTylSetupResistance.setVisible(flag);
		menuTroynicTylResistance.setVisible(flag);
		menuTroynicTylCalcVolt.setVisible(flag);
		// тройник фронт
		menuTroynicFrontSetupResistance.setVisible(flag);
		menuTroynicFrontResistance.setVisible(flag);
		menuTroynicFrontCalcVolt.setVisible(flag);

		// контакт
		menuContactSetupError.setVisible(flag);
		menuContactResistance.setVisible(flag);
		menuContactCalcVolt.setVisible(flag);

	}

	public CheckMenuItem getMenuContactSetupResistance() {
		return menuContactSetupResistance;
	}

	public CheckMenuItem getMenuTroynicFrontSetupResistance() {
		return menuTroynicFrontSetupResistance;
	}

	public CheckMenuItem getMenuTroynicTylSetupResistance() {
		return menuTroynicTylSetupResistance;
	}

	public MenuItem getMinusCl() {
		return minusCl;
	}

	public MenuItem getPlusCl() {
		return plusCl;
	}

	public Menu getMenuItemTester() {
		return menuTester;
	}

	public MenuItem getHideMult() {
		return hideMult;
	}

	public MenuItem getMenuItemReleInfo() {
		return menuItemReleInfo;
	}

	public MenuItem getMenuReleCalcVolt() {
		return menuReleCalcVolt;
	}

	public MenuItem getMenuZamenaRele() {
		return menuZamenaRele;
	}

	public MenuItem getMenuContactCalcVolt() {
		return menuContactCalcVolt;
	}

	public CheckMenuItem getMenuContactSetupSoldering() {
		return menuContactSetupSoldering;
	}

	public MenuItem getMenuContactResistance() {
		return menuContactResistance;
	}

	public CheckMenuItem getMenuContactSetPerem() {
		return menuContactSetPerem;
	}

	public MenuItem getMenuTroynicFrontCalcVolt() {
		return menuTroynicFrontCalcVolt;
	}

	public MenuItem getMenuTroynicFrontResistance() {
		return menuTroynicFrontResistance;
	}

	public CheckMenuItem getMenuTroynicFrontSetPerem() {
		return menuTroynicFrontSetPerem;
	}

	public MenuItem getMenuTroynicTylCalcVolt() {
		return menuTroynicTylCalcVolt;
	}

	public MenuItem getMenuTroynicTylResistance() {
		return menuTroynicTylResistance;
	}

	public CheckMenuItem getMenuTroynicTylSetPerem() {
		return menuTroynicTylSetPerem;
	}

	public MenuItem getMenuTroynicZamenaRele() {
		return menuTroynicZamenaRele;
	}

	public MenuItem getMenuContactZamenaRele() {
		return menuContactZamenaRele;
	}

	public MenuItem getMenuViewMontag() {
		return menuViewMontag;
	}

	public CheckMenuItem getMenuAddPlusDCM() {
		return menuAddPlusDCM;
	}

	public void showMenu(Node gr, Node anchor, double x, double y) {
		if (!isShowing()) {
			disableAllMenu();
			// для реле
			if (gr.getClass().getSuperclass().equals(Rele.class)) {
				menuRele.setDisable(false);
			}
			// для тыловых и фронтовых контактов
			if (gr.getClass().equals(FrontContact.class)
					|| gr.getClass().equals(FrontRightContact.class)
					|| gr.getClass().equals(TylRightContact.class)
					|| gr.getClass().equals(TylContact.class)) {
				menuContact.setDisable(false);
				getMenuContactSetPerem().setSelected(
						((Contact) (gr)).getIsJumperSet());
				getMenuContactSetupResistance().setSelected(
						((Contact) (gr)).getIsError()
								&& ((Contact) (gr)).getErrorMod() == 0);
			}
			// для тройников
			if (gr.getClass().equals(Troynic.class)
					|| gr.getClass().equals(TroynicRight.class)
					|| gr.getClass().equals(TroynicAutostop.class)
					|| gr.getClass().equals(PolarContact.class)) {
				menuTroynic.setDisable(false);
				getMenuTroynicFrontSetPerem().setSelected(
						((Contact) (gr)).getIsJumperSet());
				getMenuTroynicTylSetPerem().setSelected(
						((TroynicInterface) (gr)).getTylTr().getIsJumperSet());
				getMenuTroynicFrontSetupResistance().setSelected(
						((Contact) (gr)).getIsError()
								&& ((Contact) (gr)).getErrorMod() == 0);
				getMenuTroynicTylSetupResistance().setSelected(
						((TroynicInterface) (gr)).getTylTr().getIsError()
								&& ((Contact) (gr)).getErrorMod() == 0);
			}
			// для полюса ДЦМ
			if (gr.getClass().equals(PowerSourceTerminalDCM.class)) {
				menuTerminalDCM.setDisable(false);
				menuAddPlusDCM.setSelected((((PowerSourceTerminalDCM) gr)
						.getIsConductance()) ? true : false);
			}
			// для поля схемы
			if (gr.getClass().equals(AnchorPane.class)) {
				menuTester.setDisable(false);
				hideMult.setDisable(false);
			} else {
				menuViewMontag.setDisable(false);
			}
			show(anchor, x, y);
		}
	}

	private void disableAllMenu() {
		menuTester.setDisable(true);
		menuViewMontag.setDisable(true);
		hideMult.setDisable(true);
		menuRele.setDisable(true);
		menuContact.setDisable(true);
		menuTroynic.setDisable(true);
		menuTerminalDCM.setDisable(true);
	}
}
