﻿package com.alex.training.sceneElement;

import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Screen;

public class ButtonPane extends ToolBar {

    private	ToggleButton btnInf; 
    private	ToggleButton btnDiagnostics; 

	public ButtonPane() {
		super();
		// установка размеров панели -------------------------
		setMinWidth(Screen.getPrimary().getBounds().getWidth());
		setOrientation(Orientation.HORIZONTAL);
		setPadding(new Insets(8));
		
		// кнопка ИНФ/ТУ -----------------------------------
		btnInf = new ToggleButton("ИНФ/ТУ");
		btnInf.setFont(Font.font("sans", 18));
		
		btnInf.selectedProperty().addListener((property, oldValue, newValue) ->{if(newValue.equals(Boolean.TRUE)) 
			btnInf.setTextFill(Color.WHITE);
		else btnInf.setTextFill(Color.BLACK);});

		//  -----------------------------------
		    Region spacer = new Region();
	        spacer.setMinWidth(50);
		
		// кнопка Диагностика -----------------------------------
		btnDiagnostics = new ToggleButton("Диагностика");
		btnDiagnostics.setFont(Font.font("sans", 18));		
		
		getItems().addAll( btnInf, spacer, btnDiagnostics);
	}

	
	public ToggleButton getBtnInf() {
		return btnInf;
	}



	public ToggleButton getBtnDiagnostics() {
		return btnDiagnostics;
	}


}
