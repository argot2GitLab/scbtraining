﻿package com.alex.training.sceneElement;

import com.alex.training.stationElement.Transparant;

import javafx.scene.Group;
import javafx.scene.paint.Color;

public class CenterTransparant extends Group {

	private Transparant trON;
	private Transparant trGIR;
	private Transparant trORK;
	private Transparant trGOM;
	private Transparant trOP;
	private Transparant trKL;
	private Transparant trMI;
	private Transparant trORCh;
	private Transparant tr1ORCh;
	private Transparant tr2ORCh;

	public CenterTransparant(double x, double y) {

		trON = new Transparant("ОН", false);
		trON.setLayoutX(37);
		trON.setLayoutY(5);

		trGIR = new Transparant("ГИР", false);
		trGIR.setLayoutX(1);
		trGIR.setLayoutY(47);

		trORK = new Transparant("ОРК", false);
		trORK.setLayoutX(73);
		trORK.setLayoutY(47);

		trGOM = new Transparant("ГОМ", false);
		trGOM.setLayoutX(1);
		trGOM.setLayoutY(89);

		trOP = new Transparant("ОП", false);
		trOP.setLayoutX(73);
		trOP.setLayoutY(89);

		trKL = new Transparant("КЛ", false);
		trKL.setLayoutX(1);
		trKL.setLayoutY(131);

		trMI = new Transparant("МИ", false);
		trMI.setLayoutX(73);
		trMI.setLayoutY(131);

		trORCh = new Transparant("ОРЧ", false);
		trORCh.setLayoutX(37);
		trORCh.setLayoutY(173);

		tr1ORCh = new Transparant("1ОРЧ", false);
		tr1ORCh.setLayoutX(1);
		tr1ORCh.setLayoutY(215);

		tr2ORCh = new Transparant("2ОРЧ", false);
		tr2ORCh.setLayoutX(73);
		tr2ORCh.setLayoutY(215);

		getChildren().addAll(trON, trGIR, trORK, trGOM, trOP, trKL, trMI,
				trORCh, tr1ORCh, tr2ORCh);

		this.setLayoutX(x);
		this.setLayoutY(y);
	}

	public Transparant getTrON() {
		return trON;
	}

	public void setIndicationTrON(boolean flag) {
		trON.getField().setFill((flag) ? Color.WHITE : trON.getColor());
		trON.getField().setStroke((flag) ? Color.BLACK : trON.getColor());
		trON.getTxName().setFill((flag) ? Color.BLACK : Color.GREY);
	}

	public Transparant getTrGOM() {
		return trGOM;
	}

	public void setIndicationTrGOM(boolean flag) {
		trGOM.getField().setFill((flag) ? Color.WHITE : trGOM.getColor());
		trGOM.getField().setStroke((flag) ? Color.BLACK : trGOM.getColor());
		trGOM.getTxName().setFill((flag) ? Color.BLACK : Color.GREY);
	}

}
