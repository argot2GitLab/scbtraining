﻿package com.alex.training.scene;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Orientation;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Screen;

import com.alex.training.Main;
import com.alex.training.schem.BoxAs;

public class SchemaView extends Group {

	private SplitPane sp = new SplitPane();
	private SplitPane spStatus = new SplitPane();
	private AnchorPane pnStatus = new AnchorPane();
	private ScrollPane scroll;
	private List<Schema> schem;
	private Image image;
	private ImageView imv;
	private Group imGroup;

	public SchemaView(BoxAs boxAs) {

		// Сплит панель для показа дерева и схемы
		sp.setLayoutX(0);
		sp.setLayoutY(0);
		sp.setStyle("-fx-border-width:4pt;");
		sp.setPrefSize(Screen.getPrimary().getBounds().getWidth(), Screen
				.getPrimary().getBounds().getHeight() - 110);
		sp.setOrientation(Orientation.HORIZONTAL);

		// Сплит панель для строки состояния и схемы
		spStatus.setLayoutX(0);
		spStatus.setLayoutY(0);
		spStatus.setStyle("-fx-border-width:4pt;");
		// spStatus.setPrefSize(Screen.getPrimary().getBounds().getWidth(),
		// Screen
		// .getPrimary().getBounds().getHeight() - 110);
		spStatus.setOrientation(Orientation.VERTICAL);

		// скролл панель для отображения схем
		scroll = new ScrollPane();
		scroll.setHbarPolicy(ScrollBarPolicy.ALWAYS);
		scroll.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		scroll.setLayoutX(0);
		scroll.setLayoutY(0);
		scroll.setPrefViewportHeight(850);
		scroll.setPrefViewportWidth(Screen.getPrimary().getBounds().getWidth() - 390);
		scroll.setStyle("-fx-background: white");
		scroll.setPannable(true);

		// создание списка схем
		schem = new ArrayList<Schema>();
		// // загрузка всех схем-------------------
		schem.add(0, new GeneralSchema(boxAs, "res/schem/schemaZM1.xml",
				"реле зм - 1"));
		schem.add(1, new GeneralSchema(boxAs, "res/schem/schemaZM2.xml",
				"реле зм - 2"));
		schem.add(2, new GeneralSchema(boxAs, "res/schem/schemaYS1.xml",
				"реле УС - 1"));
		schem.add(3, new GeneralSchema(boxAs, "res/schem/schemaYS2.xml",
				"реле УС - 2"));
		schem.add(4, new GeneralSchema(boxAs, "res/schem/schemaPovtor1.xml",
				"повторителей - 1"));
		schem.add(5, new GeneralSchema(boxAs, "res/schem/schemaGS1.xml",
				"реле ГС группы 1"));
		schem.add(6, new GeneralSchema(boxAs, "res/schem/strelka1.xml",
				"стрелка 1"));
		schem.add(7, new GeneralSchema(boxAs, "res/schem/schemaNuChuSu1.xml",
				"НУ ЧУ СУ 1"));
		schem.add(8, new GeneralSchema(boxAs, "res/schem/schemaNuChuSu2.xml",
				"НУ ЧУ СУ 2"));
		schem.add(9, new GeneralSchema(boxAs, "res/schem/schemaSvet413.xml",
				"светофор ЛН413"));
		schem.add(10, new GeneralSchema(boxAs, "res/schem/schemaDI.xml",
				"реле ДИ"));
		for (Schema schema : schem) {
			((GeneralSchema) schema).setSchemaId(schem.indexOf(schema));
		}
//		imGroup = new Group();
//		image = new Image(Main.class.getResource("res/img/tab.jpg")
//				.toExternalForm(), false);
//
//		imv = new ImageView(image);
//		imGroup.getChildren().add(imv);

		// schemaD = new SchemaSvetoforaD(boxAs);

		// постановка по ток путевых реле
		boxAs.setNormalStateRele();

		// ---------------------------------
		// дерево для показа списка схем
		TreeItem<String> rootTree = new TreeItem<String>("Схемы");
		rootTree.setExpanded(true);
		// rootTree.setGraphic(rootimv);

		TreeItem<String> itemZm = new TreeItem<String>("Схемы реле ЗМ");
		itemZm.setExpanded(false);
		// itemZm.setGraphic(packagesimv);

		TreeItem<String> itemZm1 = new TreeItem<String>("схема зм - 1");
		// itemZm1.setExpanded(true);
		// itemZm1.setGraphic(packageimv);

		TreeItem<String> itemZm2 = new TreeItem<String>("схема зм - 2");
		// itemZm2.setExpanded(true);
		// itemZm1.setGraphic(packageimv);

		itemZm.getChildren().addAll(itemZm1, itemZm2);
		rootTree.getChildren().addAll(itemZm);

		TreeItem<String> itemYs = new TreeItem<String>("Схемы реле УС, АС, АД");
		itemYs.setExpanded(false);
		// itemYs.setGraphic(packagesimv);
		TreeItem<String> itemYs1 = new TreeItem<String>("реле УС - 1");
		itemYs1.setExpanded(false);

		TreeItem<String> itemYs2 = new TreeItem<String>("реле УС - 2");
		itemYs2.setExpanded(false);

		itemYs.getChildren().addAll(itemYs1, itemYs2);
		rootTree.getChildren().addAll(itemYs);

		TreeItem<String> itemPovtor = new TreeItem<String>("Схемы повторителей");
		itemPovtor.setExpanded(false);
		// itemPovtor.setGraphic(packagesimv);
		TreeItem<String> itemPovtor1 = new TreeItem<String>(
				"схема повторителей - 1");
		itemPovtor1.setExpanded(false);

		TreeItem<String> itemPovtor2 = new TreeItem<String>(
				"схема повторителей - 2");
		itemPovtor2.setExpanded(false);

		itemPovtor.getChildren().addAll(itemPovtor1, itemPovtor2);
		rootTree.getChildren().addAll(itemPovtor);
		// ------------------------------------------------
		TreeItem<String> itemGS = new TreeItem<String>("Схемы ГС");
		itemGS.setExpanded(false);
		// itemPovtor.setGraphic(packagesimv);
		TreeItem<String> itemGS1 = new TreeItem<String>("схема ГС группы 1");
		itemGS1.setExpanded(false);

		TreeItem<String> itemGS2 = new TreeItem<String>("схема ГС группы 2");
		itemGS2.setExpanded(false);

		itemGS.getChildren().addAll(itemGS1, itemGS2);
		rootTree.getChildren().addAll(itemGS);
		// ------------------------------------------------
		TreeItem<String> itemNu = new TreeItem<String>("Схемы НУ ЧУ СУ");
		itemNu.setExpanded(false);
		// itemPovtor.setGraphic(packagesimv);
		TreeItem<String> itemNu1 = new TreeItem<String>("НУ ЧУ СУ 1");
		itemNu1.setExpanded(false);

		TreeItem<String> itemNu2 = new TreeItem<String>("НУ ЧУ СУ 2");
		itemNu2.setExpanded(false);

		itemNu.getChildren().addAll(itemNu1, itemNu2);
		rootTree.getChildren().addAll(itemNu);
		// ------------------------------------------------

		TreeItem<String> itemSvetofor = new TreeItem<String>("Схемы светофоров");
		itemSvetofor.setExpanded(false);
		// itemSvetofor.setGraphic(packagesimv);
		TreeItem<String> itemSvetD = new TreeItem<String>("светофор Д");
		itemSvetD.setExpanded(false);
		TreeItem<String> itemSvet413 = new TreeItem<String>("светофор ЛН413");
		itemSvet413.setExpanded(false);

		itemSvetofor.getChildren().addAll(itemSvetD, itemSvet413);
		rootTree.getChildren().addAll(itemSvetofor);

		TreeItem<String> itemStrelki = new TreeItem<String>("Схемы стрелок");
		itemStrelki.setExpanded(false);
		// itemSvetofor.setGraphic(packagesimv);
		TreeItem<String> itemStr1 = new TreeItem<String>("стрелка №1");
		itemStr1.setExpanded(false);

		itemStrelki.getChildren().addAll(itemStr1);
		rootTree.getChildren().addAll(itemStrelki);

		TreeItem<String> itemDI = new TreeItem<String>("Схема реле ДИ");
		itemDI.setExpanded(false);
		// itemSvetofor.setGraphic(packagesimv);
		rootTree.getChildren().addAll(itemDI);

		TreeItem<String> itemTab = new TreeItem<String>("Таблица зависимостей");
		itemTab.setExpanded(false);
		rootTree.getChildren().addAll(itemTab);

		TreeView<String> treeView = new TreeView<String>(rootTree);
		treeView.setLayoutX(10);
		treeView.setLayoutY(10);
		treeView.setCursor(Cursor.CLOSED_HAND);
		treeView.setStyle("-fx-border-width:3pt;-fx-border-color:#f0e68c;-fx-font: 14pt Georgia;");
		treeView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

		// установка отображения схемы при нажатии на меню
		treeView.getSelectionModel().selectedItemProperty()
				.addListener((property, oldValue, newValue) -> {
					setSchema(newValue.getValue());
					// System.out.println(newValue.getValue());
					});

		sp.getItems().addAll(treeView, spStatus);
		sp.setDividerPositions(0.15);

		spStatus.getItems().addAll(pnStatus, scroll);
		spStatus.setDividerPositions(0.05);

		getChildren().addAll(sp);

	}

	public void setSchema(String selectionItem) {

		switch (selectionItem) {
		case "схема зм - 1":
			scroll.setContent((Group) schem.get(0));
			break;
		case "схема зм - 2":
			scroll.setContent((Group) schem.get(1));
			break;
		case "реле УС - 1":
			scroll.setContent((Group) schem.get(2));
			break;
		case "реле УС - 2":
			scroll.setContent((Group) schem.get(3));
			break;
		case "схема ГС группы 1":
			scroll.setContent((Group) schem.get(5));
			break;
		case "схема повторителей - 1":
			scroll.setContent((Group) schem.get(4));
			break;
		case "светофор Д":
			scroll.setContent(null);
			break;
		case "светофор ЛН413":
			scroll.setContent((Group) schem.get(9));
			break;
		case "стрелка №1":
			scroll.setContent((Group) schem.get(6));
			break;
		case "НУ ЧУ СУ 1":
			scroll.setContent((Group) schem.get(7));
			break;
		case "НУ ЧУ СУ 2":
			scroll.setContent((Group) schem.get(8));
			break;
		case "Схема реле ДИ":
			scroll.setContent((Group) schem.get(10));
			break;
//		case "Таблица зависимостей":
//			scroll.setContent(imGroup);
//			break;

		default:
			scroll.setContent(null);
			break;
		}
	}

	public List<Schema> getSchem() {
		return schem;
	}

}
