﻿/**
 * 
 */
package com.alex.training.scene;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.Separator;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Screen;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import com.alex.training.Main;
import com.alex.training.schem.BoxAs;
import com.alex.training.schem.ElCircuit;
import com.alex.training.schem.Loop;
import com.alex.training.schem.ParUnion;
import com.alex.training.schem.PowerSource;
import com.alex.training.schem.Source;
import com.alex.training.schem.User;
import com.alex.training.schemElement.AutostopMotor;
import com.alex.training.schemElement.Conder;
import com.alex.training.schemElement.Contact;
import com.alex.training.schemElement.Diod;
import com.alex.training.schemElement.Drossel;
import com.alex.training.schemElement.FrontAutostopContact;
import com.alex.training.schemElement.FrontContact;
import com.alex.training.schemElement.FrontRightContact;
import com.alex.training.schemElement.Obmotka;
import com.alex.training.schemElement.ObmotkaMotor;
import com.alex.training.schemElement.PolarContact;
import com.alex.training.schemElement.PowerSourceTerminal;
import com.alex.training.schemElement.PowerSourceTerminalDCM;
import com.alex.training.schemElement.Predochran;
import com.alex.training.schemElement.Rele;
import com.alex.training.schemElement.ReleNMPSH03_90;
import com.alex.training.schemElement.ReleNMSH;
import com.alex.training.schemElement.RelePMPUSH;
import com.alex.training.schemElement.Resistor;
import com.alex.training.schemElement.RightContact;
import com.alex.training.schemElement.SLine;
import com.alex.training.schemElement.SPoint;
import com.alex.training.schemElement.SchemElement;
import com.alex.training.schemElement.StrContact;
import com.alex.training.schemElement.StrKyrbelContact;
import com.alex.training.schemElement.SvetLamp;
import com.alex.training.schemElement.TransBSSH;
import com.alex.training.schemElement.TransSobs2Au3;
import com.alex.training.schemElement.Troynic;
import com.alex.training.schemElement.TroynicAutostop;
import com.alex.training.schemElement.TroynicInterface;
import com.alex.training.schemElement.TroynicRight;
import com.alex.training.schemElement.TylAutostopContact;
import com.alex.training.schemElement.TylContact;
import com.alex.training.schemElement.TylRightContact;
import com.alex.training.schemElement.Vyvod;
import com.alex.training.schemElement.blocks.BDR;
import com.alex.training.schemElement.blocks.BKR_76;
import com.alex.training.schemElement.blocks.StrMotor;
import com.alex.training.schemElement.blocks.SvetGol;
import com.alex.training.schemElement.blocks.ZPRSH;
import com.alex.training.util.CircuitState;
import com.alex.training.util.ElementState;
import com.alex.training.util.Helper;
import com.alex.training.util.LoopState;
import com.alex.training.util.SchemaStateListWrapper;
import com.alex.training.util.SourceState;
import com.alex.training.util.UnionState;

/**
 * @author alex
 * 
 *         редактор схем для генерции файла
 *
 */
public class Editor extends Group {

	private ToolBar tb;
	private ScrollPane scroll;
	private MenuButton btnLine;
	private Button btnGener, btnPoint;
	private Button btnRele, btnDelete, btnSave, btnOpen, btnSaveAs;
	private Button btnCreateLoop, btnDeleteLoop, btnDeleteProvod;
	private ToggleButton btnAddPoint, btnSelectElement;
	private List<SchemElement> list = new ArrayList<SchemElement>();
	private List<ElementState> states = new ArrayList<ElementState>();
	private List<Loop> loops = new ArrayList<Loop>();
	private List<ParUnion> unions = new ArrayList<ParUnion>();
	private List<ElCircuit> circuits = new ArrayList<ElCircuit>();
	private List<Source> sources = new ArrayList<Source>();
	private AnchorPane field = new AnchorPane();
	private BoxAs boxAs;
	private Double[] points;
	private List<Integer> lamps;
	private GridPane gridElement, gridCircuit;
	private SplitPane sp = new SplitPane();
	private TextField tfVarName, tfName, tfPlace, tfLayoutX, tfLayoutY, tfPoint;
	private TextField tfLoopName;
	private ComboBox<String> cbRele, cbElement;
	private ComboBox<ParUnion> cbUnion;
	private ComboBox<Loop> cbLoop;
	private ComboBox<Integer> cbNum;
	private CheckBox chbIsZam, chbPstLeft;
	private SchemElement currentSc = null;
	private Point2D dragAnchor, deltaLine;
	private Double initX, initY, dragX, dragY, scaleY;
	private Double newXPosition = 0.;
	private Double newYPosition = 0.;
//	private String varFc, varTc, varTr, varFcR, varTcR, varTrR, varRele, varLine, varPoint, varTerm, varTermD;
	private int indexLoop;
	private Loop curLoop;
	private Button btnCreateUnion;
	private int indexUnion;
	private TextField tfUnionName;
	private Button btnDeleteUnion;
	private ToggleButton btnAddLoop;
	private ComboBox<Loop> cbLoopInUnion;
	private Button btnDeleteLoopFromUnion;
	private ParUnion curUnion;
	private TitledPane tpE;
	private TitledPane tpC;
	private Accordion acc;
	private Button btnCreateCir;
	private int indexCir;
	private TextField tfCirName;
	private Button btnDeleteCir;
	private ToggleButton btnAddUnion;
	private ComboBox<ElCircuit> cbCircuit;
	private ComboBox<ParUnion> cbUnionInCir;
	private ScrollPane scrollCir;
	private ElCircuit curCircuit;
	private Button btnDeleteUnionFromCir;
	private ComboBox<Loop> cbLoopByUnion;
	private ComboBox<ParUnion> cbUnionByCir;
	private MenuButton btnCreateSource;
	private int indexSource;
	private Source curSource;
	private TextField tfSourceName;
	private ComboBox<String> cbSource;
	private Button btnDeleteSource;
	private ToggleButton btnSelectTerminal;
	private ComboBox<String> cbTerminal;
	private TextField tfTerminalName;
	private ToggleButton btnAddSource;
	private ComboBox<String> cbSourceByCir;
	private CheckBox chbConst;
	private TextField tfEds;
	private Button btnDeleteTrminal;
	private TextField tfInR;
	private ToggleButton btnAddUser;
	private ComboBox<String> cbUser;
	private CheckBox chbFirstLeft;
	private List<LoopState> loopStates = new ArrayList<LoopState>();
	private List<UnionState> unionStates = new ArrayList<UnionState>();
	private List<SourceState> sourceStates = new ArrayList<SourceState>();
	private List<CircuitState> circuitStates = new ArrayList<CircuitState>();
	private int addX;
	private int addY;
	private TextField tfFindRele;
	private ToggleButton btnLoop;
	private ToggleButton btnUnion;
	private ToggleButton btnCircuit;
	private ContextMenu contMenu;
	private ContextMenu contMenuTerminal;
	private Label lblSource;
	private ToggleButton btnAddBranch;
	private ComboBox<ElCircuit> cbBranchByCir;
	private int indexBranch;
	private ElCircuit curBranch;
	private ComboBox<ElCircuit> cbBranchInCir;
	private Button btnDeleteBranchFromCir;
	private final String isBranch = "Являеся ветвью для цепи";
	private final String isNotBranch = "Не являеся ветвью";
	private Label lblIsBranch;
	private ComboBox<String> cbTerminalCxema;
	private MenuButton btnContact;
	private MenuButton btnTerminal;
	private MenuButton btnDetali;
	private TextField tfTok;
	private MenuButton btnTrans;
	private Button btnStrMotor;
	private ContextMenu contMenuPMPUSH;
	private ContextMenu contMenuConder;
	private ContextMenu contMenuMotor;
	private ToggleButton btnSelectElementSource;
	private ContextMenu contMenuTransBSSH;
	private ContextMenu contMenuBDR;
	private ContextMenu contMenuStrCont;
	private ComboBox<Integer> cbStrNum;
	private TextField tfLamp;
	private ContextMenu contMenuTransSobs2;
	private ContextMenu contMenuSvetGol;
	private long elementId;
	private Label lblId;
	private Label tok;
	private Button btnAddTerminal;

	// ---------------------------------------------------------

	public Editor(BoxAs ba) {
		super();
		boxAs = ba;

		tb = new ToolBar();
		tb.setMinWidth(Screen.getPrimary().getBounds().getWidth());
		tb.setOrientation(Orientation.HORIZONTAL);
		tb.setPadding(new Insets(8, 70, 8, 70));
		tb.setStyle("-fx-background: lightgray");

		sp.setLayoutX(0);
		sp.setLayoutY(tb.getHeight() + 45);
		sp.setStyle("-fx-border-width:4pt;");
		sp.setPrefSize(Screen.getPrimary().getBounds().getWidth(), Screen.getPrimary().getBounds().getHeight() - 155);
		sp.setOrientation(Orientation.HORIZONTAL);

		gridElement = new GridPane();
		gridElement.setAlignment(Pos.TOP_LEFT);
		gridElement.setHgap(10);
		gridElement.setVgap(10);
		gridElement.setPadding(new Insets(25, 25, 25, 25));
		gridElement.setLayoutX(0);
		gridElement.setLayoutY(0);

		// добавление полей в инспектор и обработка ввода
		// ---------------------------------------------------
		int rowNum = 0;
		Text title = new Text("Инспектор элементов");
		title.setFont(Font.font("sans", FontWeight.BOLD, 18));
		gridElement.add(title, 0, rowNum++, 2, 1);

		Label varName = new Label("Имя переменной");
		gridElement.add(varName, 0, rowNum);
		tfVarName = new TextField();
		gridElement.add(tfVarName, 1, rowNum++);
		tfVarName.setOnAction((ae) -> {
			if (currentSc != null)
				currentSc.setVarName(tfVarName.getText());
		});

		// ---------------------- id
		Label id = new Label("id");
		gridElement.add(id, 0, rowNum);
		lblId = new Label();
		if (currentSc != null)
			lblId.setText(String.valueOf(currentSc.getIdElementInSchem()));
		gridElement.add(lblId, 1, rowNum++);

		// ---------------------------------------------
		Label findRele = new Label("поиск реле");
		gridElement.add(findRele, 0, rowNum);
		tfFindRele = new TextField();
		gridElement.add(tfFindRele, 1, rowNum++);

		tfFindRele.textProperty().addListener((property, oldValue, newValue) -> {
			System.out.println(newValue);
			if (!newValue.isEmpty())
				getListReleByFind(newValue);
			else {
				fillCbReleItems();
			}
		});

		// ---------------------------------------------
		Label rele = new Label("Реле");
		gridElement.add(rele, 0, rowNum);
		cbRele = new ComboBox<String>();
		gridElement.add(cbRele, 1, rowNum++);
		fillCbReleItems();
		cbRele.setDisable(true);
		// cbRele.setEditable(true);

		cbRele.valueProperty().addListener((property, oldValue, newValue) -> {
			// и если введенное новое значение совпадает
			// хоть с одним из списка
			if (newValue != null && !newValue.equals("")) {
				if (currentSc != null
						&& boxAs.getReles().stream().anyMatch((r) -> r.getTxName().getText().equals(newValue))) {
					// System.out.println(newValue);
					tfName.setText(newValue);
					Rele r = boxAs.getRele(newValue);
					if (r != null) {
						tfPlace.setText(r.getTxPlace().getText());
						if (currentSc.getClass().getSuperclass().equals(Contact.class)
								|| currentSc.getClass().getSuperclass().equals(RightContact.class)) {

							((Contact) currentSc).setRele(r);
						}
					}
					fillCbReleItems();
					cbRele.getSelectionModel().select(newValue);
				}
			}

		});
		// ---------------------------------------------------------------

		Label term = new Label("Полюс");
		gridElement.add(term, 0, rowNum);
		cbTerminalCxema = new ComboBox<String>();
		gridElement.add(cbTerminalCxema, 1, rowNum++);
		fillCbTerminalItems();
		cbTerminalCxema.setDisable(true);

		cbTerminalCxema.valueProperty().addListener((property, oldValue, newValue) -> {
			if (currentSc != null && newValue != null) {
				tfVarName.setText(newValue);
				PowerSourceTerminal t = (PowerSourceTerminal) boxAs.getTerminal(newValue);
				if (t != null && (currentSc.getClass().equals(PowerSourceTerminal.class)
						|| currentSc.getClass().equals(PowerSourceTerminalDCM.class)))
					currentSc.shiftCurrent(false);
				currentSc.setVarName(t.getVarName());
				((PowerSourceTerminal) currentSc).setMinus(t.isMinus());
				initInspector(t);
			}

		});
		// ---------------------------------------------------------------

		chbIsZam = new CheckBox("Под током");
		gridElement.add(chbIsZam, 0, rowNum);
		chbIsZam.setDisable(true);
		chbIsZam.selectedProperty().addListener((property, oldValue, newValue) -> {
			if (currentSc != null) {
				if (currentSc.getClass().equals(StrContact.class))
					((StrContact) currentSc).setIsZam(newValue);
				else
					((Contact) currentSc).setIsZam(newValue);

			}
		});
		chbFirstLeft = new CheckBox("1 слева");
		gridElement.add(chbFirstLeft, 1, rowNum++);
		chbFirstLeft.setDisable(true);
		chbFirstLeft.selectedProperty().addListener((property, oldValue, newValue) -> {
			if (currentSc != null)
				((Rele) currentSc).setIsFirstLeft(newValue.booleanValue());
		});
		// ---------------------------------------------------------------------------------
		Label num = new Label("Номер тройника");
		gridElement.add(num, 0, rowNum);
		cbNum = new ComboBox<Integer>();
		cbNum.getItems().addAll(Arrays.asList(10, 20, 30, 40, 50, 60, 70, 80));
		gridElement.add(cbNum, 1, rowNum++);
		cbNum.setEditable(false);
		cbNum.setDisable(true);
		cbNum.valueProperty().addListener((property, oldValue, newValue) -> {
			if (currentSc != null) {
				((Contact) currentSc).setContNum(newValue);
			}
		});
		// ---------------------------------------------------------------
		// ---------------------------------------------------------------------------------
		Label numStr = new Label("Номер стрелки");
		gridElement.add(numStr, 0, rowNum);
		cbStrNum = new ComboBox<Integer>();
		cbStrNum.getItems().addAll(Arrays.asList(1, 2, 3, 4, 5, 6));
		gridElement.add(cbStrNum, 1, rowNum++);
		cbStrNum.setEditable(false);
		cbStrNum.setDisable(true);
		cbStrNum.getSelectionModel().selectFirst();
		cbStrNum.getSelectionModel().selectedIndexProperty().addListener((property, oldValue, newValue) -> {
			if (currentSc != null) {
				if (newValue.intValue() != -1) {
					StrMotor mot = boxAs.getStrMotors().get(newValue.intValue());
					if (mot != null) {
						if (currentSc.getClass().equals(StrContact.class)) {
							((StrContact) currentSc).setMotor(mot);
						}
						if (currentSc.getClass().equals(StrMotor.class)) {
							currentSc = mot;
						}

					}
				}
			}
		});
		// ---------------------------------------------------------------
		Label lamp = new Label("Показания светофора");
		gridElement.add(lamp, 0, rowNum);
		tfLamp = new TextField();
		gridElement.add(tfLamp, 1, rowNum++);
		tfLamp.setDisable(true);
		tfLamp.setOnAction((ae) -> {
			// Создаем массив и заносим туда координаты линии
			setLampList();
		});

		// ------------------------------------------------------------------------
		Label name = new Label("Название");
		gridElement.add(name, 0, rowNum);
		tfName = new TextField();
		gridElement.add(tfName, 1, rowNum++);
		tfName.setEditable(false);
		tfName.textProperty().addListener((property, oldValue, newValue) -> {
			if (currentSc != null)
				currentSc.getTxName().setText(newValue);
		});
		tfName.setOnAction((ae) -> {
			if (currentSc != null)
				currentSc.getTxName().setText(tfName.getText());
		});
		// ---------------------------------------------------------------
		Label place = new Label("Место");
		gridElement.add(place, 0, rowNum);
		tfPlace = new TextField();
		gridElement.add(tfPlace, 1, rowNum++);
		tfPlace.setEditable(false);
		tfPlace.textProperty().addListener((property, oldValue, newValue) -> {
			if (currentSc != null) {
				if (!currentSc.getClass().equals(StrContact.class))
					currentSc.getTxPlace().setText(tfPlace.getText());
				else
					((StrContact) currentSc).setNumCont(Integer.parseInt(tfPlace.getText()));

			}
		});
		tfPlace.setOnAction((ae) -> {
			if (currentSc != null) {
				if (!currentSc.getClass().equals(StrContact.class))
					currentSc.getTxPlace().setText(tfPlace.getText());
				else
					((StrContact) currentSc).setNumCont(Integer.parseInt(tfPlace.getText()));
			}
		});
		// ------------------------------------------------------------------------

		Label point = new Label("Точки линии");
		gridElement.add(point, 0, rowNum);

		tfPoint = new TextField();
		gridElement.add(tfPoint, 1, rowNum++);
		tfPoint.setDisable(true);
		tfPoint.setEditable(true);
		tfPoint.setOnAction((ae) -> {
			// Создаем массив и заносим туда координаты линии
			setLinePoints();
		});

		// -----------------------------------------------------
		btnAddPoint = new ToggleButton("Добавить вершину");
		gridElement.add(btnAddPoint, 0, rowNum++, 2, 1);
		btnAddPoint.setDisable(true);
		btnAddPoint.selectedProperty().addListener((property, oldValue, newValue) -> {
			if (currentSc != null) {
				if (currentSc.getClass().equals(SLine.class)) {
					((SLine) currentSc).setRectPosition();
					((SLine) currentSc).selectByAddVertex(newValue.booleanValue());
				}
			}
		});

		// -------------------------------------------------
		Label pstLeft = new Label("Полюс левый");
		gridElement.add(pstLeft, 0, rowNum);
		chbPstLeft = new CheckBox();
		gridElement.add(chbPstLeft, 1, rowNum++);
		chbPstLeft.setDisable(true);
		chbPstLeft.selectedProperty().addListener((property, oldValue, newValue) -> {
			if (currentSc != null) {
				if (currentSc.getClass().equals(PowerSourceTerminal.class))
					((PowerSourceTerminal) currentSc).setIsLeft(newValue.booleanValue());
				if (currentSc.getClass().equals(PowerSourceTerminalDCM.class))
					((PowerSourceTerminalDCM) currentSc).setIsLeft(newValue.booleanValue());
				if (currentSc.getClass().equals(Vyvod.class))
					((Vyvod) currentSc).setIsLeft(newValue.booleanValue());
				if (currentSc.getClass().equals(StrContact.class))
					((StrContact) currentSc).setIsTop(newValue.booleanValue());

			}
		});

		// ---------------------------------------------------------------
		tok = new Label("Номирал предохранителя");
		gridElement.add(tok, 0, rowNum);
		tfTok = new TextField();
		gridElement.add(tfTok, 1, rowNum++);
		tfTok.setEditable(true);
		tfTok.textProperty().addListener((property, oldValue, newValue) -> {
			if (currentSc != null) {
				if (currentSc.getClass().equals(Predochran.class))
					((Predochran) currentSc).getTxNom().setText(newValue);
				if (currentSc.getClass().equals(Conder.class))
					((Conder) currentSc).setRazrad(Long.valueOf(newValue).longValue());
			}
		});
		tfTok.setOnAction((ae) -> {
			if (currentSc != null) {
				if (currentSc.getClass().equals(Predochran.class))
					((Predochran) currentSc).getTxNom().setText(tfTok.getText());
				if (currentSc.getClass().equals(Conder.class))
					((Conder) currentSc).setRazrad(Long.valueOf(tfTok.getText()).longValue());
			}
		});

		// -------------------------------------------------
		Label layoutX = new Label("layoutX");
		gridElement.add(layoutX, 0, rowNum);
		tfLayoutX = new TextField();
		gridElement.add(tfLayoutX, 1, rowNum++);
		// tfLayoutX.setEditable(false);
		tfLayoutX.setOnAction((ae) -> {
			if (currentSc != null)
				currentSc.setLayoutX(Double.parseDouble(tfLayoutX.getText()));
		});
		// ---------------------------------------------------------------

		// ---------------------------------------------------------------
		Label layoutY = new Label("layoutY");
		gridElement.add(layoutY, 0, rowNum);
		tfLayoutY = new TextField();
		gridElement.add(tfLayoutY, 1, rowNum++);
		// tfLayoutY.setEditable(false);
		tfLayoutY.setOnAction((ae) -> {
			if (currentSc != null)
				currentSc.setLayoutY(Double.parseDouble(tfLayoutY.getText()));
		});

		// -----------------------------------------------------
		btnDelete = new Button("Удалить элемент");
		gridElement.add(btnDelete, 0, rowNum++, 2, 1);
		btnDelete.setDisable(true);
		btnDelete.setOnAction((ae) -> {
			if (currentSc != null) {
				list.remove(currentSc);
				field.getChildren().remove(currentSc);
				tfVarName.setText("");
				tfVarName.setDisable(true);
				cbRele.setDisable(true);
				chbIsZam.setDisable(true);
				cbNum.setDisable(true);
				tfName.setText("");
				tfName.setDisable(true);
				tfPlace.setText("");
				tfPlace.setDisable(true);
				tfPoint.setText("");
				tfPoint.setDisable(true);
				btnAddPoint.setDisable(true);
				tfLayoutX.setText("");
				tfLayoutX.setDisable(true);
				tfLayoutY.setText("");
				tfLayoutY.setDisable(true);
				btnAddPoint.setSelected(false);
				chbFirstLeft.setDisable(true);
				currentSc = null;
			}

		});

		// --------------------------------------------------- gridElement

		// для инспектора електрических цепей-------------------------------
		gridCircuit = new GridPane();
		gridCircuit.setAlignment(Pos.TOP_LEFT);
		gridCircuit.setHgap(10);
		gridCircuit.setVgap(10);
		gridCircuit.setPadding(new Insets(25, 25, 25, 25));
		gridCircuit.setLayoutX(0);
		gridCircuit.setLayoutY(0);

		rowNum = 0;

		Text titleCircuit = new Text("Инспектор эл.цепей");
		titleCircuit.setFont(Font.font("sans", FontWeight.BOLD, 18));
		gridCircuit.add(titleCircuit, 0, rowNum++, 2, 1);

		// кнопки для выделения цветом контура соединения или цепи
		btnLoop = new ToggleButton("Контур");
		btnUnion = new ToggleButton("Соединение");
		btnCircuit = new ToggleButton("Эл. цепь");
		final ToggleGroup tgSelect = new ToggleGroup();
		btnLoop.setToggleGroup(tgSelect);
		btnLoop.setLayoutX(0);
		btnLoop.setMaxWidth(80);
		btnLoop.setMinWidth(80);
		btnUnion.setToggleGroup(tgSelect);
		btnUnion.setLayoutX(80);
		btnUnion.setMaxWidth(80);
		btnCircuit.setToggleGroup(tgSelect);
		btnCircuit.setLayoutX(160);
		btnCircuit.setMaxWidth(80);

		// выделение цветом текущего контура
		btnLoop.selectedProperty().addListener((property, oldValue, newValue) -> {
			cancelPodsvetka();
			if (newValue && curLoop != null)
				curLoop.loopPodsvetka(1);
		});

		// выделение цветом текущего соединения
		btnUnion.selectedProperty().addListener((property, oldValue, newValue) -> {
			cancelPodsvetka();
			if (newValue && curUnion != null)
				curUnion.unionPodsvetka(2);
		});

		// выделение цветом текущей цепи
		btnCircuit.selectedProperty().addListener((property, oldValue, newValue) -> {
			cancelPodsvetka();
			if (newValue && curCircuit != null)
				curCircuit.circuitPodsvetka(3);
		});

		Group grButton = new Group();
		grButton.getChildren().addAll(btnLoop, btnUnion, btnCircuit);
		gridCircuit.add(grButton, 0, rowNum++, 2, 1);

		// меню для добавления тылового контакта тройника в контур---
		MenuItem front = new MenuItem("фронт");
		MenuItem tyl = new MenuItem("тыл");
		contMenu = new ContextMenu(front, tyl);

		tyl.setOnAction((ae) -> {
			TylContact tc = null;
			if (((TroynicInterface) currentSc).getTylTr() != null)
				tc = ((TroynicInterface) currentSc).getTylTr();
			else
				tc = ((TroynicInterface) currentSc).getAsTyl(false);
			list.add(tc);
			setElementId(tc);
			tc.setVarName("tc" + String.valueOf(tc.getIdElementInSchem()));

			if (curLoop != null) {
				curLoop.addProvod(tc);
				tc.setDrowed(false);
				refreshCbElement();
			}

		});
		front.setOnAction((ae) -> {
			if (curLoop != null) {
				curLoop.addProvod(currentSc);
				refreshCbElement();
			}
		});

		// ---------------------------------------------------------------------

		// меню для добавления полюса трансформатора БСШ в контур---
		MenuItem obm12 = new MenuItem("Вывод 12");
		MenuItem obm8 = new MenuItem("Вывод 8");
		MenuItem obmFirst = new MenuItem("Первичная обмотка");
		contMenuTransBSSH = new ContextMenu(obm12, obm8, obmFirst);

		obm12.setOnAction((ae) -> {
			PowerSourceTerminal ps = ((TransBSSH) currentSc).getTerm1();
			if (list.indexOf(ps) == -1) {
				list.add(ps);
				setElementId(ps);
			}
			ps.setVarName("pstTransBssh" + String.valueOf(ps.getIdElementInSchem()));
			ps.setIndexParent(currentSc.getIdElementInSchem());

			if (curLoop != null) {
				curLoop.addProvod(ps);
				ps.setDrowed(false);
				refreshCbElement();
			}

		});
		obm8.setOnAction((ae) -> {
			PowerSourceTerminal ps = ((TransBSSH) currentSc).getTerm2();
			if (list.indexOf(ps) == -1) {
				list.add(ps);
				setElementId(ps);
			}
			ps.setVarName("pstTransBssh" + String.valueOf(ps.getIdElementInSchem()));
			ps.setIndexParent(currentSc.getIdElementInSchem());

			if (curLoop != null) {
				curLoop.addProvod(ps);
				ps.setDrowed(false);
				refreshCbElement();
			}

		});
		obmFirst.setOnAction((ae) -> {
			if (curLoop != null) {
				curLoop.addProvod(currentSc);
				refreshCbElement();
			}

		});

		// меню для добавления полюса трансформатора СОБС2 в контур---
		MenuItem obmII1 = new MenuItem("Вывод II1");
		MenuItem obmV1 = new MenuItem("Вывод V1");
		MenuItem obmV2 = new MenuItem("Вывод V2");
		MenuItem obmFirstSobs = new MenuItem("Первичная обмотка");
		contMenuTransSobs2 = new ContextMenu(obmII1, obmV1, obmV2, obmFirstSobs);

		obmII1.setOnAction((ae) -> {
			PowerSourceTerminal ps = ((TransSobs2Au3) currentSc).getTerm1();
			if (list.indexOf(ps) == -1) {
				list.add(ps);
				setElementId(ps);
			}
			ps.setVarName("pstTransSobs" + String.valueOf(ps.getIdElementInSchem()));
			ps.setIndexParent(currentSc.getIdElementInSchem());

			if (curLoop != null) {
				curLoop.addProvod(ps);
				ps.setDrowed(false);
				refreshCbElement();
			}

		});
		obmV1.setOnAction((ae) -> {
			PowerSourceTerminal ps = ((TransSobs2Au3) currentSc).getTerm2();
			if (list.indexOf(ps) == -1) {
				list.add(ps);
				setElementId(ps);
			}
			ps.setVarName("pstTransSobs" + String.valueOf(ps.getIdElementInSchem()));
			ps.setIndexParent(currentSc.getIdElementInSchem());

			if (curLoop != null) {
				curLoop.addProvod(ps);
				ps.setDrowed(false);
				refreshCbElement();
			}

		});
		obmV2.setOnAction((ae) -> {
			PowerSourceTerminal ps = ((TransSobs2Au3) currentSc).getTerm3();
			if (list.indexOf(ps) == -1) {
				list.add(ps);
				setElementId(ps);
			}
			ps.setVarName("pstTransSobs" + String.valueOf(ps.getIdElementInSchem()));
			ps.setIndexParent(currentSc.getIdElementInSchem());

			if (curLoop != null) {
				curLoop.addProvod(ps);
				ps.setDrowed(false);
				refreshCbElement();
			}

		});
		obmFirstSobs.setOnAction((ae) -> {
			if (curLoop != null) {
				curLoop.addProvod(currentSc);
				refreshCbElement();
			}

		});
		// -------------------------------

		// меню для добавления лампы в контур---
		MenuItem lampBo = new MenuItem("белый огонь");
		MenuItem lampGo = new MenuItem("желтый огонь");
		MenuItem lampKGo = new MenuItem("красно-желтый огонь");
		MenuItem lampZo = new MenuItem("зеленый огонь");
		MenuItem lampKo = new MenuItem("красный огонь");
		contMenuSvetGol = new ContextMenu(lampBo, lampGo, lampKGo, lampZo, lampKo);

		lampBo.setOnAction((ae) -> {
			SvetLamp sl = ((SvetGol) currentSc).getLamps().get(0);
			if (list.indexOf(sl) == -1) {
				list.add(sl);
				setElementId(sl);
			}
			sl.setVarName("lampBO" + String.valueOf(sl.getIdElementInSchem()));
			sl.setIndexParent(currentSc.getIdElementInSchem());
			sl.setIndexLamp(0);
			if (curLoop != null) {
				curLoop.addProvod(sl);
				sl.setDrowed(false);
				refreshCbElement();
			}
		});
		lampGo.setOnAction((ae) -> {
			SvetLamp sl = ((SvetGol) currentSc).getLamps().get(1);
			if (list.indexOf(sl) == -1) {
				list.add(sl);
				setElementId(sl);
			}
			sl.setVarName("lampGO" + String.valueOf(sl.getIdElementInSchem()));
			sl.setIndexParent(currentSc.getIdElementInSchem());
			sl.setIndexLamp(1);
			if (curLoop != null) {
				curLoop.addProvod(sl);
				sl.setDrowed(false);
				refreshCbElement();
			}
		});
		lampKGo.setOnAction((ae) -> {
			SvetLamp sl = ((SvetGol) currentSc).getLamps().get(2);
			if (list.indexOf(sl) == -1) {
				list.add(sl);
				setElementId(sl);
			}
			sl.setVarName("lampKGO" + String.valueOf(sl.getIdElementInSchem()));
			sl.setIndexParent(currentSc.getIdElementInSchem());
			sl.setIndexLamp(2);
			if (curLoop != null) {
				curLoop.addProvod(sl);
				sl.setDrowed(false);
				refreshCbElement();
			}
		});
		lampZo.setOnAction((ae) -> {
			SvetLamp sl = ((SvetGol) currentSc).getLamps().get(3);
			if (list.indexOf(sl) == -1) {
				list.add(sl);
				setElementId(sl);
			}
			sl.setVarName("lampZO" + String.valueOf(sl.getIdElementInSchem()));
			sl.setIndexParent(currentSc.getIdElementInSchem());
			sl.setIndexLamp(3);
			if (curLoop != null) {
				curLoop.addProvod(sl);
				sl.setDrowed(false);
				refreshCbElement();
			}
		});
		lampKo.setOnAction((ae) -> {
			SvetLamp sl = ((SvetGol) currentSc).getLamps().get(4);
			if (list.indexOf(sl) == -1) {
				list.add(sl);
				setElementId(sl);
			}
			sl.setVarName("lampKO" + String.valueOf(sl.getIdElementInSchem()));
			sl.setIndexParent(currentSc.getIdElementInSchem());
			sl.setIndexLamp(4);
			if (curLoop != null) {
				curLoop.addProvod(sl);
				sl.setDrowed(false);
				refreshCbElement();
			}
		});

		// меню для добавления полюса БДР и БКР в контур---
		MenuItem bdrPlus = new MenuItem("Вывод плюс");
		MenuItem bdrMinus = new MenuItem("Вывод минус");
		MenuItem bdr = new MenuItem("как целый элемент");
		contMenuBDR = new ContextMenu(bdrPlus, bdrMinus, bdr);

		bdrPlus.setOnAction((ae) -> {
			PowerSourceTerminal ps = ((Source) currentSc).getTerminals().get(0);
			if (list.indexOf(ps) == -1) {
				list.add(ps);
				setElementId(ps);
			}
			ps.setVarName("pstBdrPlus" + String.valueOf(ps.getIdElementInSchem()));
			ps.setIndexParent(currentSc.getIdElementInSchem());

			if (curLoop != null) {
				curLoop.addProvod(ps);
				ps.setDrowed(false);
				refreshCbElement();
			}

		});
		bdrMinus.setOnAction((ae) -> {
			PowerSourceTerminal ps = ((Source) currentSc).getMinusTerminals().get(0);
			if (list.indexOf(ps) == -1) {
				list.add(ps);
				setElementId(ps);
			}
			ps.setVarName("pstBdrMinus" + String.valueOf(ps.getIdElementInSchem()));
			ps.setIndexParent(currentSc.getIdElementInSchem());

			if (curLoop != null) {
				curLoop.addProvod(ps);
				ps.setDrowed(false);
				refreshCbElement();
			}

		});
		bdr.setOnAction((ae) -> {
			if (curLoop != null) {
				curLoop.addProvod(currentSc);
				refreshCbElement();
			}

		});

		// ---------------------------------------------------------------------

		// Создание контура -----------------------------------------------
		Text titleLoop = new Text("Контур");
		titleLoop.setFont(Font.font("sans", FontWeight.BOLD, 16));
		gridCircuit.add(titleLoop, 0, rowNum++, 2, 1);

		// ----------------------------------------
		btnCreateLoop = new Button("Создать контур");
		gridCircuit.add(btnCreateLoop, 0, rowNum);
		indexLoop = 0;
		btnCreateLoop.setOnAction((ae) -> {
			curLoop = new Loop();
			loops.add(curLoop);
			indexLoop = loops.indexOf(curLoop);
			curLoop.setVarName("loop" + indexLoop);
			tfLoopName.setText(curLoop.getVarName());
			cbLoop.getItems().add(indexLoop, curLoop);
			cbLoop.getSelectionModel().select(indexLoop);
		});

		tfLoopName = new TextField();
		gridCircuit.add(tfLoopName, 1, rowNum++); // ???????

		tfLoopName.setOnAction((ae) -> {
			curLoop.setVarName(tfLoopName.getText());
			cbLoop.getItems().clear();
			cbLoop.getItems().addAll(loops);
			cbLoop.getSelectionModel().select(indexLoop);
		});
		// -------------------------------

		btnDeleteLoop = new Button("Удалить контур");
		gridCircuit.add(btnDeleteLoop, 0, rowNum++, 2, 1);

		btnDeleteLoop.setOnAction((ae) -> {
			loops.remove(curLoop);
			tfLoopName.setText("");
			cbLoop.getItems().clear();
			cbLoop.getItems().addAll(loops);
			curLoop.loopPodsvetka(0);
			curLoop = (!loops.isEmpty()) ? loops.get(0) : null;
			if (curLoop == null) {
				indexLoop = -1;
				cbLoop.setValue(null);
			} else {
				cbLoop.setValue(curLoop);
			}
			refreshCbElement();

		});

		// ---------------------------------------
		Label lblListLoop = new Label("Список контуров");
		gridCircuit.add(lblListLoop, 0, rowNum);

		cbLoop = new ComboBox<Loop>();
		gridCircuit.add(cbLoop, 1, rowNum++);
		cbLoop.valueProperty().addListener((property, oldValue, newValue) -> {

			btnSelectElement.setSelected(false);
			if (newValue != null) {
				indexLoop = loops.indexOf(newValue);
				System.out.println(indexLoop + " - index" + newValue.toString());

				tfLoopName.setText(newValue.getVarName());
				curLoop = newValue;
				if (oldValue != null)
					oldValue.loopPodsvetka(0);
				refreshCbElement();
			}
		});
		// --------------------------------------

		// --------------------------------------
		btnSelectElement = new ToggleButton("Выбрать");
		gridCircuit.add(btnSelectElement, 0, rowNum++);

		btnSelectElement.selectedProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue()) {
				btnAddUser.setSelected(false);
				btnSelectTerminal.setSelected(false);
			}
		});

		// ---------------------------------------

		// ---------------------------------------
		Label lbElements = new Label("Список проводников");
		gridCircuit.add(lbElements, 0, rowNum);

		cbElement = new ComboBox<String>();
		gridCircuit.add(cbElement, 1, rowNum++);

		cbElement.valueProperty().addListener((property, oldValue, newValue) -> {
			if (currentSc != null)
				currentSc.shiftCurrent(false);
			if (newValue != null && tpC.isExpanded()) {
				if (curLoop.getProvod(newValue).getClass().getSuperclass().equals(SchemElement.class)) {
					currentSc = (SchemElement) curLoop.getProvod(newValue);
					if (currentSc != null)
						currentSc.shiftCurrent(true);
				}
			}

		});

		// --------------------------------------

		// --------------------------------------
		btnDeleteProvod = new Button("Удалить элемент из контура");
		gridCircuit.add(btnDeleteProvod, 0, rowNum++, 2, 1);

		btnDeleteProvod.setOnAction((ae) -> {
			if (curLoop != null && currentSc != null && !curLoop.getLoop().isEmpty()) {
				curLoop.removeProvod(currentSc);
				refreshCbElement();
			}

		});

		Separator sep = new Separator(Orientation.HORIZONTAL);
		sep.setMinHeight(30.);
		gridCircuit.add(sep, 0, rowNum++, 2, 1);
		// -------------------------------------- контур

		// Создание соединения -----------------------------------------------
		Text titleUnion = new Text("Соединение");
		titleUnion.setFont(Font.font("sans", FontWeight.BOLD, 16));
		gridCircuit.add(titleUnion, 0, rowNum++, 2, 1);

		// ----------------------------------------
		btnCreateUnion = new Button("Создать cоединение");
		gridCircuit.add(btnCreateUnion, 0, rowNum);
		indexUnion = 0;
		btnCreateUnion.setOnAction((ae) -> {
			curUnion = new ParUnion();
			unions.add(curUnion);
			indexUnion = unions.indexOf(curUnion);
			curUnion.setVarName("union" + indexUnion);
			tfUnionName.setText(curUnion.getVarName());
			cbUnion.getItems().add(indexUnion, curUnion);
			cbUnion.getSelectionModel().select(indexUnion);
		});

		tfUnionName = new TextField();
		gridCircuit.add(tfUnionName, 1, rowNum++); // ???????

		tfUnionName.setOnAction((ae) -> {
			curUnion.setVarName(tfUnionName.getText());
			cbUnion.getItems().clear();
			cbUnion.getItems().addAll(unions);
			cbUnion.getSelectionModel().select(indexUnion);
		});
		// -------------------------------

		btnDeleteUnion = new Button("Удалить соединение");
		gridCircuit.add(btnDeleteUnion, 0, rowNum++, 2, 1);

		btnDeleteUnion.setOnAction((ae) -> {
			if (curUnion != null) {
				unions.remove(curUnion);
				cbUnion.getItems().clear();
				cbUnion.getItems().addAll(unions);
				curUnion.unionPodsvetka(0);
				curUnion = (!unions.isEmpty()) ? unions.get(0) : null;
				if (curUnion == null) {
					indexUnion = -1;
					cbUnion.setValue(null);
				} else {
					cbUnion.setValue(curUnion);
				}
			}
		});

		// ---------------------------------------
		Label lblListUnion = new Label("Список соединений");
		gridCircuit.add(lblListUnion, 0, rowNum);

		cbUnion = new ComboBox<ParUnion>();
		gridCircuit.add(cbUnion, 1, rowNum++);
		cbUnion.valueProperty().addListener((property, oldValue, newValue) -> {

			if (newValue != null) {
				indexUnion = unions.indexOf(newValue);
				System.out.println(indexUnion + " - index " + newValue.toString());

				tfUnionName.setText(newValue.getVarName());
				curUnion = newValue;

				if (curLoop != null && curUnion != null) {
					refreshLoopInUnion();
				}
			}
		});
		// --------------------------------------

		// --------------------------------------
		btnAddLoop = new ToggleButton("Добавить контур");
		gridCircuit.add(btnAddLoop, 0, rowNum);

		cbLoopByUnion = new ComboBox<Loop>();
		gridCircuit.add(cbLoopByUnion, 1, rowNum++);
		cbLoopByUnion.setDisable(true);

		cbLoopByUnion.valueProperty().addListener((property, oldValue, newValue) -> {

			if (newValue != null) {
				indexLoop = loops.indexOf(newValue);
				curLoop = newValue;

				if (curLoop != null && curUnion != null) {
					curUnion.addLoop(curLoop);
					refreshLoopInUnion();
					// System.out.println(indexLoop + " - xren index"
					// + newValue.toString());

				}

				btnAddLoop.setSelected(false);
				// if (oldValue != null)
				// oldValue.loopPodsvetka(false);

			}
		});

		btnAddLoop.selectedProperty().addListener((property, oldValue, newValue) -> {
			cbLoopByUnion.setDisable(!newValue.booleanValue());
			if (newValue.booleanValue()) {
				cbLoopByUnion.getItems().clear();
				cbLoopByUnion.getItems().addAll(loops);
			}
		});

		// btnAddLoop.setOnAction((ae) -> {
		// cbLoopByUnion.setDisable(true);
		// });

		// ---------------------------------------

		// ---------------------------------------
		Label lblLoops = new Label("Список контуров");
		gridCircuit.add(lblLoops, 0, rowNum);

		cbLoopInUnion = new ComboBox<Loop>();
		gridCircuit.add(cbLoopInUnion, 1, rowNum++);

		cbLoopInUnion.valueProperty().addListener((property, oldValue, newValue) -> {
			if (curLoop != null)
				cancelPodsvetka();
			if (newValue != null && tpC.isExpanded() && btnLoop.isSelected()) {
				curLoop = (Loop) newValue;
				curLoop.loopPodsvetka(1);
			}

		});

		// --------------------------------------
		btnDeleteLoopFromUnion = new Button("Удалить контур из соединения");
		gridCircuit.add(btnDeleteLoopFromUnion, 0, rowNum++, 2, 1);
		btnDeleteLoopFromUnion.setOnAction((ae) -> {
			curUnion.getUnion().remove(cbLoopInUnion.getValue());
			refreshLoopInUnion();
		});

		// --------------------------------------

		Separator sep1 = new Separator(Orientation.HORIZONTAL);
		sep1.setMinHeight(30.);
		gridCircuit.add(sep1, 0, rowNum++, 2, 1);
		// --------------------------------------
		// соединение-------------------------------------------------

		// Создание источника -----------------------------------------------
		Text titleSource = new Text("Источник");
		titleSource.setFont(Font.font("sans", FontWeight.BOLD, 16));
		gridCircuit.add(titleSource, 0, rowNum++, 2, 1);

		// ----------------------------------------
		btnCreateSource = new MenuButton("Создать источник");
		gridCircuit.add(btnCreateSource, 0, rowNum);
		// indexSource=0;
		// загрузка существующих источников
		sources.addAll(boxAs.getSources());
		// Создание нового источника

		MenuItem mItemConst = new MenuItem("Постоянного тока");
		// mItemHLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemConst.setOnAction((ae) -> {
			createSource(ae, true);
		});

		MenuItem mItemPerem = new MenuItem("Переменного тока");
		// mItemVLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemPerem.setOnAction((ae) -> {
			createSource(ae, false);
		});
		btnCreateSource.getItems().addAll(mItemConst, mItemPerem);

		tfSourceName = new TextField();
		gridCircuit.add(tfSourceName, 1, rowNum++); // ???????

		tfSourceName.setOnAction((ae) -> {
			curSource.setVarName(tfSourceName.getText());
			cbSource.getItems().clear();
			cbSource.getItems().addAll(sources.stream().map((s) -> s.getVarName()).collect(Collectors.toList()));
			cbSource.getSelectionModel().select(indexSource);
		});
		// -------------------------------
		// -------------------------------
		btnSelectElementSource = new ToggleButton("Выбрать источник");
		gridCircuit.add(btnSelectElementSource, 0, rowNum++);

		btnSelectElementSource.selectedProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue()) {
				btnSelectElement.setSelected(false);
				btnAddUser.setSelected(false);
				btnSelectTerminal.setSelected(false);
			}
		});

		// -------------------------------

		btnDeleteSource = new Button("Удалить источник");
		gridCircuit.add(btnDeleteSource, 0, rowNum++, 2, 1);

		btnDeleteSource.setOnAction((ae) -> {
			if (!curSource.equals(boxAs.getSources().get(0))) {
				sources.remove(curSource);
				cbSource.getItems().clear();
				cbSource.getItems().addAll(sources.stream().map((s) -> s.getVarName()).collect(Collectors.toList()));
				curSource = (!sources.isEmpty()) ? sources.get(0) : null;
				if (curSource == null) {
					cbSource.setValue(null);
					tfSourceName.setText("");
				} else {
					cbSource.setValue(curSource.getVarName());
					tfSourceName.setText(curSource.getVarName());
				}
			}
		});

		// ---------------------------------------
		Label lblListSource = new Label("Список источников");
		gridCircuit.add(lblListSource, 0, rowNum);

		cbSource = new ComboBox<String>();
		gridCircuit.add(cbSource, 1, rowNum++);
		cbSource.valueProperty().addListener((property, oldValue, newValue) -> {

			btnSelectTerminal.setSelected(false);
			if (newValue != null) {
				indexSource = sources.indexOf(newValue);
				tfSourceName.setText(newValue);
				curSource = getSource(newValue);
				if (curSource != null) {
					tfEds.setText(String.valueOf(curSource.getEds()));
					tfInR.setText(String.valueOf(curSource.getInR()));
					refreshCbTerminal();
				}
			}
		});
		// --------------------------------------
		// btnAddTerminal = new Button("Добавить полюс");
		// gridCircuit.add(btnAddTerminal, 0, rowNum++, 2, 1);

		// --------------------------------------

		btnSelectTerminal = new ToggleButton("Выбрать полюс");
		gridCircuit.add(btnSelectTerminal, 0, rowNum);

		btnSelectTerminal.selectedProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue()) {
				btnSelectElement.setSelected(false);
				btnAddUser.setSelected(false);
			}
		});
		tfTerminalName = new TextField();
		gridCircuit.add(tfTerminalName, 1, rowNum++);
		tfTerminalName.setEditable(false);
		// ---------------------------------------

		// ---------------------------------------
		Label lbTerminals = new Label("Список полюсов");
		gridCircuit.add(lbTerminals, 0, rowNum);

		cbTerminal = new ComboBox<String>();
		gridCircuit.add(cbTerminal, 1, rowNum++);

		cbTerminal.valueProperty().addListener((property, oldValue, newValue) -> {
			if (currentSc != null)
				currentSc.shiftCurrent(false);
			if (newValue != null && tpC.isExpanded()) {
				currentSc = curSource.getTerminal(newValue);
				if (currentSc == null && curSource.getIsConst())
					currentSc = ((Source) curSource).getMinusTerminal(newValue);
				if (currentSc != null)
					currentSc.shiftCurrent(true);
			}

		});
		// --------------------------------------

		// --------------------------------------
		btnDeleteTrminal = new Button("Удалить полюс");
		gridCircuit.add(btnDeleteTrminal, 0, rowNum++, 2, 1);

		// --------------------------------------------
		// chbConst = new CheckBox("Постоянный ток");
		// gridCircuit.add(chbConst, 0, rowNum++, 2, 1);
		// chbConst.selectedProperty().addListener(
		// (property, oldValue, newValue) -> {
		// if (curSource != null)
		// curSource.setIsConst(newValue.booleanValue());
		// });
		// --------------------------------------------------------

		Label lblEds = new Label("ЭДС");
		gridCircuit.add(lblEds, 0, rowNum);

		tfEds = new TextField();
		gridCircuit.add(tfEds, 1, rowNum++);

		tfEds.setOnAction((ae) -> {
			if (curSource != null)
				curSource.setEds(Double.valueOf(tfEds.getText()));

		});

		// --------------------------------------------------------
		Label lblInnerR = new Label("Внутреннее сопротивление");
		gridCircuit.add(lblInnerR, 0, rowNum++);

		tfInR = new TextField();
		gridCircuit.add(tfInR, 1, rowNum++);

		tfInR.setOnAction((ae) -> {
			if (curSource != null)
				curSource.setInR(Double.valueOf(tfInR.getText()));

		});
		// --------------------------------------------------------
		Separator sep2 = new Separator(Orientation.HORIZONTAL);
		sep.setMinHeight(30.);
		gridCircuit.add(sep2, 0, rowNum++, 2, 1);
		// --------------------------------------------

		// полюс

		// Создание эл. цепи -----------------------------------------------
		Text titleCir = new Text("Эл. цепь");
		titleCir.setFont(Font.font("sans", FontWeight.BOLD, 16));
		gridCircuit.add(titleCir, 0, rowNum++, 2, 1);

		// ----------------------------------------
		btnCreateCir = new Button("Создать эл. цепь");
		gridCircuit.add(btnCreateCir, 0, rowNum);
		indexCir = 0;
		btnCreateCir.setOnAction((ae) -> {
			curCircuit = new ElCircuit();
			circuits.add(curCircuit);
			indexCir = circuits.indexOf(curCircuit);
			curCircuit.setVarName("cir" + indexCir);
			tfCirName.setText(curCircuit.getVarName());
			cbCircuit.getItems().add(indexCir, curCircuit);
			cbCircuit.getSelectionModel().select(indexCir);
		});

		tfCirName = new TextField();
		gridCircuit.add(tfCirName, 1, rowNum++); // ???????

		tfCirName.setOnAction((ae) -> {
			curCircuit.setVarName(tfCirName.getText());
			cbCircuit.getItems().clear();
			cbCircuit.getItems().addAll(circuits);
			cbCircuit.getSelectionModel().select(indexCir);
		});
		// -------------------------------

		btnDeleteCir = new Button("Удалить эл. цепь");
		gridCircuit.add(btnDeleteCir, 0, rowNum++, 2, 1);

		btnDeleteCir.setOnAction((ae) -> {
			circuits.remove(curCircuit);
			cbCircuit.getItems().clear();
			cbCircuit.getItems().addAll(circuits);
			curCircuit = (!circuits.isEmpty()) ? circuits.get(0) : null;
			if (curCircuit == null) {
				indexCir = -1;
				cbCircuit.setValue(null);
			} else {
				cbCircuit.setValue(curCircuit);
			}
		});

		// ---------------------------------------
		Label lblListCir = new Label("Список цепей");
		gridCircuit.add(lblListCir, 0, rowNum);

		cbCircuit = new ComboBox<ElCircuit>();
		gridCircuit.add(cbCircuit, 1, rowNum++);
		cbCircuit.valueProperty().addListener((property, oldValue, newValue) -> {

			if (newValue != null) {
				indexCir = circuits.indexOf(newValue);
				System.out.println(indexCir + " - index " + newValue.toString());

				tfCirName.setText(newValue.getVarName());
				curCircuit = newValue;
				lblSource.setText((curCircuit.getSource() != null) ? curCircuit.getSource().getVarName() : "");
				lblIsBranch.setText((curCircuit.isBranch() ? isBranch : isNotBranch));
				refreshCbUser();
				refreshUnionInCir();
				refreshBranchInCir();
			} else {
				curCircuit = null;
				tfCirName.setText("");
			}
		});
		// --------------------------------------

		// --------------------------------------
		btnAddUnion = new ToggleButton("Добавить соединение");
		gridCircuit.add(btnAddUnion, 0, rowNum);

		cbUnionByCir = new ComboBox<ParUnion>();
		gridCircuit.add(cbUnionByCir, 1, rowNum++);
		cbUnionByCir.setDisable(true);

		cbUnionByCir.valueProperty().addListener((property, oldValue, newValue) -> {

			if (newValue != null) {
				indexUnion = unions.indexOf(newValue);
				curUnion = newValue;

				if (curCircuit != null && curUnion != null) {
					curCircuit.addUnion(curUnion);
					refreshUnionInCir();
					System.out.println(indexUnion + " - xren index" + newValue.toString());

				}

				btnAddUnion.setSelected(false);
				// if (oldValue != null)
				// oldValue.loopPodsvetka(false);

			}
		});

		btnAddUnion.selectedProperty().addListener((property, oldValue, newValue) -> {
			cbUnionByCir.setDisable(!newValue.booleanValue());
			if (newValue.booleanValue()) {
				cbUnionByCir.getItems().clear();
				cbUnionByCir.getItems().addAll(unions);
			}
		});

		// ---------------------------------------

		// ---------------------------------------
		Label lblUnions = new Label("Список соединений");
		gridCircuit.add(lblUnions, 0, rowNum);

		cbUnionInCir = new ComboBox<ParUnion>();
		gridCircuit.add(cbUnionInCir, 1, rowNum++);

		cbUnionInCir.valueProperty().addListener((property, oldValue, newValue) -> {
			if (curUnion != null)
				cancelPodsvetka();
			if (newValue != null && tpC.isExpanded() && btnUnion.isSelected()) {
				curUnion = newValue;
				curUnion.unionPodsvetka(2);
			}

		});

		// --------------------------------------
		btnDeleteUnionFromCir = new Button("Удалить соединение из эл. цепи");
		gridCircuit.add(btnDeleteUnionFromCir, 0, rowNum++, 2, 1);
		// --------------------------------------

		lblIsBranch = new Label("");
		gridCircuit.add(lblIsBranch, 0, rowNum++, 2, 1);
		lblIsBranch.setFont(Font.font("sans", FontWeight.BOLD, 18));

		// --------------------------------------
		btnAddBranch = new ToggleButton("Добавить ветвь");
		gridCircuit.add(btnAddBranch, 0, rowNum);

		cbBranchByCir = new ComboBox<ElCircuit>();
		gridCircuit.add(cbBranchByCir, 1, rowNum++);
		cbBranchByCir.setDisable(true);

		cbBranchByCir.valueProperty().addListener((property, oldValue, newValue) -> {

			if (newValue != null) {
				indexBranch = circuits.indexOf(newValue);
				curBranch = newValue;

				if (curCircuit != null && curBranch != null) {
					curBranch.setBranch(true);
					curCircuit.addBranch(curBranch);
					refreshBranchInCir();
					// System.out.println(indexUnion + " - xren index"
					// + newValue.toString());
				}

				btnAddBranch.setSelected(false);

			}
		});

		btnAddBranch.selectedProperty().addListener((property, oldValue, newValue) -> {
			cbBranchByCir.setDisable(!newValue.booleanValue());
			if (newValue.booleanValue()) {
				cbBranchByCir.getItems().clear();
				cbBranchByCir.getItems().addAll(circuits);
			}
		});

		// ---------------------------------------

		// ---------------------------------------
		Label lblBranches = new Label("Список ветвей");
		gridCircuit.add(lblBranches, 0, rowNum);

		cbBranchInCir = new ComboBox<ElCircuit>();
		gridCircuit.add(cbBranchInCir, 1, rowNum++);

		cbUnionInCir.valueProperty().addListener((property, oldValue, newValue) -> {
			// if (curBranch != null)
			// cancelPodsvetka();
			// if (newValue != null && tpC.isExpanded()
			// && btnUnion.isSelected()) {
			// curUnion = newValue;
			// curUnion.unionPodsvetka(2);
			// }

		});

		// --------------------------------------
		btnDeleteBranchFromCir = new Button("Удалить ветвь из эл. цепи");
		gridCircuit.add(btnDeleteBranchFromCir, 0, rowNum++, 2, 1);

		// --------------------------------------

		btnAddSource = new ToggleButton("Установить источник");
		gridCircuit.add(btnAddSource, 0, rowNum);

		cbSourceByCir = new ComboBox<String>();
		gridCircuit.add(cbSourceByCir, 1, rowNum++);
		cbSourceByCir.setDisable(true);

		Label lblCurSource = new Label("Текущий истоник");
		gridCircuit.add(lblCurSource, 0, rowNum);

		lblSource = new Label("");
		lblSource.setFont(Font.font("sans", FontWeight.BOLD, 18));
		gridCircuit.add(lblSource, 1, rowNum++);

		cbSourceByCir.getSelectionModel().selectedIndexProperty().addListener((property, oldValue, newValue) -> {

			if (newValue.intValue() != -1) {
				curSource = sources.get(newValue.intValue());
				indexSource = sources.indexOf(curSource);

				if (curCircuit != null && curSource != null) {
					curCircuit.setSource(curSource);
					lblSource.setText(curCircuit.getSource().getVarName());
				}

				btnAddSource.setSelected(false);
			}
		});

		btnAddSource.selectedProperty().addListener((property, oldValue, newValue) -> {
			cbSourceByCir.setDisable(!newValue.booleanValue());
			if (newValue.booleanValue()) {
				btnAddUser.setSelected(false);
				cbSourceByCir.getItems().clear();
				cbSourceByCir.getItems()
						.addAll(sources.stream().map((s) -> s.getVarName()).collect(Collectors.toList()));
			}
		});

		// -------------------------------------------------------------------------
		btnAddUser = new ToggleButton("Добавить пользователя");
		gridCircuit.add(btnAddUser, 0, rowNum);

		cbUser = new ComboBox<String>();
		gridCircuit.add(cbUser, 1, rowNum++);

		cbUser.valueProperty().addListener((property, oldValue, newValue) -> {
			if (currentSc != null)
				currentSc.shiftCurrent(false);
			if (newValue != null && tpC.isExpanded()) {
				currentSc = curCircuit.getUser(newValue);
				if (currentSc != null)
					currentSc.shiftCurrent(true);
			}

		});

		btnAddUser.selectedProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue()) {
				btnAddSource.setSelected(false);
				btnSelectElement.setSelected(false);
				btnSelectTerminal.setSelected(false);
			}
		});

		// -------------------------------------------------------------------------
		Separator sep3 = new Separator(Orientation.HORIZONTAL);
		sep3.setMinHeight(30.);
		gridCircuit.add(sep3, 0, rowNum++, 2, 1);
		// -------------------------------------- соединение

		// скролл панель для инспектора эл. цепей
		scrollCir = new ScrollPane();
		scrollCir.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		scrollCir.setLayoutX(0);
		scrollCir.setLayoutY(0);
		scrollCir.setPrefViewportHeight(850);
		scrollCir.setContent(gridCircuit);
		// панели для аккордиона
		tpE = new TitledPane("Схема", gridElement);
		tpC = new TitledPane("Эл. Цепи", scrollCir);

		acc = new Accordion();
		acc.setLayoutX(10);
		acc.setLayoutY(10);
		acc.setStyle("-fx-border-width:4pt;-fx-border-color:olive;");
		acc.getPanes().addAll(tpE, tpC);

		// выключение подсветки элементов при свертывании панели
		// инспектора эл. цепей.
		tpC.expandedProperty().addListener((property, oldValue, newValue) -> {
			if (!newValue) {
				tgSelect.getToggles().stream().forEach((btn) -> btn.setSelected(false));
				cancelPodsvetka();
			}
		});

		scroll = new ScrollPane();
		scroll.setHbarPolicy(ScrollBarPolicy.ALWAYS);
		scroll.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		scroll.setLayoutX(0);
		scroll.setLayoutY(0);
		scroll.setPrefViewportHeight(850);
		scroll.setPrefViewportWidth(Screen.getPrimary().getBounds().getWidth() - 390);
		scroll.setStyle("-fx-background: white");
		scroll.setContent(field);

		sp.getItems().addAll(acc, scroll);
		sp.setDividerPositions(0.15);

		getChildren().addAll(tb, sp);

		// ---------------------------------------------------------------

		// кнопки в панели инструментов
		btnOpen = new Button("Открыть");
		btnOpen.setOnAction((ae) -> {
			handleOpen();
		});

		// -----------------------------------------------------
		btnSave = new Button("Сохранить");
		btnSave.setOnAction((ae) -> {
			handleSave();
		});

		// -----------------------------------------------------

		// -----------------------------------------------------
		btnSaveAs = new Button("Сохранить как");
		btnSaveAs.setOnAction((ae) -> {
			handleSaveAs();
		});

		// ---------------------------------------------------------------
		btnGener = new Button("Создать конструктор");

		// кнопка меню контакт ---------------------------------
		btnContact = new MenuButton("Контакт");

		MenuItem mItemTylL = new MenuItem("тыл конт");
		// mItemHLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemTylL.setOnAction((ae) -> {
			createTyl(ae);
		});

		MenuItem mItemTylR = new MenuItem("тыл конт прав");
		// mItemVLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemTylR.setOnAction((ae) -> {
			createTylR(ae);
		});
		MenuItem mItemFrontL = new MenuItem("фронт конт");
		// mItemVLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemFrontL.setOnAction((ae) -> {
			createFrontL(ae);
		});
		MenuItem mItemFrontR = new MenuItem("фронт конт прав");
		// mItemVLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemFrontR.setOnAction((ae) -> {
			createFrontR(ae);
		});
		MenuItem mItemTroynic = new MenuItem("тройник");
		// mItemVLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemTroynic.setOnAction((ae) -> {
			createTroynic(ae);
		});
		MenuItem mItemTroynicR = new MenuItem("тройник прав");
		// mItemTroynicR.setStyle("-fx-font:bold italic 14pt Times;");
		mItemTroynicR.setOnAction((ae) -> {
			createTroynicR(ae);
		});
		MenuItem mItemTroynicAuto = new MenuItem("тройник автостоп");
		// mItemTroynicAuto.setStyle("-fx-font:bold italic 14pt Times;");
		mItemTroynicAuto.setOnAction((ae) -> {
			createTroynicAuto(ae);
		});
		MenuItem mItemFrontAuto = new MenuItem("фронт автостоп");
		// mItemFrontAuto.setStyle("-fx-font:bold italic 14pt Times;");
		mItemFrontAuto.setOnAction((ae) -> {
			createFrontAuto(ae);
		});
		MenuItem mItemTylAuto = new MenuItem("тыл автостоп");
		// mItemTylAuto.setStyle("-fx-font:bold italic 14pt Times;");
		mItemTylAuto.setOnAction((ae) -> {
			createTylAuto(ae);
		});
		MenuItem mItemPolarContact = new MenuItem("поляризованный контакт");
		// mItemTroynicR.setStyle("-fx-font:bold italic 14pt Times;");
		mItemPolarContact.setOnAction((ae) -> {
			createPolarCont(ae);
		});
		MenuItem mItemStrContact = new MenuItem("контакт автопереключателя");
		// mItemStrContact.setStyle("-fx-font:bold italic 14pt Times;");
		mItemStrContact.setOnAction((ae) -> {
			createStrCont(ae);
		});
		MenuItem mItemStrKyrbelContact = new MenuItem("курбельная заслонка");
		// mItemStrKyrbelContact.setStyle("-fx-font:bold italic 14pt Times;");
		mItemStrKyrbelContact.setOnAction((ae) -> {
			createStrKyrbelCont(ae);
		});

		btnContact.getItems().addAll(mItemFrontL, mItemTylL, mItemTroynic, mItemFrontR, mItemTylR, mItemTroynicR,
				mItemPolarContact, mItemStrContact, mItemStrKyrbelContact, mItemTroynicAuto, mItemFrontAuto,
				mItemTylAuto);
		// --------------------------------------------------------

		// кнопка меню линия ---------------------------------
		btnLine = new MenuButton("Линия");

		MenuItem mItemHLine = new MenuItem("Горизонтальная");
		// mItemHLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemHLine.setOnAction((ae) -> {
			createLine(ae, true);
		});

		MenuItem mItemVLine = new MenuItem("Вертикальная");
		// mItemVLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemVLine.setOnAction((ae) -> {
			createLine(ae, false);
		});
		btnLine.getItems().addAll(mItemHLine, mItemVLine);
		// --------------------------------------------------------
		// кнопка меню полюс ---------------------------------
		btnTerminal = new MenuButton("Полюс");

		MenuItem mItemTerm = new MenuItem("полюс");
		// mItemTerm.setStyle("-fx-font:bold italic 14pt Times;");
		mItemTerm.setOnAction((ae) -> {
			createTerm(ae);
		});

		MenuItem mItemTermDcm = new MenuItem("полюс ДЦМ");
		// mItemTermDcm.setStyle("-fx-font:bold italic 14pt Times;");
		mItemTermDcm.setOnAction((ae) -> {
			createTermDCM(ae);
		});
		btnTerminal.getItems().addAll(mItemTerm, mItemTermDcm);
		// --------------------------------------------------------

		// кнопка меню элементы схем ---------------------------------
		btnDetali = new MenuButton("Элементы");

		Menu mDiod = new Menu("диод");
		// mItemDiod.setStyle("-fx-font:bold italic 14pt Times;");

		MenuItem mItemHDiod = new MenuItem("Горизонтальный");
		// mItemHLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemHDiod.setOnAction((ae) -> {
			createDiod(ae, true);
		});

		MenuItem mItemVDiod = new MenuItem("Вертикальный");
		// mItemVLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemVDiod.setOnAction((ae) -> {
			createDiod(ae, false);
		});

		mDiod.getItems().addAll(mItemHDiod, mItemVDiod);

		Menu mConder = new Menu("Конденсатор");
		// mItemDiod.setStyle("-fx-font:bold italic 14pt Times;");

		MenuItem mItemHConder = new MenuItem("Горизонтальный");
		// mItemHLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemHConder.setOnAction((ae) -> {
			createConder(ae, true);
		});

		MenuItem mItemVConder = new MenuItem("Вертикальный");
		// mItemVLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemVConder.setOnAction((ae) -> {
			createConder(ae, false);
		});

		mConder.getItems().addAll(mItemHConder, mItemVConder);

		MenuItem mItemPred = new MenuItem("Предохранитель");
		// mItemTermDcm.setStyle("-fx-font:bold italic 14pt Times;");
		mItemPred.setOnAction((ae) -> {
			createPred(ae);
		});

		MenuItem mItemBDR = new MenuItem("БДР");
		// mItemBDR.setStyle("-fx-font:bold italic 14pt Times;");
		mItemBDR.setOnAction((ae) -> {
			createBDR(ae);
		});

		MenuItem mItemBKR = new MenuItem("БКР-76");
		// mItemBKR.setStyle("-fx-font:bold italic 14pt Times;");
		mItemBKR.setOnAction((ae) -> {
			createBKR(ae);
		});

		Menu mResistor = new Menu("Резистор");
		// mItemDiod.setStyle("-fx-font:bold italic 14pt Times;");

		MenuItem mItemHRes = new MenuItem("Горизонтальный");
		// mItemHLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemHRes.setOnAction((ae) -> {
			createResistor(ae, true);
		});

		MenuItem mItemVRes = new MenuItem("Вертикальный");
		// mItemVLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemVRes.setOnAction((ae) -> {
			createResistor(ae, false);
		});

		mResistor.getItems().addAll(mItemHRes, mItemVRes);

		Menu mDrossel = new Menu("Дроссель");
		// mDrossel.setStyle("-fx-font:bold italic 14pt Times;");

		MenuItem mItemHDr = new MenuItem("Горизонтальный");
		// mItemHLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemHDr.setOnAction((ae) -> {
			createDrossel(ae, true);
		});

		MenuItem mItemVDr = new MenuItem("Вертикальный");
		// mItemVLine.setStyle("-fx-font:bold italic 14pt Times;");
		mItemVDr.setOnAction((ae) -> {
			createResistor(ae, false);
		});

		mDrossel.getItems().addAll(mItemHDr, mItemVDr);

		MenuItem mItemVyvod = new MenuItem("Вывод блока");
		// mItemVyvod.setStyle("-fx-font:bold italic 14pt Times;");
		mItemVyvod.setOnAction((ae) -> {
			createVyvod(ae);
		});

		MenuItem mItemZPRSH = new MenuItem("зПРШ-2");
		// mItemVyvod.setStyle("-fx-font:bold italic 14pt Times;");
		mItemZPRSH.setOnAction((ae) -> {
			createZPRSH(ae);
		});
		MenuItem mItemSvetGol = new MenuItem("Светофорная головка");
		// mItemSvetGol.setStyle("-fx-font:bold italic 14pt Times;");
		mItemSvetGol.setOnAction((ae) -> {
			createSvetGol(ae);
		});

		btnDetali.getItems().addAll(mDiod, mConder, mItemPred, mResistor, mDrossel, mItemBDR, mItemBKR, mItemVyvod,
				mItemZPRSH, mItemSvetGol);
		// --------------------------------------------------------

		// кнопка меню полюс ---------------------------------
		btnTrans = new MenuButton("Трансформатор");

		MenuItem mItemTrBSSH = new MenuItem("Трансформатор в БСШ");
		// mItemTrBSSH.setStyle("-fx-font:bold italic 14pt Times;");
		mItemTrBSSH.setOnAction((ae) -> {
			createTrBSSH(ae);
		});
		MenuItem mItemTrSOBS2 = new MenuItem("Трансформатор СОБС2");
		// mItemTrSOBS2.setStyle("-fx-font:bold italic 14pt Times;");
		mItemTrSOBS2.setOnAction((ae) -> {
			createTrSOBS(ae);
		});

		btnTrans.getItems().addAll(mItemTrBSSH, mItemTrSOBS2);
		// --------------------------------------------------------
		// меню для добавления полюса кондера в контур---
		MenuItem plusTermConder = new MenuItem("плюсовой вывод");
		MenuItem minusTermConder = new MenuItem("минусовой вывод");
		MenuItem genConder = new MenuItem("Конденсатор");
		contMenuConder = new ContextMenu(plusTermConder, minusTermConder, genConder);

		plusTermConder.setOnAction((ae) -> {
			if (currentSc.getClass().equals(Conder.class)) {
				long i = ((Conder) currentSc).getTerminals().get(0).getIdElementInSchem();
				if (i >= 0) {
					curLoop.addProvod(Helper.getElementById(list, i));
					refreshCbElement();
				}
			}
		});

		minusTermConder.setOnAction((ae) -> {
			if (currentSc.getClass().equals(Conder.class)) {
				long i = ((Conder) currentSc).getMinusTerminals().get(0).getIdElementInSchem();
				if (i >= 0) {
					curLoop.addProvod(Helper.getElementById(list, i));
					refreshCbElement();
				}
			}
		});

		genConder.setOnAction((ae) -> {
			if (currentSc.getClass().equals(Conder.class)) {
				curLoop.addProvod(currentSc);
				refreshCbElement();
			}
		});

		// --------------------------------------

		// меню для добавления полюса в источник постоянного тока---
		MenuItem plus = new MenuItem("плюс батареи");
		MenuItem minus = new MenuItem("минус батареи");
		contMenuTerminal = new ContextMenu(plus, minus);

		plus.setOnAction((ae) -> {
			if (currentSc.getClass().equals(PowerSourceTerminal.class))
				curSource.addTerminal((PowerSourceTerminal) currentSc);
			if (currentSc.getClass().equals(PowerSourceTerminalDCM.class))
				curSource.addTerminal((PowerSourceTerminalDCM) currentSc);
			if (currentSc.getClass().equals(Conder.class)) {
				PowerSourceTerminal psw = ((Conder) currentSc).getTerminals().get(0);
				list.add(psw);
				setElementId(psw);
				psw.setVarName("pstCond" + String.valueOf(psw.getIdElementInSchem()));
				psw.setIndexParent(currentSc.getIdElementInSchem());
				psw.setDrowed(false);
				curSource.addTerminal(psw);
			}
			refreshCbTerminal();
		});
		minus.setOnAction((ae) -> {
			if (currentSc.getClass().equals(PowerSourceTerminal.class))
				((PowerSource) curSource).addMinusTerminal((PowerSourceTerminal) currentSc);
			if (currentSc.getClass().equals(PowerSourceTerminalDCM.class))
				((PowerSource) curSource).addMinusTerminal((PowerSourceTerminalDCM) currentSc);
			if (currentSc.getClass().equals(Conder.class)) {
				PowerSourceTerminal pswM = ((Conder) currentSc).getMinusTerminals().get(0);
				list.add(pswM);
				setElementId(pswM);
				pswM.setVarName("pstMCond" + String.valueOf(pswM.getIdElementInSchem()));
				pswM.setIndexParent(currentSc.getIdElementInSchem());
				pswM.setMinus(true);
				pswM.setDrowed(false);
				((PowerSource) curSource).addMinusTerminal(pswM);
			}

			refreshCbTerminal();
		});
		// --------------------------------------
		// меню для добавления обмотки в ПМПУШ
		MenuItem obm13 = new MenuItem("Обмотка 1-3");
		MenuItem obm42 = new MenuItem("Обмотка 4-2");
		Menu indMenu = new Menu("Обмотка индукции");
		MenuItem obmInd = new MenuItem("обмотка");
		MenuItem plusTermInd = new MenuItem("плюсовой вывод");
		MenuItem minusTermInd = new MenuItem("минусовой вывод");
		indMenu.getItems().addAll(obmInd, plusTermInd, minusTermInd);
		contMenuPMPUSH = new ContextMenu(obm13, obm42, indMenu);

		obm13.setOnAction((ae) -> {
			if (btnAddUser.isSelected() && !btnSelectElement.isSelected()) {
				long i = 0;
				if (currentSc.getClass().equals(RelePMPUSH.class)) {
					i = ((RelePMPUSH) currentSc).getObm13().getIdElementInSchem();
				} else
					i = ((ReleNMPSH03_90) currentSc).getObm13().getIdElementInSchem();
				User u = null;
				if (i != -1)
					u = (User) Helper.getElementById(list, i);
				if (u != null) {
					curCircuit.addUser(u);
					refreshCbUser();
					btnAddUser.setSelected(false);
				}
			}
			if (!btnAddUser.isSelected() && btnSelectElement.isSelected()) {
				Obmotka obm = null;
				if (currentSc.getClass().equals(RelePMPUSH.class))
					obm = ((RelePMPUSH) currentSc).getObm13();
				else
					obm = ((ReleNMPSH03_90) currentSc).getObm13();
				if (obm != null) {
					if (list.indexOf(obm) == -1) {
						list.add(obm);
						setElementId(obm);
						obm.setVarName("obm" + String.valueOf(obm.getIdElementInSchem()));
						obm.setIndexRele(boxAs.getReles().indexOf(currentSc));
					}
					if (curLoop != null) {
						curLoop.addProvod(obm);
						obm.setDrowed(false);
						refreshCbElement();
					}
				}
			}
		});
		obm42.setOnAction((ae) -> {
			if (btnAddUser.isSelected() && !btnSelectElement.isSelected()) {
				long i = 0;
				if (currentSc.getClass().equals(RelePMPUSH.class)) {
					i = ((RelePMPUSH) currentSc).getObm42().getIdElementInSchem();
				} else
					i = ((ReleNMPSH03_90) currentSc).getObm42().getIdElementInSchem();
				User u = null;
				if (i != -1)
					u = (User) Helper.getElementById(list, i);
				if (u != null) {
					curCircuit.addUser(u);
					refreshCbUser();
					btnAddUser.setSelected(false);
				}
			}
			if (!btnAddUser.isSelected() && btnSelectElement.isSelected()) {
				Obmotka obm = null;
				if (currentSc.getClass().equals(RelePMPUSH.class))
					obm = ((RelePMPUSH) currentSc).getObm42();
				else
					obm = ((ReleNMPSH03_90) currentSc).getObm42();
				if (obm != null) {
					if (list.indexOf(obm) == -1) {
						list.add(obm);
						setElementId(obm);
						obm.setVarName("obm" + String.valueOf(obm.getIdElementInSchem()));
						obm.setIndexRele(boxAs.getReles().indexOf(currentSc));
					}
					if (curLoop != null) {
						curLoop.addProvod(obm);
						obm.setDrowed(false);
						refreshCbElement();
					}
				}
			}
		});

		obmInd.setOnAction((ae) -> {
			if (btnAddUser.isSelected() && !btnSelectElement.isSelected()) {
				long i = 0;
				if (currentSc.getClass().equals(ReleNMPSH03_90.class)) {
					Obmotka obm = (Obmotka) ((ReleNMPSH03_90) currentSc).getObmotki().get(2);
					i = obm.getIdElementInSchem();
					if (i == -1) {
						list.add(obm);
						setElementId(obm);
						obm.setVarName("obm" + String.valueOf(obm.getIdElementInSchem()));
						obm.setIndexRele(boxAs.getReles().indexOf(currentSc));
					}

					User u = null;
					i = obm.getIdElementInSchem();
					if (i != -1)
						u = (User) Helper.getElementById(list, i);
					if (u != null) {
						curCircuit.addUser(u);
						refreshCbUser();
						btnAddUser.setSelected(false);
					}
				}
			}
		});

		plusTermInd.setOnAction((ae) -> {
			if (!btnAddUser.isSelected() && btnSelectElement.isSelected()) {
				Obmotka obm = null;
				obm = (Obmotka) ((ReleNMPSH03_90) currentSc).getObmotki().get(2);
				if (obm != null) {
					PowerSourceTerminal pst = obm.getTerminal(0);
					if (list.indexOf(pst) == -1) {
						list.add(pst);
						setElementId(pst);
						pst.setVarName("pst" + String.valueOf(pst.getIdElementInSchem()));
						pst.setIndexParent(obm.getIdElementInSchem());
					}
					if (curLoop != null) {
						curLoop.addProvod(pst);
						obm.setDrowed(false);
						refreshCbElement();
					}
				}
			}

		});

		minusTermInd.setOnAction((ae) -> {
			if (!btnAddUser.isSelected() && btnSelectElement.isSelected()) {
				Obmotka obm = null;
				obm = (Obmotka) ((ReleNMPSH03_90) currentSc).getObmotki().get(2);
				if (obm != null) {
					PowerSourceTerminal pst = obm.getMinusTerminal(0);
					if (list.indexOf(pst) == -1) {
						list.add(pst);
						setElementId(pst);
						pst.setVarName("pstM" + String.valueOf(pst.getIdElementInSchem()));
						pst.setIndexParent(obm.getIdElementInSchem());
					}
					if (curLoop != null) {
						curLoop.addProvod(pst);
						obm.setDrowed(false);
						refreshCbElement();
					}
				}
			}

		});

		// -------------------------------------

		// меню для установки стрелочного контакта ----------------
		MenuItem workCont = new MenuItem("Рабочий контакт");
		MenuItem contrlCont = new MenuItem("Контрольнй контакт");
		MenuItem isPlus = new MenuItem("Замкнут в плюсе");
		MenuItem isMinus = new MenuItem("Замкнут в минусе");
		contMenuStrCont = new ContextMenu(workCont, contrlCont, isPlus, isMinus);

		workCont.setOnAction((ae) -> {
			((StrContact) currentSc).setIsWork(true);
		});
		contrlCont.setOnAction((ae) -> {
			((StrContact) currentSc).setIsWork(false);
		});
		isPlus.setOnAction((ae) -> {
			((StrContact) currentSc).setIsMinus(false);
		});
		isMinus.setOnAction((ae) -> {
			((StrContact) currentSc).setIsMinus(true);
		});
		// -------------------------------------------------------

		// меню для добавления обмотки
		MenuItem obmАВNorm = new MenuItem("Обмотка АВ перевод в -");
		MenuItem obmАCNorm = new MenuItem("Обмотка АС перевод в -");
		MenuItem obmBCNorm = new MenuItem("Обмотка ВС перевод в -");
		MenuItem obmBА = new MenuItem("Обмотка ВА перевод в +");
		MenuItem obmBC = new MenuItem("Обмотка ВС перевод в +");
		MenuItem obmАC = new MenuItem("Обмотка АС перевод в +");
		contMenuMotor = new ContextMenu(obmАВNorm, obmАCNorm, obmBCNorm, obmBА, obmBC, obmАC);

		obmАВNorm.setOnAction((ae) -> {
			addObmMAsUser(currentSc, 0);
			addObmMAsElement(currentSc, 0);
		});
		obmАCNorm.setOnAction((ae) -> {
			addObmMAsUser(currentSc, 1);
			addObmMAsElement(currentSc, 1);
		});
		obmBCNorm.setOnAction((ae) -> {
			addObmMAsUser(currentSc, 2);
			addObmMAsElement(currentSc, 2);
		});
		obmBА.setOnAction((ae) -> {
			addObmMAsUser(currentSc, 3);
			addObmMAsElement(currentSc, 3);
		});
		obmBC.setOnAction((ae) -> {
			addObmMAsUser(currentSc, 4);
			addObmMAsElement(currentSc, 4);
		});
		obmАC.setOnAction((ae) -> {
			addObmMAsUser(currentSc, 5);
			addObmMAsElement(currentSc, 5);
		});

		btnPoint = new Button("Точка");
		btnRele = new Button("Реле");
		btnStrMotor = new Button("МСТ");

		Separator sepTb = new Separator();
		sepTb.setOrientation(Orientation.VERTICAL);
		sepTb.setStyle("-fx-background-color: olive; -fx-background-radius: 2;");

		tb.getItems().addAll(btnOpen, btnSaveAs, sepTb, btnContact, btnRele, btnTerminal, btnLine, btnPoint, btnDetali,
				btnTrans, btnStrMotor);

		// масштабирование поля ---------------------------------------
		scaleY = 1.;
		field.setOnScroll((se) -> {
			if (se.isControlDown()) {
				scaleY += se.getDeltaY() / 1000;
				// System.out.println(scaleY + " -y");
				field.setScaleX(scaleY);
				field.setScaleY(scaleY);
			}
		});
		// ==============================================================

		btnStrMotor.setOnAction(ae -> {
			createMotor(ae);
		});

		btnPoint.setOnAction((ae) -> {
			createSPoint(ae);
		});
		btnRele.setOnAction((ae) -> {
			createRele(ae);
		});
		// для примера

	}

	private void createBKR(ActionEvent ae) {
		BKR_76 bd = new BKR_76("");
		list.add(bd);
		setElementId(bd);
		bd.setVarName("bkr" + String.valueOf(bd.getIdElementInSchem()));
		field.getChildren().add(bd);
		getShift();
		bd.drowElement(Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY));
		initMouse(bd);
	}

	private void createDrossel(ActionEvent ae, boolean b) {
		Drossel res = new Drossel(b);
		list.add(res);
		setElementId(res);
		res.setVarName("dr" + String.valueOf(res.getIdElementInSchem()));
		field.getChildren().add(res);
		getShift();
		res.drowElement(Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY));
		initMouse(res);
	}

	private void createTylAuto(ActionEvent ae) {
		TylAutostopContact tr = new TylAutostopContact("", boxAs.getRele(0), 5, false);
		list.add(tr);
		setElementId(tr);
		tr.setVarName("tcAuto" + String.valueOf(tr.getIdElementInSchem()));
		field.getChildren().add(tr);
		tr.drowElement(Math.rint(field.getWidth() * scroll.getHvalue()),
				Math.rint(field.getHeight() * scroll.getVvalue()));
		initMouse(tr);
	}

	private void createFrontAuto(ActionEvent ae) {
		FrontAutostopContact tr = new FrontAutostopContact("", boxAs.getRele(0), 1, false);
		list.add(tr);
		setElementId(tr);
		tr.setVarName("fcAuto" + String.valueOf(tr.getIdElementInSchem()));
		field.getChildren().add(tr);
		tr.drowElement(Math.rint(field.getWidth() * scroll.getHvalue()),
				Math.rint(field.getHeight() * scroll.getVvalue()));
		initMouse(tr);
	}

	private void createTroynicAuto(ActionEvent ae) {
		TroynicAutostop tr = new TroynicAutostop("", boxAs.getRele(0), 1, false);
		list.add(tr);
		setElementId(tr);
		tr.setVarName("trAuto" + String.valueOf(tr.getIdElementInSchem()));
		field.getChildren().add(tr);
		tr.drowElement(Math.rint(field.getWidth() * scroll.getHvalue()),
				Math.rint(field.getHeight() * scroll.getVvalue()));
		initMouse(tr);
	}

	private void setLampList() {
		String[] lampAr = new String[tfLamp.getText().length() % 2 + 1];
		lampAr = tfLamp.getText().split(",");
		lamps = new ArrayList<Integer>();
		for (int i = 0; i < lampAr.length; i++) {
			try {
				int n = Integer.parseInt(lampAr[i]);
				lamps.add(i, n);
			} catch (NumberFormatException e) {
				System.out.println("ошибка ввода");
			}
		}
		if (currentSc != null)
			((SvetGol) currentSc).setNumColor(lamps);
	}

	private void createSvetGol(ActionEvent ae) {
		List<Integer> num = Arrays.asList(0, 2, 3, 4);
		SvetGol svGol = new SvetGol("", num);
		list.add(svGol);
		setElementId(svGol);
		svGol.setVarName("trans" + String.valueOf(svGol.getIdElementInSchem()));
		field.getChildren().add(svGol);
		getShift();
		svGol.drowElement(Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY));
		initMouse(svGol);
	}

	private void createTrSOBS(ActionEvent ae) {
		TransSobs2Au3 trans = new TransSobs2Au3("", "");
		list.add(trans);
		setElementId(trans);
		trans.setVarName("trans" + String.valueOf(trans.getIdElementInSchem()));
		field.getChildren().add(trans);
		getShift();
		trans.drowElement(Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY));
		initMouse(trans);
	}

	private void createZPRSH(ActionEvent ae) {
		ZPRSH zp = new ZPRSH("");
		list.add(zp);
		setElementId(zp);
		zp.setVarName("zp" + String.valueOf(zp.getIdElementInSchem()));
		field.getChildren().add(zp);
		getShift();
		zp.drowElement(Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY));
		initMouse(zp);
	}

	public void addObmMAsElement(SchemElement curSc, int ind) {
		if (!btnAddUser.isSelected() && btnSelectElement.isSelected()) {
			ObmotkaMotor obm = null;
			if (curSc.getClass().equals(StrMotor.class))
				obm = (ObmotkaMotor) ((StrMotor) curSc).getObmotki().get(ind);
			if (curSc.getClass().equals(AutostopMotor.class))
				obm = (ObmotkaMotor) ((AutostopMotor) curSc).getObmotki().get(ind);
			if (obm != null) {
				list.add(obm);
				setElementId(obm);
				obm.setVarName("obmM" + String.valueOf(obm.getIdElementInSchem()));
				obm.setIndexMotor(curSc.getIdElementInSchem());
				if (curLoop != null) {
					curLoop.addProvod(obm);
					obm.setDrowed(false);
					refreshCbElement();
				}
			}
		}
	}

	public void addObmMAsUser(SchemElement curSc, int ind) {
		if (btnAddUser.isSelected() && !btnSelectElement.isSelected()) {
			long i = -1;
			if (curSc.getClass().equals(StrMotor.class))
				i = ((ObmotkaMotor) ((StrMotor) curSc).getObmotki().get(ind)).getIdElementInSchem();
			if (curSc.getClass().equals(AutostopMotor.class))
				i = ((ObmotkaMotor) ((AutostopMotor) curSc).getObmotki().get(ind)).getIdElementInSchem();
			User u = null;
			if (i != -1)
				u = (User) Helper.getElementById(list, i);
			if (u != null) {
				curCircuit.addUser(u);
				refreshCbUser();
				btnAddUser.setSelected(false);
			}

		}
	}

	public void createSource(ActionEvent ae, boolean isConst) {
		curSource = new PowerSource();
		curSource.setIsConst(isConst);
		sources.add(curSource);
		indexSource = sources.indexOf(curSource);
		curSource.setVarName("source" + indexSource);
		tfSourceName.setText(curSource.getVarName());
		cbSource.getItems().add(indexSource, curSource.getVarName());
		cbSource.getSelectionModel().select(indexSource);
	}

	private void createStrKyrbelCont(ActionEvent ae) {
		StrKyrbelContact kyr = new StrKyrbelContact(true);
		list.add(kyr);
		setElementId(kyr);
		kyr.setVarName("kyr" + String.valueOf(kyr.getIdElementInSchem()));
		field.getChildren().add(kyr);
		getShift();
		kyr.drowElement(Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY));
		initMouse(kyr);
	}

	private void createVyvod(ActionEvent ae) {
		Vyvod v = new Vyvod("11", "", true);
		list.add(v);
		setElementId(v);
		v.setVarName("v" + String.valueOf(v.getIdElementInSchem()));
		field.getChildren().add(v);
		getShift();
		v.drowElement(Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY));
		initMouse(v);
	}

	private void createStrCont(ActionEvent ae) {
		StrContact stc = new StrContact(true, true, 11, boxAs.getStrMotors().get(0));
		list.add(stc);
		setElementId(stc);
		stc.setVarName("stc" + String.valueOf(stc.getIdElementInSchem()));
		field.getChildren().add(stc);
		stc.drowElement(Math.rint(field.getWidth() * scroll.getHvalue()),
				Math.rint(field.getHeight() * scroll.getVvalue()));
		initMouse(stc);
	}

	private void createMotor(ActionEvent ae) {
		StrMotor sm = new StrMotor("", "");
		list.add(sm);
		setElementId(sm);
		sm.setVarName("sm" + String.valueOf(sm.getIdElementInSchem()));
		field.getChildren().add(sm);
		getShift();
		sm.drowElement(Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY));
		initMouse(sm);
	}

	private void createTrBSSH(ActionEvent ae) {
		TransBSSH trans = new TransBSSH("", "");
		list.add(trans);
		setElementId(trans);
		trans.setVarName("trans" + String.valueOf(trans.getIdElementInSchem()));
		field.getChildren().add(trans);
		getShift();
		trans.drowElement(Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY));
		initMouse(trans);
	}

	private void setElementId(SchemElement se) {
		elementId = list.size() - 1;
		while (true) {

			if (list.stream().anyMatch((sc) -> sc.getIdElementInSchem() == elementId))
				elementId++;
			else {
				se.setIdElementInSchem(elementId);
				break;
			}
		}
	}

	private void createBDR(ActionEvent ae) {
		BDR bd = new BDR();
		list.add(bd);
		setElementId(bd);
		bd.setVarName("bdr" + String.valueOf(bd.getIdElementInSchem()));
		field.getChildren().add(bd);
		getShift();
		bd.drowElement(Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY));
		initMouse(bd);
	}

	private void createResistor(ActionEvent ae, boolean isHorizont) {
		Resistor res = new Resistor("", "", isHorizont);
		list.add(res);
		setElementId(res);
		res.setVarName("res" + String.valueOf(res.getIdElementInSchem()));
		field.getChildren().add(res);
		getShift();
		res.drowElement(Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY));
		initMouse(res);
	}

	private void createPred(ActionEvent ae) {
		Predochran pd = new Predochran("", "", "");
		list.add(pd);
		setElementId(pd);
		pd.setVarName("pd" + String.valueOf(pd.getIdElementInSchem()));
		field.getChildren().add(pd);
		getShift();
		pd.drowElement(Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY));
		initMouse(pd);
	}

	private void createConder(ActionEvent ae, boolean isHorizont) {
		Conder cd = new Conder(isHorizont);
		list.add(cd);
		setElementId(cd);
		cd.setVarName("cd" + String.valueOf(cd.getIdElementInSchem()));
		field.getChildren().add(cd);
		getShift();
		cd.drowElement(Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY));
		initMouse(cd);
	}

	private void createDiod(ActionEvent ae, boolean isHorizont) {
		Diod d = new Diod(isHorizont);
		list.add(d);
		setElementId(d);
		d.setVarName("d" + String.valueOf(d.getIdElementInSchem()));
		field.getChildren().add(d);
		getShift();
		d.drowElement(Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY));
		initMouse(d);
	}

	private void createPolarCont(ActionEvent ae) {
		PolarContact pc = new PolarContact("", "", boxAs.getRele(0), 1, true);
		list.add(pc);
		setElementId(pc);
		pc.setVarName("pc" + String.valueOf(pc.getIdElementInSchem()));
		field.getChildren().add(pc);
		pc.drowElement(Math.rint(field.getWidth() * scroll.getHvalue()),
				Math.rint(field.getHeight() * scroll.getVvalue()));
		initMouse(pc);
	}

	private void fillCbTerminalItems() {
		cbTerminalCxema.getItems().clear();

		for (Source source : sources) {
			if (!source.getTerminals().isEmpty())
				source.getTerminals().forEach((t) -> cbTerminalCxema.getItems().add(t.getVarName()));
			if (source.getIsConst()) {
				if (!source.getMinusTerminals().isEmpty())
					source.getMinusTerminals().forEach((t) -> cbTerminalCxema.getItems().add(t.getVarName()));
			}
		}
	}

	private void refreshBranchInCir() {
		// cancelPodsvetka();
		if (curCircuit != null) {
			cbBranchInCir.getItems().clear();
			cbBranchInCir.getItems().addAll(curCircuit.getBranches());
			cbBranchInCir.setValue((!curCircuit.getBranches().isEmpty()) ? curCircuit.getBranches().get(0) : null);
			// if (btnCircuit.isSelected() && tpC.isExpanded())
			// curCircuit.circuitPodsvetka(3);
		}

	}

	private void cancelPodsvetka() {
		if (currentSc != null)
			currentSc.shiftCurrent(false);
		if (!loops.isEmpty())
			loops.stream().forEach((l) -> l.loopPodsvetka(0));

	}

	private void refreshCbTerminal() {
		cbTerminal.getItems().clear();
		if (curSource != null && !curSource.getTerminals().isEmpty()) {
			cbTerminal.getItems()
					.addAll(curSource.getTerminals().stream().map((t) -> t.getVarName()).collect(Collectors.toList()));
			if (curSource.getIsConst() && !((Source) curSource).getMinusTerminals().isEmpty()) {
				cbTerminal.getItems().addAll(((Source) curSource).getMinusTerminals().stream()
						.map((mt) -> mt.getVarName()).collect(Collectors.toList()));
			}
			cbTerminal.setValue(curSource.getTerminals().get(0).getVarName());
		}
		btnSelectTerminal.setSelected(false);
	}

	/**
	 *
	 */
	public void refreshLoopInUnion() {
		cancelPodsvetka();
		if (curUnion != null) {
			cbLoopInUnion.getItems().clear();
			cbLoopInUnion.getItems().addAll(curUnion.getUnion());
			cbLoopInUnion.setValue((!curUnion.getUnion().isEmpty()) ? curUnion.getUnion().get(0) : null);
			if (btnUnion.isSelected() && tpC.isExpanded())
				curUnion.unionPodsvetka(2);
		}
	}

	public void refreshUnionInCir() {
		cancelPodsvetka();
		if (curCircuit != null) {
			cbUnionInCir.getItems().clear();
			cbUnionInCir.getItems().addAll(curCircuit.getCircuit());
			cbUnionInCir.setValue((!curCircuit.getCircuit().isEmpty()) ? curCircuit.getCircuit().get(0) : null);
			if (btnCircuit.isSelected() && tpC.isExpanded())
				curCircuit.circuitPodsvetka(3);
		}

	}

	private void setLinePoints() {
		String[] lpoints = new String[tfPoint.getText().length() % 2 + 1];
		lpoints = tfPoint.getText().split(",");
		points = new Double[lpoints.length];
		for (int i = 0; i < lpoints.length; i++) {
			points[i] = Double.valueOf(lpoints[i]);
		}
		if (currentSc != null) {
			((SLine) currentSc).getLine().getPoints().clear();
			((SLine) currentSc).getLine().getPoints().addAll(points);
			((SLine) currentSc).setRectPosition();
		}
	}

	private void createFrontL(ActionEvent ae) {
		FrontContact fc = new FrontContact("", "", boxAs.getRele(0), 1, true);
		list.add(fc);
		setElementId(fc);
		fc.setVarName("fc" + String.valueOf(fc.getIdElementInSchem()));
		field.getChildren().add(fc);
		fc.drowElement(Math.rint(field.getWidth() * scroll.getHvalue()),
				Math.rint(field.getHeight() * scroll.getVvalue()));
		initMouse(fc);
	}

	private void createFrontR(ActionEvent ae) {
		FrontRightContact frc = new FrontRightContact("", "", boxAs.getRele(0), 1, true);
		list.add(frc);
		setElementId(frc);
		frc.setVarName("frc" + String.valueOf(frc.getIdElementInSchem()));
		field.getChildren().add(frc);
		frc.drowElement(Math.rint(field.getWidth() * scroll.getHvalue()),
				Math.rint(field.getHeight() * scroll.getVvalue()));
		initMouse(frc);
	}

	private void createTroynic(ActionEvent ae) {
		Troynic tr = new Troynic("", "", boxAs.getRele(0), 1, true);
		list.add(tr);
		setElementId(tr);
		tr.setVarName("tr" + String.valueOf(tr.getIdElementInSchem()));
		field.getChildren().add(tr);
		tr.drowElement(Math.rint(field.getWidth() * scroll.getHvalue()),
				Math.rint(field.getHeight() * scroll.getVvalue()));
		initMouse(tr);
	}

	private void createTroynicR(ActionEvent ae) {
		TroynicRight trr = new TroynicRight("", "", boxAs.getRele(0), 1, false);
		list.add(trr);
		setElementId(trr);
		trr.setVarName("trr" + String.valueOf(trr.getIdElementInSchem()));
		field.getChildren().add(trr);
		trr.drowElement(Math.rint(field.getWidth() * scroll.getHvalue()),
				Math.rint(field.getHeight() * scroll.getVvalue()));
		initMouse(trr);
	}

	private void createRele(ActionEvent ae) {
		Rele r = new ReleNMSH(cbRele.getValue(), "", false);
		list.add(r);
		setElementId(r);
		r.setVarName("r" + String.valueOf(r.getIdElementInSchem()));
		field.getChildren().add(r);
		r.drowElement(Math.rint(field.getWidth() * scroll.getHvalue()),
				Math.rint(field.getHeight() * scroll.getVvalue()));
		initMouse(r);
	}

	private void createTyl(ActionEvent ae) {
		TylContact tc = new TylContact("", "", boxAs.getRele(0), 1, true);
		list.add(tc);
		setElementId(tc);
		tc.setVarName("tc" + String.valueOf(tc.getIdElementInSchem()));
		field.getChildren().add(tc);
		tc.drowElement(Math.rint(field.getWidth() * scroll.getHvalue()),
				Math.rint(field.getHeight() * scroll.getVvalue()));
		initMouse(tc);
	}

	private void createTylR(ActionEvent ae) {
		TylRightContact trc = new TylRightContact("", "", boxAs.getRele(0), 1, true);
		list.add(trc);
		setElementId(trc);
		trc.setVarName("trc" + String.valueOf(trc.getIdElementInSchem()));
		field.getChildren().add(trc);
		trc.drowElement(Math.rint(field.getWidth() * scroll.getHvalue()),
				Math.rint(field.getHeight() * scroll.getVvalue()));
		initMouse(trc);

	}

	private void createTerm(ActionEvent ae) {
		PowerSourceTerminal pst = new PowerSourceTerminal("", "");
		list.add(pst);
		setElementId(pst);
		pst.setVarName("pst" + String.valueOf(pst.getIdElementInSchem()));
		field.getChildren().add(pst);
		pst.drowElement(Math.rint(field.getWidth() * scroll.getHvalue()),
				Math.rint(field.getHeight() * scroll.getVvalue()));
		initMouse(pst);
	}

	private void createTermDCM(ActionEvent ae) {
		PowerSourceTerminalDCM pstd = new PowerSourceTerminalDCM("", "");
		list.add(pstd);
		setElementId(pstd);
		pstd.setVarName("pstd" + String.valueOf(pstd.getIdElementInSchem()));
		field.getChildren().add(pstd);
		pstd.drowElement(Math.rint(field.getWidth() * scroll.getHvalue()),
				Math.rint(field.getHeight() * scroll.getVvalue()));
		initMouse(pstd);
	}

	private void createSPoint(ActionEvent ae) {
		SPoint sp = new SPoint();
		list.add(sp);
		setElementId(sp);
		sp.setVarName("sp" + String.valueOf(sp.getIdElementInSchem()));
		field.getChildren().add(sp);
		getShift();
		sp.drowElement(Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY));
		initMouse(sp);
	}

	private void createLine(ActionEvent ae, boolean isHorizont) {

		double dlinaX, dlinaY;

		dlinaX = (isHorizont) ? 200 : 0;
		dlinaY = (isHorizont) ? 0 : 200;

		getShift();
		points = new Double[] { Math.rint(field.getWidth() * scroll.getHvalue() + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + addY),
				Math.rint(field.getWidth() * scroll.getHvalue() + dlinaX + addX),
				Math.rint(field.getHeight() * scroll.getVvalue() + dlinaY + addY) };

		SLine sl = new SLine(points);
		list.add(sl);
		setElementId(sl);
		sl.setVarName("sl" + String.valueOf(sl.getIdElementInSchem()));
		field.getChildren().add(sl);
		sl.drowElement(50, 50);
		initMouse(sl);

	}

	// получение смещения элемента при создании, в зависимости от полложения
	// бегунка
	private void getShift() {
		addX = (scroll.getHvalue() >= 0.5) ? -100 : 100;
		addY = (scroll.getVvalue() >= 0.5) ? -100 : 100;
	}

	private void initCirciutInspector() {
		if (!loops.isEmpty()) {
			cbLoop.getItems().addAll(loops);
			cbLoop.getSelectionModel().select(0);
		}
		if (!unions.isEmpty()) {
			cbUnion.getItems().addAll(unions);
			cbUnion.getSelectionModel().select(0);
		}
		if (!sources.isEmpty()) {
			cbSource.getItems().addAll(sources.stream().map((s) -> s.getVarName()).collect(Collectors.toList()));
			cbSource.getSelectionModel().select(0);
			fillCbTerminalItems();
		}
		if (!circuits.isEmpty()) {
			cbCircuit.getItems().addAll(circuits);
			cbCircuit.getSelectionModel().select(0);
		}
	}

	private void initInspector(SchemElement sc) {
		if (currentSc != null)
			currentSc.shiftCurrent(false);
		currentSc = sc;
		if (tpE.isExpanded())
			currentSc.shiftCurrent(true);
		cbTerminalCxema.setDisable(true);
		btnDelete.setDisable(false);
		tfVarName.setDisable(false);
		tfVarName.setText(sc.getVarName());
		chbFirstLeft.setDisable(true);
		tfTok.setDisable(true);
		cbStrNum.setDisable(true);
		tfLamp.setDisable(true);
		lblId.setText(String.valueOf(currentSc.getIdElementInSchem()));

		if (!(sc.getClass().equals(PowerSourceTerminal.class) || sc.getClass().equals(PowerSourceTerminalDCM.class))) {
			chbPstLeft.setDisable(true);
		}
		if (!sc.getClass().equals(SLine.class)) {
			btnAddPoint.setSelected(false);
			tfLayoutX.setText(String.valueOf(sc.getLayoutX()));
			tfLayoutY.setText(String.valueOf(sc.getLayoutY()));
			tfLayoutX.setDisable(false);
			tfLayoutY.setDisable(false);
		}
		// если добавляем
		// контакты-----------------------------------------------------------
		if (sc.getClass().getSuperclass().equals(Contact.class)
				|| sc.getClass().getSuperclass().equals(RightContact.class)) {
			cbRele.setDisable(false);
			tfName.setEditable(false);
			tfName.setDisable(false);
			tfPoint.setDisable(true);
			tfPlace.setEditable(false);
			tfLayoutX.setDisable(false);
			tfLayoutY.setDisable(false);
			btnAddPoint.setDisable(true);
			Rele r = ((Contact) sc).getRele();
			if (r != null)
				cbRele.getSelectionModel().select(boxAs.getReles().indexOf(r));
			tfName.setText(((Contact) sc).getRele().getTxName().getText());
			tfPlace.setText(sc.getTxPlace().getText());
			chbIsZam.setDisable(false);
			chbIsZam.setSelected(((Contact) sc).getIsZam());
			cbNum.setDisable(false);
			// if (!sc.getClass().equals(PolarContact.class))
			// cbNum.setValue(((Contact) sc).getContNum());
			// else
			cbNum.setValue(((Contact) sc).getContNum());

		}
		// если добавляем
		// реле-----------------------------------------------------------
		if (sc.getClass().getSuperclass().equals(Rele.class)) {
			chbIsZam.setDisable(true);
			cbNum.setDisable(true);
			cbRele.setDisable(false);
			tfName.setDisable(true);
			tfPoint.setDisable(true);
			tfPlace.setDisable(true);
			btnAddPoint.setDisable(true);
			tfName.setText(sc.getTxName().getText());
			tfPlace.setText(sc.getTxPlace().getText());
			chbFirstLeft.setDisable(false);
			chbFirstLeft.setSelected(((Rele) sc).getIsFirstLeft());

		}

		// если добавляем
		// полюс -----------------------------------------------------------
		if (sc.getClass().equals(PowerSourceTerminal.class) || sc.getClass().equals(PowerSourceTerminalDCM.class)
				|| sc.getClass().equals(Vyvod.class)) {
			cbTerminalCxema.setDisable(false);
			chbIsZam.setDisable(true);
			cbNum.setDisable(true);
			cbRele.setDisable(true);
			tfName.setEditable(true);
			tfName.setDisable(false);
			tfPoint.setDisable(true);
			tfPlace.setDisable(true);
			btnAddPoint.setDisable(true);
			tfName.setText(sc.getTxName().getText());
			chbPstLeft.setDisable(false);
			if (sc.getClass().equals(Vyvod.class))
				chbPstLeft.setSelected(((Vyvod) sc).getIsLeft());
			else
				chbPstLeft.setSelected(getLeftTerminal(sc));

		}
		// если добавляем
		// предохранитель
		// -----------------------------------------------------------
		if (sc.getClass().equals(Predochran.class)) {
			chbIsZam.setDisable(true);
			cbNum.setDisable(true);
			cbRele.setDisable(true);
			tfName.setEditable(true);
			tfName.setDisable(false);
			tfPoint.setDisable(true);
			tfPlace.setDisable(false);
			btnAddPoint.setDisable(true);
			tfName.setText(sc.getTxName().getText());
			tfPlace.setText(sc.getTxPlace().getText());
			chbPstLeft.setDisable(true);
			tfTok.setDisable(false);
			tok.setText("Номирал предохранителя");
		}
		// если добавляем
		// зПРШ-2
		// -----------------------------------------------------------
		if (sc.getClass().equals(ZPRSH.class)) {
			chbIsZam.setDisable(true);
			cbNum.setDisable(true);
			cbRele.setDisable(true);
			tfName.setEditable(true);
			tfName.setDisable(false);
			tfPoint.setDisable(true);
			tfPlace.setDisable(false);
			btnAddPoint.setDisable(true);
			tfPlace.setText(sc.getTxPlace().getText());
			chbPstLeft.setDisable(true);
			tfTok.setDisable(false);
		}
		// --------------------------------------------
		// если добавляем
		// БКР
		// -----------------------------------------------------------
		if (sc.getClass().equals(BKR_76.class)) {
			chbIsZam.setDisable(true);
			cbNum.setDisable(true);
			cbRele.setDisable(true);
			tfName.setEditable(true);
			tfName.setDisable(true);
			tfPoint.setDisable(true);
			tfPlace.setDisable(false);
			tfPlace.setEditable(true);
			btnAddPoint.setDisable(true);
			tfPlace.setText(sc.getTxPlace().getText());
			chbPstLeft.setDisable(true);
			tfTok.setDisable(false);
		}
		// если добавляем
		// Транс СОБС2
		// -----------------------------------------------------------
		if (sc.getClass().equals(TransSobs2Au3.class)) {
			chbIsZam.setDisable(true);
			cbNum.setDisable(true);
			cbRele.setDisable(true);
			tfName.setEditable(true);
			tfName.setDisable(false);
			tfPoint.setDisable(true);
			tfPlace.setDisable(false);
			tfPlace.setEditable(true);
			btnAddPoint.setDisable(true);
			tfPlace.setText(sc.getTxPlace().getText());
			tfName.setText(sc.getTxName().getText());
			chbPstLeft.setDisable(true);
			tfTok.setDisable(false);
		}
		// если добавляем
		// светофорную головку
		// -----------------------------------------------------------
		if (sc.getClass().equals(SvetGol.class)) {
			chbIsZam.setDisable(true);
			cbNum.setDisable(true);
			cbRele.setDisable(true);
			tfName.setEditable(true);
			tfName.setDisable(false);
			tfPoint.setDisable(true);
			tfPlace.setDisable(true);
			btnAddPoint.setDisable(true);
			tfName.setText(sc.getTxName().getText());
			chbPstLeft.setDisable(true);
			tfTok.setDisable(false);
			tfLamp.setDisable(false);
			String sn = "";
			for (Integer i : ((SvetGol) sc).getNumColor()) {
				sn += String.format("%d", i) + ",";
			}
			sn = sn.substring(0, sn.length() - 1);
			tfLamp.setText(sn);
		}

		// если добавляем
		// линию -----------------------------------------------------------
		if (sc.getClass().equals(SLine.class)) {
			chbIsZam.setDisable(true);
			cbNum.setDisable(true);
			cbRele.setDisable(true);
			tfName.setDisable(true);
			tfPlace.setDisable(true);
			tfPoint.setDisable(false);
			btnAddPoint.setDisable(false);
			String po = "";
			for (double point : ((SLine) sc).getLine().getPoints()) {
				po += String.format("%.0f", point) + ", ";
			}
			po = po.substring(0, po.length() - 2);
			tfPoint.setText(po);
			// System.out.println(po);
		}
		// если добавляем
		// точку,диод
		// резистор, БДР, транс БСШ
		// курбельную заслонку-------------------------
		if (sc.getClass().equals(SPoint.class) || sc.getClass().equals(Diod.class)
				|| sc.getClass().equals(Resistor.class) || sc.getClass().equals(BDR.class)
				|| sc.getClass().equals(TransBSSH.class) || sc.getClass().equals(Drossel.class)
				|| sc.getClass().equals(StrKyrbelContact.class)) {
			chbIsZam.setDisable(true);
			cbNum.setDisable(true);
			tfName.setDisable(true);
			cbRele.setDisable(true);
			tfPoint.setDisable(true);
			tfPlace.setDisable(true);
			btnAddPoint.setDisable(true);
		}

		// если добавляем
		// конденсатор,
		if (sc.getClass().equals(Conder.class)) {

			chbIsZam.setDisable(true);
			cbNum.setDisable(true);
			tfName.setDisable(true);
			cbRele.setDisable(true);
			tfPoint.setDisable(true);
			tfPlace.setDisable(true);
			btnAddPoint.setDisable(true);
			tfTok.setDisable(false);
			tok.setText("Время разряда");
			tfTok.setText(String.valueOf(((Conder) sc).getRazrad()));

		}

		// если добавляем
		// стрелочный мотор -------------------------
		if (sc.getClass().equals(StrMotor.class)) {
			chbIsZam.setDisable(true);
			cbNum.setDisable(true);
			tfName.setDisable(true);
			cbRele.setDisable(true);
			tfPoint.setDisable(true);
			tfPlace.setDisable(true);
			btnAddPoint.setDisable(true);
			cbStrNum.setDisable(false);
			cbStrNum.getSelectionModel().select(boxAs.getStrMotors().indexOf(sc));
		}
		// если добавляем
		if (sc.getClass().equals(StrContact.class)) {
			chbIsZam.setDisable(false);
			cbNum.setDisable(true);
			tfName.setDisable(true);
			cbRele.setDisable(true);
			tfPoint.setDisable(true);
			tfPlace.setDisable(false);
			tfPlace.setEditable(true);
			btnAddPoint.setDisable(true);
			chbPstLeft.setDisable(false);
			cbStrNum.setDisable(false);
			cbStrNum.getSelectionModel().select(boxAs.getStrMotors().indexOf(((StrContact) sc).getMotor()));

		}

	} // initInspector

	// ----------------------------------------------------------------------

	/**
	 * @param sc
	 * @return
	 */
	private boolean getLeftTerminal(SchemElement sc) {
		return (sc.getClass().equals(PowerSourceTerminal.class)) ? ((PowerSourceTerminal) sc).getIsLeft()
				: ((PowerSourceTerminalDCM) sc).getIsLeft();
	}

	// обработка событий мыши для вновь созданного
	// элемента-------------------------------
	private void initMouse(SchemElement sc) {
		sc.setOnMousePressed((me) -> {
			// запись позиции когда нажата кнопка мыши
			initX = sc.getTranslateX();
			initY = sc.getTranslateY();
			dragAnchor = new Point2D(me.getSceneX(), me.getSceneY());
		});

		// перетскивание
		sc.setOnMouseDragged((me) -> {
			if (tpE.isExpanded()) {
				dragX = me.getSceneX() - dragAnchor.getX();
				dragY = me.getSceneY() - dragAnchor.getY();
				// calculate new position
				newXPosition = initX + dragX;
				newYPosition = initY + dragY;
				if (!btnAddPoint.isSelected()) {
					sc.setTranslateX(newXPosition);
					sc.setTranslateY(newYPosition);
				}
			}
		});

		sc.setOnMouseReleased((me) -> {
			// добавление вершины для линии ---------------------------------
			if (sc.getClass().equals(SLine.class)) {
				if (btnAddPoint.isSelected()) {
					deltaLine = ((SLine) sc).getLastPoint();
					if (Math.abs(dragX) > Math.abs(dragY)) {
						((SLine) sc).getLine().getPoints().addAll(deltaLine.getX() + newXPosition, deltaLine.getY());
					} else {
						((SLine) sc).getLine().getPoints().addAll(deltaLine.getX(), deltaLine.getY() + newYPosition);
					}
					initInspector(sc);
					setLinePoints();
				} else {

					Double[] points = new Double[((SLine) sc).getLine().getPoints().size()];
					for (int i = 0; i < ((SLine) sc).getLine().getPoints().size(); i++) {
						points[i] = ((SLine) sc).getLine().getPoints().get(i)
								+ ((i == 0 || i % 2 == 0) ? newXPosition : newYPosition);
					}
					((SLine) sc).getLine().getPoints().clear();
					((SLine) sc).getLine().getPoints().addAll(points);
				}
			} else { // перетаскивание если не линия
				sc.setLayoutX(sc.getLayoutX() + newXPosition);
				sc.setLayoutY(sc.getLayoutY() + newYPosition);
			}
			// -------------------------------------------------------------------------
			sc.setTranslateX(0);
			sc.setTranslateY(0);
			newXPosition = 0.;
			newYPosition = 0.;
			initInspector(sc);
		});

		sc.setOnMouseClicked((e) -> {
			// добавление элементов в контур
			if ((e.getClickCount() == 2) && (e.getButton().equals(MouseButton.PRIMARY)) && btnSelectElement.isSelected()
					&& tpC.isExpanded()) {
				if (curLoop != null) {
					if (sc.getClass().equals(Troynic.class) || sc.getClass().equals(TroynicRight.class)
							|| sc.getClass().equals(TroynicAutostop.class)
							|| sc.getClass().equals(PolarContact.class)) {
						currentSc = sc;
						contMenu.show(this, e.getSceneX(), e.getSceneY());
						return;
					}
					if (sc.getClass().equals(RelePMPUSH.class) || sc.getClass().equals(ReleNMPSH03_90.class)) {
						currentSc = sc;
						contMenuPMPUSH.show(this, e.getSceneX(), e.getSceneY());
						return;
					}
					if (sc.getClass().equals(Conder.class)) {
						currentSc = sc;
						contMenuConder.show(this, e.getSceneX(), e.getSceneY());
						return;
					}
					if (sc.getClass().equals(StrMotor.class)) {
						currentSc = sc;
						contMenuMotor.show(this, e.getSceneX(), e.getSceneY());
						return;
					}
					if (sc.getClass().equals(AutostopMotor.class)) {
						currentSc = sc;
						contMenuMotor.show(this, e.getSceneX(), e.getSceneY());
						return;
					}
					if (sc.getClass().equals(TransBSSH.class)) {
						currentSc = sc;
						contMenuTransBSSH.show(this, e.getSceneX(), e.getSceneY());
						return;
					}
					if (sc.getClass().equals(TransSobs2Au3.class)) {
						currentSc = sc;
						contMenuTransSobs2.show(this, e.getSceneX(), e.getSceneY());
						return;
					}
					if (sc.getClass().equals(BDR.class)) {
						currentSc = sc;
						contMenuBDR.show(this, e.getSceneX(), e.getSceneY());
						return;
					}
					if (sc.getClass().equals(BKR_76.class)) {
						currentSc = sc;
						contMenuBDR.show(this, e.getSceneX(), e.getSceneY());
						return;
					}
					if (sc.getClass().equals(SvetGol.class)) {
						currentSc = sc;
						contMenuSvetGol.show(this, e.getSceneX(), e.getSceneY());
						return;
					}

					curLoop.addProvod(sc);
					refreshCbElement();
				}
			}
			// добавление полюсов в источник питания
			if ((e.getClickCount() == 2) && (e.getButton().equals(MouseButton.PRIMARY))
					&& btnSelectTerminal.isSelected() && tpC.isExpanded()) {
				if (curSource != null) {
					if (!curSource.equals(boxAs.getSources().get(0))) {
						if (!curSource.getIsConst()) {

							if (sc.getClass().equals(PowerSourceTerminal.class)
									|| sc.getClass().getSuperclass().equals(PowerSourceTerminal.class)) {
								curSource.addTerminal(
										(sc.getClass().equals(PowerSourceTerminal.class) ? (PowerSourceTerminal) sc
												: (PowerSourceTerminalDCM) sc));
								refreshCbTerminal();
							}
						} else {
							contMenuTerminal.show(this, e.getSceneX(), e.getSceneY());
							currentSc = sc;
						}
					} // boxAs
				} // null

			}

			// добавление источника(элемента) в список источников
			if ((e.getClickCount() == 2) && (e.getButton().equals(MouseButton.PRIMARY))
					&& btnSelectElementSource.isSelected() && tpC.isExpanded()) {
				try {
					SchemElement scEl = sc;
					if (sc.getClass().equals(ReleNMPSH03_90.class)) {
						scEl = (Obmotka) ((ReleNMPSH03_90) sc).getObmotki().get(2);
						if (scEl.getIdElementInSchem() == -1) {
							list.add(scEl);
							setElementId(scEl);
							scEl.setVarName("obmInd" + String.valueOf(scEl.getIdElementInSchem()));
							scEl.setDrowed(false);
						}
					}
					curSource = (Source) scEl;
					if (curSource != null) {
						sources.add(curSource);
						indexSource = sources.indexOf(curSource);
						tfSourceName.setText(curSource.getVarName());
						cbSource.getItems().add(indexSource, curSource.getVarName());
						cbSource.getSelectionModel().select(indexSource);
						btnSelectElementSource.setSelected(false);
						for (PowerSourceTerminal pst : curSource.getTerminals()) {
							if (pst.getIdElementInSchem() == -1) {
								list.add(pst);
								setElementId(pst);
								pst.setVarName("pst" + String.valueOf(pst.getIdElementInSchem()));
								pst.setDrowed(false);

							}
							pst.setIndexParent(((SchemElement) curSource).getIdElementInSchem());

						}
						if (curSource.getIsConst()) {
							for (PowerSourceTerminal pst : curSource.getMinusTerminals()) {
								if (pst.getIdElementInSchem() == -1) {
									list.add(pst);
									setElementId(pst);
									pst.setVarName("pstM" + String.valueOf(pst.getIdElementInSchem()));
									pst.setDrowed(false);
								}
								pst.setIndexParent(((SchemElement) curSource).getIdElementInSchem());

							}
						}
					}
				} catch (Exception ex) {
					System.out.println("Элемент не является источником");
					btnSelectElementSource.setSelected(false);
				}

			}

			// добавление потребителей в цепь
			if ((e.getClickCount() == 2) && (e.getButton().equals(MouseButton.PRIMARY)) && btnAddUser.isSelected()
					&& tpC.isExpanded()) {
				if (curCircuit != null) {
					try {
						if ((sc.getClass().equals(RelePMPUSH.class) || sc.getClass().equals(ReleNMPSH03_90.class))) {
							currentSc = sc;
							contMenuPMPUSH.show(this, e.getSceneX(), e.getSceneY());
							return;
						}
						if (sc.getClass().equals(StrMotor.class)) {
							currentSc = sc;
							contMenuMotor.show(this, e.getSceneX(), e.getSceneY());
							return;
						}
						if (sc.getClass().equals(StrMotor.class) || sc.getClass().equals(AutostopMotor.class)) {
							currentSc = sc;
							contMenuMotor.show(this, e.getSceneX(), e.getSceneY());
							return;
						}

						curCircuit.addUser((User) sc);
						refreshCbUser();
						btnAddUser.setSelected(false);

					} catch (Exception ex) {
						System.out.println("Объект не является пользователем");
					}
				}
			}
			// открытие всплывающего меню
			// установка стрелочного контакта
			if ((e.getClickCount() == 1) && (e.getButton().equals(MouseButton.SECONDARY)) && tpE.isExpanded()) {
				if (sc.getClass().equals(StrContact.class))
					contMenuStrCont.show(this, e.getSceneX(), e.getSceneY());
			}

		});

	}

	// обновление списка потребителей в эл цепи
	private void refreshCbUser() {
		cbUser.getItems().clear();
		cbUser.setValue("");
		if (curCircuit != null && !curCircuit.getUsers().isEmpty()) {
			cbUser.getItems()
					.addAll(curCircuit.getUsers().stream()
							.map((u) -> (!((SchemElement) u).getTxName().getText().equals(""))
									? ((SchemElement) u).getTxName().getText()
									: ((SchemElement) u).getVarName())
							.collect(Collectors.toList()));

			String str = (!((SchemElement) curCircuit.getUsers().get(0)).getTxName().getText().equals(""))
					? ((SchemElement) curCircuit.getUsers().get(0)).getTxName().getText()
					: ((SchemElement) curCircuit.getUsers().get(0)).getVarName();
			cbUser.setValue(str);
		}

	}

	public void refreshCbElement() {
		cbElement.getItems().clear();
		if (curLoop != null && !curLoop.getLoop().isEmpty()) {
			cbElement.getItems()
					.addAll(curLoop.getLoop().stream().map((el) -> el.getVarName()).collect(Collectors.toList()));
			if (btnLoop.isSelected()) {
				curLoop.loopPodsvetka(1);
			}
			cbElement.setValue(curLoop.getLoop().get(0).getVarName());
		} else {
			// cbElement.setValue("");
			cbElement.getItems().clear();
		}
	}

	/**
	 * Saves the current element data to the specified file.
	 * 
	 * @param file
	 */
	public void saveSchemaDataToFile(File file) {
		try {

			// сохранение списка состояний элементов
			states.clear();
			list.stream().forEachOrdered((sc) -> {
				saveState(sc);
			});

			// сохранение списка состояний контуров
			loopStates.clear();
			loops.stream().forEachOrdered((l) -> {
				saveLoopState(l);
			});

			// сохранение списка состояний соединений
			unionStates.clear();
			unions.stream().forEachOrdered((u) -> {
				saveUnionState(u);
			});

			// сохранение списка состояний ИСТОЧНИКОВ
			sourceStates.clear();
			sources.stream().filter((s) -> !boxAs.getSources().stream().anyMatch((bs) -> bs.equals(s)))
					.forEachOrdered((s) -> {
						saveSourceState(s);
					});

			// сохранение списка состояний эл. цепей
			circuitStates.clear();
			circuits.stream().forEachOrdered((c) -> {
				saveCircuitState(c);
			});

			// сериализация схемы
			// -------------------------------------------------------
			JAXBContext contextelement = JAXBContext.newInstance(SchemaStateListWrapper.class);
			Marshaller mElement = contextelement.createMarshaller();
			mElement.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			// обертывание списка элементов
			SchemaStateListWrapper wrapperElement = new SchemaStateListWrapper();
			wrapperElement.setList(states);
			wrapperElement.setListLoop(loopStates);
			wrapperElement.setListUnion(unionStates);
			wrapperElement.setListSource(sourceStates);
			wrapperElement.setListCircuit(circuitStates);

			// Marshalling and saving XML to the file.
			mElement.marshal(wrapperElement, file);

			Helper.setSchemaFilePath(file);
		} catch (Exception e) { // catches ANY exception
			e.printStackTrace();
		}
	}

	private void saveCircuitState(ElCircuit c) {
		CircuitState cs = new CircuitState();
		if (c != null) {
			cs.setVarName(c.getVarName());
			if (c.getSource() != null)
				cs.setIndexSource(sources.indexOf(c.getSource()));
			// массив индексов соединений
			if (!c.getCircuit().isEmpty())
				cs.setIndexUnions(c.getCircuit().stream().map((u) -> new Integer(unions.indexOf(u)))
						.collect(Collectors.toList()).toArray(new Integer[c.getCircuit().size()]));
			// массив индексов пользователей
			if (!c.getUsers().isEmpty())
				cs.setIndexUsers(c.getUsers().stream().map((uz) -> (int) ((SchemElement) uz).getIdElementInSchem())
						.collect(Collectors.toList()).toArray(new Integer[c.getUsers().size()]));
			// массив индексов ветвей
			if (!c.getBranches().isEmpty())
				cs.setIndexBranches(c.getBranches().stream().map((br) -> new Integer(circuits.indexOf((br))))
						.collect(Collectors.toList()).toArray(new Integer[c.getBranches().size()]));

			cs.setBranch(c.isBranch());

			circuitStates.add(cs);
		}
	}

	private void saveSourceState(Source s) {
		SourceState st = new SourceState();
		if (s != null) {
			st.setVarName(s.getVarName());
			st.setConst(s.getIsConst());
			st.setInnerR(s.getInR());
			st.setEds(s.getEds());
			st.setIndexSource(sources.indexOf(s));
			// массив индексов полюсов
			if (!s.getTerminals().isEmpty()) {
				st.setIndexTerminals(s.getTerminals().stream().map((t) -> (int) t.getIdElementInSchem())
						.collect(Collectors.toList()).toArray(new Integer[s.getTerminals().size()]));
			}
			if (s.getIsConst()) {
				if (!s.getMinusTerminals().isEmpty())
					st.setIndexMinusTerminals(s.getMinusTerminals().stream().map((t) -> (int) t.getIdElementInSchem())
							.collect(Collectors.toList()).toArray(new Integer[s.getMinusTerminals().size()]));
			}
			sourceStates.add(st);
		}
	}

	private void saveUnionState(ParUnion u) {
		UnionState us = new UnionState();
		if (u != null) {
			us.setVarName(u.getVarName());
			// массив индексов контуров (в общем списке контуров)
			if (!u.getUnion().isEmpty()) {
				us.setIndexLoops(u.getUnion().stream().map((l) -> new Integer(loops.indexOf((l))))
						.collect(Collectors.toList()).toArray(new Integer[u.getUnion().size()]));
			}

			unionStates.add(us);

		}

	}

	private void saveLoopState(Loop l) {

		LoopState loopS = new LoopState();
		if (l != null) {
			loopS.setVarName(l.getVarName());

			// массив индексов элементов (в общем списке элементов) списка
			// элементов
			// контура
			if (!l.getLoop().isEmpty()) {
				loopS.setIndexElements(l.getLoop().stream().map((sc) -> (int) sc.getIdElementInSchem())
						.collect(Collectors.toList()).toArray(new Integer[l.getLoop().size()]));

			}
			loopStates.add(loopS);
		}
	}

	private void handleOpen() {
		FileChooser fileChooser = new FileChooser();

		// Set extension filter
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
		fileChooser.getExtensionFilters().add(extFilter);

		// временно для отладки
		// File file = new File(
//				"D:\\Alex\\javaFx\\SchemEditor\\src\\com\\alex\\schemeditor\\res\\schem\\");

		File file = new File(Main.class.getResource("res/schem").getPath());
		fileChooser.setInitialDirectory(file);

		// Show open file dialog
		file = fileChooser.showOpenDialog(null);

		if (file != null) {
			field.getChildren().clear();
			System.out.println(file.getName());
			InputStream ip = Main.class.getResourceAsStream("res/schem/" + file.getName());
			Helper.loadSchemaFromFile(ip, boxAs, list, loops, unions, sources, circuits);
			list.stream().forEach((sc) -> initMouse(sc));
			field.getChildren().addAll(list);

			initCirciutInspector();
		}
	}

	/**
	 * Saves the file to the file that is currently open. If there is no open file,
	 * the "save as" dialog is shown.
	 */
	private void handleSave() {
		File elementFile = Helper.getSchemaFilePath();
		if (elementFile != null) {
			saveSchemaDataToFile(elementFile);
		} else {
			handleSaveAs();
		}
	}

	/**
	 * Opens a FileChooser to let the user select a file to save to.
	 */

	private void handleSaveAs() {
		FileChooser fileChooser = new FileChooser();

		// Set extension filter
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
		// "OUT files (*.out)", "*.out");
		fileChooser.getExtensionFilters().add(extFilter);

		// временно для отладки
		File file = new File("D:\\Alex\\javaFx\\SchemEditor\\src\\com\\alex\\schemeditor\\res\\schem\\");

		// File file = new File(Main.class.getResource("res/schem").getPath());
		fileChooser.setInitialDirectory(file);

		// Show save file dialog
		file = fileChooser.showSaveDialog(null);

		if (file != null) {
			// Make sure it has the correct extension
			if (!file.getPath().endsWith(".xml")) {
				file = new File(file.getPath() + ".xml");
			}
			saveSchemaDataToFile(file);
		}
	}

	/**
	 * @param sc сохранение состояния элемента для сериализации
	 */
	private void saveState(SchemElement sc) {
		ElementState ems = new ElementState();
		// установка класса
		// объекта----------------------------------------------------
		if (sc.getClass().getSuperclass().equals(Rele.class))
			ems.setCl(sc.getClass().getSuperclass());
		else
			ems.setCl(sc.getClass());
		// ---------------------------------------------------------------------

		// имя переменной и идентификатор в схеме -------
		ems.setVarName(sc.getVarName());
		ems.setId(sc.getIdElementInSchem());
		// -------------------------------------------

		// название и место элемента------------------------------
		if (!sc.getClass().equals(SLine.class) && !sc.getClass().equals(SPoint.class)
				&& !sc.getClass().equals(Diod.class) && !sc.getClass().equals(Conder.class)
				&& !sc.getClass().equals(TransBSSH.class) && !sc.getClass().equals(BDR.class)
				&& !sc.getClass().equals(Drossel.class) && !sc.getClass().equals(StrContact.class)
				&& !sc.getClass().equals(StrMotor.class)) {
			ems.setName(sc.getTxName().getText());
			ems.setPlace(sc.getTxPlace().getText());
		}
		// -----------------------------------------------------------

		// массив точек для линии------------------------------------------
		if (sc.getClass().equals(SLine.class)) {
			ems.setPoints(
					((SLine) sc).getLine().getPoints().toArray(new Double[((SLine) sc).getLine().getPoints().size()]));
		}
		// ----------------------------------------------------------------------
		// массив ламп для светофорной головки-----------------------------
		if (sc.getClass().equals(SvetGol.class)) {
			ems.setPoints(((SvetGol) sc).getNumColor().stream().map((n) -> n.doubleValue()).collect(Collectors.toList())
					.toArray((new Double[((SvetGol) sc).getNumColor().size()])));
		}
		// ----------------------------------------------------------------------

		// установка для контакта реле, номер тройника, замкнутость
		if (sc.getClass().getSuperclass().equals(Contact.class)
				|| sc.getClass().getSuperclass().equals(RightContact.class)) {

			Rele r = ((Contact) sc).getRele();
			if (r != null)
				ems.setIndexRele(boxAs.getReles().indexOf(r));
			ems.setTrNum(((Contact) sc).getContNum() / 10);
			ems.setZam(((Contact) sc).getIsZam());
		}

		// установка номера тройника для тылового контакта этого тройника и (-1)
		// если не тыловой контакт или отдельный тыловой контакт
		ems.setIndexTroynic(-1);
		if (sc.getClass().equals(TylContact.class)) {

			// находим тройник у которого данный тыловой контакт является тылом
			// тройника
			List<TroynicInterface> lTr = new ArrayList<TroynicInterface>();
			lTr = list.stream()
					.filter((tr) -> (tr.getClass().equals(Troynic.class) || tr.getClass().equals(TroynicRight.class)
							|| tr.getClass().equals(TroynicAutostop.class) || tr.getClass().equals(PolarContact.class)))
					.map((t) -> (TroynicInterface) t).collect(Collectors.toList());
			// устанавливаем номер тройника в тыловой контакт
			if (!lTr.isEmpty()) {
				lTr.forEach((tr) -> {
					if (tr.getTylTr() != null) {
						if (tr.getTylTr().equals(sc)) {
							ems.setIndexTroynic(((SchemElement) tr).getIdElementInSchem());
						}
					}
				});
			} else
				ems.setIndexTroynic(-1);

		}

		// ------------------------------------------------------------------------
		// установка для обмотки реле
		if (sc.getClass().equals(Obmotka.class)) {
			ems.setIndexRele(((Obmotka) sc).getIndexRele());
			ems.setIndexTerminal(((Obmotka) sc).getIndexObm());
		}
		// установка для светофорной лампы
		if (sc.getClass().equals(SvetLamp.class)) {
			ems.setIndexParent(((SvetLamp) sc).getIndexParent());
			ems.setIndexTerminal(((SvetLamp) sc).getIndexLamp());
		}
		// ------------------------------------------------------------------------
		// установка для обмотки мотора
		if (sc.getClass().equals(ObmotkaMotor.class)) {
			ems.setIndexRele(((ObmotkaMotor) sc).getIndexMotor());
			ems.setIndexTerminal(((ObmotkaMotor) sc).getIndexObmotka());
		}
		// ------------------------------------------------------------------------

		// местоположение если не линия----------------------------------------
		if (!sc.getClass().equals(SLine.class)) {
			ems.setlX(sc.getLayoutX());
			ems.setlY(sc.getLayoutY());
		}
		// ---------------------------------------------------------------
		// индекс реле для реле --------------------------------------
		if (sc.getClass().getSuperclass().equals(Rele.class)) {

			ems.setIndexRele(boxAs.getReles().indexOf(boxAs.getRele(sc.getTxName().getText())));
			// положение контактов для реле
			// --------------------------------------

			ems.setFirstLeft(((Rele) sc).getIsFirstLeft());
		}
		// индекс реле для стрелочного мотора
		// --------------------------------------
		if (sc.getClass().getSuperclass().equals(StrMotor.class)) {

			ems.setIndexRele(boxAs.getStrMotors().indexOf(sc));
		}
		// ----------------------------------------------------------------

		// сторона полюса------------------------------------
		if ((sc.getClass().equals(PowerSourceTerminal.class) || sc.getClass().equals(PowerSourceTerminalDCM.class))) {
			ems.setLeft(getLeftTerminal(sc));
			ems.setMinus(((PowerSourceTerminal) sc).isMinus());
			ems.setIndexParent(((PowerSourceTerminal) sc).getIndexParent());
			if (sources.stream().anyMatch((s) -> s.isTerminalConteined(sc.getVarName()))) {
				for (Source source : sources) {
					if (source.isTerminalConteined(sc.getVarName())) {
						// ems.setIndexSource(boxAs.getSources().indexOf(source));
						ems.setIndexSource(sources.indexOf(source));
						if (ems.isMinus())
							ems.setIndexTerminal(source.getIndexMinusTerminal(sc.getVarName()));
						else
							ems.setIndexTerminal(source.getIndexTerminal(sc.getVarName()));
					}
				}

			} else {
				ems.setIndexSource(-1);
				ems.setIndexTerminal(-1);
				// пока элемент в котором полюс не добавлен в список источников
				// но
				// уже добавлен в контур
				if (((PowerSourceTerminal) sc).getIndexParent() != -1) {
					Source temp = (Source) Helper.getElementById(list, ((PowerSourceTerminal) sc).getIndexParent());
					int i = (ems.isMinus()) ? temp.getMinusTerminals().indexOf(sc) : temp.getTerminals().indexOf(sc);
					ems.setIndexTerminal(i);
				}

			}
		}
		// сторона вывода------------------------------------
		if (sc.getClass().equals(Vyvod.class)) {
			ems.setLeft(((Vyvod) sc).getIsLeft());
		}
		// ----------------------------------------------------------------------

		// горизонтальность объектов---------------------------------------
		if (sc.getClass().equals(Diod.class)) {
			ems.setHorizont(((Diod) sc).getIsHorizont());
		}
		if (sc.getClass().equals(Conder.class)) {
			ems.setHorizont(((Conder) sc).getIsHorizont());
		}
		if (sc.getClass().equals(Resistor.class)) {
			ems.setHorizont(((Resistor) sc).getIsHorizont());
		}
		if (sc.getClass().equals(Drossel.class)) {
			ems.setHorizont(((Drossel) sc).getIsHorizont());
		}
		// --------------------------------------------------------------
		// номинал для предохранителя
		if (sc.getClass().equals(Predochran.class)) {
			ems.setNomTok(((Predochran) sc).getTxNom().getText());
		}

		// время разряда для конденсатора
		if (sc.getClass().equals(Conder.class)) {
			ems.setNomTok(String.valueOf(((Conder) sc).getRazrad()));
		}
		// для стрелочного контакта--------------------------
		if (sc.getClass().equals(StrContact.class)) {
			ems.setTop(((StrContact) sc).getIsTop());
			ems.setZam(((StrContact) sc).getIsZam());
			ems.setTrNum(((StrContact) sc).getNumCont());
			// isMinus контакт замкнут когда стрелка в минусе
			ems.setMinus(((StrContact) sc).getIsMinus());
			// isLeft - true - рабочий контакт, false - контрольный
			ems.setLeft(((StrContact) sc).getIsWork());
			StrMotor r = ((StrContact) sc).getMotor();
			if (r != null)
				ems.setIndexRele(boxAs.getStrMotors().indexOf(r));

		}
		// -----------------------------------------------------------

		// нужно ли отрисовывать объект-----------------------------------
		ems.setDrowed(sc.isDrowed());
		// ---------------------------------------------------------------
		states.add(ems);
	}

	private void getListReleByFind(String str) {
		if (!boxAs.getReles().stream().anyMatch((r) -> r.getTxName().getText().equals(str))) {
			cbRele.getItems().clear();
			cbRele.getItems().addAll(boxAs.getReles().stream().map((r) -> r.getTxName().getText())
					.filter((nm) -> nm.length() >= str.length())
					.filter((name) -> name.substring(0, str.length()).equals(str)).collect(Collectors.toList()));
		}
	}

	private void fillCbReleItems() {
		cbRele.getItems().clear();
		cbRele.getItems()
				.addAll(boxAs.getReles().stream().map((r) -> r.getTxName().getText()).collect(Collectors.toList()));
		// System.out.println("pizdets - ");
	}

	private Source getSource(String str) {
		Source so = null;
		for (Source s : sources) {
			if (s.getVarName().equals(str))
				so = s;
		}
		return so;
	}
}
