﻿package com.alex.training.scene;

import java.util.ArrayList;
import java.util.List;

import javafx.animation.AnimationTimer;
import javafx.scene.Group;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.AudioClip;
import javafx.stage.Screen;

import com.alex.training.Main;
import com.alex.training.sceneElement.CenterTransparant;
import com.alex.training.sceneElement.TransparantPane;
import com.alex.training.schem.BoxAs;
import com.alex.training.schemElement.PowerSourceTerminalDCM;
import com.alex.training.schemElement.Rele;
import com.alex.training.schemElement.SchemElement;
import com.alex.training.stationElement.Ampermetr;
import com.alex.training.stationElement.LeftStrelka;
import com.alex.training.stationElement.LeftSvetofor;
import com.alex.training.stationElement.RightSvetofor;
import com.alex.training.stationElement.StationElement;
import com.alex.training.stationElement.StationObject;
import com.alex.training.stationElement.Strelka;
import com.alex.training.stationElement.Svetofor;
import com.alex.training.stationElement.Syezd;
import com.alex.training.stationElement.TrackCircuit;

/**
 * @author alex
 *
 *         класс отображает арм ДСЦП
 */
public class Station extends Group {

	private TransparantPane tp; // верхняя панель транспарантов
	// список объектов мнемосхемы
	private List<TrackCircuit> circuits = new ArrayList<TrackCircuit>();
	// private Map<Integer, Group> mapObject = new HashMap<>();
	private ScrollPane scroll;
	private TrackCircuit tcTemp;
	private Strelka strTemp;
	private Svetofor svTemp;
	private StationObject stationObject;
	private CenterTransparant centerTrans;
	private Ampermetr ampermetr;
	private List<Strelka> strelki = new ArrayList<Strelka>();
	private List<Svetofor> svetofori = new ArrayList<Svetofor>();

	// идентификатор мршрутных секций для замыкающего реле 1з
	// или идентификатор реле УС по светофору ЛН413
	private int mod1z;

	private BoxAs boxAs;
	private Rele relTemp;

	/**
	 * @param ba
	 */
	public Station(BoxAs ba) {

		this.boxAs = ba;

		// верхняя панель транспарантов------------------------------------
		tp = new TransparantPane();

		// поле станции на которое добавляются элементы--------------------
		AnchorPane field = new AnchorPane();
		scroll = new ScrollPane();
		scroll.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scroll.setLayoutX(2);
		scroll.setLayoutY(tp.getHeight() + 67);
		scroll.setPrefViewportHeight(Screen.getPrimary().getBounds().getHeight() - 190);
		scroll.setPrefViewportWidth(Screen.getPrimary().getBounds().getWidth());
		scroll.setStyle("-fx-background: grey");
		scroll.setContent(field);
		scroll.setPannable(true);

		// Массивы с названиями объектов для создания в цикле
		String tc[] = new String[] { "399", "401", "403", "405", "406а-406", "406c", "408c", // 0-6
				"408", "410c", "412c", "412", // 7-10
				"407", "409", "409c", "411", // 11-14
				"411c", "413c", "413a", "413б", "415", "417", // 15-20
				"414", "416", "418", "420б", "420а", "420", // 21-26
				"5", "3а", "3", "3б", "7", // 27-31;
				"4а", "4" }; // 32-33,
		String sv[] = new String[] { "405", "ЛН407", "ЛН409", "ЛН411", "ОП", "ЛН413", "415", "417", "ДОП", // 0-8
				"ЛН3", "ЛН7", "ЛН9", "ЛН4", // 9-12
				"404М", "406М", "408", "Д", "ЛН410", "ЛН412", "ЛН414", "ЛН416" // 13-20

		};

		double width = 160;
		double x = 5;
		double xTemp = 0;
		double y = 650;
		TrackCircuit g;

		// рельсовые цепи до платформы ---------------------------
		for (int i = 0; i < 7; i++) {

			if (i == 4) {
				y = 210;
				width = 320;
				x = 5;
			}
			if (i >= 5) {
				width = 160;
			}

			g = new TrackCircuit(tc[i], false, width, x, y);
			circuits.add(i, g);
			field.getChildren().add(circuits.get(i));
			x += width;
		}

		// элемент станция
		stationObject = new StationObject();
		stationObject.setLayoutX(x);
		stationObject.setLayoutY(360);
		field.getChildren().add(stationObject);

		// рельсовые цепи платформы 2-го пути---------------------------
		width = 90;
		for (int i = 7; i < 11; i++) {
			g = new TrackCircuit(tc[i], false, width, x, y);
			circuits.add(i, g);
			field.getChildren().add(circuits.get(i));
			x += width;
		}
		// рельсовые цепи платформы 1-го пути---------------------------
		y = 650;
		x -= 360;
		for (int i = 11; i < 15; i++) {
			g = new TrackCircuit(tc[i], false, width, x, y);
			circuits.add(i, g);
			field.getChildren().add(circuits.get(i));
			x += width;
		}
		xTemp = x; // координата конца платформы

		// рельсовые цепи 1-го пути после платформы ---------------------------
		for (int i = 15; i < 21; i++) {
			if (i == 15) {
				width = 180;
				g = new LeftStrelka(tc[i], false, width, x, y, true, true);
				x += 120;
			} else {
				width = 160;
				g = new TrackCircuit(tc[i], false, width, x, y);
			}
			circuits.add(i, g);
			field.getChildren().add(circuits.get(i));
			x += width;
		}
		// рельсовые цепи 2-го пути после платформы ---------------------------
		y = 210;
		x = xTemp;
		for (int i = 21; i < 27; i++) {
			if (i == 21) {
				width = 180;
				g = new LeftStrelka(tc[i], false, width, x, y, true, false);
				x += 120;
			} else {
				width = 160;
				g = new TrackCircuit(tc[i], false, width, x, y);
			}
			circuits.add(i, g);
			field.getChildren().add(circuits.get(i));
			x += width;
		}

		strTemp = (LeftStrelka) circuits.get(21);
		strTemp.getNoStr().setText("2");

		// рельсовые цепи 3-го пути ---------------------------
		y = 540;
		x = xTemp + 275;
		xTemp = x;
		for (int i = 27; i < 32; i++) {
			if (i == 27) {
				width = 30;
				g = new TrackCircuit(tc[i], false, width, x, y);

			} else {
				if (i == 28) {
					width = 90;
					g = new Syezd(tc[i], false, width, x, y, true);
					x += 330;
				} else {
					width = 160;
					g = new TrackCircuit(tc[i], false, width, x, y);
				}
			}
			circuits.add(i, g);
			field.getChildren().add(circuits.get(i));
			x += width;
		}
		Syezd s = (Syezd) circuits.get(28);
		s.getStrR().getTrack().setWidth(90);
		s.getStrR().getNoStr().setText("5");
		s.getStrL().getNoStr().setText("3");

		// рельсовые цепи 4-го пути ---------------------------
		y = 320;
		x = xTemp;
		xTemp = x;
		for (int i = 32; i < 34; i++) {
			if (i == 32) {
				width = 120;
				g = new Syezd(tc[i], false, width, x, y, false);
				x += 330;
			} else {
				width = 160;
				g = new TrackCircuit(tc[i], false, width, x, y);
			}
			circuits.add(i, g);
			field.getChildren().add(circuits.get(i));
			x += width + 30;
		}
		s = (Syezd) circuits.get(32);
		// s.getStrR().getTrack().setWidth(120);
		s.getStrR().getNoStr().setText("6");
		s.getStrL().getNoStr().setText("4");

		// создание списка стрелок (пригодится дальше???)
		strelki.add(0, (Strelka) circuits.get(15));
		strelki.add(1, (Strelka) circuits.get(21));
		strelki.add(2, ((Syezd) circuits.get(28)).getStrL());
		strelki.add(3, ((Syezd) circuits.get(28)).getStrR());
		strelki.add(4, ((Syezd) circuits.get(32)).getStrL());
		strelki.add(5, ((Syezd) circuits.get(32)).getStrR());

		// светофоры 1-го пути ---------------------------

		x = 160;
		width = 160;
		for (int i = 0; i < 6; i++) {

			if (i == 4) {
				y = 720;
				x -= width;
				svTemp = new RightSvetofor(sv[i], false, x, y, true);
				width = 360;

			} else {
				y = 670;
				svTemp = new LeftSvetofor(sv[i], false, x, y, true);

			}
			svetofori.add(i, svTemp);
			field.getChildren().add(svetofori.get(i));
			x += width;
		}

		x -= 60;
		xTemp = x;
		width = 160;
		x += width;
		for (int i = 6; i < 8; i++) {
			svTemp = new LeftSvetofor(sv[i], false, x, y, true);
			svetofori.add(i, svTemp);
			field.getChildren().add(svetofori.get(i));
			x += width;
		}

		// светофор доп -------------------------------------------
		x = xTemp;
		y = 580;
		svTemp = new RightSvetofor(sv[8], false, x, y, true);
		svetofori.add(8, svTemp);
		field.getChildren().add(svetofori.get(8));
		// --------------------------------------------------------------

		// светофоры 3-го пути ---------------------------
		x += 430;
		xTemp = x;
		width = 160;
		for (int i = 9; i < 12; i++) {

			if (i == 10) {
				y = 560;
				svTemp = new LeftSvetofor(sv[i], false, x, y, true);

			} else {
				y = 465;
				svTemp = new RightSvetofor(sv[i], false, x, y, true);

			}
			svetofori.add(i, svTemp);
			field.getChildren().add(svetofori.get(i));
			x += width;
		}

		// светофор ЛН4 -------------------------------------------
		x = xTemp + 30;
		y = 245;
		svTemp = new RightSvetofor(sv[12], false, x, y, true);
		svetofori.add(12, svTemp);
		field.getChildren().add(svetofori.get(12));
		// --------------------------------------------------------------

		// светофоры 2-го пути ---------------------------
		y = 125;
		x = 325;
		width = 320;
		for (int i = 13; i < 21; i++) {

			if (i == 15) {
				x += 40;
				width = 300;
			}
			if (i == 16) {
				x -= width;
				y = 230;
				svTemp = new LeftSvetofor(sv[i], false, x, y, true);
				x += 300;
				width = 160;
			} else {
				if (i == 17)
					y = 230;
				else
					y = 125;
				svTemp = new RightSvetofor(sv[i], false, x, y, true);
			}
			svetofori.add(i, svTemp);
			field.getChildren().add(svetofori.get(i));
			x += width;
		}
		// -------------------------------------------------------------------------

		// центральная панель транспарантов
		centerTrans = new CenterTransparant(1050, 280);
		field.getChildren().add(centerTrans);
		// --------------------------------------------------------

		// амперметр
		ampermetr = new Ampermetr(1300, 400);
		field.getChildren().add(ampermetr);
		// --------------------------------------------------------

		// Установка объектов для местного
		// управления и привидение станции в нормальный
		// вид---------------------------------------
		setNormalStation();

		// ------------------------------------------------------------------------------------

		getChildren().addAll(tp, scroll);

		// для отладки
		// -------------------------------------------------------------------------------
		TrackCircuit t = (TrackCircuit) circuits.get(28);
		// t.setShunt(true);
		// RightSvetofor lvs = (RightSvetofor)mapObject.get(45);
		// lvs.nabor();
		// stationObject.setError(true);
		// ampermetr.normPerevod(1);
		// -------------------------------------------------------------------------

		/*
		 * связывание объектов с положением реле
		 */

		// сигнализация--------------------------------------------
		// занятость р.ц от повторителей ----------------------

		// 399---

		// 401

		// 403 - п403П
		boxAs.getRele(295).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(2).setShunt(!newValue.booleanValue());
		});
		// 405 - п405П
		boxAs.getRele(296).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(3).setShunt(!newValue.booleanValue());
		});
		// 407 2п407П
		boxAs.getRele(209).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(11).setShunt(!newValue.booleanValue());
		});
		// 409 2п409П
		boxAs.getRele(211).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(12).setShunt(!newValue.booleanValue());
		});
		// 409с 3п409сП
		boxAs.getRele(214).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(13).setShunt(!newValue.booleanValue());
		});
		// 411 3п411П
		boxAs.getRele(217).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(14).setShunt(!newValue.booleanValue());
		});
		// 411с 3п411сП
		boxAs.getRele(220).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(15).setShunt(!newValue.booleanValue());
		});
		// 413с 2п413сП
		boxAs.getRele(222).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(16).setShunt(!newValue.booleanValue());
		});
		// 408с 2п408сП
		boxAs.getRele(224).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(6).setShunt(!newValue.booleanValue());
		});
		// 408 2п408П
		boxAs.getRele(226).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(7).setShunt(!newValue.booleanValue());
		});
		// 410с 2п410сП
		boxAs.getRele(228).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(8).setShunt(!newValue.booleanValue());
		});
		// 412с 3п412сП
		boxAs.getRele(231).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(9).setShunt(!newValue.booleanValue());
		});
		// 412 3п412П
		boxAs.getRele(234).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(10).setShunt(!newValue.booleanValue());
		});
		// 3а 3п3аП
		boxAs.getRele(238).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(28).setShunt(!newValue.booleanValue());
		});
		// 4а 3п4аП
		boxAs.getRele(242).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(32).setShunt(!newValue.booleanValue());
		});
		// 3 п3П
		boxAs.getRele(245).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(29).setShunt(!newValue.booleanValue());
		});
		// 4 2п4П
		boxAs.getRele(248).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(33).setShunt(!newValue.booleanValue());
		});
		// 5 2п5П
		boxAs.getRele(252).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(27).setShunt(!newValue.booleanValue());
		});
		// 3б 2п3бП
		boxAs.getRele(256).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(30).setShunt(!newValue.booleanValue());
		});
		// 416 п416П
		boxAs.getRele(267).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(22).setShunt(!newValue.booleanValue());
		});
		// 418 п418П
		boxAs.getRele(268).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(23).setShunt(!newValue.booleanValue());
		});
		// 420б п420бП
		boxAs.getRele(269).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(24).setShunt(!newValue.booleanValue());
		});
		// 420а п420аП
		boxAs.getRele(270).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(25).setShunt(!newValue.booleanValue());
		});
		// 420 п420П
		boxAs.getRele(275).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(26).setShunt(!newValue.booleanValue());
		});
		// 414 3п414П
		boxAs.getRele(185).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			circuits.get(21).setShunt(!newValue.booleanValue());
		});
		// ---------------------------------- путевые реле

		// индикация светофоров------------------------------
		// ЛН413 413ЗО
		boxAs.getRele(196).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			svetofori.get(5).openSvetofor(newValue.booleanValue());
		});
		// ЛН413 413БО
		boxAs.getRele(1).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			svetofori.get(5).openSvetoforWhite(newValue.booleanValue());
		});
		// -----------------------------------------------------
		// контроль положения стрелок 1 ДИ ----------------------
		boxAs.getRele(300).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (!newValue.booleanValue()) {
				strelkaLossCheckup();
			}
		});
		// -------------------------------------------------
		// 1 ПК - 2п1ПК
		boxAs.getRele(149).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			strelki.get(0).setPlusCheckup(newValue.booleanValue());
		});

		// 1 МК - 2п1МК
		boxAs.getRele(151).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			strelki.get(0).setMinusCheckup(newValue.booleanValue());
		});
		// ------------------- для отладки

		// boxAs.getRele(279)
		// .isCurrentProperty()
		// .addListener(
		// (property, oldValue, newValue) -> {
		// System.out.println("xren -1НС-1 -"
		// + newValue.booleanValue());
		// });
		// boxAs.getRele(278)
		// .isCurrentProperty()
		// .addListener(
		// (property, oldValue, newValue) -> {
		// System.out.println("xren -1ПС -"
		// + newValue.booleanValue());
		// });
		// ------------------- для отладки
		// 6 ПК - 2п6ПК
		boxAs.getRele(163).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			strelki.get(5).setPlusCheckup(newValue.booleanValue());
		});

		// 6 МК - 2п6МК
		boxAs.getRele(263).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			strelki.get(5).setMinusCheckup(newValue.booleanValue());
		});
		// 5 ПК - 2п5ПК
		boxAs.getRele(161).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			strelki.get(3).setPlusCheckup(newValue.booleanValue());
		});

		// 5 МК - 2п5МК
		boxAs.getRele(352).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			strelki.get(3).setMinusCheckup(newValue.booleanValue());
		});
		// 4 ПК - 2п4ПК
		boxAs.getRele(155).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			strelki.get(4).setPlusCheckup(newValue.booleanValue());
		});

		// 4 МК - 2п4МК
		boxAs.getRele(157).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			strelki.get(4).setMinusCheckup(newValue.booleanValue());
		});
		// 3 ПК - 2п3ПК
		boxAs.getRele(159).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			strelki.get(2).setPlusCheckup(newValue.booleanValue());
		});

		// 3 МК - п3МК
		boxAs.getRele(292).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			strelki.get(2).setMinusCheckup(newValue.booleanValue());
		});
		// 2 ПК - 2п2ПК
		boxAs.getRele(153).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			strelki.get(1).setPlusCheckup(newValue.booleanValue());
		});

		// 3 МК - п2МК
		boxAs.getRele(264).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			strelki.get(1).setMinusCheckup(newValue.booleanValue());
		});

		// -----------------------------------------------------

		// реле ОН - отмена набора--------------
		boxAs.getRele(51).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			centerTrans.setIndicationTrON(newValue.booleanValue());
			if (newValue.booleanValue())
				cancelSelectIndication();
			// отмена режима зм если нажимался - нужно ли???
			circuits.stream().forEach((c) -> c.setIndicationZm(false));

		});
		// реле 2пГОМ - отмена набора--------------
		boxAs.getRele(174).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			centerTrans.setIndicationTrGOM(newValue.booleanValue());
		});
		// ----------------------------------------------------
		// ---------индикация реле ЗМ-------------------
		// реле 10зм --------------
		boxAs.getRele(27).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(15), null, newValue.booleanValue(), false);
			else
				circuits.get(15).stopAtZm();
		});
		// реле 15зм --------------
		boxAs.getRele(30).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(15), circuits.get(17), boxAs.getRele(27).getIsCurrent(),
						newValue.booleanValue());
			else
				circuits.get(17).stopAtZm();
		});
		// реле 13зм --------------
		boxAs.getRele(28).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(15), circuits.get(29), boxAs.getRele(27).getIsCurrent(),
						newValue.booleanValue());
			else
				circuits.get(29).stopAtZm();
		});

		// реле 14зм --------------
		boxAs.getRele(29).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(15), circuits.get(33), boxAs.getRele(27).getIsCurrent(),
						newValue.booleanValue());
			else
				circuits.get(33).stopAtZm();
		});

		// реле 20зм --------------
		boxAs.getRele(21).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(21), null, newValue.booleanValue(), false);
			else
				circuits.get(21).stopAtZm();
		});

		// реле 24зм --------------
		boxAs.getRele(23).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(21), circuits.get(33), boxAs.getRele(21).getIsCurrent(),
						newValue.booleanValue());
			else
				circuits.get(33).stopAtZm();
		});

		// реле 23зм --------------
		boxAs.getRele(22).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(21), circuits.get(29), boxAs.getRele(21).getIsCurrent(),
						newValue.booleanValue());
			else
				circuits.get(29).stopAtZm();
		});

		// реле 30зм --------------
		boxAs.getRele(10).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(28), null, newValue.booleanValue(), false);
			else
				circuits.get(28).stopAtZm();
		});

		// реле 31зм --------------
		boxAs.getRele(14).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(28), circuits.get(14), boxAs.getRele(10).getIsCurrent(),
						newValue.booleanValue());
			else
				circuits.get(14).stopAtZm();
		});
		// реле 32зм --------------
		boxAs.getRele(13).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(28), circuits.get(10), boxAs.getRele(10).getIsCurrent(),
						newValue.booleanValue());
			else
				circuits.get(10).stopAtZm();
		});
		// реле 40зм --------------
		boxAs.getRele(15).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(32), null, newValue.booleanValue(), false);
			else
				circuits.get(32).stopAtZm();
		});

		// реле 41зм --------------
		boxAs.getRele(19).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(32), circuits.get(14), boxAs.getRele(15).getIsCurrent(),
						newValue.booleanValue());
			else
				circuits.get(14).stopAtZm();
		});
		// реле 42зм --------------
		boxAs.getRele(20).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(32), circuits.get(10), boxAs.getRele(15).getIsCurrent(),
						newValue.booleanValue());
			else
				circuits.get(10).stopAtZm();
		});

		// реле 70зм --------------
		boxAs.getRele(39).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(30), null, newValue.booleanValue(), false);
			else
				circuits.get(30).stopAtZm();
		});

		// реле 79зм --------------
		boxAs.getRele(54).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(30), circuits.get(31), boxAs.getRele(39).getIsCurrent(),
						newValue.booleanValue());
			else
				circuits.get(31).stopAtZm();
		});

		// реле 90зм --------------
		boxAs.getRele(25).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(31), null, newValue.booleanValue(), false);
			else
				circuits.get(31).stopAtZm();
		});

		// реле 97зм --------------
		boxAs.getRele(24).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(31), circuits.get(30), boxAs.getRele(25).getIsCurrent(),
						newValue.booleanValue());
			else
				circuits.get(30).stopAtZm();
		});

		// реле 110зм --------------
		boxAs.getRele(40).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(2), null, newValue.booleanValue(), false);
			else
				circuits.get(2).stopAtZm();
		});

		// реле 1113зм --------------
		boxAs.getRele(12).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(2), circuits.get(14), boxAs.getRele(40).getIsCurrent(),
						newValue.booleanValue());
			else
				circuits.get(14).stopAtZm();
		});
		// реле 60зм --------------
		boxAs.getRele(36).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(25), null, newValue.booleanValue(), false);
			else
				circuits.get(25).stopAtZm();
		});

		// реле 62зм --------------
		boxAs.getRele(37).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				setIndicationZm(circuits.get(25), circuits.get(10), boxAs.getRele(36).getIsCurrent(),
						newValue.booleanValue());
			else
				circuits.get(10).stopAtZm();
		});
		// реле
		// зм------------------------------------------------------------------

		// реле
		// ус------------------------------------------------------------------
		// реле п413УС
		boxAs.getRele(6).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			svetofori.get(5).setUsIndication(newValue.booleanValue());
			// установка модификатора для индикации рере 1з
			if (newValue.booleanValue())
				mod1z = 1;
		});

		// реле 413-3/4УС
		boxAs.getRele(7).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			svetofori.get(5).setUsIndication(newValue.booleanValue());
		});

		// реле п413/3УС
		boxAs.getRele(130).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			// установка модификатора для индикации рере 1з
			if (newValue.booleanValue())
				mod1z = 2;
		});
		// реле п413/3УС
		boxAs.getRele(33).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			// установка модификатора для индикации рере 1з
			if (newValue.booleanValue())
				mod1z = 3;
		});

		// реле 2п3БУС
		boxAs.getRele(294).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			svetofori.get(9).setUsIndication(newValue.booleanValue());
		});
		// реле 2п3УС
		boxAs.getRele(34).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			svetofori.get(9).setUsIndication(newValue.booleanValue());
		});

		// реле 2п4бУС
		boxAs.getRele(141).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			svetofori.get(12).setUsIndication(newValue.booleanValue());
		});
		// реле Д-3/4УС
		boxAs.getRele(138).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			svetofori.get(16).setUsIndication(newValue.booleanValue());
		});
		// реле п416/410УС
		boxAs.getRele(65).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			svetofori.get(20).setUsIndication(newValue.booleanValue());
		});
		// реле 407-411УС
		boxAs.getRele(76).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			svetofori.get(1).setUsIndication(newValue.booleanValue());
		});
		// реле п7УС
		boxAs.getRele(46).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			svetofori.get(10).setUsIndication(newValue.booleanValue());
		});
		// реле п9УС
		boxAs.getRele(52).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			svetofori.get(11).setUsIndication(newValue.booleanValue());
		});
		// реле
		// ус------------------------------------------------------------------

		// индикация замыкающих реле
		// --------------------------------------------
		// 1з
		boxAs.getRele(92).isCurrentProperty().addListener((property, oldValue, newValue) -> {

			switch (mod1z) {
			case 1: {
				strelki.get(0).setZam(!newValue.booleanValue());
				circuits.get(16).setZam(!newValue.booleanValue());
				circuits.get(17).setZam(!newValue.booleanValue());
			}
				break;
			case 2: {
				strelki.get(0).setZam(!newValue.booleanValue());
				circuits.get(27).setZam(!newValue.booleanValue());
				strelki.get(2).setZam(!newValue.booleanValue());
				strelki.get(3).setZam(!newValue.booleanValue());
				circuits.get(29).setZam(!newValue.booleanValue());
			}
				break;
			case 3: {
				strelki.get(0).setZam(!newValue.booleanValue());
				circuits.get(27).setZam(!newValue.booleanValue());
				circuits.get(28).setZam(!newValue.booleanValue());
				circuits.get(32).setZam(!newValue.booleanValue());
				circuits.get(33).setZam(!newValue.booleanValue());
			}
				break;

			default:
				break;
			}
		});

		// индикация замыкающих реле
		// --------------------------------------------

		// --------------- сигнализация--конец----------------

		// svTemp = (LeftSvetofor) svetofori.get(16);
		// relTemp = (ReleOMSHM) boxAs.getRele(0);
		// relTemp.isCurrentProperty().addListener(
		// (property, oldValue, newValue) -> {
		// svTemp.openSvetoforWhite();
		// });

		/*
		 * обработка событий ----------------------------------------
		 */

		// field.setOnMouseClicked((me) -> {
		// if ((me.getClickCount() == 1)
		// && (me.getButton().equals(MouseButton.SECONDARY))) {
		// boxAs.getRele(218).setIsCurrent(false);
		// }
		// });
		// установка выбора стрелки для перевода
		strelki.stream().forEach((o) -> o.getTrack().setOnMouseClicked((e) -> selectStrelkaByPerevod(o, e)));

		// перевод стрелки -------- без учета реле
		// пока не нарисованы все схемы стрелок
		// strelki.stream()
		// .filter((o) -> !o.equals(strelki.get(0)))
		// .forEach(
		// (o) -> {
		// o.getPlusFree().setOnMouseClicked(
		// (e) -> perevodStrelkiSimulator(o, 1, e));
		// o.getMinusFree().setOnMouseClicked(
		// (e) -> perevodStrelkiSimulator(o, 2, e));
		// o.getMinus().setOnMouseClicked(
		// (e) -> perevodStrelkiSimulator(o, 2, e));
		// o.getPlus().setOnMouseClicked(
		// (e) -> perevodStrelkiSimulator(o, 1, e));
		// });

		// перевод стрелки --------------- с учетом реле
		strelki.stream().forEach((o) -> {
			o.getPlusFree().setOnMouseClicked((me) -> {
				perevodStrelki(o, true, me);
			});
			o.getPlus().setOnMouseClicked((me) -> {
				perevodStrelki(o, true, me);
			});
			o.getMinusFree().setOnMouseClicked((me) -> {
				perevodStrelki(o, false, me);
			});
			o.getMinus().setOnMouseClicked((me) -> {
				perevodStrelki(o, false, me);
			});
		});
		// -----------------------------------------------------
		// перевод стрелок------конец---------

		// привязка свойства работы мотора стрелки для расчета тока
		boxAs.getStrMotors().stream().forEach((o) -> {
			o.isWorkProperty().addListener((property, oldValue, newValue) -> countingTokStr());
		});

		// установка режима набора для управляемых светофоров
		// ---------------------------------
		// Лн413 -
		// выбор р.ц. 413а, 3,4
		svetofori.get(5).addTrack(circuits.get(17));
		svetofori.get(5).addTrack(circuits.get(29));
		svetofori.get(5).addTrack(circuits.get(33));
		svetofori.get(5).getCell().setOnMouseClicked((e) -> {
			svetoforControlByMouse(svetofori.get(5), e);
		});
		// -----------------------------------------------------
		// Лн3 -
		// выбор р.ц. 411, 412
		svetofori.get(9).addTrack(circuits.get(10));
		svetofori.get(9).addTrack(circuits.get(14));
		svetofori.get(9).getCell().setOnMouseClicked((e) -> {
			svetoforControlByMouse(svetofori.get(9), e);
		});
		// -----------------------------------------------------
		// Лн4 -
		// выбор р.ц. 411, 412
		svetofori.get(12).addTrack(circuits.get(10));
		svetofori.get(12).addTrack(circuits.get(14));
		svetofori.get(12).getCell().setOnMouseClicked((e) -> {
			svetoforControlByMouse(svetofori.get(12), e);
		});
		// -----------------------------------------------------

		// Д -
		// выбор р.ц. 3, 4
		svetofori.get(16).addTrack(circuits.get(29));
		svetofori.get(16).addTrack(circuits.get(33));
		svetofori.get(16).getCell().setOnMouseClicked((e) -> {
			svetoforControlByMouse(svetofori.get(16), e);
		});
		// -----------------------------------------------------
		// ЛН407 -
		// выбор р.ц. 411
		svetofori.get(1).addTrack(circuits.get(14));
		svetofori.get(1).getCell().setOnMouseClicked((e) -> {
			svetoforControlByMouse(svetofori.get(1), e);
		});
		// -----------------------------------------------------
		// ЛН416 -
		// выбор р.ц. 412
		svetofori.get(20).addTrack(circuits.get(10));
		svetofori.get(20).getCell().setOnMouseClicked((e) -> {
			svetoforControlByMouse(svetofori.get(20), e);
		});
		// -----------------------------------------------------
		// ЛН7 -
		// выбор р.ц. 7
		svetofori.get(10).addTrack(circuits.get(31));
		svetofori.get(10).getCell().setOnMouseClicked((e) -> {
			svetoforControlByMouse(svetofori.get(10), e);
		});
		// -----------------------------------------------------
		// ЛН9 -
		// выбор р.ц. 3
		svetofori.get(11).addTrack(circuits.get(29));
		svetofori.get(11).getCell().setOnMouseClicked((e) -> {
			svetoforControlByMouse(svetofori.get(11), e);
		});
		// -----------------------------------------------------

		// отправка плюсового полюса в схему ЗМ -----------------------------
		// нажатие мышью на р.ц 413а, зм10, зм15
		circuits.get(17).getTrack().setOnMouseClicked((e) -> {
			postPlusInZM(circuits.get(17), e, boxAs.getTerminal("pdcm11"), boxAs.getTerminal("pdcm15"),
					boxAs.getRele(27));
		});

		// нажатие мышью на р.ц 3, зм10, зм13
		// зм20, зм23, зм90, зм97
		circuits.get(29).getTrack().setOnMouseClicked((e) -> {
			if (svetofori.get(5).getIsSelectMode())
				postPlusInZM(circuits.get(29), e, boxAs.getTerminal("pdcm11"), boxAs.getTerminal("pdcm13"),
						boxAs.getRele(27));
			if (svetofori.get(16).getIsSelectMode())
				postPlusInZM(circuits.get(29), e, boxAs.getTerminal("pdcm12"), boxAs.getTerminal("pdcm13"),
						boxAs.getRele(21));
			if (svetofori.get(11).getIsSelectMode())
				postPlusInZM(circuits.get(29), e, boxAs.getTerminal("pdcm19"), boxAs.getTerminal("pdcm17"),
						boxAs.getRele(25));
		});
		// нажатие мышью на р.ц 4, зм10, зм14
		// зм20, зм24
		circuits.get(33).getTrack().setOnMouseClicked((e) -> {
			if (svetofori.get(5).getIsSelectMode())
				postPlusInZM(circuits.get(33), e, boxAs.getTerminal("pdcm11"), boxAs.getTerminal("pdcm14"),
						boxAs.getRele(27));
			if (svetofori.get(16).getIsSelectMode())
				postPlusInZM(circuits.get(33), e, boxAs.getTerminal("pdcm12"), boxAs.getTerminal("pdcm14"),
						boxAs.getRele(21));
		});
		// нажатие мышью на р.ц 411, зм110, зм1113
		// зм30, зм31, зм40, зм41
		circuits.get(14).getTrack().setOnMouseClicked((e) -> {
			if (svetofori.get(1).getIsSelectMode())
				postPlusInZM(circuits.get(14), e, boxAs.getTerminal("pdcm21"), boxAs.getTerminal("pdcm22"),
						boxAs.getRele(40));
			if (svetofori.get(9).getIsSelectMode())
				postPlusInZM(circuits.get(14), e, boxAs.getTerminal("pdcm13"), boxAs.getTerminal("pdcm11"),
						boxAs.getRele(10));
			if (svetofori.get(12).getIsSelectMode())
				postPlusInZM(circuits.get(14), e, boxAs.getTerminal("pdcm14"), boxAs.getTerminal("pdcm11"),
						boxAs.getRele(15));
		});
		// нажатие мышью на р.ц 412, зм60, зм62
		// зм30, зм32, зм40, зм42
		circuits.get(10).getTrack().setOnMouseClicked((e) -> {
			if (svetofori.get(20).getIsSelectMode())
				postPlusInZM(circuits.get(10), e, boxAs.getTerminal("pdcm16"), boxAs.getTerminal("pdcm12"),
						boxAs.getRele(36));
			if (svetofori.get(9).getIsSelectMode())
				postPlusInZM(circuits.get(10), e, boxAs.getTerminal("pdcm13"), boxAs.getTerminal("pdcm12"),
						boxAs.getRele(10));
			if (svetofori.get(12).getIsSelectMode())
				postPlusInZM(circuits.get(10), e, boxAs.getTerminal("pdcm14"), boxAs.getTerminal("pdcm12"),
						boxAs.getRele(15));
		});

		// нажатие мышью на р.ц 7, зм70, зм79
		circuits.get(31).getTrack().setOnMouseClicked((e) -> {
			if (svetofori.get(10).getIsSelectMode())
				postPlusInZM(circuits.get(31), e, boxAs.getTerminal("pdcm17"), boxAs.getTerminal("pdcm19"),
						boxAs.getRele(39));
		});
		// обработка событий ------------конец-------------

	}// --------------------------------------------------- конструктор

	private void strelkaLossCheckup() {
		final AudioClip player;
		// стрелка 1 // 1 ПК - 2п1ПК // 1 МК - 2п1МК
		if (!boxAs.getRele(149).getIsCurrent() && !boxAs.getRele(151).getIsCurrent()) {
			player = new AudioClip(Main.class.getResource("res/audio/1strLossCheckup.mp3").toExternalForm());
			player.play();
			strelki.get(0).lossCheckup();
		}
	}

	public void perevodStrelki(Strelka str, boolean inPlus, MouseEvent me) {
		if ((me.getClickCount() == 1) && (me.getButton().equals(MouseButton.PRIMARY)) && (str.getIsSelectMode())) {
			str.stopSelect();
			switch (strelki.indexOf(str)) {
			case 0:
				((PowerSourceTerminalDCM) boxAs.getTerminal((inPlus) ? "pdcm25" : "pdcm26")).setPlusDcm();
				break;

			default:
				break;
			}

		}
	}

	/**
	 * @param startTr   р.ц. на которой индицируется начальный зм
	 * @param endTr     р.ц. на которой индицируется конечный зм
	 * @param upStartZm под током ли начальный зм
	 * @param upEndZm   под током ли конечный зм
	 */
	public void setIndicationZm(TrackCircuit startTr, TrackCircuit endTr, boolean upStartZm, boolean upEndZm) {

		if (upStartZm && !upEndZm) {
			startTr.setMod(1);
			startTr.startAtZm();
		}

		if (upStartZm && upEndZm && endTr != null) {
			startTr.stopAtZm();
			endTr.setMod(1);
			endTr.setIndicationZm(true);
			startTr.setIndicationZm(true);
		}

	}

	/**
	 * @param tr    - р.ц. на которую нажимаем мышью
	 * @param e     - событие мыши
	 * @param pdcmS Полюс ДЦМ в схеме начального ЗМ
	 * @param pdcmE Полюс ДЦМ в схеме конечного ЗМ
	 * 
	 *              При выбранном наборе на световоре отправка плюсового полюса в
	 *              схему ЗМ
	 */
	public void postPlusInZM(TrackCircuit tr, MouseEvent e, SchemElement pdcmS, SchemElement pdcmE, Rele startZm) {
		if ((e.getClickCount() == 1) && (e.getButton().equals(MouseButton.PRIMARY))) {
			if (tr.getIsControl() && tr.getIsSelectMode() && tr.getMod() == 2) {
				cancelSelectIndication();
				final AnimationTimer at = new AnimationTimer() {
					long count = 0;

					@Override
					public void handle(long arg0) {
						count++;
						if (count == 2) {
							((PowerSourceTerminalDCM) pdcmS).setPlusDcm();
						}
						if (count == 60) {
							if (startZm.getIsCurrent())
								((PowerSourceTerminalDCM) pdcmE).setPlusDcm();
							stop();
							count = 0;
						}
					}
				};
				at.start();
			}

		}

	}

	/**
	 * @param o светофор по которому задается маршрут
	 * @param e событие мыши
	 * 
	 *          идикация светофора и сопутсвующих р.ц. при событиях мыши
	 */
	private void svetoforControlByMouse(Svetofor o, MouseEvent e) {
		cancelSelectIndicationByMouse(o); // отмена набора
		// отмена индикации всех выбранных цепей кроме
		circuits.stream().filter((c) -> c.getIsSelectMode()).forEach((c) -> c.stopSelectTrack());

		// установка режима набора для выбранного светофора
		if ((e.getClickCount() == 1) && (e.getButton().equals(MouseButton.PRIMARY)) && o.getIsControl()
				&& !e.isSecondaryButtonDown()) {
			o.setSelectModSvetofor();
		}
		// установка режима отмены ОН,ГОМ
		if ((e.getClickCount() == 1) && (e.getButton().equals(MouseButton.PRIMARY)) && e.isSecondaryButtonDown()
				&& o.getIsControl()) {
			cancelSelectIndication(); // отмена набора
			// отправка плюсового полюса в схему реле ОН
			((PowerSourceTerminalDCM) boxAs.getTerminal("pdcm113")).setPlusDcm();
			// отправка плюсового полюса в схему реле ГОМ
			((PowerSourceTerminalDCM) boxAs.getTerminal("pdcmGom")).setPlusDcm();

			// задержка оправки плюса в схему Зс, чтобы ГОМ успел встать под ток
			final AnimationTimer atZaderZs = new AnimationTimer() {
				long count = 0;

				@Override
				public void handle(long now) {
					count++;
					if (count == 5) {

						// отправка плюсового полюса в схему реле зС
						//
						switch (svetofori.indexOf(o)) {
						// 413зС
						case 5: {
							((PowerSourceTerminalDCM) boxAs.getTerminal("pdcm11")).setPlusDcm();
						}
							break;
						// 3зС
						case 9:
							((PowerSourceTerminalDCM) boxAs.getTerminal("pdcm13")).setPlusDcm();
							break;
						// 4зС
						case 12:
							((PowerSourceTerminalDCM) boxAs.getTerminal("pdcm14")).setPlusDcm();
							break;
						// 7зС
						case 10:
							((PowerSourceTerminalDCM) boxAs.getTerminal("pdcm17")).setPlusDcm();
							break;
						// 9зС
						case 11:
							((PowerSourceTerminalDCM) boxAs.getTerminal("pdcm19")).setPlusDcm();
							break;
						// ДзС
						case 16:
							((PowerSourceTerminalDCM) boxAs.getTerminal("pdcm12")).setPlusDcm();
							break;
						// 416-410зС
						case 20:
							((PowerSourceTerminalDCM) boxAs.getTerminal("pdcm16")).setPlusDcm();
							break;
						// 407-411зС
						case 1:
							((PowerSourceTerminalDCM) boxAs.getTerminal("pdcm21")).setPlusDcm();
							break;

						default:
							break;
						}
						stop();
						count = 0;
					}
				}
			};// ----------------- atZaderZs
			atZaderZs.start();
		}
	}

	/**
	 * @param st -текущая стрелка Выбор стрелки для перевода
	 */
	private void selectStrelkaByPerevod(Strelka st, MouseEvent e) {
		cancelSelectIndicationByMouse(st);
		circuits.stream().filter((c) -> c.getIsSelectMode() && !c.equals(st)).forEach((c) -> c.stopSelectTrack());
		st.clickOnGeneral(e);
	}

	/**
	 * 
	 * отмена режима набора на всех выбранных элементах реле ОН ! без нажати мышью
	 * на объект при всех других случаях
	 */
	private void cancelSelectIndication() {
		strelki.stream().filter((o) -> o.getIsSelectMode()).forEach((o) -> o.stopSelect());
		svetofori.stream().filter((o) -> o.getIsSelectMode()).forEach((o) -> o.stopSelect());
		circuits.stream().filter((c) -> c.getIsSelectMode()).forEach((c) -> c.stopSelectTrack());
	}

	/**
	 * @param st отмена режима набора на всех элементах кроме выбрнного при нажатии
	 *           мышью на объект
	 */
	private void cancelSelectIndicationByMouse(StationElement st) {
		strelki.stream().filter((o) -> o.getIsSelectMode() && !o.equals(st)).forEach((o) -> o.stopSelect());
		svetofori.stream().filter((o) -> o.getIsSelectMode() && !o.equals(st)).forEach((o) -> o.stopSelect());
	}

	// перевод срелок без учета аппаратуры
	private void perevodStrelkiSimulator(Strelka st, int endMode, MouseEvent e) {

		final AnimationTimer atStrPerevod = new AnimationTimer() {
			long count = 0;

			@Override
			public void handle(long now) {
				if (count == 10) {

					st.stopSelect();
					if (st.getCheckup() == endMode) {
						this.stop();
						return;
					}
				}
				if (count == 60) {

					st.switching();
				}
				if (count == 210) {
					switch (endMode) {
					case 1: // перевод стрелки в плюс

						st.setPlusCheckup(true);
						break;
					case 2:

						st.setMinusCheckup(true);

						break;
					default:
						break;
					}
					this.stop();
				}
				count++;
			}
		};

		if ((e.getClickCount() == 1) && (e.getButton().equals(MouseButton.PRIMARY)) && (st.getIsSelectMode())) {
			atStrPerevod.start();
		}

	}

	// отображение станции в нормальном состоянии
	// сброс всех действий
	private void setNormalStation() {
		// освобождение всех рц
		circuits.stream().forEach((o) -> o.setShunt(false));

		// установка не индицируемых светофоров --------------------------------
		svTemp = (LeftSvetofor) svetofori.get(0);
		svTemp.setIndication(false);
		svTemp = (LeftSvetofor) svetofori.get(6);
		svTemp.setIndication(false);
		svTemp = (LeftSvetofor) svetofori.get(7);
		svTemp.setIndication(false);
		svTemp = (RightSvetofor) svetofori.get(13);
		svTemp.setIndication(false);
		// --------------------------------------------------------
		// Установка светофоров автоблокировки
		// --------------------------------------
		svTemp = (RightSvetofor) svetofori.get(14);
		svTemp.openSvetofor(true);
		svTemp = (RightSvetofor) svetofori.get(15);
		svTemp.openSvetofor(true);
		// ------------------------------------------------------------------
		// Установка управляемых объектов --------------------------------------
		svTemp = (LeftSvetofor) svetofori.get(1);
		svTemp.setLocalControl(true);
		svTemp = (LeftSvetofor) svetofori.get(5);
		svTemp.setLocalControl(true);
		svTemp = (LeftSvetofor) svetofori.get(10);
		svTemp.setLocalControl(true);
		svTemp = (LeftSvetofor) svetofori.get(16);
		svTemp.setLocalControl(true);
		svTemp = (RightSvetofor) svetofori.get(9);
		svTemp.setLocalControl(true);
		svTemp = (RightSvetofor) svetofori.get(11);
		svTemp.setLocalControl(true);
		svTemp = (RightSvetofor) svetofori.get(12);
		svTemp.setLocalControl(true);
		svTemp = (RightSvetofor) svetofori.get(20);
		svTemp.setLocalControl(true);
		// стрелки---------------

		strelki.stream().forEach((o) -> o.setLocalControl(true));
		// ------------------------------------------------------------------

	}

	// отображение тока стрелок
	private void countingTokStr() {
		double tok = 0;
		int colStr = (int) boxAs.getStrMotors().stream().filter((m) -> m.getIsWork()).count();
		switch (colStr) {
		case 0:
			tok = 0;
			break;
		case 1:
			tok = 1.54;
			break;
		case 2:
			tok = 1.72;
			break;
		case 3:
			tok = 1.92;
			break;
		case 4:
			tok = 2.32;
			break;

		default:
			break;
		}
		ampermetr.showTok(tok);
		// мигание транспаранта нагрузка
		int mod = (tok <= 1.8) ? 2 : 3;
		if (tok != 0)
			tp.getNagr().setBlinking(mod);
		else
			tp.getNagr().stopBlinking();
	}

	// private void setIndicationSvetoforD(boolean dBo, boolean dKo) {
	//
	// }

}
