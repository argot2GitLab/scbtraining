﻿package com.alex.training.scene;

import java.util.Arrays;
import java.util.List;

import javafx.scene.Group;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;

import com.alex.training.sceneElement.SchemContextMenu;
import com.alex.training.schem.BoxAs;
import com.alex.training.schem.ElCircuit;
import com.alex.training.schem.Loop;
import com.alex.training.schem.ParUnion;
import com.alex.training.schem.PowerSource;
import com.alex.training.schemElement.Clamp;
import com.alex.training.schemElement.PowerSourceTerminal;
import com.alex.training.schemElement.ReleNMSHM1_1120;
import com.alex.training.schemElement.ReleOMSHM;
import com.alex.training.schemElement.SLine;
import com.alex.training.schemElement.SPoint;
import com.alex.training.schemElement.SchemElement;
import com.alex.training.schemElement.Tester;
import com.alex.training.schemElement.TransSobs2Au3;
import com.alex.training.schemElement.Troynic;
import com.alex.training.schemElement.TylContact;
import com.alex.training.schemElement.blocks.BKR_76;
import com.alex.training.schemElement.blocks.SvetGol;

public class SchemaSvetoforaD extends Group {

	private ScrollPane scroll;
	private Double[] points;
	private PowerSourceTerminal fazaA, fazaB;
	private PowerSourceTerminal transG, transKo, transBo;
	private TransSobs2Au3 trans;
	private SLine sl1PrBo, sl1PrBoTr, sl2PrBo, sl2PrBoTr, sl3PrBo, sl4PrBo;
	private SLine sl1PrKo, sl2PrKo, sl3PrKo;
	private SLine slObrKo, slObrBo, slObrBoTr, slObrBoSvetGol, slObrBo1,
			slObrKo1, slObrKo1SvetGol;
	private SLine slPrBo1, slPrKo1, slObrKo1Tr;
	private Troynic dSu6, p414P6;
	private ReleOMSHM dBo, dKo;
	private SvetGol svetGol;
	private SPoint spDsu63P414p63, spObrBoObrBo1, spPrBoPrBo1, spPrKoPrKo1,
			spObrKoObrKo1;
	private AnchorPane field;

	// для тестирования --------------------
	private Text lamp = new Text("Белый");
	private Text lampK = new Text("Красный");
	private Loop loopBo, loopKo, loopKo1, loopKo2, loopKo3;
	private ParUnion unionBo, unionKo, unionKo12, unionKo3;
	private List<SchemElement> lBo, lKo, lKo1, lCheckMult;
	private ElCircuit ecKo, ecBo;
	private PowerSource pSourceBO, pSourceKO;
	private BoxAs boxAs;

	public SchemaSvetoforaD(BoxAs ba) {

		this.boxAs = ba;
		// поле схемы на которое добавляются элементы--------------------
		field = new AnchorPane();
		scroll = new ScrollPane();
		scroll.setHbarPolicy(ScrollBarPolicy.AS_NEEDED);
		scroll.setVbarPolicy(ScrollBarPolicy.ALWAYS);
		scroll.setLayoutX(2);
		scroll.setPrefViewportHeight((Screen.getPrimary().getBounds()
				.getHeight()));
		scroll.setPrefViewportWidth(Screen.getPrimary().getBounds().getWidth() - 20);
		scroll.setStyle("-fx-background: white");
		scroll.setContent(field);
		getChildren().addAll(scroll);

		// превичная обмотка
		// Начало от фазы А
		double startX = 50;
		double x = startX;
		double xTemp;
		double startY = 40;
		double y = startY;

		fazaA = new PowerSourceTerminal("18-19A", "");
		fazaA.drowElement(x, y);
		x += fazaA.getEndX();
		// Трансформатор
		trans = new TransSobs2Au3("ДС", "18-105");
		trans.drowElement(x, y);
		y += trans.getEndY();
		fazaB = new PowerSourceTerminal("18-20B", "");
		fazaB.drowElement(startX, y);

		field.getChildren().addAll(fazaA, fazaB, trans);

		// прямой провод
		// БО-----------------------------------------------------------
		x += trans.getEndX();
		y = startY + trans.getTopY();
		points = new Double[] { x, y, x + 40, y };
		sl1PrBo = new SLine(points);
		sl1PrBo.drowElement(0, 0);
		// прямой провод БО для подключения тестера к тройнику
		points = new Double[] { x + 80, y, x + 40, y };
		sl1PrBoTr = new SLine(points);
		sl1PrBoTr.drowElement(0, 0);

		// тройник ДСУ
		x += 80;
		dSu6 = new Troynic("ДСУ", "18-25", boxAs.getRele(3), 6, false);
		dSu6.drowElement(x, y - 40);

		// соединение
		x += dSu6.getEndX();
		xTemp = x; // вывод тройника ДСУ
		points = new Double[] { x, y, x + 80, y };
		sl2PrBo = new SLine(points);
		sl2PrBo.drowElement(0, 0);

		// прямой провод БО для подключения тестера к тройнику
		points = new Double[] { x + 160, y, x + 80, y };
		sl2PrBoTr = new SLine(points);
		sl2PrBoTr.drowElement(0, 0);
		x += 160;

		// тройник П414П
		p414P6 = new Troynic("П414П", "1-111", boxAs.getRele(4), 6, true);
		p414P6.drowElement(x, y - 40);

		// соединение
		x += p414P6.getEndX();
		points = new Double[] { x, y, x + 160, y };
		sl3PrBo = new SLine(points);
		sl3PrBo.drowElement(0, 0);
		x += 160;

		// огневое реле ДБО
		dBo = (ReleOMSHM) boxAs.getRele(0);
		dBo.drowElement(x, y - 40);

		// соединение
		x += dBo.getEndX();
		points = new Double[] { x, y, x + 200, y };
		sl4PrBo = new SLine(points);
		sl4PrBo.drowElement(0, 0);
		x += 200;

		field.getChildren().addAll(sl1PrBo, dSu6, sl1PrBoTr, sl2PrBo, p414P6,
				sl2PrBoTr, sl3PrBo, dBo, sl4PrBo);

		// прямой провод
		// KО-----------------------------------------------------------

		// соединение
		y += 130;
		points = new Double[] { x - 200, y, x, y };
		sl3PrKo = new SLine(points);
		sl3PrKo.drowElement(0, 0);
		x -= 200;

		// огневое реле ДКО
		x -= dBo.getEndX();
		dKo = (ReleOMSHM) boxAs.getRele(2);
		dKo.drowElement(x, y - 40);

		// соединение
		points = new Double[] { xTemp, dSu6.getEndY() + 40, xTemp, y, x, y };
		sl1PrKo = new SLine(points);
		sl1PrKo.drowElement(0, 0);

		// соединение
		x = xTemp + 160 + dBo.getEndX();
		points = new Double[] { x, dSu6.getEndY() + 40, x, y };
		sl2PrKo = new SLine(points);
		sl2PrKo.drowElement(0, 0);

		// точка соединения ДСУ тыл - П414П тыл
		spDsu63P414p63 = new SPoint();
		spDsu63P414p63.drowElement(x, y);

		field.getChildren().addAll(sl1PrKo, sl2PrKo, dKo, sl3PrKo,
				spDsu63P414p63);

		// обратный провод(от Светофорной головки)
		// КО-----------------------------------------------------------
		points = new Double[] { 820., 305., 820., 245., 1031., 245. };
		slObrKo = new SLine(points);
		slObrKo.drowElement(0, 0);

		// обратный провод(отрезок от траса)
		// KО1-----------------------------------------------------------
		x = 192;
		points = new Double[] { x, 120., x += 80, 120. };
		slObrKo1Tr = new SLine(points);
		slObrKo1Tr.drowElement(0, 0);
		// обратный провод (Общий)
		// KО1-----------------------------------------------------------
		points = new Double[] { 820., 305., x, 305., x, 120. };
		// points = new Double[] { x, 120., x, y += 95, 820.,305. };
		slObrKo1 = new SLine(points);
		slObrKo1.drowElement(0, 0);
		// обратный провод(от Светофорной головки)
		// KО1-----------------------------------------------------------
		points = new Double[] { 820., 305., 1031., 305. };
		slObrKo1SvetGol = new SLine(points);
		slObrKo1SvetGol.drowElement(0, 0);

		// точка соединения обратный КО и обратный КО1
		spObrKoObrKo1 = new SPoint();
		spObrKoObrKo1.drowElement(820, 305);

		// обратный провод (отрезок от транса)
		// БО-----------------------------------------------------------
		x = 192;
		points = new Double[] { x, 140., x += 60, 140. };
		// System.out.println(y) - ==305;
		// , x, y += 35, 850., y,
		// 850., y -= 225, 1031., y };
		slObrBoTr = new SLine(points);
		slObrBoTr.drowElement(0, 0);

		// обратный провод (от светофорной головки)
		// БО-----------------------------------------------------------
		points = new Double[] { 1031., 115., 850., 115., 850., 175. };
		// System.out.println(y) - ==305;
		slObrBoSvetGol = new SLine(points);
		slObrBoSvetGol.drowElement(0, 0);

		// обратный провод (общий)
		// БО-----------------------------------------------------------
		points = new Double[] { 850., 175., 850., 340., x, 340., x, 140. };
		// System.out.println(y) - ==305;
		slObrBo = new SLine(points);
		slObrBo.drowElement(0, 0);

		// обратный провод
		// БО1-----------------------------------------------------------
		points = new Double[] { 850., 175., 1031., 175. };
		slObrBo1 = new SLine(points);
		slObrBo1.drowElement(0, 0);

		// точка соединения обратный БО и обратный БО1
		spObrBoObrBo1 = new SPoint();
		spObrBoObrBo1.drowElement(850, 175);

		field.getChildren().addAll(slObrKo1Tr, slObrBoTr, slObrKo1SvetGol,
				slObrBoSvetGol, spObrBoObrBo1, spObrKoObrKo1, slObrKo,
				slObrBo1, slObrKo1, slObrBo);

		// прямой провод
		// БО1-----------------------------------------------------------
		// points = new Double[] { 1031., 140., 880., 140., 880., 80. };
		points = new Double[] { 880., 80., 880., 140., 1031., 140. };
		slPrBo1 = new SLine(points);
		slPrBo1.drowElement(0, 0);

		// точка соединения прямой БО и прямой БО1
		spPrBoPrBo1 = new SPoint();
		spPrBoPrBo1.drowElement(880, 80);
		// прямой провод
		// КО1-----------------------------------------------------------
		points = new Double[] { 880., 210., 880., 270., 1031., 270. };
		slPrKo1 = new SLine(points);
		slPrKo1.drowElement(0, 0);

		// точка соединения прямой КО и прямой КО1
		spPrKoPrKo1 = new SPoint();
		spPrKoPrKo1.drowElement(880, 210);

		field.getChildren().addAll(slPrBo1, spPrBoPrBo1, slPrKo1, spPrKoPrKo1);

		// светофорная головка
		List<Integer> numLamp = Arrays.asList(0, 4); //
		svetGol = new SvetGol("Светофор Д", numLamp);
		svetGol.drowElement(1031, 20);
		field.getChildren().addAll(svetGol);

		// для тестирования----------------------------
		lamp.setFont(Font.font("sans", FontWeight.BOLD, 40));
		lamp.setFill(Color.GREEN);
		lamp.setX(400);
		lamp.setY(400);
		lamp.setVisible(false);
		lampK.setFont(Font.font("sans", FontWeight.BOLD, 40));
		lampK.setFill(Color.RED);
		lampK.setX(400);
		lampK.setY(450);
		lampK.setVisible(false);

		// ReleDSH2 rel = new ReleDSH2("test","",true);
		// rel.drowElement(400, 500);
		// field.getChildren().addAll(rel);

		ReleNMSHM1_1120 rel = new ReleNMSHM1_1120("3NY", "2-35", false);
		rel.drowElement(400, 500);
		field.getChildren().addAll(rel);

		BKR_76 pred = new BKR_76("13-26");
		pred.drowElement(400, 600);
		field.getChildren().addAll(pred);

		// -----------------------------------------------------

		// список элементов схемы в которых надо высветить точки подключения
		// тестера
		lCheckMult = Arrays.asList(fazaA, fazaB, trans, sl1PrBo, sl1PrBoTr,
				sl2PrBo, sl2PrBoTr, sl3PrBo, dBo, sl4PrBo, sl1PrKo, sl2PrKo,
				dKo, sl3PrKo, slObrKo1, slObrKo1Tr, slObrBo, slObrBoTr, /*
																		 * slPrBo1,
																		 * slPrKo1
																		 * ,
																		 */
				svetGol);
		// список контура белого огня
		transG = trans.getTerm1();
		transBo = trans.getTerm2();
		lBo = Arrays.asList(transG, sl1PrBo, sl1PrBoTr, dSu6, sl2PrBo,
				sl2PrBoTr, p414P6, sl3PrBo, dBo, sl4PrBo, svetGol, slObrBo,
				slObrBoTr, transBo);

		// списки контуров красного огня
		TylContact dSu63 = dSu6.getAsTyl(false);
		transKo = trans.getTerm3();

		lKo = Arrays.asList(transG, sl1PrBo, sl1PrBoTr, dSu63, sl1PrKo,
				sl2PrKo, dKo, sl3PrKo, // добавил sl2PrKo ???
				svetGol, slObrKo1, slObrKo1Tr, transKo);

		TylContact p414P63 = p414P6.getAsTyl(true);

		lKo1 = Arrays.asList(transG, sl1PrBo, sl1PrBoTr, dSu6, sl2PrBo,
				sl2PrBoTr, p414P63, sl2PrKo, sl1PrKo, dKo, sl3PrKo, svetGol,
				slObrKo1, slObrKo1Tr, transKo);
		// контуры белого и красного огней
		loopBo = new Loop(lBo);
		unionBo = new ParUnion(Arrays.asList(loopBo));
		loopKo = new Loop(Arrays.asList(transG, sl1PrBo, sl1PrBoTr));
		loopKo1 = new Loop(Arrays.asList(dSu63, sl1PrKo));
		loopKo2 = new Loop(Arrays.asList(dSu6, sl2PrBo, sl2PrBoTr, p414P63,
				sl2PrKo));
		loopKo3 = new Loop(Arrays.asList(dKo, sl3PrKo, svetGol, slObrKo1,
				slObrKo1Tr, transKo));
		unionKo = new ParUnion(Arrays.asList(loopKo));
		unionKo12 = new ParUnion(Arrays.asList(loopKo1, loopKo2));
		unionKo3 = new ParUnion(Arrays.asList(loopKo3));

		// електрические цепи белого и красного огней
		ecKo = new ElCircuit(Arrays.asList(unionKo, unionKo12, unionKo3),
				Arrays.asList(dKo));
		ecBo = new ElCircuit(Arrays.asList(unionBo), Arrays.asList(dBo));
		// источники питания белого и красного огней
		pSourceBO = new PowerSource(13.0);
		transG.setVarName("transG");
		transBo.setVarName("transBo");
		transKo.setVarName("transKo");
		pSourceBO.addTerminal(transG);
		pSourceBO.addTerminal(transBo);
		pSourceKO = new PowerSource(12.0);
		pSourceKO.addTerminal(transG);
		pSourceKO.addTerminal(transKo);
		ecBo.setSource(pSourceBO);
		ecKo.setSource(pSourceKO);
		ecKo.elCircuitStart();
		ecBo.elCircuitStart();
		// tester.setCircuits(Arrays.asList(ecBo, ecKo));

		ecBo.userSuccessProperty().addListener(
				(property, oldValue, newValue) -> lamp.setVisible(newValue
						.booleanValue()));
		ecKo.userSuccessProperty().addListener(
				(property, oldValue, newValue) -> lampK.setVisible(newValue
						.booleanValue()));
	}
}
