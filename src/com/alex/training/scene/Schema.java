package com.alex.training.scene;

import java.util.List;

import com.alex.training.schem.ElCircuit;
import com.alex.training.schemElement.SchemElement;

public interface Schema {
	public List<SchemElement> getListElement();

	public List<ElCircuit> getListCircuits();

	public boolean isElementConteined(SchemElement sc);

	public String getSchemaName();

}
