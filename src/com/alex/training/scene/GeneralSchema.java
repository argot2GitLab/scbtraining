package com.alex.training.scene;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.Group;
import javafx.scene.layout.AnchorPane;

import com.alex.training.Main;
import com.alex.training.schem.BoxAs;
import com.alex.training.schem.ElCircuit;
import com.alex.training.schem.Loop;
import com.alex.training.schem.ParUnion;
import com.alex.training.schem.SchemWork;
import com.alex.training.schem.Source;
import com.alex.training.schemElement.SchemElement;
import com.alex.training.util.Helper;

public class GeneralSchema extends Group implements Schema {

	private BoxAs boxAs;
	private AnchorPane field;
	private List<SchemElement> list = new ArrayList<SchemElement>();
	private List<Loop> loops = new ArrayList<Loop>();
	private List<ParUnion> unions = new ArrayList<ParUnion>();
	private List<Source> sources = new ArrayList<Source>();
	private List<ElCircuit> circuits = new ArrayList<ElCircuit>();

	private SchemWork schemWork;
	private long schemaId;
	private String nameSchema;

	public GeneralSchema(BoxAs ba, String path, String ns) {

		boxAs = ba;
		setSchemaName(ns);
		field = new AnchorPane();
		getChildren().addAll(field);

		InputStream is = Main.class.getResourceAsStream(path);

		Helper.loadSchemaFromFile(is, boxAs, list, loops, unions, sources,
				circuits);
		field.getChildren().addAll(list);

		// -----------------------------------------------------------------------------------------------------------

		// запуск работы эл. цепей
		circuits.forEach((c) -> c.elCircuitStart());
		// -----------------------------------------------------------------------------------------------------------

		schemWork = new SchemWork(field, list, circuits, boxAs);
	}

	public long getSchemaId() {
		return schemaId;
	}

	public void setSchemaId(long schemaId) {
		this.schemaId = schemaId;
	}

	@Override
	public List<SchemElement> getListElement() {
		return list;
	}

	@Override
	public List<ElCircuit> getListCircuits() {
		return circuits;
	}

	@Override
	public boolean isElementConteined(SchemElement sc) {
		return list.contains(sc);
	}

	public SchemWork getSchemWork() {
		return schemWork;
	}

	@Override
	public String getSchemaName() {
		return nameSchema;
	}

	public void setSchemaName(String ns) {
		this.nameSchema = ns;
	}

	@Override
	public String toString() {
		return nameSchema;
	}

	public AnchorPane getField() {
		return field;
	}

}
