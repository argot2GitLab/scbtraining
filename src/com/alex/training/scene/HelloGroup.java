﻿/**
 * 
 */
package com.alex.training.scene;

import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import com.alex.training.Main;

/**
 * @author alex
 * 
 *         Страница запуска приложения
 *
 */
public class HelloGroup extends Group {

	private MediaPlayer playerVideo;
	private MediaView media;
	private Text txS;
	private Text txTr;

	public HelloGroup(Scene scene) {
		Image image = new Image(Main.class.getResource("res/img/hello6.jpg")
				.toExternalForm(), false);

		ImageView imv = new ImageView(image);
		imv.setFitHeight(scene.getHeight());
		imv.setFitWidth(scene.getWidth());
		// imv.setBlendMode(BlendMode.DARKEN);

		Media video = new Media(Main.class.getResource("res/video/full.flv")
				.toExternalForm());
		playerVideo = new MediaPlayer(video);

		media = new MediaView(playerVideo);
		media.setFitHeight(scene.getHeight());
		media.setFitWidth(scene.getWidth());
		media.setViewport(new Rectangle2D(scene.getWidth()/6, 0, scene.getWidth() - scene.getWidth()/6,
				scene.getHeight()));
		media.setPreserveRatio(false);
		media.setVisible(false);

		txS = new Text("Поиск неисаравностей в устройствах СЦБ");
		txS.setFont(Font.font("sans", FontWeight.BOLD, FontPosture.ITALIC, 50));
		txS.setX(scene.getWidth() - scene.getWidth() / 3);
		txS.setY(scene.getHeight() / 2 - scene.getHeight() / 8);
		txS.setWrappingWidth(txS.getBoundsInLocal().getWidth() / 2);
		txS.setFill(Color.AQUA);

		txTr = new Text("");

		getChildren().addAll(imv, txS, media);
	}

	public MediaPlayer getPlayerVideo() {
		return playerVideo;
	}

	public MediaView getMedia() {
		return media;
	}

	public Text getTxS() {
		return txS;
	}

}
