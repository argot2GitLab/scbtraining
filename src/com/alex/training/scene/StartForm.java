package com.alex.training.scene;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.stream.Collectors;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Separator;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.util.Pair;

import com.alex.training.Main;
import com.alex.training.schem.BoxAs;
import com.alex.training.schem.ElCircuit;
import com.alex.training.schem.User;
import com.alex.training.schem.error.AbstractElementError;
import com.alex.training.schem.error.ArmError;
import com.alex.training.schem.error.ColdSoldering;
import com.alex.training.schem.error.ElementError;
import com.alex.training.schem.error.PerechodResistence;
import com.alex.training.schemElement.SchemElement;

public class StartForm extends Group {

	private Button btnStart;
	private MenuBar menuBar;
	private Menu menuHelp;
	private MenuItem itemHelp;
	private MenuItem itemAbout;
	private TextField tfLogin;
	private PasswordField pfPass;
	private Button btnEnter;
	private Text txHello;
	private final String error = "ДСЦП сообщила: ";
	private final String recovery = "Неисправность устранена.";
	private Text txInfo;
	private AnchorPane aPaneTask;
	private List<Schema> schem;
	private List<RadioButton> rButtons;
	private List<ArmError> armErrors;
	private BoxAs boxAs;
	private List<ElementError> elErrors;
	private User rel = null;
	private SchemElement errorScElement = null;
	private String nameSchema = "";
	private ElCircuit errorCir = null;
	private Button btnSetup;
	private Button btnReset;
	private Button btnAnswer;
	private String elements;
	private int numAnswer;

	// свойство администратор ли пользователь
	public BooleanProperty isAdmin = new SimpleBooleanProperty();

	public final BooleanProperty isAdminProperty() {
		return isAdmin;
	}

	public final void setIsAdmin(boolean newValue) {
		isAdmin.set(newValue);
	}

	public final boolean getIsAdmin() {
		return isAdmin.get();
	}

	// свойство установлена ли ошибка в системе
	public BooleanProperty isSystemError = new SimpleBooleanProperty();
	private ElementError elementError;

	public final BooleanProperty isSystemErrorProperty() {
		return isSystemError;
	}

	public final void setIsSystemError(boolean newValue) {
		isSystemError.set(newValue);
	}

	public final boolean getIsSystemError() {
		return isSystemError.get();
	}

	public StartForm(List<Schema> sc, BoxAs ba) {
		boxAs = ba;
		if (sc != null)
			schem = sc;
		setIsAdmin(false);
		// основная панель окна
		BorderPane bp = new BorderPane();
		bp.setPrefSize(Screen.getPrimary().getBounds().getWidth(), Screen
				.getPrimary().getBounds().getHeight() - 110);

		getChildren().addAll(bp);

		// создание панели меню и установака в borderPane
		menuBar = new MenuBar();
		menuHelp = new Menu("Помощь");
		itemHelp = new MenuItem("Справка");
		itemAbout = new MenuItem("О программе");
		menuHelp.getItems().addAll(itemHelp, itemAbout);
		menuBar.getMenus().addAll(menuHelp);
		bp.setTop(menuBar);

		SplitPane spMain = new SplitPane();
		// spMain.setLayoutX(0);
		// spMain.setLayoutY(0);
		spMain.setStyle("-fx-border-width:4pt;");
		spMain.setOrientation(Orientation.HORIZONTAL);
		bp.setCenter(spMain);

		SplitPane spUser = new SplitPane();
		spUser.setOrientation(Orientation.VERTICAL);
		spUser.setStyle("-fx-border-width:4pt;");
		spUser.setPrefSize(spUser.getBoundsInLocal().getWidth(), spUser
				.getBoundsInLocal().getHeight());

		// панель аутентификации пользователей
		GridPane gpLogin = new GridPane();
		gpLogin.setAlignment(Pos.TOP_LEFT);
		gpLogin.setHgap(10);
		gpLogin.setVgap(10);
		gpLogin.setPadding(new Insets(25, 25, 25, 25));
		gpLogin.setLayoutX(0);
		gpLogin.setLayoutY(0);

		Label lbLogin = new Label("Логин");
		tfLogin = new TextField();
		gpLogin.add(lbLogin, 0, 0);
		gpLogin.add(tfLogin, 1, 0);

		Label lbPass = new Label("Пароль");
		pfPass = new PasswordField();
		gpLogin.add(lbPass, 0, 1);
		gpLogin.add(pfPass, 1, 1);

		btnEnter = new Button("Войти");
		gpLogin.add(btnEnter, 1, 2, 2, 1);
		// панель аутентификации пользователей -------- конец

		AnchorPane aPaneTable = new AnchorPane();

		// панель задач
		aPaneTask = new AnchorPane();
		txHello = new Text(
				"Здравствуйте, Вас приветствует программа по поиску неисправностей в устройствах СЦБ. Для начала задания нажмите кнопку <Старт>.");
		txHello.setFont(Font.font("sans", FontWeight.BOLD, 30));
		txHello.setWrappingWidth(Screen.getPrimary().getBounds().getWidth() / 1.5);
		txHello.setFill(Color.CYAN);
		txHello.setLayoutX(100);
		txHello.setLayoutY(50);

		btnStart = new Button("Старт");
		btnStart.setPrefSize(100, 40);
		btnStart.setLayoutX(100);
		btnStart.setLayoutY(txHello.getBoundsInLocal().getHeight() + 50);

		btnReset = new Button("Сбросить");
		btnReset.setPrefSize(100, 40);
		btnReset.setLayoutX(400);
		btnReset.setLayoutY(txHello.getBoundsInLocal().getHeight() + 50);
		btnReset.setDisable(true);

		btnSetup = new Button("Установить");
		btnSetup.setPrefSize(100, 40);
		btnSetup.setLayoutX(700);
		btnSetup.setLayoutY(txHello.getBoundsInLocal().getHeight() + 50);
		btnSetup.setDisable(true);

		Separator sep = new Separator(Orientation.HORIZONTAL);
		sep.setLayoutX(100);
		sep.setLayoutY(btnStart.getLayoutY() + 50);
		sep.setPrefWidth(txHello.getBoundsInLocal().getWidth());

		txInfo = new Text();
		txInfo.setFont(Font.font("times new roman", 24));
		txInfo.setWrappingWidth(Screen.getPrimary().getBounds().getWidth() / 1.5);
		txInfo.setLayoutX(100);
		txInfo.setLayoutY(sep.getLayoutY() + 50);
		txInfo.setFill(Color.RED);
		txInfo.setText(error);
		txInfo.setVisible(false);

		Separator sep1 = new Separator(Orientation.HORIZONTAL);
		sep1.setLayoutX(100);
		sep1.setLayoutY(txInfo.getLayoutY() + 100);
		sep1.setPrefWidth(txHello.getBoundsInLocal().getWidth());

		// Создание списка кнопок для выбора правильных ответов
		ToggleGroup tGroup = new ToggleGroup();

		rButtons = new ArrayList<RadioButton>();
		double y = sep1.getLayoutY() + 50;
		for (int i = 0; i < 7; i++) {
			rButtons.add(i, new RadioButton());
			rButtons.get(i).setLayoutX(100);
			rButtons.get(i).setLayoutY(y);
			rButtons.get(i).setToggleGroup(tGroup);
			rButtons.get(i).setVisible(false);
			rButtons.get(i).setFont(Font.font(18));
			y += 50;
		}

		btnAnswer = new Button("Ответить");
		btnAnswer.setPrefSize(100, 40);
		btnAnswer.setLayoutX(100);
		btnAnswer.setLayoutY(y);
		btnAnswer.setVisible(false);

		aPaneTask.getChildren().addAll(txHello, btnStart, btnSetup, btnReset,
				sep, txInfo, sep1, rButtons.get(0), rButtons.get(1),
				rButtons.get(2), rButtons.get(3), rButtons.get(4),
				rButtons.get(5), rButtons.get(6), btnAnswer);
		// панель задач ------------- конец

		// вставка в сплит панеи
		spUser.getItems().addAll(gpLogin, aPaneTable);
		spUser.setDividerPositions(0.15);

		spMain.getItems().addAll(spUser, aPaneTask);
		spMain.setDividerPositions(0.2);

		// --------------------------- создание неисправностей
		initErrors();

		// установка роли пользователя( админ или нет) для всех схем
		// для вывода меню
		isAdminProperty().addListener(
				(property, oldValue, newValue) -> {
					for (Schema scm : schem) {
						((GeneralSchema) scm).getSchemWork().getsContextMenu()
								.setIsAdmin(newValue.booleanValue());
					}
				});
		//
		isSystemErrorProperty().addListener((property, oldValue, newValue) -> {
			if (!newValue.booleanValue())
				uninstallError();
		});

		btnAnswer.setOnAction((ae) -> {
			checkAnswer();
		});
	} // --------------------------- конструктор

	private void checkAnswer() {
		int num = 0;

		for (RadioButton rb : rButtons) {
			if (rb.isSelected())
				num = rButtons.indexOf(rb);
		}
		if (numAnswer == num) {
			txInfo.setText("ПРАВИЛЬНЫЙ ОТВЕТ!");
			txInfo.setFill(Color.GREEN);
			btnReset.setDisable(true);
			btnStart.setDisable(false);
			setRButtosVisble(false);
			btnAnswer.setVisible(false);
			boxAs.setNormalStateRele();
		} else {
			txInfo.setText("ОТВЕТ НЕВЕРНЫЙ!");
			txInfo.setFill(Color.RED);
		}

	}

	/**
	 * создание списков неисправностей и загрузка файлов свойств
	 */
	private void initErrors() {
		armErrors = new ArrayList<ArmError>();
		armErrors
				.add(0,
						new ArmError(
								"не задается маршрут по светофору Лн413 с первого главного пути на третий станционный путь."));
		armErrors.add(1, new ArmError(
				"не разделывается маршрут по светофору Лн413."));

		InputStream is = Main.class
				.getResourceAsStream("res/error/error.properties");
		Properties property = new Properties();
		try {
			property.load(is);
			elements = property.getProperty("noSetTrack1_3");
			for (String element : elements.split(",")) {
				armErrors.get(0).getElements().add(Integer.valueOf(element));
			}
		} catch (Exception e) {
			System.err.println("ОШИБКА: Файл свойств повреждён!");
		}

		// -----------------------------------------------------------------------------
		// создание списка неисправностей элементов

		elErrors = Arrays.asList(new PerechodResistence(), new ColdSoldering());
		for (int i = 0; i < elErrors.size(); i++) {
			((AbstractElementError) elErrors.get(i)).setIdError(i);
		}
	}

	public Button getBtnStart() {
		return btnStart;
	}

	public Button getBtnEnter() {
		return btnEnter;
	}

	public Text getTxHello() {
		return txHello;
	}

	public Text getTxInfo() {
		return txInfo;
	}

	public Button getBtnSetup() {
		return btnSetup;
	}

	public Button getBtnReset() {
		return btnReset;
	}

	public SchemElement getErrorScElement() {
		return errorScElement;
	}

	// установка неисправности и вывод значений в список
	// ответов-------------------
	public void setupError() {
		// выбираем неисправность арма
		ArmError armEr = armErrors.get(0);
		txInfo.setFill(Color.RED);
		txInfo.setText(error + armEr.getInfo());
		txInfo.setVisible(true);

		// Получаем случайный элемент и устанавливаем неисправность

		List<Pair<Integer, Integer>> listPair = armEr.getRandomPairs(7);
		Random rand = new Random(new Date().getTime());
		numAnswer = rand.nextInt(listPair.size());
		for (int i = 0; i < listPair.size(); i++) {
			// Устанавливаем ошибку на один элемент а остальные выводим в список
			// ответов
			Pair<Integer, Integer> pair = listPair.get(i);
			List<SchemElement> elements = new ArrayList<SchemElement>();

			elements = schem
					.get(pair.getKey())
					.getListElement()
					.stream()
					.filter((el) -> el.getIdElementInSchem() == pair.getValue())
					.collect(Collectors.toList());
			if (!elements.isEmpty()) {
				errorScElement = elements.get(0);
			}
			if (errorScElement != null) {
				// берем ошибку
				// случайная ошибка из списка
				// получаем временный список ошибок которые возможно
				// установить на выбранный схемный элемент
				List<ElementError> list = new ArrayList<ElementError>();
				list = elErrors
						.stream()
						.filter((el) -> ((AbstractElementError) el)
								.isClassConteined(errorScElement.getClass())
								|| ((AbstractElementError) el)
										.isClassConteined(errorScElement
												.getClass().getSuperclass()))
						.collect(Collectors.toList());
				if (!list.isEmpty()) {
					int j = rand.nextInt(list.size());
					elementError = list.get(j);
				}
				// устанавливаем связь общего свойства ошибки системы
				// при изменении ошибки элемента
				// на случай если потом захочется установить сразу несколько
				// неисправностей
				if (elementError != null) {

					elementError.setupElementErrorInfo(errorScElement);
					nameSchema = schem.get(pair.getKey()).toString();

					rButtons.get(i).setText(
							elementError.getInfo() + "- схема " + nameSchema);
					// устанавливаем случайно выбранную ошибку
					if (i == numAnswer) {
						errorScElement.isErrorProperty().addListener(
								(property, oldValue, newValue) -> {
									setIsSystemError(newValue.booleanValue());
								});
						// режим ошибки для элемента фактически является номером
						// ошибки
						errorScElement
								.setErrorMod(((AbstractElementError) elementError)
										.getIdError());
						errorScElement.setIsError(true);
						System.out.println(elementError.getInfo() + "- схема "
								+ nameSchema);
					}

				}

			}
		}
	}

	public void uninstallError() {
		txInfo.setText(recovery);
		txInfo.setFill(Color.GREEN);
		setRButtosVisble(true);
		btnAnswer.setVisible(true);

	}

	private void setRButtosVisble(boolean flag) {
		for (RadioButton but : rButtons) {
			but.setVisible(flag);
		}
	}

	// Проверка пароля
	public boolean checkPass() {
		if (tfLogin.getText() != null && tfLogin.getText().equals("1")
				&& pfPass.getText().equals("1"))
			setIsAdmin(true);
		else
			setIsAdmin(false);

		tfLogin.setText("");
		pfPass.setText("");

		return getIsAdmin();
	}
}
