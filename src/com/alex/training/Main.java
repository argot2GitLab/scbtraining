﻿package com.alex.training;

import javafx.application.Application;
import javafx.geometry.Side;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;

import com.alex.training.scene.Editor;
import com.alex.training.scene.HelloGroup;
import com.alex.training.scene.SchemaView;
import com.alex.training.scene.StartForm;
import com.alex.training.scene.Station;
import com.alex.training.schem.BoxAs;

public class Main extends Application {

	private void init(Stage primaryStage) {

		try {
			Group root = new Group();
			Scene scene = new Scene(root, Screen.getPrimary().getBounds()
					.getWidth(),
					Screen.getPrimary().getBounds().getHeight() - 80,
					Color.GREY);
			scene.getStylesheets().add(
					getClass().getResource("application.css").toExternalForm());
			primaryStage.setTitle("Тренажер ст. Площадь Ленина");

			// Установка иконки
			Image image = new Image(Main.class
					.getResource("res/img/alexTM.jpg").toExternalForm(), false);
			primaryStage.getIcons().add(image);

			primaryStage.setResizable(false);
			primaryStage.setScene(scene);

			// Создание окна приветствия-------------------------------
			HelloGroup helloGroup = new HelloGroup(scene);
			root.getChildren().addAll(helloGroup);

			helloGroup.getTxS().setOnMouseEntered(
					(me) -> {
						helloGroup.getTxS().setEffect(
								new DropShadow(BlurType.GAUSSIAN,
										Color.DARKGRAY, 5, 3, 3, 3));

					});
			helloGroup.getTxS().setOnMouseExited((me) -> {
				helloGroup.getTxS().setEffect(null);
			});
			// Создание окна приветствия-------------------------------
			helloGroup.getTxS().setOnMouseClicked((me) -> {
				helloGroup.getMedia().setVisible(true);
				helloGroup.getPlayerVideo().play();
			});

			// окна приветствия==============================================

			BoxAs boxAs = new BoxAs();

			// Панель закладок -----------------------------
			TabPane tp = new TabPane();
			tp.setLayoutX(2);
			tp.setLayoutY(2);
			tp.setSide(Side.TOP);
			tp.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
			tp.setTabMinHeight(30);
			tp.setTabMinWidth(100);
			tp.setMaxWidth(Screen.getPrimary().getBounds().getWidth());
			tp.setMinWidth(Screen.getPrimary().getBounds().getWidth());

			// АРМ ДСЦП---------------------
			Tab tabControl = new Tab("Арм ДСЦП");
			Group rootControl = new Group();
			tabControl.setContent(rootControl);
			Station station = new Station(boxAs);
			rootControl.getChildren().addAll(station);

			// Закладка схемы ---------------------

			Tab tabSchema = new Tab("Схемы");
			Group rootSchema = new Group();
			tabSchema.setContent(rootSchema);
			SchemaView schema = new SchemaView(boxAs);
			rootSchema.getChildren().add(schema);

			// Форма запуска задания--------------
			Tab tabSelect = new Tab("Выбор задания");
			Group rootSelectTask = new Group();
			tabSelect.setContent(rootSelectTask);
			StartForm start = new StartForm(schema.getSchem(), boxAs);
			rootSelectTask.getChildren().addAll(start);

			// Редактор схем--------------
			Tab tabEditor = new Tab("Редактор схем");
			Group rootEditor = new Group();
			tabEditor.setContent(rootEditor);
			Editor editor = new Editor(boxAs);
			rootEditor.getChildren().addAll(editor);

			tp.getTabs().addAll(tabSelect, tabControl, tabSchema);
			// ===============================================================

			SingleSelectionModel<Tab> selectionModel = tp.getSelectionModel();
			selectionModel.select(0);
			tp.getTabs().get(1).setDisable(true);
			tp.getTabs().get(2).setDisable(true);

			// загрузка основных окон (панели закладок)
			helloGroup.getPlayerVideo().setOnEndOfMedia(() -> {
				helloGroup.getPlayerVideo().stop();
				root.getChildren().addAll(tp);
				root.getChildren().remove(helloGroup);
			});
			// Вход под паролем администратора
			start.getBtnEnter().setOnAction((ae) -> {
				if (start.checkPass()) {
					tp.getTabs().get(1).setDisable(false);
					tp.getTabs().get(2).setDisable(false);
					start.getBtnStart().setDisable(true);
					start.getBtnReset().setDisable(true);
					start.getBtnSetup().setDisable(false);
					tp.getTabs().addAll(tabEditor);

				}
			});

			// запуск неисправности
			start.getBtnStart().setOnAction((ae) -> {
				tp.getTabs().get(1).setDisable(false);
				tp.getTabs().get(2).setDisable(false);
				start.setupError();
				start.getBtnStart().setDisable(true);
			});

			// установка неисправности вручную
			start.getBtnSetup().setOnAction((ae) -> {
				start.setIsAdmin(false);
			});

			// сброс неисправности
			start.getBtnReset().setOnAction((ae) -> {
				tp.getTabs().get(1).setDisable(true);
				tp.getTabs().get(2).setDisable(true);
				start.getBtnStart().setDisable(false);
				start.getBtnReset().setDisable(true);
				start.uninstallError();
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void start(Stage primaryStage) {
		init(primaryStage);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
