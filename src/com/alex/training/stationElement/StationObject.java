﻿package com.alex.training.stationElement;

import javafx.animation.AnimationTimer;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class StationObject extends Group {

	private Rectangle platforma;
	private Rectangle control;
	private Text w1;
	private Text w2;
	private Text trA;
	private Text trB;
	private Text trP;
	private Text trN;
	private Text trK;
	private Text trSh;
	private Font font;
	private Disperser cover;
	private final Color color = Color.gray(0.41, 1);

	public StationObject() {
		// общий элемент
		platforma = new Rectangle(360, 100, Color.gray(0.35, 1));
		platforma.setX(1);
		platforma.setY(20);
		platforma.setStroke(Color.BLACK);
		// I главный станционный путь
		w1 = new Text("I главный станционный путь");
		w1.setFill(Color.YELLOW);
		w1.setFont(Font.font("sans", FontWeight.MEDIUM, 20));
		w1.setX(181 - w1.getBoundsInLocal().getWidth() / 2);
		w1.setY(150);
		// II главный станционный путь
		w2 = new Text("II главный станционный путь");
		w2.setFill(Color.YELLOW);
		w2.setFont(Font.font("sans", FontWeight.MEDIUM, 20));
		w2.setX(181 - w1.getBoundsInLocal().getWidth() / 2);
		w2.setY(5);
		// местный или диспетчерский контроль
		control = new Rectangle(20, 50, Color.YELLOW);
		control.setX(335);
		control.setY(60);
		// колпачок
		cover = new Disperser();
		cover.setLayoutX(270);
		cover.setLayoutY(15);
		cover.fillDisperser();

		// Транспаранты
		font = Font.font("sans", FontWeight.MEDIUM, 34);
		trA = new Text("А");
		trA.setFont(font);
		trA.setFill(Color.WHITE);
		trA.setX(40);
		trA.setY(60);

		trB = new Text("Б");
		trB.setFont(font);
		trB.setFill(color);
		trB.setX(70);
		trB.setY(60);

		trP = new Text("П");
		trP.setFont(font);
		trP.setFill(color);
		trP.setX(40);
		trP.setY(110);

		trN = new Text("Н");
		trN.setFont(font);
		trN.setFill(color);
		trN.setX(70);
		trN.setY(110);

		trK = new Text("К");
		trK.setFont(font);
		trK.setFill(color);
		trK.setX(120);
		trK.setY(110);

		trSh = new Text("Ш");
		trSh.setFont(font);
		trSh.setFill(color);
		trSh.setX(170);
		trSh.setY(110);

		getChildren().addAll(platforma, w1, w2, control, cover, trA, trB, trP,
				trN, trK, trSh);
	}

	public void setComplectA(boolean flag) {
		if (flag) {
			trA.setFill(Color.WHITE);
			trB.setFill(color);
		} else {
			trA.setFill(color);
			trB.setFill(Color.WHITE);
		}
	}

	public void setError(boolean flag) {
		AnimationTimer at = new AnimationTimer() {
			long count = 0;
			boolean f = true;

			@Override
			public void handle(long arg0) {
				if (count % 30 == 0) {
					Color col = (f) ? Color.RED : color;
					trN.setFill(col);
					f = !f;
				}
				count++;
			}
		};
		if (flag)
			at.start();
		else
			at.stop();

	}

}
