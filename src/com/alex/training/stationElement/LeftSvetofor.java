﻿package com.alex.training.stationElement;

import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Path;

public class LeftSvetofor extends Svetofor {

	public LeftSvetofor(String name, boolean isControl, double x, double y, boolean isIndication) {
		super(name, isControl, isIndication);

		
		final double centerY = 68;
		
		// Название светофора
		svName.setX(1);
		svName.setY(centerY + 5);
		
		final double footX = svName.getBoundsInLocal().getWidth() + 5;

		verticalFoot.setX(footX);
		verticalFoot.setY(50);
		horizontFoot.setX(footX + 8);
		horizontFoot.setY(65.5);
		foot = Path.union(horizontFoot, verticalFoot);
		foot.setStroke(Color.WHITE);
		foot.setFill(color);

		// Элемент ячейка
		cell.setCenterX(footX + 33);
		cell.setCenterY(centerY);

		// Текстовый элемент Набор, Разделка, авария
		txtOperation.setX(footX);
		txtOperation.setY(centerY + 35);

		// Полоски разделки
		Line line1, line2, line3, line4;
		line1 = new Line(footX + 14, centerY-22, footX + 18, centerY-18);
		line2 = new Line(footX + 52,centerY-22, footX + 48, centerY-18);
		line3 = new Line(footX + 14, centerY+22, footX + 18, centerY+18);
		line4 = new Line(footX + 52, centerY+22, footX + 48, centerY+18);
		lines = Path.union(line1, line2);
		lines = Path.union(lines, line3);
		lines = Path.union(lines, line4);
		lines.setVisible(false);

		// колпачок
		cover.setLayoutX(footX);
		cover.setLayoutY(8);
		
		// кружки ОП
		cop.setLayoutX(footX + 18);
		cop.setLayoutY(centerY-31);
		
		// Зона выбора
		selectZone.setLayoutX(footX-5);
		selectZone.setLayoutY(centerY-35);

		
		getChildren().addAll(selectZone,foot, cell, svName, txtOperation, lines, cover,
				cop);


		this.setLayoutX(x-footX);
		this.setLayoutY(y-35);

//		// обработка событий-------------------------------
//		foot.setOnMouseEntered((MouseEvent e) -> {
//			onMouseEntered();
//		});
//		foot.setOnMouseExited((MouseEvent e) -> {
//			onMouseExited();
//		});




	}

}
