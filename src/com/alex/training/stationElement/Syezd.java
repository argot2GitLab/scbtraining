﻿package com.alex.training.stationElement;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class Syezd extends TrackCircuit {

	private LeftStrelka strL;
	private RightStrelka strR;

	public Syezd(String name, boolean isControl, double width, double x,
			double y, boolean isMinusUp) {
		super("", isControl, width, x + width + 240, y);
		strL = new LeftStrelka("", isControl, width, x, y, false, isMinusUp);
		strR = new RightStrelka("", isControl, width, x + width + 120, y,
				false, isMinusUp);
		this.track = strR.getTrack();

		Text n = new Text(name);
		n.setFill(Color.WHITE);
		n.setFont(Font.font("sans", FontWeight.BOLD, 14));
		n.setX(x + width + 115);
		double h = (isMinusUp) ? y + 40 : y - 15;
		n.setY(h);

		getChildren().addAll(strL, strR, n);

	}

	public LeftStrelka getStrL() {
		return strL;
	}

	public RightStrelka getStrR() {
		return strR;
	}

	@Override
	public void setShunt(boolean f) {
		strL.setShunt(f);
		strR.setShunt(f);
	}

}
