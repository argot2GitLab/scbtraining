﻿package com.alex.training.stationElement;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

public class RightStrelka extends Strelka {
	
	public RightStrelka() {
		// TODO Auto-generated constructor stub
	}

	public RightStrelka(String name, boolean isControl, double widthGeneral,
			double x, double y, boolean isOne, boolean isMinusUp) {
		super(name, isControl, widthGeneral, x, y);

		double widthPlus = 120;
		color = Color.LIGHTGREY;

		track.setX(x+widthPlus);
		szTrack.setLayoutX(x+widthPlus);

		// Плюсовое ответвление без контроля--------------------------
		plusFree.setWidth(widthPlus);
		plusFree.setHeight(5);
		plusFree.setFill(color);
		
		plusFree.setX(x);
		plusFree.setY(y + height / 2 - 2.5);
		plusFree.setVisible(false);
		// ---------------------------------------------------------

		// минусовое ответвление без
		// контроля-----------------------------------------
		Double points[];
		if (!isMinusUp) {
			// минус вниз на одной
			// стрелке------------------------------------------
			points = (isOne) ? new Double[] { x + widthPlus,
					y + height / 2 + 2.5, // первая
					x + widthPlus, y + height / 2 - 2.5, // вторая
					x + widthPlus - 2, y + height / 2 - 2.5, // 3
					x + widthPlus - 82, y + 110 + height / 2 - 2.5, // 4
					x + widthPlus - 95, y + 110 + height / 2 - 2.5, // 5
					x + widthPlus - 95, y + 110 + height / 2 + 2.5, // 6
					x + widthPlus - 80, y + 110 + height / 2 + 2.5, // 7
			}
			// минус вниз на
			// съезде----------------------------------------------
					: new Double[] { x + widthPlus, y + height / 2 + 2.5, // первая
							x + widthPlus, y + height / 2 - 2.5, // вторая
							x + widthPlus - 1, y + height / 2 - 2.5, // 3
							x + widthPlus - 123, y + 110, // 4
							x + widthPlus - 116, y + 110 // 5
					};
		} else {
			// минус вверх на одной
			// стрелке------------------------------------------
			points = (isOne) ? new Double[] { x + widthPlus,
					y + height / 2 - 2.5, // первая
					x + widthPlus, y + height / 2 + 2.5, // вторая
					x + widthPlus - 2, y + height / 2 + 2.5, // 3
					x + widthPlus - 82, y - 110 + height / 2 + 2.5, // 4
					x + widthPlus - 95, y - 110 + height / 2 + 2.5, // 5
					x + widthPlus - 95, y - 110 + height / 2 - 2.5, // 6
					x + widthPlus - 80, y - 110 + height / 2 - 2.5, // 7
			// минус вверх на съезде------------------------------------------
			}
					: new Double[] { x + widthPlus, y + height / 2 - 2.5, // первая
							x + widthPlus, y + height / 2 + 2.5, // вторая
							x + widthPlus - 1, y + height / 2 + 2.5, // 3
							x + widthPlus - 123, y - 110, // 4
							x + widthPlus - 116, y - 110 // 5
					};

		}
		minusFree.getPoints().addAll(points);

		minusFree.setFill(color);
		minusFree.setVisible(true);
		// -----------------------------------------------------------------

		// Плюсовое положение стрелки----------------------
		plus.setWidth(widthPlus);
		plus.setHeight(height);
		plus.setFill(color);
		
		plus.setX(x);
		plus.setY(y);
		plus.setStroke(Color.BLACK);
		plus.setVisible(true);
		// ------------------------------------------------------

		// минусовое положение стрелки------------------------------------------
		if (!isMinusUp) {
			// минус вниз на одной
			// стрелке------------------------------------------
			points = (isOne) ? new Double[] { x + widthPlus, y + height, // первая
					x + widthPlus, y, // вторая
					x + widthPlus - 5, y, // 3
					x + widthPlus - 85, y + 110, // 4
					x + widthPlus - 125, y + 110, // 5
					x + widthPlus - 125, y + 110 + height, // 6
					x + widthPlus - 80, y + 110 + height, // 7
			// минус вниз на
			// съезде----------------------------------------------
			}
					: new Double[] { x + widthPlus, y + height, // первая
							x + widthPlus, y, // вторая
							x + widthPlus - 5, y, // 3
							x + widthPlus - 130, y + 110, // 4
							x + widthPlus - 108, y + 110, // 5
					};

		} else {
			// минус вверх на одной
			// стрелке------------------------------------------
			points = (isOne) ? new Double[] { x + widthPlus, y, // первая
					x + widthPlus, y + height, // вторая
					x + widthPlus - 5, y + height, // 3
					x + widthPlus - 85, y - 110 + height, // 4
					x + widthPlus - 95, y - 110 + height, // 5
					x + widthPlus - 95, y - 110., // 6
					x + widthPlus - 80, y - 110., // 7
			// минус вверх на съезде------------------------------------------
			}
					: new Double[] { x + widthPlus, y, // первая
							x + widthPlus, y + height, // вторая
							x + widthPlus - 5, y + height, // 3
							x + widthPlus - 130, y - 110, // 4
							x + widthPlus - 108, y - 110 // 5
					};
		}
		minus.getPoints().addAll(points);
		minus.setFill(color);
		minus.setStroke(Color.BLACK);
		minus.setVisible(false);

		// многофункциональный контрольный элемент стрелки
		// --------------------------
		square.setX(x + widthPlus);
		square.setY(y - 15);
		square.setFill(Color.TRANSPARENT);
		square.setStroke(Color.BLACK);
		square.setVisible(false);
		//

		// номер стрелки------------------------------
		if (isMinusUp) {
			// минус вверх
			noStr.setX(x + widthPlus - 15);
			noStr.setY(y + height + 20);
		} else {
			// минус вниз
			noStr.setX(x + widthPlus -15);
			noStr.setY(y - 5);
		}
		// ---------------------------------------------------------

		// индикация плюсового положения--------------------------
		double h = (isMinusUp) ? y - 5 : y + 20;
		l = new Line(x + widthPlus - 40, h, x + widthPlus - 55, h);
		l.setStrokeWidth(2.5);
		// -------------------------------------------------------

		// зона выбора -------------------------------------------
		h = (isMinusUp) ? y-115 : y -20;
		selectZone = new SelectZone(widthPlus + 2, 155);
		selectZone.setLayoutX(x);
		selectZone.setLayoutY(h);
		selectZone.setVisible(true);
		// --------------------------------------------------------


		getChildren().addAll(selectZone,plusFree, minusFree, plus, minus, noStr, l,
				square);

		tcName.setX(x+widthPlus+widthGeneral/2-tcName.getBoundsInLocal().getWidth()/2);
	}

}
