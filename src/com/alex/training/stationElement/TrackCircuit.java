﻿package com.alex.training.stationElement;

import javafx.animation.AnimationTimer;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Path;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class TrackCircuit extends StationElement {

	protected Rectangle track;
	protected final double height = 15;
	private Shape indZm;
	private Shape arrow;
	protected Color color;
	protected double x, y;
	protected Text tcName;
	protected SelectZone szTrack;
	private long count = 0;
	private long countZm = 0;
	private boolean flagZm;
	
	public TrackCircuit() {
		super();
		x = 2;
		y = 25;
	}

	// модификатор начальной, конечной или промежуточной секции
	// 0- промежуточная
	// 1- начальная
	// 2 - конечная------------------------------------------------------
	public IntegerProperty mod = new SimpleIntegerProperty();

	public final IntegerProperty modProperty() {
		return mod;
	}

	public final void setMod(int newValue) {
		mod.set(newValue);
	}

	public final int getMod() {
		return mod.get();
	}

	// ---------------------------------------------------------------

	public TrackCircuit(String name, boolean isControl, double width, double x,
			double y) {
		super(name, isControl);
		this.x = x; // установка начальных координат
		this.y = y;
		mod.set(0);
		// рельсовая цепь
		track = new Rectangle(width, height);
		track.setX(x);
		track.setY(y);
		track.setFill(Color.LIGHTGREY);
		track.setStroke(Color.BLACK);

		// название рц
		tcName = new Text(name);
		tcName.setFont(Font.font("sans", FontWeight.BOLD, 14));
		tcName.setFill(Color.WHITE);
		tcName.setX(x + track.getWidth() / 2
				- tcName.getBoundsInLocal().getWidth() / 2);
		tcName.setY(y - 5);

		// индикация реле задания маршрута ЗМ
		Rectangle r1 = new Rectangle(10, 10);
		r1.setX(x);
		r1.setY(y + 17);
		Rectangle r2 = new Rectangle(10, 10);
		r2.setX(width - 10 + x);
		r2.setY(y + 17);
		indZm = Path.union(r1, r2);
		indZm.setFill(Color.WHITE);
		indZm.setStroke(Color.BLACK);
		indZm.setVisible(false);

		// стрелка подсказки окончания маршрутов
		Rectangle ra = new Rectangle(4, 8);
		ra.setX(x + width / 2 - 2);
		ra.setY(y + 27);
		Polyline ar = new Polyline();
		ar.getPoints().addAll(
				new Double[] { x + width / 2, y + 19., x + width / 2 - 4,
						y + 27., x + width / 2 + 4, y + 27., x + width / 2,
						y + 19. });
		ar.setFill(Color.WHITE);

		arrow = Path.union(ra, ar);
		arrow.setFill(Color.WHITE);
		arrow.setVisible(false);

		// зона выбора----------------------------------------------
		szTrack = new SelectZone(width, 40);
		szTrack.setLayoutX(x);
		szTrack.setLayoutY(y - 20);
		szTrack.setVisible(false);
		// изменение длины зоны выбора при изменении длины рц
		track.widthProperty().addListener(
				(property, oldValue, newValue) -> szTrack.setWidth(newValue
						.doubleValue()));
		// -----------------------------------------------------------

		getChildren().addAll(szTrack, track, tcName, indZm, arrow);

		// обработка событий -------------------------------

		track.setOnMouseEntered((MouseEvent e) -> {
			setMyCursor();
			if (this.isControl.get()) {
				szTrack.setVisible(true);
				szTrack.start();
			}
		});
		track.setOnMouseExited((MouseEvent e) -> {
			this.setCursor(Cursor.DEFAULT);
			szTrack.stop();
			szTrack.setVisible(false);
		});

	} // конструктор -----------------------------------------

	private final AnimationTimer at = new AnimationTimer() {

		private boolean flag = true;

		@Override
		public void handle(long arg0) {

			// один раз в секунду
			if ((count % 30) == 0 && isControl.get()) {

				if (mod.get() == 2) {
					setSelectTrack(flag);
					flag = !flag;
				}
			}
			if (count == 900) {
				stopSelectTrack();
			}
			count++;
		}
	};

	private final AnimationTimer atStartZm = new AnimationTimer() {


		@Override
		public void handle(long arg0) {

			// один раз в 0,5 секунд
			if ((countZm % 30) == 0 && isControl.get()) {

				if (mod.get() == 1) {
					setIndicationZm(flagZm);
					flagZm = !flagZm;

				}
			}
			countZm++;
		}
	};
	
	

	public void setTrack(Rectangle track) {
		this.track = track;
	}

	public void setIndicationZm(boolean f) {
		Paint p = track.getFill();
		indZm.setVisible(f);
		if (p.equals(Color.LIGHTGREY) || p.equals(Color.WHITE))
			track.setFill((f) ? Color.WHITE : Color.LIGHTGREY);
	}

	public void setShunt(boolean f) {
		color = (f) ? Color.RED : Color.LIGHTGREY;
		track.setFill(color);
	}

	public void setSelectTrack(boolean f) {
		color = (f) ? Color.WHITE : Color.BLACK;
		arrow.setVisible(f);
		track.setStroke(color);
	}

	public void setZam(boolean f) {
		color = (f) ? Color.YELLOW : Color.LIGHTGREY;
		track.setFill(color);
	}

	public void startSelect() {
		if (isControl.get())
			setIsSelectMode(true);
		at.start();
	}

	@Override
	public void stopSelect() {
	}

	public void stopSelectTrack() {
		at.stop();
		setIsSelectMode(false);
		setSelectTrack(false);
		count = 0;
		if (mod.get() == 2)
			setIsControl(false);
	}

	public Rectangle getTrack() {
		return track;
	}

	public void setLocalControl(boolean flag) {
		isControl.set(flag);
		szTrack.setVisible(flag);
	}

	public void startAtZm() {
		flagZm = true;
		atStartZm.start();
	}

	public void stopAtZm() {
		atStartZm.stop();
		countZm = 0;
		setIndicationZm(false);
	}

}
