﻿package com.alex.training.stationElement;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;

/**
 * @author Alex
 *
 */
public class LeftStrelka extends Strelka {

	public LeftStrelka() {
		x = 2;
		y = 120;

	}

	/**
	 * @author Alex Параметры конструктора name - название рц isControl -
	 *         возможно ли управление стрелкой widthGeneral - длина общего
	 *         участка стрелки х, у -координаты начала стрелки isOne - флаг
	 *         одиночная стрелка или на съезде isMinusUp - направление
	 *         минусового ответвления относительно плюса
	 */

	public LeftStrelka(String name, boolean isControl, double widthGeneral,
			double x, double y, boolean isOne, boolean isMinusUp) {
		super(name, isControl, widthGeneral, x, y);

		double widthPlus = 120;
		color = Color.LIGHTGREY;

		// Плюсовое ответвление без контроля--------------------------
		plusFree.setWidth(widthPlus);
		plusFree.setHeight(5);
		plusFree.setFill(color);

		plusFree.setX(x + widthGeneral);
		plusFree.setY(y + height / 2 - 2.5);
		plusFree.setVisible(false);
		// ---------------------------------------------------------

		// минусовое ответвление без
		// контроля-----------------------------------------
		Double points[];
		if (!isMinusUp) {
			// минус вниз на одной
			// стрелке------------------------------------------
			points = (isOne) ? new Double[] { x + widthGeneral,
					y + height / 2 + 2.5, // первая
					x + widthGeneral, y + height / 2 - 2.5, // вторая
					x + widthGeneral + 2, y + height / 2 - 2.5, // 3
					x + widthGeneral + 82, y + 110 + height / 2 - 2.5, // 4
					x + widthGeneral + 95, y + 110 + height / 2 - 2.5, // 5
					x + widthGeneral + 95, y + 110 + height / 2 + 2.5, // 6
					x + widthGeneral + 80, y + 110 + height / 2 + 2.5, // 7
			}
			// минус вниз на
			// съезде----------------------------------------------
					: new Double[] { x + widthGeneral, y + height / 2 + 2.5, // первая
							x + widthGeneral, y + height / 2 - 2.5, // вторая
							x + widthGeneral + 1, y + height / 2 - 2.5, // 3
							x + widthGeneral + 123, y + 110, // 4
							x + widthGeneral + 116, y + 110 // 5
					};
		} else {
			// минус вверх на одной
			// стрелке------------------------------------------
			points = (isOne) ? new Double[] { x + widthGeneral,
					y + height / 2 - 2.5, // первая
					x + widthGeneral, y + height / 2 + 2.5, // вторая
					x + widthGeneral + 2, y + height / 2 + 2.5, // 3
					x + widthGeneral + 82, y - 110 + height / 2 + 2.5, // 4
					x + widthGeneral + 95, y - 110 + height / 2 + 2.5, // 5
					x + widthGeneral + 95, y - 110 + height / 2 - 2.5, // 6
					x + widthGeneral + 80, y - 110 + height / 2 - 2.5, // 7
			// минус вверх на съезде------------------------------------------
			}
					: new Double[] { x + widthGeneral, y + height / 2 - 2.5, // первая
							x + widthGeneral, y + height / 2 + 2.5, // вторая
							x + widthGeneral + 1, y + height / 2 + 2.5, // 3
							x + widthGeneral + 123, y - 110, // 4
							x + widthGeneral + 116, y - 110 // 5
					};

		}
		minusFree.getPoints().addAll(points);

		minusFree.setFill(color);
		minusFree.setVisible(true);
		// -----------------------------------------------------------------

		// Плюсовое положение стрелки----------------------
		plus.setWidth(widthPlus);
		plus.setHeight(height);
		plus.setFill(color);

		plus.setX(x + widthGeneral);
		plus.setY(y);
		plus.setStroke(Color.BLACK);
		plus.setVisible(true);
		// ------------------------------------------------------

		// минусовое положение стрелки------------------------------------------
		if (!isMinusUp) {
			// минус вниз на одной
			// стрелке------------------------------------------
			points = (isOne) ? new Double[] { x + widthGeneral, y + height, // первая
					x + widthGeneral, y, // вторая
					x + widthGeneral + 5, y, // 3
					x + widthGeneral + 85, y + 110., // 4
					x + widthGeneral + 125, y + 110., // 5
					x + widthGeneral + 125, y + 110 + height, // 6
					x + widthGeneral + 80, y + 110 + height, // 7
			// минус вниз на
			// съезде----------------------------------------------
			}
					: new Double[] { x + widthGeneral, y + height, // первая
							x + widthGeneral, y, // вторая
							x + widthGeneral + 5, y, // 3
							x + widthGeneral + 130, y + 110, // 4
							x + widthGeneral + 108, y + 110, // 5
					};

		} else {
			// минус вверх на одной
			// стрелке------------------------------------------
			points = (isOne) ? new Double[] { x + widthGeneral, y, // первая
					x + widthGeneral, y + height, // вторая
					x + widthGeneral + 5, y + height, // 3
					x + widthGeneral + 85, y - 110 + height, // 4
					x + widthGeneral + 95, y - 110 + height, // 5
					x + widthGeneral + 95, y - 110., // 6
					x + widthGeneral + 80, y - 110., // 7
			// минус вверх на съезде------------------------------------------
			}
					: new Double[] { x + widthGeneral, y, // первая
							x + widthGeneral, y + height, // вторая
							x + widthGeneral + 5, y + height, // 3
							x + widthGeneral + 130, y - 110, // 4
							x + widthGeneral + 108, y - 110 // 5
					};
		}
		minus.getPoints().addAll(points);
		minus.setFill(color);
		minus.setStroke(Color.BLACK);
		minus.setVisible(false);

		// многофункциональный контрольный элемент стрелки
		// --------------------------
		square.setX(x + widthGeneral);
		square.setY(y - 15);
		square.setFill(Color.TRANSPARENT);
		square.setStroke(Color.BLACK);
		square.setVisible(false);
		//

		// номер стрелки------------------------------
		if (isMinusUp) {
			// минус вверх
			noStr.setX(x + widthGeneral + 10);
			noStr.setY(y + height + 20);
		} else {
			// минус вниз
			noStr.setX(x + widthGeneral + 10);
			noStr.setY(y - 5);
		}
		// ---------------------------------------------------------

		// индикация плюсового положения--------------------------
		double h = (isMinusUp) ? y - 5 : y + 20;
		l = new Line(x + widthGeneral + 40, h, x + widthGeneral + 55, h);
		l.setStrokeWidth(2.5);
		// -------------------------------------------------------

		// зона выбора -------------------------------------------
		h = (isMinusUp) ? y-115 : y - 20;
		selectZone = new SelectZone(widthPlus + 2, 155);
		selectZone.setLayoutX(x + widthGeneral);
		selectZone.setLayoutY(h);
		selectZone.setVisible(false);
		// --------------------------------------------------------

		getChildren().addAll(selectZone, plusFree, minusFree, plus, minus, noStr, l,
				square  );

	}

}
