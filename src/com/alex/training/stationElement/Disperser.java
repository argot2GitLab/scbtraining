﻿package com.alex.training.stationElement;

import javafx.scene.Group;
import javafx.scene.effect.InnerShadow;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Path;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.transform.Rotate;

public class Disperser extends Group {

	private Rectangle supBottom;
	private Rectangle supTop;
	private Shape cover;
	
	
	public Disperser() {
		// нижний прямоугольник основания
		supBottom = new Rectangle(35, 30);
		supBottom.setX(44);
		supBottom.setY(27);
		supBottom.setStroke(Color.BLACK);
		supBottom.setArcHeight(10);
		supBottom.setArcWidth(10);
		supBottom.setFill(Color.RED);
		
		// Верхний прямоугольник основания
		supTop = new Rectangle(35, 30);
		supTop.setX(40);
		supTop.setY(20);
		supTop.setStroke(Color.BLACK);
		supTop.setArcHeight(10);
		supTop.setArcWidth(10);
		supTop.setFill(Color.RED);

		// Вертикальные линии основания
		Line leftVLine = new Line(supBottom.getX()+2, supBottom.getY() + 29,
				supTop.getX()+2, supTop.getY() + 29);
		Line centerVLine = new Line(supBottom.getX() + 33,
				supBottom.getY() + 29, supTop.getX() + 33, supTop.getY() + 29);
		Line rightVLine = new Line(supBottom.getX() + 33, supBottom.getY() + 2,
				supTop.getX() + 33, supTop.getY() + 2);
		Shape lines = Path.union(leftVLine, centerVLine);
		lines = Path.union(lines, rightVLine);
		lines.setStroke(Color.BLACK);

		// конус
		Polyline conus = new Polyline();
		conus.getPoints()
				.addAll(new Double[] { 26., 12.,
						21., 38., 45., 38., 40., 12.,
						26., 12. });
		conus.setFill(Color.RED);
		Ellipse bEl = new Ellipse(33, 38, 12, 7);
		Ellipse tEl = new Ellipse(33, 12, 7, 4);
		cover = Path.union(bEl,conus);
		cover = Path.union(cover, tEl);
		cover.setFill(Color.RED);
		cover.setStroke(Color.BLACK);
		
		
		supTop.getTransforms().addAll(new Rotate(50, Rotate.X_AXIS),
				new Rotate(30, Rotate.Z_AXIS));
		supBottom.getTransforms().addAll(new Rotate(50, Rotate.X_AXIS),
				new Rotate(30, Rotate.Z_AXIS));
		leftVLine.getTransforms().addAll(new Rotate(50, Rotate.X_AXIS),
				new Rotate(30, Rotate.Z_AXIS));
		centerVLine.getTransforms().addAll(new Rotate(50, Rotate.X_AXIS),
				new Rotate(30, Rotate.Z_AXIS));
		rightVLine.getTransforms().addAll(new Rotate(50, Rotate.X_AXIS),
				new Rotate(30, Rotate.Z_AXIS));

		InnerShadow is = new InnerShadow();
        is.setOffsetX(0.2f);
        is.setOffsetY(0.2f);
        cover.setEffect(is);
		supBottom.setEffect(is);

		getChildren().addAll(supBottom, supTop, leftVLine, centerVLine,
				rightVLine, cover);
	}

	
	public void fillDisperser() {
		supTop.setFill(Color.gray(0.4,1));
		supBottom.setFill(Color.gray(0.4,1));
		cover.setFill(Color.gray(0.4,1));
	}
}
