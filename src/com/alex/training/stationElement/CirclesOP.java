﻿package com.alex.training.stationElement;

import javafx.animation.AnimationTimer;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class CirclesOP extends Group {

	private Circle c1, c2, c3;

	final private AnimationTimer at = new AnimationTimer() {

		long count = 0;
		long sec = 0;

		@Override
		public void handle(long now) {
			// один раз в секунду
			if ((count % 60) == 0) {
				sec++;
			}
			if (sec == 3)
				setOP1(true);
			if (sec == 6)
				setOP2(true);
			if (sec == 9)
				setOP3(true);
			if (sec == 12) {
				endOP();
				stop();
			}

			count++;
		}
	};

	public CirclesOP() {
		c1 = new Circle(5, 5, 4, Color.LIGHTGREY);
		c1.setStroke(Color.BLACK);
		c2 = new Circle(15, 5, 4, Color.YELLOW);
		c2.setStroke(Color.BLACK);
		c3 = new Circle(25, 5, 4, Color.GREEN);
		c3.setStroke(Color.BLACK);

		endOP();

		getChildren().addAll(c1, c2, c3);
	}

	public void start() {
		at.start();
	}

	public void stop() {
		at.stop();
	}

	public void setOP1(boolean f) {
		c1.setVisible(f);
	}

	public void setOP2(boolean f) {
		c2.setVisible(f);
	}

	public void setOP3(boolean f) {
		c3.setVisible(f);
	}
	public void endOP() {
		c1.setVisible(false);
		c2.setVisible(false);
		c3.setVisible(false);
	}

}
