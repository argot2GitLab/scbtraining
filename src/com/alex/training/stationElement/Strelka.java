﻿package com.alex.training.stationElement;

import javafx.animation.AnimationTimer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Cursor;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public abstract class Strelka extends TrackCircuit {

	protected Rectangle plus = new Rectangle(45, 45);
	protected Rectangle square = new Rectangle();
	protected Rectangle plusFree = new Rectangle();
	protected Polygon minusFree = new Polygon();
	protected Polygon minus = new Polygon();
	protected Text noStr;
	protected SelectZone selectZone;
	protected Line l;
	private long count = 0;

	// свойство работы двигателя в текущий момент
	// ------------------------------------------------
	public BooleanProperty motorWork = new SimpleBooleanProperty();

	public final BooleanProperty motorWorkProperty() {
		return motorWork;
	}

	public final void setMotorWork(boolean newValue) {
		motorWork.set(newValue);
	}

	public final boolean getMotorWork() {
		return motorWork.get();
	}

	private final AnimationTimer atStr = new AnimationTimer() {

		boolean f;
		Color col;

		@Override
		public void handle(long now) {
			if (count % 30 == 0) {
				color = (f) ? Color.BLACK : Color.YELLOW;
				minus.setStroke(color);
				plus.setStroke(color);
				col = (f) ? Color.WHITE : Color.DEEPPINK;
				minusFree.setFill(col);
				plusFree.setFill(col);
				f = !f;
			}

			if (count == 900) {
				stopSelect();

			}

			count++;

		}
	};

	public Strelka() {

	}

	public Strelka(String name, boolean isControl, double width, double x,
			double y) {
		super(name, isControl, width, x, y);

		setMotorWork(false);

		// Номер стрелки
		noStr = new Text("1");
		noStr.setFill(Color.BLACK);
		noStr.setFont(Font.font("sans", FontWeight.BOLD, 16));

		// обработка событий -------------------------------

		plus.setOnMouseEntered((MouseEvent e) -> {
			onMouseEntered();
		});
		plus.setOnMouseExited((MouseEvent e) -> {
			onMouseExited();
		});
		minus.setOnMouseEntered((MouseEvent e) -> {
			onMouseEntered();
		});
		minus.setOnMouseExited((MouseEvent e) -> {
			onMouseExited();
		});
		plusFree.setOnMouseEntered((MouseEvent e) -> {
			onMouseEntered();
		});
		plusFree.setOnMouseExited((MouseEvent e) -> {
			onMouseExited();
		});

		minusFree.setOnMouseEntered((MouseEvent e) -> {
			onMouseEntered();
		});
		minusFree.setOnMouseExited((MouseEvent e) -> {
			onMouseExited();
		});
	}

	private void onMouseExited() {
		this.setCursor(Cursor.DEFAULT);
		selectZone.stop();
		selectZone.setVisible(false);
	}

	private void onMouseEntered() {
		setMyCursor();
		selectZone.setVisible(true);
		selectZone.start();
	}

	public void lossCheckup() {
		plus.setVisible(false);
		plusFree.setVisible(true);
		minus.setVisible(false);
		minusFree.setVisible(true);
		color = Color.DEEPPINK;
		plusFree.setFill(color);
		minusFree.setFill(color);
	}

	public void setPlusCheckup(boolean flag) {
		plus.setVisible(flag);
		plusFree.setVisible(!flag);
		color = Color.LIGHTGREY;
		plus.setFill(color);
		minusFree.setFill(color);

		// потом переключим на контакты реле HC ----------------!!!!
		setMotorWork(false);
	}

	public void setMinusCheckup(boolean flag) {
		minus.setVisible(flag);
		minusFree.setVisible(!flag);
		color = Color.LIGHTGREY;
		plusFree.setFill(color);
		minus.setFill(color);

		// потом переключим на контакты реле HC-------------------!!!!
		setMotorWork(false);

	}

	public void switching() {
		plus.setVisible(false);
		plusFree.setVisible(true);
		minus.setVisible(false);
		minusFree.setVisible(true);
		color = Color.LIGHTGREY;
		plusFree.setFill(color);
		minusFree.setFill(color);

		// потом переключим на контакты реле HC
		setMotorWork(true);
	}

	public int getCheckup() {
		int modCheckup = 0;
		color = Color.LIGHTGREY;
		if ((plus.isVisible() == true) && (plusFree.isVisible() == false)
				&& (minus.isVisible() == false)
				&& (minusFree.isVisible() == true)
				&& (plus.getFill().equals(color))
				&& (minusFree.getFill().equals(color)))
			modCheckup = 1;

		if ((minus.isVisible() == true) && (minusFree.isVisible() == false)
				&& (plus.isVisible() == false)
				&& (plusFree.isVisible() == true)
				&& (plusFree.getFill().equals(color))
				&& (minus.getFill().equals(color)))
			modCheckup = 2;

		color = Color.DEEPPINK;
		if ((minus.isVisible() == false) && (minusFree.isVisible() == true)
				&& (plus.isVisible() == false)
				&& (plusFree.isVisible() == true)
				&& (plusFree.getFill().equals(color))
				&& (minusFree.getFill().equals(color)))
			modCheckup = 0;

		return modCheckup;
	}

	public void startSelectZone() {
		selectZone.start();
	}

	public void stopSelectZone() {
		selectZone.stop();
	}

	public Text getNoStr() {
		return noStr;
	}

	@Override
	public void setLocalControl(boolean flag) {
		super.setLocalControl(flag);
		color = (flag) ? Color.LIGHTGREEN : Color.BLACK;
		track.setStroke(color);
	}

	public void clickOnGeneral(MouseEvent e) {
		if ((e.getClickCount() == 1)
				&& (e.getButton().equals(MouseButton.PRIMARY))
				&& isControl.get()) {
			if (!getIsSelectMode()) {
				atStr.start();
				setIsSelectMode(true);
			} else {
				stopSelect();
			}
		}
	}

	@Override
	public void stopSelect() {
		atStr.stop();
		setIsSelectMode(false);
		color = Color.BLACK;
		minus.setStroke(color);
		plus.setStroke(color);
		color = Color.LIGHTGREY;
		minusFree.setFill(color);
		plusFree.setFill(color);
		count = 0;
	}

	public Rectangle getPlusFree() {
		return plusFree;
	}

	public Polygon getMinusFree() {
		return minusFree;
	}

	public Rectangle getPlus() {
		return plus;
	}

	public Polygon getMinus() {
		return minus;
	}

	@Override
	public void setShunt(boolean f) {
		color = (f) ? Color.RED : Color.LIGHTGREY;
		track.setFill(color);
		plus.setFill(color);
		minus.setFill(color);
	}

	@Override
	public void setZam(boolean f) {
		color = (f) ? Color.YELLOW : Color.LIGHTGREY;
		track.setFill(color);
		minus.setFill(color);
		plus.setFill(color);
	}

}
