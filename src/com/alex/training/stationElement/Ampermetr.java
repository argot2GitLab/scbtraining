﻿package com.alex.training.stationElement;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class Ampermetr extends Group {

	private Rectangle amper;
	private List<Rectangle> shcala;
	private Text txAmper;
	private Text name;

	// модификатор перевода стрелок
	// 1- перевод одной стрелки
	// 2- перевод двух стрелок
	// 3 - перевод 3 стрелок
	// 4 - перевод 4 стрелок
	// 5 - работа 1 стрелки на фрикцию
	// ------------------------------------------------------
	public IntegerProperty mod = new SimpleIntegerProperty();

	public final IntegerProperty modProperty() {
		return mod;
	}

	public final void setMod(int newValue) {
		mod.set(newValue);
	}

	public final int getMod() {
		return mod.get();
	}

	// ---------------------------------------------------------------

	public Ampermetr(double x, double y) {

		amper = new Rectangle(75, 40);
		amper.setX(10);
		amper.setY(21);
		amper.setFill(Color.LIGHTGREY);
		amper.setStroke(Color.BLACK);

		shcala = new ArrayList<Rectangle>();
		double xS = 10;
		for (int i = 0; i < 15; i++) {
			shcala.add(i, new Rectangle(5, 7));
			shcala.get(i).setStroke(Color.BLACK);
			shcala.get(i).setFill(Color.LIGHTGREY);
			shcala.get(i).setX(xS);
			shcala.get(i).setY(61);
			xS += 5;
			this.getChildren().add(shcala.get(i));
		}

		name = new Text("ток стрелок");
		name.setX(3);
		name.setY(17);
		name.setFont(Font.font("sans", FontWeight.MEDIUM, 16));

		txAmper = new Text("0.00");
		txAmper.setFont(Font.font("sans", FontWeight.MEDIUM, 30));
		txAmper.setX(10 + 37.5 - txAmper.getBoundsInLocal().getWidth() / 2);
		txAmper.setY(54);

		getChildren().addAll(amper, name, txAmper);
		this.setLayoutX(x);
		this.setLayoutY(y);

	}

	// показ тока на амперметре
	public void showTok(double tok) {
		txAmper.setText(String.format("%.2f", tok));
		Color color = (tok == 0.) ? Color.LIGHTGREY : Color.GREEN;
		for (int i = 0; i < 7; i++) {
			if (tok != 0. && i == 5) {
				color = Color.YELLOW;
			}
			shcala.get(i).setFill(color);

		}

	}
}
