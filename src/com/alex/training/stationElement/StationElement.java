﻿/**
 * 
 */
package com.alex.training.stationElement;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.ImageCursor;
import javafx.scene.image.Image;
import javafx.scene.text.Text;

import com.alex.training.Main;

/**
 * @author alex
 *
 */
public abstract class StationElement extends Group {

	protected Text name;

	// свойство может ли объект управляться с АРМ
	public BooleanProperty isControl = new SimpleBooleanProperty();

	public final BooleanProperty isControlProperty() {
		return isControl;
	}

	public final void setIsControl(boolean newValue) {
		isControl.set(newValue);
	}

	public final boolean getIsControl() {
		return isControl.get();
	}

	// выбран ли режим управления объектомы
	public BooleanProperty isSelectMode = new SimpleBooleanProperty();

	public final BooleanProperty isSelectModeProperty() {
		return isSelectMode;
	}

	public final void setIsSelectMode(boolean newValue) {
		isSelectMode.set(newValue);
	}

	public final boolean getIsSelectMode() {
		return isSelectMode.get();
	}

	protected Cursor myCursor;

	public StationElement(String name, boolean isControl) {
		setIsSelectMode(false);
		if (name != null)
			this.name = new Text(name);
		this.isControl.setValue(isControl);
		Image image = new Image(Main.class.getResource("res/img/unCursor.png")
				.toExternalForm(), false);
		myCursor = (new ImageCursor(image));
	}

	public StationElement() {
		setIsSelectMode(false);
		name = new Text("");
		isControl.set(false);
	}

	public Text getName() {
		return name;
	}

	protected void setMyCursor() {
		setCursor((isControl.get()) ? Cursor.HAND : myCursor);
	}

	public void stopSelect() {
	};

}
