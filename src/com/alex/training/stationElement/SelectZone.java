﻿package com.alex.training.stationElement;

import javafx.animation.AnimationTimer;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class SelectZone extends Group {
	
	private Rectangle zone;
	
	
	private final AnimationTimer at = new AnimationTimer() {
		long count =0;
		boolean flag = true;
		@Override
		public void handle(long now) {
			if ((count % 15) == 0) {
				
				if(flag) {
					zone.getStrokeDashArray().clear();
					zone.setStroke(Color.WHITE);
					zone.getStrokeDashArray().addAll(20.,8.,2.,8.);

				} else {
					zone.getStrokeDashArray().clear();
					zone.setStroke(Color.LIGHTGREEN);
					zone.getStrokeDashArray().addAll(20.,8.,2.,8.,2.,8.);
					
				}
				flag = !flag;
			}
			count++;
			
		}
	};

	public SelectZone() {
		
	}

	public SelectZone(double width, double height ) {
		
		zone = new Rectangle(width, height);
		zone.setX(2);
		zone.setY(2);
		zone.setFill(Color.TRANSPARENT);
		
		getChildren().add(zone);
		
	}
	
	public void start() {
		at.start();
		
	}
	public void stop() {
		at.stop();
		
	}
	
	public void setWidth(double w) {
		zone.setWidth(w);
	}


}
