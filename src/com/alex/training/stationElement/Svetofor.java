﻿package com.alex.training.stationElement;

import java.util.ArrayList;
import java.util.List;

import javafx.animation.AnimationTimer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.Cursor;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public abstract class Svetofor extends StationElement {

	protected Text svName;
	protected Text txtOperation;
	protected Circle cell;
	protected Shape foot, lines;
	protected Disperser cover;
	protected CirclesOP cop;
	protected Rectangle verticalFoot;
	protected Rectangle horizontFoot;
	protected Color color = Color.GRAY;
	final protected String nabor = "Набор";
	final protected String razdelka = "Разделка";
	protected SelectZone selectZone;
	protected BooleanProperty isIndication = new SimpleBooleanProperty();
	private long count = 0;
	// список р.ц. куда можно задать маршрут по светофору
	private List<TrackCircuit> tracks = new ArrayList<TrackCircuit>();

	protected final BooleanProperty isIndicationProperty() {
		return isIndication;
	}

	protected final void setIsIndication(boolean newValue) {
		isIndication.set(newValue);
	}

	protected final boolean getIsIndication() {
		return isControl.get();
	}

	private final AnimationTimer atNabor = new AnimationTimer() {

		@Override
		public void handle(long now) {
			if (count == 900) {
				stopSelect();
			}
			count++;
		}
	};

	public Svetofor(String name, boolean isControl, boolean isIndication) {
		super(name, isControl);

		isIndicationProperty().set(isIndication);

		// Название светофора
		Font font = Font.font("sans", FontWeight.BOLD, 18);
		svName = new Text(name);
		svName.setFont(font);

		// Элемент ножка
		verticalFoot = new Rectangle(8, 36);
		horizontFoot = new Rectangle(7, 7);

		// Элемент ячейка

		if (!isIndication)
			color = Color.AQUA;
		cell = new Circle(18, color);
		cell.setStroke(Color.WHITE);

		// Текстовый элемент Набор, Разделка, авария
		txtOperation = new Text();
		txtOperation.setVisible(false);

		// колпачок
		cover = new Disperser();
		cover.setVisible(false);

		// кружки ОП
		cop = new CirclesOP();
		// ---------------------------------------------

		// Зона выбора
		selectZone = new SelectZone(60, 60);
		selectZone.setVisible(false);

		// обработка событий -------------------------------
		cell.setOnMousePressed((me) -> {
			if (me.isSecondaryButtonDown() && getIsControl()
					&& me.getButton().equals(MouseButton.SECONDARY)) {
				setIndicationCancelMod(me.isSecondaryButtonDown());
			}
		});

		cell.setOnMouseReleased((me) -> {
			if (!me.isSecondaryButtonDown() && getIsControl()
					&& me.getButton().equals(MouseButton.SECONDARY)) {
				setIndicationCancelMod(me.isSecondaryButtonDown());
			}
		});

		cell.setOnMouseEntered((MouseEvent e) -> {
			onMouseEntered();
		});
		cell.setOnMouseExited((MouseEvent e) -> {
			onMouseExited();
		});

	}

	public void nabor() {
		txtOperation.setText(nabor);
		txtOperation.setFill(Color.WHITE);
		txtOperation.setFont(Font.font("sans", 16));
		txtOperation.setVisible(true);
	}

	/**
	 * установка и снятие режима набора
	 */
	public void setSelectModSvetofor() {
		if (!getIsSelectMode()) {
			nabor();
			setIsSelectMode(true);
			getTracks().stream().forEach((t) -> {
				t.setMod(2);
				t.setIsControl(true);
				t.setIsSelectMode(true);
				t.startSelect();
			});
			atNabor.start();
		} else {
			getTracks().stream().forEach((t) -> {
				t.setIsControl(false);
				t.stopSelectTrack();
				t.setIsSelectMode(false);
			});
			stopSelect();
		}
	}

	public void setIndication(boolean f) {
		color = (f) ? Color.GRAY : Color.DARKTURQUOISE;
		cell.setFill(color);
		isIndication.set(f);
	}

	public void perekrSvetofor() {

		cell.setFill(Color.CRIMSON);
	}

	public void closeSvetofor() {

		cell.setFill(Color.RED);
	}

	public boolean isPerekp() {

		return (cell.getFill().equals(Color.CRIMSON)) ? true : false;
	}

	public void startRazdelka() {
		if (isPerekp()) {
			txtOperation.setText(razdelka);
			txtOperation.setFont(Font.font("sans", 16));
			txtOperation.setFill(Color.LIGHTCORAL);
			txtOperation.setVisible(true);
		}

	}

	public void setLocalControl(boolean f) {
		color = (f) ? Color.LIGHTGREEN : Color.BLACK;
		svName.setFill(color);
		isControl.set(f);
	}

	public void cancelTrack() {
		txtOperation.setVisible(false);
		foot.setFill(color);
		cell.setFill(color);

	}

	public void cancelFullReservedTrack() {
		txtOperation.setVisible(false);
		foot.setFill(Color.GRAY);
		cell.setFill(Color.CRIMSON);

	}

	public void setUsIndication(boolean flag) {
		foot.setFill((flag) ? Color.LIGHTGREEN : Color.GRAY);

	}

	public boolean isLocalControl() {
		return isControl.get();
	}

	public void openSvetofor(boolean flag) {
		if (isIndication.get())
			cell.setFill((flag) ? Color.GREEN : Color.GREY);
	}

	public void openSvetoforWhite(boolean flag) {
		cell.setFill((flag) ? Color.WHITE : Color.GREY);
	}

	// индикация режима отмены (4 полоски)
	public void setIndicationCancelMod(boolean f) {
		lines.setVisible(f);
	}

	public void setBlock(boolean f) {
		cover.setVisible(f);
		cell.setStroke((f) ? Color.RED : Color.WHITE);
	}

	public boolean isBlock() {
		return (cover.isVisible() && cell.getStroke().equals(Color.RED)) ? true
				: false;
	}

	public void startOPNormal() {
		cop.start();
	}

	public void setOP1(boolean f) {
		cop.setOP1(f);
	}

	public void setOP2(boolean f) {
		cop.setOP2(f);
	}

	public void setOP3(boolean f) {
		cop.setOP3(f);
	}

	public void endOP() {
		cop.setOP1(false);
		cop.setOP2(false);
		cop.setOP3(false);
	}

	public void setCrash(boolean f) {
		txtOperation.setText("A");
		txtOperation.setFill(Color.RED);
		txtOperation.setVisible(f);
	}

	protected void onMouseExited() {
		setCursor(Cursor.DEFAULT);
		selectZone.stop();
		selectZone.setVisible(false);
	}

	protected void onMouseEntered() {
		setMyCursor();
		if (isControl.get()) {
			selectZone.setVisible(true);
			selectZone.start();
		}
	}

	@Override
	public void stopSelect() {
		atNabor.stop();
		txtOperation.setVisible(false);
		setIsSelectMode(false);
		count = 0;
	}

	public Circle getCell() {
		return cell;
	}

	public Shape getFoot() {
		return foot;
	}

	public List<TrackCircuit> getTracks() {
		return tracks;
	}

	public void addTrack(TrackCircuit tr) {
		if (tr != null && tracks.stream().allMatch((t) -> !t.equals(tr)))
			tracks.add(tr);

	}
}
