﻿package com.alex.training.stationElement;

import javafx.animation.AnimationTimer;
import javafx.scene.Group;
import javafx.scene.ImageCursor;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import com.alex.training.Main;

public class TopTransparant extends Group {

	private Rectangle inner;
	private Rectangle outer;
	private Text tx;
	private Color color;
	private boolean isGreen;
	private int mod = 1; // модификатор для миганя
	// 1- желтый цвет
	// 2 -синий цвет
	// 3 - касный цвет
	private final AnimationTimer at = new AnimationTimer() {

		long count = 0;
		boolean flag = true;

		@Override
		public void handle(long arg0) {
			// TODO Auto-generated method stub
			if (count % 30 == 0) {
				switch (mod) {
				case 1: {
					color = (flag) ? Color.YELLOW : Color.GREY;
					inner.setFill(color);
				}
					break;
				case 2: {
					color = (flag) ? Color.BLUE : Color.GREY;
					inner.setFill(color);
				}
					break;
				case 3: {
					color = (flag) ? Color.RED : Color.GREY;
					inner.setFill(color);
				}
					break;

				default:
					break;
				}
				flag = !flag;
			}
			count++;

		}
	};

	/**
	 * @author alex параметры конструктора text надпись на транспаранте isGreen
	 *         зеленый транспарант или нет
	 */
	public TopTransparant(String text, boolean isGreen) {

		this.isGreen = isGreen;

		// внешний прямоугольник
		outer = new Rectangle(100, 50);
		outer.setX(1);
		outer.setY(1);

		color = Color.GREY;
		outer.setFill(color);

		color = (isGreen) ? Color.GREEN : Color.GREY;

		// внутренний прямоугольник
		inner = new Rectangle(90, 40);
		inner.setX(6);
		inner.setY(6);
		inner.setFill(color);

		color = Color.WHITE;
		inner.setStroke(color);

		// текст транспаранта ---------------------------
		tx = new Text(text);
		tx.setFont(Font.font("sans", FontWeight.BOLD, 20));
		tx.setFill(color);
		tx.setX(1 + outer.getWidth() / 2 - tx.getBoundsInLocal().getWidth() / 2);
		tx.setY(1 + outer.getHeight() / 2 + tx.getBoundsInLocal().getHeight()
				/ 4);

		Image image = new Image(Main.class.getResource("res/img/unCursor.png")
				.toExternalForm(), false);
		this.getChildren().addAll(outer, inner, tx);
		outer.setOnMouseEntered((MouseEvent me) -> {
			this.setCursor(new ImageCursor(image));
		});

	}

	public Text getTx() {
		return tx;
	}

	public Rectangle getInner() {
		return inner;
	}

	public Rectangle getOuter() {
		return outer;
	}

	public void setupYellow() {
		inner.setFill(Color.YELLOW);
	}

	public void setupDefault() {
		color = (isGreen) ? Color.GREEN : Color.GREY;
		inner.setFill(color);
	}

	public void setBlinking(int mod) {
		this.mod = mod;
		at.start();
	}

	public void stopBlinking() {
		at.stop();
		setupDefault();
	}

}
