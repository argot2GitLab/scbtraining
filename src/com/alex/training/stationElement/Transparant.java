﻿package com.alex.training.stationElement;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class Transparant extends StationElement {

	
	private Rectangle field;
	private Text txName;
	private final Color color = Color.gray(0.53, 1);
	private Font font;


	
	
	public Transparant() {
		// TODO Auto-generated constructor stub
	}

	public Transparant(String name, boolean isControl) {
		super(name, isControl);
		
		
		field = new Rectangle(70, 40);
		field.setX(2);
		field.setY(2);
		field.setFill(color);
		
		font = Font.font("sans", FontWeight.MEDIUM, 26);
		txName = new Text(name);
		txName.setFont(font);
		txName.setFill(Color.GREY);
		txName.setX(36 - txName.getBoundsInLocal().getWidth()/2);
		txName.setY(34);
		
		getChildren().addAll(field, txName);
	}

	public Color getColor() {
		return color;
	}

	public Rectangle getField() {
		return field;
	}

	public Text getTxName() {
		return txName;
	}
	
	
	
}
