﻿package com.alex.training.util;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.alex.training.scene.Editor;
import com.alex.training.schem.BoxAs;
import com.alex.training.schem.ElCircuit;
import com.alex.training.schem.Loop;
import com.alex.training.schem.Motor;
import com.alex.training.schem.ParUnion;
import com.alex.training.schem.PowerSource;
import com.alex.training.schem.Source;
import com.alex.training.schem.User;
import com.alex.training.schemElement.Conder;
import com.alex.training.schemElement.Diod;
import com.alex.training.schemElement.Drossel;
import com.alex.training.schemElement.FrontAutostopContact;
import com.alex.training.schemElement.FrontContact;
import com.alex.training.schemElement.FrontRightContact;
import com.alex.training.schemElement.Obmotka;
import com.alex.training.schemElement.ObmotkaMotor;
import com.alex.training.schemElement.PolarContact;
import com.alex.training.schemElement.PowerSourceTerminal;
import com.alex.training.schemElement.PowerSourceTerminalDCM;
import com.alex.training.schemElement.Predochran;
import com.alex.training.schemElement.Rele;
import com.alex.training.schemElement.ReleNMPSH03_90;
import com.alex.training.schemElement.RelePMPUSH;
import com.alex.training.schemElement.Resistor;
import com.alex.training.schemElement.SLine;
import com.alex.training.schemElement.SPoint;
import com.alex.training.schemElement.SchemElement;
import com.alex.training.schemElement.StrContact;
import com.alex.training.schemElement.StrKyrbelContact;
import com.alex.training.schemElement.SvetLamp;
import com.alex.training.schemElement.TransBSSH;
import com.alex.training.schemElement.TransSobs2Au3;
import com.alex.training.schemElement.Troynic;
import com.alex.training.schemElement.TroynicAutostop;
import com.alex.training.schemElement.TroynicInterface;
import com.alex.training.schemElement.TroynicRight;
import com.alex.training.schemElement.TylAutostopContact;
import com.alex.training.schemElement.TylContact;
import com.alex.training.schemElement.TylRightContact;
import com.alex.training.schemElement.Vyvod;
import com.alex.training.schemElement.blocks.BDR;
import com.alex.training.schemElement.blocks.BKR_76;
import com.alex.training.schemElement.blocks.StrMotor;
import com.alex.training.schemElement.blocks.SvetGol;
import com.alex.training.schemElement.blocks.ZPRSH;

public class Helper {

	/**
	 * Returns the elements file preference, i.e. the file that was last opened.
	 * The preference is read from the OS specific registry. If no such
	 * preference can be found, null is returned.
	 * 
	 * @return
	 * 
	 */

	public static File getSchemaFilePath() {
		Preferences prefs = Preferences.userNodeForPackage(Editor.class);
		String filePath = prefs.get("filePath", null);
		if (filePath != null) {
			return new File(filePath);
		} else {
			return null;
		}
	}

	/**
	 * Sets the file path of the currently loaded file. The path is persisted in
	 * the OS specific registry.
	 * 
	 * @param file
	 *            the file or null to remove the path
	 */
	public static void setSchemaFilePath(File file) {
		Preferences prefs = Preferences.userNodeForPackage(Editor.class);
		if (file != null) {
			prefs.put("filePath", file.getPath());

		} else {
			prefs.remove("filePath");
		}
	}

	/**
	 * Loads element data from the specified file. The current element data will
	 * be replaced.
	 * 
	 * @param is
	 */
	public static void loadSchemaFromFile(InputStream inp, BoxAs ba,
			List<SchemElement> listEl, List<Loop> listLoop,
			List<ParUnion> listUnion, List<Source> listSource,
			List<ElCircuit> listCircuit) {
		try {
			InputStream is = inp;
			JAXBContext context = JAXBContext
					.newInstance(SchemaStateListWrapper.class);
			Unmarshaller um = context.createUnmarshaller();
			SchemaStateListWrapper wrapper = (SchemaStateListWrapper) um
					.unmarshal(is);

			List<ElementState> states = new ArrayList<ElementState>();
			if (wrapper.getList() != null) {
				states.addAll(wrapper.getList());

				// загрузка элементов из сохраненных состояний
				listEl.clear();
				states.stream().forEachOrdered((em) -> {
					loadState(em, ba, listEl);
				});
			}

			// debug-----------
			// debug-----------

			// загрузка контуров
			List<LoopState> loopStates = new ArrayList<LoopState>();
			if (wrapper.getListLoop() != null) {
				loopStates.addAll(wrapper.getListLoop());
				listLoop.clear();
				loopStates.stream().forEachOrdered((l) -> {
					loadLoopState(l, listLoop, listEl);
				});
			}

			// загрузка соединений
			List<UnionState> unionStates = new ArrayList<UnionState>();
			if (wrapper.getListUnion() != null) {
				unionStates.addAll(wrapper.getListUnion());
				listUnion.clear();
				unionStates.stream().forEachOrdered((l) -> {
					loadUnionState(l, listUnion, listLoop);
				});
			}

			// загрузка источников
			List<SourceState> sourceStates = new ArrayList<SourceState>();
			listSource.clear();

			ba.getSources().forEach((s) -> {
				listSource.add(s);
			});

			if (wrapper.getListSource() != null) {
				sourceStates.addAll(wrapper.getListSource());
				// ---------------------------------------------------
				sourceStates.stream().forEachOrdered((s) -> {
					loadSourceState(s, listSource, listEl);
				});
			}

			// загрузка эл. цепей
			List<CircuitState> circuitStates = new ArrayList<CircuitState>();
			if (wrapper.getListCircuit() != null) {
				circuitStates.addAll(wrapper.getListCircuit());
				listCircuit.clear();
				circuitStates.stream().forEach(
						(c) -> {
							loadCircuitState(c, listCircuit, listSource,
									listEl, listUnion);
						});
			}

			// загрузка ветвей для эл. цепей
			circuitStates.stream().forEach(
					(c) -> {
						loadCircuitBranchState(c, circuitStates.indexOf(c),
								listCircuit);
					});

			// Save the file path to the registry.
			// setSchemaFilePath(file);

			// распечатка списка элементов схемы
			// listEl.forEach((sc) -> {
			// System.out.println(sc.toString() + "  -  " + sc.getVarName()
			// + " -  " + sc.getTxName().getText()
			// + "   номер в списке -  " + listEl.indexOf(sc)
			// + " id - " + sc.getIdElementInSchem());
			// });

			// установка идентификатора элемента на старых схемах !!! временно
			// listEl.forEach((sc) -> {
			// sc.setIdElementInSchem(listEl.indexOf(sc));
			// });
			// ------------------------------

		} catch (Exception e) { // catches ANY exception
			e.printStackTrace();
		}
	}

	private static void loadCircuitState(CircuitState c,
			List<ElCircuit> listCircuit, List<Source> listSource,
			List<SchemElement> listEl, List<ParUnion> listUnion) {
		ElCircuit circuit = new ElCircuit();
		if (c != null) {
			circuit.setVarName(c.getVarName());
			circuit.setBranch(c.isBranch());

			// заполнение списка соединений цепи
			if (c.getIndexUnions() != null && c.getIndexUnions().length > 0)
				for (Integer i : c.getIndexUnions()) {
					circuit.addUnion(listUnion.get(i));
				}
			// заполнение списка пользователей цепи
			if (c.getIndexUsers() != null && c.getIndexUsers().length > 0)
				for (Integer i : c.getIndexUsers()) {
					circuit.addUser((User) getElementById(listEl, i));
				}

			if (c.getIndexSource() != -1)
				circuit.setSource(listSource.get(c.getIndexSource()));

			listCircuit.add(circuit);
		}

	}

	private static void loadCircuitBranchState(CircuitState c, int indCir,
			List<ElCircuit> listCircuit) {
		ElCircuit circuit = listCircuit.get(indCir);
		if (c != null) {
			// заполнение списка ветвей цепи
			if (c.getIndexBranches() != null && c.getIndexBranches().length > 0)
				for (Integer i : c.getIndexBranches()) {
					circuit.addBranch(listCircuit.get(i));
				}
		}

	}

	private static void loadSourceState(SourceState s, List<Source> listSource,
			List<SchemElement> listEl) {

		PowerSource source = new PowerSource();
		source.setIsConst(s.isConst());
		if (s != null) {
			source.setVarName(s.getVarName());
			source.setEds(s.getEds());
			source.setInR(s.getInnerR());
			source.setIsConst(s.isConst());
			// заполнение списка полюсов источника
			for (Integer i : s.getIndexTerminals()) {
				source.addTerminal((PowerSourceTerminal) getElementById(listEl,
						i));
			}
			if (s.isConst()) {
				// заполнение списка полюсов источника
				for (Integer i : s.getIndexMinusTerminals()) {
					source.addMinusTerminal((PowerSourceTerminal) getElementById(
							listEl, i));
				}

			}

			listSource.add(source);
		}
	}

	private static void loadUnionState(UnionState us, List<ParUnion> listUnion,
			List<Loop> listLoop) {
		ParUnion union = new ParUnion();
		if (us != null) {
			union.setVarName(us.getVarName());
			// заполнение списка контуров соединения
			for (Integer i : us.getIndexLoops()) {
				union.addLoop(listLoop.get(i));
			}
			listUnion.add(union);
		}
	}

	private static void loadLoopState(LoopState l, List<Loop> listLoop,
			List<SchemElement> listEl) {
		Loop loop = new Loop();
		if (l != null) {
			loop.setVarName(l.getVarName());
			// заполнение списка элементов контура
			for (Integer i : l.getIndexElements()) {
				// loop.getLoop().add(listEl.get(i));
				loop.addProvod(getElementById(listEl, i));
			}
			listLoop.add(loop);
		}
	}

	public static void loadState(ElementState ems, BoxAs boxAs,
			List<SchemElement> list) {
		SchemElement sc = null;

		if (ems.getCl().equals(FrontContact.class)) {
			sc = new FrontContact(ems.getName(), ems.getPlace(),
					boxAs.getRele(ems.getIndexRele()), ems.getTrNum(),
					ems.isZam());
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(FrontAutostopContact.class)) {
			sc = new FrontAutostopContact(ems.getName(), boxAs.getRele(ems
					.getIndexRele()), ems.getTrNum(), ems.isZam());
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(TylAutostopContact.class)) {
			sc = new TylAutostopContact(ems.getName(), boxAs.getRele(ems
					.getIndexRele()), ems.getTrNum(), ems.isZam());
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(TroynicAutostop.class)) {
			sc = new TroynicAutostop(ems.getName(), boxAs.getRele(ems
					.getIndexRele()), ems.getTrNum(), ems.isZam());
			sc.setVarName(ems.getVarName());
		}
		// для тылового контакта
		if (ems.getCl().equals(TylContact.class)) {
			// если тыловой контакт как тыл тройника
			if (ems.getIndexTroynic() != -1) {
				TroynicInterface tr = (TroynicInterface) getElementById(list,
						ems.getIndexTroynic());
				// если у этого тройника уже есть тыловой контакт
				// для исключения дублирования в старых схемах создаем новый
				// пустой контакт
				if (tr.getTylTr() != null) {
					// sc = tr.getTylTr();
				}
				// если нету то создаем новый и присваиваем его тройнику
				else {
					sc = tr.getAsTyl(ems.isZam());
					sc.setVarName(ems.getVarName());
				}
			} else {
				sc = new TylContact(ems.getName(), ems.getPlace(),
						boxAs.getRele(ems.getIndexRele()), ems.getTrNum(),
						ems.isZam());
				sc.setVarName(ems.getVarName());
			}

		}
		if (ems.getCl().equals(Troynic.class)) {
			sc = new Troynic(ems.getName(), ems.getPlace(), boxAs.getRele(ems
					.getIndexRele()), ems.getTrNum(), ems.isZam());
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(TroynicRight.class)) {
			sc = new TroynicRight(ems.getName(), ems.getPlace(),
					boxAs.getRele(ems.getIndexRele()), ems.getTrNum(),
					ems.isZam());
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(TylRightContact.class)) {
			sc = new TylRightContact(ems.getName(), ems.getPlace(),
					boxAs.getRele(ems.getIndexRele()), ems.getTrNum(),
					ems.isZam());
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(FrontRightContact.class)) {
			sc = new FrontRightContact(ems.getName(), ems.getPlace(),
					boxAs.getRele(ems.getIndexRele()), ems.getTrNum(),
					ems.isZam());
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(PolarContact.class)) {
			sc = new PolarContact(ems.getName(), ems.getPlace(),
					boxAs.getRele(ems.getIndexRele()), ems.getTrNum(),
					ems.isZam());
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(Rele.class)) {
			sc = boxAs.getRele(ems.getIndexRele());
			sc.setVarName(ems.getVarName());
			((Rele) sc).setIsFirstLeft(ems.isFirstLeft());
		}
		if (ems.getCl().equals(ZPRSH.class)) {
			sc = new ZPRSH(ems.getPlace());
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(BKR_76.class)) {
			sc = new BKR_76(ems.getPlace());
			sc.setVarName(ems.getVarName());
		}

		if (ems.getCl().equals(PowerSourceTerminal.class)) {
			if (ems.getIndexSource() < 2 && ems.getIndexSource() != -1
					&& ems.getIndexTerminal() != -1) {
				if (ems.isMinus())
					sc = ((PowerSource) boxAs.getSources().get(
							ems.getIndexSource())).getMinusTerminal(ems
							.getIndexTerminal());

				else
					sc = boxAs.getSources().get(ems.getIndexSource())
							.getTerminal(ems.getIndexTerminal());
				((PowerSourceTerminal) sc).setIsLeft(ems.isLeft());

			} else {
				// если у полюса есть родитель
				// перечисление всех классов с полюсами
				if (ems.getIndexParent() != -1) {
					Source parent = (Source) getElementById(list,
							ems.getIndexParent());
					if (parent != null) {
						sc = (ems.isMinus()) ? parent.getMinusTerminals().get(
								ems.getIndexTerminal()) : parent.getTerminals()
								.get(ems.getIndexTerminal());
						sc.setVarName(ems.getVarName());
						((PowerSourceTerminal) sc).setIndexParent(ems
								.getIndexParent());
					}

				} else {
					sc = new PowerSourceTerminal(ems.getName(), "");
					sc.setVarName(ems.getVarName());
					((PowerSourceTerminal) sc).setIsLeft(ems.isLeft());
				}
			}
		}
		if (ems.getCl().equals(PowerSourceTerminalDCM.class)) {
			if (ems.getIndexSource() != -1 && ems.getIndexTerminal() != -1) {
				sc = boxAs.getSources().get(ems.getIndexSource())
						.getTerminal(ems.getIndexTerminal());
			} else
				sc = new PowerSourceTerminalDCM(ems.getName(), "");
			sc.setVarName(ems.getVarName());
			((PowerSourceTerminalDCM) sc).setIsLeft(ems.isLeft());
		}

		if (ems.getCl().equals(SLine.class)) {
			Double[] points = ems.getPoints();
			sc = new SLine(points);
			sc.setVarName(ems.getVarName());
			sc.drowElement(0, 0);
		}
		if (ems.getCl().equals(SvetGol.class)) {
			List<Integer> num = new ArrayList<Integer>();
			for (int i = 0; i < ems.getPoints().length; i++) {
				num.add(ems.getPoints()[i].intValue());
			}
			sc = new SvetGol(ems.getName(), num);
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(SPoint.class)) {
			sc = new SPoint();
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(Diod.class)) {
			sc = new Diod(ems.isHorizont());
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(Conder.class)) {
			sc = new Conder(ems.isHorizont());
			sc.setVarName(ems.getVarName());
			if (ems.getNomTok() != null)
				((Conder) sc).setRazrad(Long.valueOf(ems.getNomTok())
						.longValue());
		}
		if (ems.getCl().equals(Resistor.class)) {
			sc = new Resistor("", "", ems.isHorizont());
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(Drossel.class)) {
			sc = new Drossel(ems.isHorizont());
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(BDR.class)) {
			sc = new BDR();
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(Predochran.class)) {
			sc = new Predochran(ems.getName(), ems.getPlace(), ems.getNomTok());
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(TransBSSH.class)) {
			sc = new TransBSSH("", "");
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(TransSobs2Au3.class)) {
			sc = new TransSobs2Au3(ems.getName(), ems.getPlace());
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(StrMotor.class)) {
			sc = boxAs.getStrMotors().get((int) ems.getIndexRele());
		}
		if (ems.getCl().equals(StrContact.class)) {
			sc = new StrContact(ems.isZam(), ems.isTop(), ems.getTrNum(), boxAs
					.getStrMotors().get((int) ems.getIndexRele()));
			sc.setVarName(ems.getVarName());
			((StrContact) sc).setIsMinus(ems.isMinus());
			((StrContact) sc).setIsWork(ems.isLeft());
		}
		if (ems.getCl().equals(Vyvod.class)) {
			sc = new Vyvod(ems.getName(), "", ems.isLeft());
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(StrKyrbelContact.class)) {
			sc = new StrKyrbelContact(true);
			sc.setVarName(ems.getVarName());
		}
		if (ems.getCl().equals(Obmotka.class)) {
			Rele r = boxAs.getReles().get((int) ems.getIndexRele());
			if (r.getClass().equals(RelePMPUSH.class)) {
				sc = (SchemElement) ((RelePMPUSH) r).getObmotki().get(
						ems.getIndexTerminal());
			}
			if (r.getClass().equals(ReleNMPSH03_90.class)) {
				sc = (SchemElement) ((ReleNMPSH03_90) r).getObmotki().get(
						ems.getIndexTerminal());
			}
			sc.setVarName(ems.getVarName());
			((Obmotka) sc).setIndexRele(ems.getIndexRele());
			((Obmotka) sc).setIndexObm(ems.getIndexTerminal());
		}
		if (ems.getCl().equals(ObmotkaMotor.class)) {
			Motor m = (Motor) getElementById(list, ems.getIndexRele());
			sc = (SchemElement) m.getObmotki().get(ems.getIndexTerminal());
			sc.setVarName(ems.getVarName());
			((ObmotkaMotor) sc).setIndexMotor(ems.getIndexRele());
			((ObmotkaMotor) sc).setIndexObmotka(ems.getIndexTerminal());
		}
		if (ems.getCl().equals(SvetLamp.class)) {
			SvetGol m = (SvetGol) getElementById(list, ems.getIndexParent());
			if (m != null) {
				sc = m.getLamps().get(ems.getIndexTerminal());
				sc.setVarName(ems.getVarName());
				((SvetLamp) sc).setIndexParent((ems.getIndexParent()));
				((SvetLamp) sc).setIndexLamp(ems.getIndexTerminal());
			}
		}

		if (sc != null) {
			sc.setIdElementInSchem(ems.getId());
			sc.setDrowed(ems.isDrowed());
			if (!ems.getCl().equals(SLine.class) && sc.isDrowed())
				sc.drowElement(ems.getlX(), ems.getlY());
			list.add(sc);
		}
	}

	public static SchemElement getElementById(List<SchemElement> list, long id) {
		for (SchemElement schemElement : list) {
			if (schemElement.getIdElementInSchem() == id)
				return schemElement;
		}
		return null;
	}
}
