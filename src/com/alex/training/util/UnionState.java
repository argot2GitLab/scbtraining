﻿package com.alex.training.util;

public class UnionState {

	private String varName;
	private Integer [] indexLoops;

	public UnionState() {
		super();
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	public Integer[] getIndexLoops() {
		return indexLoops;
	}

	public void setIndexLoops(Integer[] indexLoops) {
		this.indexLoops = indexLoops;
	}


}
