﻿package com.alex.training.util;

import com.alex.training.schemElement.SchemElement;


public class ElementState {

	private String varName, name, place, nomTok;
	private Class<SchemElement> cl;
	private Double[] points;
	private long indexRele;
	private int trNum;
	// индекс полюса источника если -1 то полюс не является полюсом
	// предустановленного источника
	private int indexTerminal, indexSource;
	// идекс родителя например для полюса кондера, транса
	private long indexParent;
	// индекс тройника для тылового контакта этого тройника
	private long indexTroynic;
	private boolean isMinus;
	private boolean isZam, isLeft, isFirstLeft, isDrowed, isHorizont, isTop;
	private double lX, lY;
	private long id;

	public ElementState() {
		// TODO Auto-generated constructor stub
	}

	public Class getCl() {
		return cl;
	}

	public void setCl(Class cl) {
		this.cl = cl;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public Double[] getPoints() {
		return points;
	}

	public void setPoints(Double[] points) {
		this.points = points;
	}

	public long getIndexRele() {
		return indexRele;
	}

	public void setIndexRele(long l) {
		this.indexRele = l;
	}

	public int getTrNum() {
		return trNum;
	}

	public void setTrNum(int trNum) {
		this.trNum = trNum;
	}

	public boolean isZam() {
		return isZam;
	}

	public void setZam(boolean isZam) {
		this.isZam = isZam;
	}

	public boolean isLeft() {
		return isLeft;
	}

	public void setLeft(boolean isLeft) {
		this.isLeft = isLeft;
	}

	public double getlX() {
		return lX;
	}

	public void setlX(double lX) {
		this.lX = lX;
	}

	public double getlY() {
		return lY;
	}

	public void setlY(double lY) {
		this.lY = lY;
	}

	public boolean isFirstLeft() {
		return isFirstLeft;
	}

	public void setFirstLeft(boolean isFirstLeft) {
		this.isFirstLeft = isFirstLeft;
	}

	public boolean isDrowed() {
		return isDrowed;
	}

	public void setDrowed(boolean isDrowed) {
		this.isDrowed = isDrowed;
	}

	public int getIndexTerminal() {
		return indexTerminal;
	}

	public void setIndexTerminal(int indexTerminal) {
		this.indexTerminal = indexTerminal;
	}

	public int getIndexSource() {
		return indexSource;
	}

	public void setIndexSource(int indexSource) {
		this.indexSource = indexSource;
	}

	public boolean isMinus() {
		return isMinus;
	}

	public void setMinus(boolean isMinus) {
		this.isMinus = isMinus;
	}

	public boolean isHorizont() {
		return isHorizont;
	}

	public void setHorizont(boolean isHorizont) {
		this.isHorizont = isHorizont;
	}

	public String getNomTok() {
		return nomTok;
	}

	public void setNomTok(String nomTok) {
		this.nomTok = nomTok;
	}

	public boolean isTop() {
		return isTop;
	}

	public void setTop(boolean isTop) {
		this.isTop = isTop;
	}

	public long getIndexParent() {
		return indexParent;
	}

	public void setIndexParent(long l) {
		this.indexParent = l;
	}

	public long getIndexTroynic() {
		return indexTroynic;
	}

	public void setIndexTroynic(long l) {
		this.indexTroynic = l;
	}

}
