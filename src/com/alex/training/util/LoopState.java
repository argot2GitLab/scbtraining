﻿package com.alex.training.util;

public class LoopState {

	private String varName;
	private Integer[] indexElements;

	public LoopState() {
		super();
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	public Integer[] getIndexElements() {
		return indexElements;
	}

	public void setIndexElements(Integer[] index) {
		this.indexElements = index;
	}

}
