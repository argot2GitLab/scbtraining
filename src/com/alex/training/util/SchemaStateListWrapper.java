﻿package com.alex.training.util;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "schemaStates")
public class SchemaStateListWrapper {

	private List<ElementState> states;
	private List<LoopState> loopStates;
	private List<UnionState> unionStates;
	private List<SourceState> sourceStates;
	private List<CircuitState> circuitStates;

	@XmlElement(type = ElementState.class)
	public List<ElementState> getList() {
		return states;
	}

	public void setList(List<ElementState> list) {
		this.states = list;
	}

	@XmlElement(type = LoopState.class)
	public List<LoopState> getListLoop() {
		return loopStates;
	}

	public void setListLoop(List<LoopState> list) {
		this.loopStates = list;
	}

	@XmlElement(type = UnionState.class)
	public List<UnionState> getListUnion() {
		return unionStates;
	}

	public void setListUnion(List<UnionState> list) {
		this.unionStates = list;
	}

	@XmlElement(type = SourceState.class)
	public List<SourceState> getListSource() {
		return sourceStates;
	}

	public void setListSource(List<SourceState> sourceStates) {
		this.sourceStates = sourceStates;
	}

	@XmlElement(type = CircuitState.class)
	public List<CircuitState> getListCircuit() {
		return circuitStates;
	}

	public void setListCircuit(List<CircuitState> circuitStates) {
		this.circuitStates = circuitStates;
	}

}
