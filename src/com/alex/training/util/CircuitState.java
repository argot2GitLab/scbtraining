﻿package com.alex.training.util;

public class CircuitState {

	private String varName;
	private Integer [] indexUnions;
	private Integer [] indexUsers;
	private Integer [] indexBranches;
	private int indexSource;
	private boolean isBranch;
	
	
	public CircuitState() {
		super();
	}


	public String getVarName() {
		return varName;
	}


	public void setVarName(String varName) {
		this.varName = varName;
	}


	public Integer[] getIndexUnions() {
		return indexUnions;
	}


	public void setIndexUnions(Integer[] indexUnions) {
		this.indexUnions = indexUnions;
	}


	public Integer[] getIndexUsers() {
		return indexUsers;
	}


	public void setIndexUsers(Integer[] indexUsers) {
		this.indexUsers = indexUsers;
	}


	public int getIndexSource() {
		return indexSource;
	}


	public void setIndexSource(int indexSource) {
		this.indexSource = indexSource;
	}


	public Integer[] getIndexBranches() {
		return indexBranches;
	}


	public void setIndexBranches(Integer[] indexBranches) {
		this.indexBranches = indexBranches;
	}


	public boolean isBranch() {
		return isBranch;
	}


	public void setBranch(boolean isBranch) {
		this.isBranch = isBranch;
	}
	
	
}
