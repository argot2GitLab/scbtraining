﻿package com.alex.training.util;

public class SourceState {

	private String varName;
	private double innerR;
	private double eds;
	private boolean isConst;
	private Integer[] indexTerminals;
	private Integer[] indexMinusTerminals;
	private int indexSource;

	public SourceState() {
		super();
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	public double getInnerR() {
		return innerR;
	}

	public void setInnerR(double innerR) {
		this.innerR = innerR;
	}

	public double getEds() {
		return eds;
	}

	public void setEds(double eds) {
		this.eds = eds;
	}

	public boolean isConst() {
		return isConst;
	}

	public void setConst(boolean isConst) {
		this.isConst = isConst;
	}

	public Integer[] getIndexTerminals() {
		return indexTerminals;
	}

	public void setIndexTerminals(Integer[] indexTerminals) {
		this.indexTerminals = indexTerminals;
	}

	public Integer[] getIndexMinusTerminals() {
		return indexMinusTerminals;
	}

	public void setIndexMinusTerminals(Integer[] indexMinusTerminals) {
		this.indexMinusTerminals = indexMinusTerminals;
	}

	public int getIndexSource() {
		return indexSource;
	}

	public void setIndexSource(int indexSource) {
		this.indexSource = indexSource;
	}

	
}
