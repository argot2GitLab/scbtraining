﻿package com.alex.training.schemElement;

import java.util.ArrayList;
import java.util.List;

import javafx.animation.AnimationTimer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class ReleNMSHM1_1120 extends Rele {

	private Arc zamedlenie;
	private long count;

	private final AnimationTimer atZamedl = new AnimationTimer() {

		@Override
		public void handle(long now) {
			if (count == 12) {
				setIsCurrent(false);
				stop();
			}

			count++;

		}

	};

	public ReleNMSHM1_1120(String name, String place, boolean isCur) {
		super(name, place, isCur);

		double centerY = 40;

		l1 = new Line(0, centerY, 22, centerY);
		l2 = new Line(58, centerY, 80, centerY);
		l3 = new Line(0, centerY + 10, 23, centerY + 10);
		l4 = new Line(57, centerY + 10, 80, centerY + 10);

		zamedlenie = new Arc(40, 48, 16, 10, 180, 180);
		zamedlenie.setFill(Color.BLACK);

		txV1 = new Text("3");
		txV1.setX(12);
		txV1.setY(centerY - 2);

		txV2 = new Text("1");
		txV2.setX(60);
		txV2.setY(centerY - 2);

		txV3 = new Text("4");
		txV3.setX(12);
		txV3.setY(centerY + 22);

		txV4 = new Text("2");
		txV4.setX(60);
		txV4.setY(centerY + 22);
		this.getChildren().addAll(cir, txName, txPlace, l1, l2, l3, l4, txV1,
				txV2, txV3, txV4, zamedlenie);

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);

	}

	@Override
	public void setCurrent(boolean flag) {
		if (!flag)
			startZamedlenie();
		else
			setIsCurrent(true);

	}

	@Override
	public void setTok(double i) {
		// TODO Auto-generated method stub

	}

	@Override
	public void changeContact(boolean firstLeft) {
		// TODO Auto-generated method stub

	}

	public void startZamedlenie() {
		count = 0;
		atZamedl.start();
	}

}
