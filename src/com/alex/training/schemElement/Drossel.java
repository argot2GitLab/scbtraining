﻿package com.alex.training.schemElement;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;

public class Drossel extends SchemElement {

	protected Arc[] ar = new Arc[3];
	private Line l;

	// Свойство горизонтальности расположения

	public BooleanProperty isHorizont = new SimpleBooleanProperty();

	public final BooleanProperty isHorizontProperty() {
		return isHorizont;
	}

	public final void setIsHorizont(boolean newValue) {
		isHorizont.set(newValue);
	}

	public final boolean getIsHorizont() {
		return isHorizont.get();
	}

	public Drossel(boolean isHor) {
		super("", "");

		setIsHorizont(isHor);

		l = new Line(12, 8, 60, 8);
		l.setStrokeWidth(2);

		double deltaX = 0;
		for (int i = 0; i < ar.length; i++) {
			ar[i] = new Arc(20 + deltaX, 20, 8, 8, 0, 180);
			ar[i].setFill(Color.TRANSPARENT);
			ar[i].setStroke(Color.BLACK);

			getChildren().add(ar[i]);
			deltaX += 16;
		}

		if (!getIsHorizont()) {
			getTransforms().addAll(new Rotate(-90, Rotate.Z_AXIS));
		}

		isHorizontProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				getTransforms().addAll(new Rotate(90, Rotate.Z_AXIS));
			else
				getTransforms().addAll(new Rotate(-90, Rotate.Z_AXIS));
		});

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(l);

	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
