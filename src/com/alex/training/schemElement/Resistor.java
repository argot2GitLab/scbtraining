﻿package com.alex.training.schemElement;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;

public class Resistor extends SchemElement {

	private Rectangle rect;
	private Line l;

	// Свойство горизонтальности расположения

	public BooleanProperty isHorizont = new SimpleBooleanProperty();

	public final BooleanProperty isHorizontProperty() {
		return isHorizont;
	}

	public final void setIsHorizont(boolean newValue) {
		isHorizont.set(newValue);
	}

	public final boolean getIsHorizont() {
		return isHorizont.get();
	}

	public Resistor(String name, String place, boolean isHor) {
		super(name, place);

		setIsHorizont(isHor);

		rect = new Rectangle(30, 15);
		rect.setX(10);
		rect.setY(32.5);
		rect.setFill(Color.WHITE);
		rect.setStroke(Color.BLACK);

		l = new Line(0, 40, 50, 40);

		if (!getIsHorizont()) {
			getTransforms().addAll(new Rotate(-90, Rotate.Z_AXIS));
		}

		isHorizontProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				getTransforms().addAll(new Rotate(90, Rotate.Z_AXIS));
			else
				getTransforms().addAll(new Rotate(-90, Rotate.Z_AXIS));
		});

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(l, rect, txName);

	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
