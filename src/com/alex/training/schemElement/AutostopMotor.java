﻿package com.alex.training.schemElement;

import com.alex.training.schem.Motor;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class AutostopMotor extends Rele implements Motor {

	public AutostopMotor() {
		super();
	}

	public AutostopMotor(boolean isCur) {
		super();
		setIsCurrent(isCur);
	}

	// Свойство двигатель в работе
	public BooleanProperty isWork = new SimpleBooleanProperty();

	public final BooleanProperty isWorkProperty() {
		return isWork;
	}

	public final void setIsWork(boolean newValue) {
		isWork.set(newValue);
	}

	public final boolean getIsWork() {
		return isWork.get();
	}

	public AutostopMotor(String name, boolean isCur) {
		super(name, "автостоп", isCur);

		cir = new Circle(40, 40, 25);
		cir.setFill(Color.WHITE);
		cir.setStroke(Color.BLACK);
		Text tM = new Text("М");
		Text t3 = new Text("3~");
		tM.setX(30);
		tM.setY(35);
		tM.setFont(Font.font("sans", FontWeight.MEDIUM, 20));
		t3.setX(30);
		t3.setY(55);
		t3.setFont(Font.font("sans", FontWeight.MEDIUM, 20));
		txPlace.setY(80);
		txName.setY(100);

		// перевод скобы в открытое положение--------------------------
		obmotki.add(0, new ObmotkaMotor(11.5, 0)); // обмотка АВ при закрытом
													// положении скобы
		obmotki.add(1, new ObmotkaMotor(11.5, 1)); // обмотка АС при закрытом
													// положении скобы
		obmotki.add(2, new ObmotkaMotor(11.5, 2)); // обмотка ВС при закрытом
													// положении скобы

		((ObmotkaMotor) obmotki.get(0)).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			setCurrent(newValue.booleanValue());
		});
		((ObmotkaMotor) obmotki.get(1)).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			setCurrent(newValue.booleanValue());
		});
		((ObmotkaMotor) obmotki.get(2)).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			setCurrent(newValue.booleanValue());
		});
		// ------------------------------------------

		// перевод скобы закрытое положение
		obmotki.add(3, new ObmotkaMotor(11.5, 3)); // обмотка ВА при открытом
													// положении
													// скобы
		obmotki.add(4, new ObmotkaMotor(11.5, 4)); // обмотка ВС при открытом
													// положении
													// скобы
		obmotki.add(5, new ObmotkaMotor(11.5, 5)); // обмотка АС при открытом
													// положении//
													// скобы

		((ObmotkaMotor) obmotki.get(3)).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			setCurrent(newValue.booleanValue());
		});
		((ObmotkaMotor) obmotki.get(4)).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			setCurrent(newValue.booleanValue());
		});
		((ObmotkaMotor) obmotki.get(5)).isCurrentProperty().addListener((property, oldValue, newValue) -> {
			setCurrent(newValue.booleanValue());
		});

		this.getChildren().addAll(cir, tM, t3, txName, txPlace);
	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
	}

	@Override
	public void setCurrent(boolean flag) {
		setIsCurrent(flag);
	}

	@Override
	public void changeContact(boolean firstLeft) {
	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub

	}

}
