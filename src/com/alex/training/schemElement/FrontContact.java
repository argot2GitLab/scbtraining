﻿/**
 * 
 */
package com.alex.training.schemElement;

import javax.xml.ws.handler.MessageContext;

import javafx.scene.paint.Color;
import javafx.scene.shape.Polyline;
import javafx.scene.text.Text;

/**
 * @author alex
 *
 */
public class FrontContact extends Contact {

	public FrontContact(Rele rel) {
		super(rel);
	}

	/**
	 * @param name
	 *            название и место реле
	 */
	public FrontContact(String name, String place, Rele rel, int trNum,
			boolean isZam) {
		super(name, place, rel, trNum, isZam);

		// фронтовой контакт
		front = new Polyline();
		points = new Double[] { 50., y + 10., 50., y, 80., y };
		front.getPoints().addAll(points);

		fNum = new Text(String.valueOf(getContNum() + 2));
		fNum.setX(60);
		fNum.setY(y + 15);

		// перемычка для шунтирования контакта
		points = new Double[] { 0., 40., 0., 20., 80., 20., 80., 40. };
		jumper.getPoints().addAll(points);
		jumper.setStroke(Color.CRIMSON);
		jumper.setStrokeWidth(5);


		// ДЛЯ РЕДАКТОРА--------------------------------------------
		contNumProperty().addListener((property, oldValue, newValue) -> {
			gNum.setText(String.valueOf(newValue.intValue() + 1));
			fNum.setText(String.valueOf(newValue.intValue() + 2));
			System.out.println("xyu -" + newValue.intValue());
		});

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(general, front, contact, txName, txPlace, gNum,
				fNum);
	}

	@Override
	public void setupError(boolean flag, int mod) {
		switch (mod) {
		case 0: {
			resError = (flag) ? 2000 : 0;
			if (getIsZam())
				setResistance(resError);
		}
			break;
		case 1: {
			resError = (flag) ? obryv : 0;
			if (getIsZam())
				setResistance(resError);
		}
			break;

		default:
			break;
		}
	}

}
