﻿package com.alex.training.schemElement;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.text.Text;

public abstract class RightContact extends Contact {

	private final double endXZamR = 21;
	private final double endXR = 22;
	private final double endYZamR = y + 10;
	private final double endYR = y + 22;

	// для работы электрических схем----------------------------------

	public RightContact(Rele rel) {
		super(rel);
	}

	public RightContact(String name, String place, Rele rel, int trNum,
			boolean isZam) {
		super(name, place, rel, trNum, isZam);

		this.isZam.set(isZam);

		// общий элемент контакта
		points = new Double[] { 55., y + 10, 55., y, 80., y };
		general.getPoints().clear();
		general.getPoints().addAll(points);
		// сам перемещающийся контакт
		contact = new Line();
		contact.setStartX(54);
		contact.setStartY(y + 10);
		contact.setEndX((this.isZam.get()) ? endXZamR : endXR);
		contact.setEndY((this.isZam.get()) ? endYZamR : endYR);
		contact.setStrokeWidth(2.5);
		
		this.isZam.addListener((property, oldValue, newValue) -> {
			contact.setEndX((this.isZam.get()) ? endXZamR : endXR);
			contact.setEndY((this.isZam.get()) ? endYZamR : endYR);
		});


	
		txName.setX(40 - txName.getBoundsInLocal().getWidth() / 2);
		txName.setY(y - 10);

		txPlace.setX(40 - txPlace.getBoundsInLocal().getWidth() / 2);
		txPlace.setY(y - 25);

		gNum.setX(60);
		gNum.setY(y + 15);

	}

}
