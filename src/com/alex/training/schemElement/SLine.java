﻿package com.alex.training.schemElement;

import javafx.animation.AnimationTimer;
import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polyline;

public class SLine extends SchemElement {

	protected Polyline line = new Polyline();
	protected Double[] points;
	private Point2D lastPoint;

	public SLine(Double[] points) {
		super();

		this.points = points;
		line.getPoints().addAll(points);
		// подсветка точек подключения тестера
		startRect.setX(line.getPoints().get(0).doubleValue() - 5);
		startRect.setY(line.getPoints().get(1) - 5);
		lastPoint = getLastPoint();
		startRect1.setX(lastPoint.getX() - 5);
		startRect1.setY(lastPoint.getY() - 5);
		line.setStroke(Color.BLACK);

		/**
		 * для отладки режим подсветки провода для составления контура
		 */
		line.setOnMouseEntered((e) -> {
			curColor = line.getStroke();
			shiftLine(true);
		});
		line.setOnMouseExited((e) -> {
			shiftLine(false);
		});

	}

	private final AnimationTimer atSelect = new AnimationTimer() {

		boolean f;
		long count = 0;
		double op = 1;

		@Override
		public void handle(long now) {
			if (count % 30 == 0) {
				op = (f) ? 1 : 0;
				startRect1.setOpacity(op);
				f = !f;
			}

			count++;

		}
	};

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		getChildren().addAll(line, startRect, startRect1);

	}

	// для редактора мигание на конце линии
	public void selectByAddVertex(boolean flag) {
		startRect1.setVisible(flag);
		if (flag) {
			atSelect.start();
			getChildren().add(startRect1);
		} else {
			atSelect.stop();
			getChildren().remove(startRect1);
		}
	}

	public Polyline getLine() {
		return line;
	}

	public void setLine(Polyline line) {
		this.line = line;
	}

	public Point2D getLastPoint() {
		double lastX, lastY;
		lastX = line.getPoints().get(line.getPoints().size() - 2);
		lastY = line.getPoints().get(line.getPoints().size() - 1);
		Point2D lastP = new Point2D(lastX, lastY);
		return lastP;

	}

	public void setRectPosition() {
		lastPoint = getLastPoint();
		startRect1.setX(lastPoint.getX() - 5);
		startRect1.setY(lastPoint.getY() - 5);
	}

	public Double[] getPoints() {
		return points;
	}

	public void setPoints(Double[] points) {
		this.points = points;
	}

	@Override
	public void podsvetka(int mod) {
		Color color = Color.BLACK;

		switch (mod) {
		case 1:
			color = Color.BLUEVIOLET;
			break;
		case 2:
			color = Color.GREEN;
			break;
		case 3:
			color = Color.YELLOW;
			break;

		default:
			break;
		}

		curColor = color;
		line.setStroke(color);
	}

	/**
	 * для отладки режим подсветки провода при рисовании схем
	 */
	public void shiftLine(boolean flag) {
		line.setStroke((flag) ? Color.RED : curColor);
		line.setStrokeWidth((flag) ? 3 : 1);

	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}
}
