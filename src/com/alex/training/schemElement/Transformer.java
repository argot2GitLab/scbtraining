﻿package com.alex.training.schemElement;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

import com.alex.training.schem.Source;
import com.alex.training.schem.User;

public abstract class Transformer extends SchemElement implements Source, User {

	protected Arc[] ar = new Arc[6];
	protected Circle[] cir = new Circle[4];
	protected Line serd;

	protected double radius = 10;
	protected double centerY = 50;
	protected double centerX = 26;
	protected double angle = 270;

	protected Line l1, l2, l3, l4;
	protected Text txFirst1, txFirst2, txSecond1, txSecond2, txType;

	// для электрических цепей
	protected List<PowerSourceTerminal> terminals = new ArrayList<PowerSourceTerminal>();
	protected List<PowerSourceTerminal> minusTerminals = new ArrayList<PowerSourceTerminal>();

	protected PowerSourceTerminal term1, term2;


	public Transformer(String name, String place) {
		super(name, place);

		// первичная и вторичная обмотки трансформатора
		for (int i = 0; i < ar.length; i++) {
			ar[i] = new Arc(centerX, centerY, radius, radius, angle, 180);
			ar[i].setFill(Color.TRANSPARENT);
			ar[i].setStroke(Color.BLACK);

			getChildren().add(ar[i]);
			centerY += radius * 2;
			if (i == 2) {
				centerY = 50;
				centerX = 56;
				angle = 90;
			}
		}

		// кружки по краям обмоток
		centerX = 23;
		centerY = 40;
		radius = 3;
		for (int i = 0; i < cir.length; i++) {
			cir[i] = new Circle(centerX, centerY, radius, Color.TRANSPARENT);
			cir[i].setStroke(Color.BLACK);
			centerY += 60;
			if (i == 1) {
				centerY = 40;
				centerX = 59;
			}
			getChildren().add(cir[i]);
		}

		// сердечник
		serd = new Line(41, 42, 41, 95);
		serd.setStroke(Color.BLACK);
		serd.setStrokeWidth(3);

		endY = 100; // нижний вывод трансформатора
		endX = 82; // дальний вывод трансформатора

		// отводы от катушек
		l1 = new Line(0, 40, 20, 40);
		l2 = new Line(0, endY, 20, endY);
		l3 = new Line(62, 40, endX, 40);
		l4 = new Line(62, endY, endX, endY);

		endY = 100 - 40; // нижний вывод трансформатора
		// название и место
		centerY = 40; // верхний вывод трансформатора
		txName.setX(41 - txName.getBoundsInLocal().getWidth() / 2);
		txName.setY(centerY - 13);

		txPlace.setX(41 - txPlace.getBoundsInLocal().getWidth() / 2);
		txPlace.setY(centerY - 28);

		centerX = 23;
		// номера выводов
		txFirst1 = new Text("I1");
		txFirst1.setX(centerX - 20);
		txFirst1.setY(centerY + 14);

		txFirst2 = new Text("I2");
		txFirst2.setX(centerX - 20);
		txFirst2.setY(centerY + 55);

		centerX = 59;

		txSecond1 = new Text("III1");
		txSecond1.setX(centerX + 7);
		txSecond1.setY(centerY + 14);

		txSecond2 = new Text("III2");
		txSecond2.setX(centerX + 7);
		txSecond2.setY(centerY + 57);

		// подсветка точек подключения тестера
		startRect.setX(-5);
		startRect.setY(35);
		startRect1.setX(-5);
		startRect1.setY(95);

		// выводы вторичной обмотки для работы эл.цепи
		term1 = new PowerSourceTerminal("", "");
		term1.setMinus(false);
		term2 = new PowerSourceTerminal("", "");
		term2.setMinus(false);

	}
	// свойство тип тока постоянный - true
	// ------------------------------------------------
	public BooleanProperty isConst = new SimpleBooleanProperty();

	public BooleanProperty isConstProperty() {
		return isConst;
	}

	public void setIsConst(boolean newValue) {
		isConst.set(newValue);
	}

	public boolean getIsConst() {
		return isConst.get();
	}

	// ЭДС источника
	public DoubleProperty eds = new SimpleDoubleProperty();

	public DoubleProperty edsProperty() {
		return eds;
	}

	public void setEds(double newValue) {
		eds.set(newValue);
	}

	public double getEds() {
		return eds.get();
	}

	// внутреннее сопротивление источника
	public DoubleProperty inR = new SimpleDoubleProperty();

	public DoubleProperty inRProperty() {
		return inR;
	}

	public void setInR(double newValue) {
		inR.set(newValue);
	}

	public double getInR() {
		return inR.get();
	}

	/**
	 * список полюсов источника пиания
	 * 
	 */


	public PowerSourceTerminal getTerminal(int i) {
		return terminals.get(i);
	}

	public List<PowerSourceTerminal> getMinusTerminals() {
		return minusTerminals;
	}

	public void addTerminal(PowerSourceTerminal t) {
		if (t != null
				&& terminals.stream().allMatch((l) -> !l.equals(t))
				&& terminals.stream().allMatch(
						(te) -> !te.getVarName().equals(t.getVarName())))
			terminals.add(t);

	}

	public void addMinusTerminal(PowerSourceTerminal t) {
		if (t != null
				&& minusTerminals.stream().allMatch((l) -> !l.equals(t))
				&& minusTerminals.stream().allMatch(
						(te) -> !te.getVarName().equals(t.getVarName())))
			minusTerminals.add(t);
	}

	public List<PowerSourceTerminal> getTerminals() {
		return terminals;
	}

	public boolean isTerminalConteined(String varName) {
		return (getTerminal(varName) != null) ? true : false;
	}

	public SchemElement getTerminal(String varName) {
		SchemElement temp = null;
		for (SchemElement sc : terminals) {
			if (sc.getVarName().equals(varName))
				temp = sc;
		}
		if (temp == null)
			temp = getMinusTerminal(varName);

		return temp;
	}

	public SchemElement getMinusTerminal(String newValue) {
		SchemElement temp = null;
		for (SchemElement sc : minusTerminals) {
			if (sc.getVarName().equals(newValue))
				temp = sc;
		}
		return temp;
	}

	public boolean isTerminalConteined(PowerSourceTerminal term) {
		return terminals.stream().anyMatch((t) -> t.equals(term))
				|| minusTerminals.stream().anyMatch((t) -> t.equals(term));
	}

	public int getIndexTerminal(PowerSourceTerminal term) {
		return (isTerminalConteined(term)) ? (terminals.stream()
				.anyMatch((t) -> t.equals(term))) ? terminals.indexOf(term)
				: minusTerminals.indexOf(term) : -1;
	}

	public int getIndexTerminal(String varName) {
		int i = -1;
		if (getTerminals().stream().anyMatch(
				(t) -> t.getVarName().equals(varName)))
			i = terminals.indexOf(this.getTerminal(varName));
		return i;
	}

	public int getIndexMinusTerminal(String varName) {
		int i = -1;
		if (getMinusTerminals().stream().anyMatch(
				(t) -> t.getVarName().equals(varName)))
			i = minusTerminals.indexOf(this.getTerminal(varName));
		return i;
	}

	public PowerSourceTerminal getMinusTerminal(int i) {
		return minusTerminals.get(i);
	}



	public Text getTxFirst1() {
		return txFirst1;
	}

	public Text getTxFirst2() {
		return txFirst2;
	}

	public Text getTxSecond1() {
		return txSecond1;
	}

	public Text getTxSecond2() {
		return txSecond2;
	}

	public Text getTxType() {
		return txType;
	}

	public double getEndX() {
		return endX;
	}

	public double getEndY() {
		return endY;
	}

	public double getTopY() {
		return centerY;
	}

	public PowerSourceTerminal getTerm1() {
		return term1;
	}

	public PowerSourceTerminal getTerm2() {
		return term2;
	}

}
