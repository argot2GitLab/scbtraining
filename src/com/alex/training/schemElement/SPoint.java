﻿package com.alex.training.schemElement;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class SPoint extends SchemElement {

	protected Circle cir;

	public SPoint() {
		super();

		cir = new Circle(0, 0, 3, Color.BLACK);

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().add(cir);
	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
