﻿package com.alex.training.schemElement;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.transform.Rotate;

import com.alex.training.schem.Source;
import com.alex.training.schem.User;

public class Diod extends SchemElement implements Source, User {

	private Polyline triangle;
	private Line l1, l2;
	private Double[] points;

	protected List<PowerSourceTerminal> terminals = new ArrayList<PowerSourceTerminal>();
	protected List<PowerSourceTerminal> minusTerminals = new ArrayList<PowerSourceTerminal>();

	// Свойство горизонтальности расположения

	public BooleanProperty isHorizont = new SimpleBooleanProperty();

	public final BooleanProperty isHorizontProperty() {
		return isHorizont;
	}

	public final void setIsHorizont(boolean newValue) {
		isHorizont.set(newValue);
	}

	public final boolean getIsHorizont() {
		return isHorizont.get();
	}

	// свойство тип тока постоянный - true
	// ------------------------------------------------
	public BooleanProperty isConst = new SimpleBooleanProperty();

	public BooleanProperty isConstProperty() {
		return isConst;
	}

	public void setIsConst(boolean newValue) {
		isConst.set(newValue);
	}

	public boolean getIsConst() {
		return isConst.get();
	}

	// ЭДС источника
	public DoubleProperty eds = new SimpleDoubleProperty();

	public DoubleProperty edsProperty() {
		return eds;
	}

	public void setEds(double newValue) {
		eds.set(newValue);
	}

	public double getEds() {
		return eds.get();
	}

	// внутреннее сопротивление источника
	public DoubleProperty inR = new SimpleDoubleProperty();

	public DoubleProperty inRProperty() {
		return inR;
	}

	public void setInR(double newValue) {
		inR.set(newValue);
	}

	public double getInR() {
		return inR.get();
	}

	public Diod(boolean isHorizont) {

		setIsHorizont(isHorizont);

		triangle = new Polyline();
		points = new Double[] { 7., 20., 7., 40., 17., 30., 7., 20. };
		triangle.getPoints().addAll(points);

		l1 = new Line(0, 30, 24, 30);
		l2 = new Line(17, 22, 17, 38);

		if (!getIsHorizont()) {
			getTransforms().addAll(new Rotate(-90, Rotate.Z_AXIS));
		}

		isHorizontProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				getTransforms().addAll(new Rotate(90, Rotate.Z_AXIS));
			else
				getTransforms().addAll(new Rotate(-90, Rotate.Z_AXIS));
		});

		setIsConst(true);
		// полюсы для работы эл.цепи
		terminals.add(0, new PowerSourceTerminal("", ""));
		terminals.get(0).setResistance(0);
		minusTerminals.add(0, new PowerSourceTerminal("", ""));
		minusTerminals.get(0).setMinus(true);

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		// TODO Auto-generated method stub
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(triangle, l1, l2);

	}

	@Override
	public void setCurrent(boolean flag) {
	}

	@Override
	public void setTok(double i) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub

	}

	@Override
	public PowerSourceTerminal getTerminal(int i) {
		return terminals.get(i);
	}

	@Override
	public List<PowerSourceTerminal> getMinusTerminals() {
		return minusTerminals;
	}

	@Override
	public void addTerminal(PowerSourceTerminal t) {
		if (t != null
				&& terminals.stream().allMatch((l) -> !l.equals(t))
				&& terminals.stream().allMatch(
						(te) -> !te.getVarName().equals(t.getVarName()))) {
			terminals.add(t);
		}
	}

	@Override
	public void addMinusTerminal(PowerSourceTerminal t) {
		if (t != null
				&& minusTerminals.stream().allMatch((l) -> !l.equals(t))
				&& minusTerminals.stream().allMatch(
						(te) -> !te.getVarName().equals(t.getVarName())))
			minusTerminals.add(t);
	}

	@Override
	public List<PowerSourceTerminal> getTerminals() {
		return terminals;
	}

	@Override
	public boolean isTerminalConteined(String varName) {
		return (getTerminal(varName) != null) ? true : false;
	}

	@Override
	public SchemElement getTerminal(String varName) {
		SchemElement temp = null;
		for (SchemElement sc : terminals) {
			if (sc.getVarName().equals(varName))
				temp = sc;
		}
		if (temp == null)
			temp = getMinusTerminal(varName);

		return temp;
	}

	@Override
	public SchemElement getMinusTerminal(String newValue) {
		SchemElement temp = null;
		for (SchemElement sc : minusTerminals) {
			if (sc.getVarName().equals(newValue))
				temp = sc;
		}
		return temp;
	}

	@Override
	public boolean isTerminalConteined(PowerSourceTerminal term) {
		return terminals.stream().anyMatch((t) -> t.equals(term))
				|| minusTerminals.stream().anyMatch((t) -> t.equals(term));
	}

	@Override
	public int getIndexTerminal(PowerSourceTerminal term) {
		return (isTerminalConteined(term)) ? (terminals.stream()
				.anyMatch((t) -> t.equals(term))) ? terminals.indexOf(term)
				: minusTerminals.indexOf(term) : -1;
	}

	@Override
	public int getIndexTerminal(String varName) {
		int i = -1;
		if (getTerminals().stream().anyMatch(
				(t) -> t.getVarName().equals(varName)))
			i = terminals.indexOf(this.getTerminal(varName));
		return i;
	}

	@Override
	public int getIndexMinusTerminal(String varName) {
		int i = -1;
		if (getMinusTerminals().stream().anyMatch(
				(t) -> t.getVarName().equals(varName)))
			i = minusTerminals.indexOf(this.getTerminal(varName));
		return i;
	}

	@Override
	public PowerSourceTerminal getMinusTerminal(int i) {
		return minusTerminals.get(i);
	}

}