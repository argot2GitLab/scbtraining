﻿package com.alex.training.schemElement;

import javafx.animation.AnimationTimer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

public class PowerSourceTerminalDCM extends PowerSourceTerminal {

	private Circle cir;
	// длителрность подачи плюса в схему (fps 1/60 секунды)
	private long zadergka = 30;

	public PowerSourceTerminalDCM(String name, String place) {
		super(name, place);

		setIsLeft(true);

		// начальная установка проводимости
		// соответствующая отсутсвию питания в полюсе ДЦМ
		setIsConductance(false);
		setResistance(obryv);

		double centerY = 40;
		endX = 60;
		source = new Line(30, centerY, endX, centerY);

		cir = new Circle(15, Color.TRANSPARENT);
		cir.setLayoutX(15);
		cir.setLayoutY(centerY);
		cir.setStroke(Color.BLACK);

		txName.setX(7);
		txName.setY(centerY + 5);

		endY = centerY; // вывод для подключения следующих элементов

		// установка стороны отображения для графики
		isLeftProperty().addListener((property, oldValue, newValue) -> {
			source.setStartX((newValue.booleanValue()) ? 30 : 0);
			source.setEndX((newValue.booleanValue()) ? 60 : 30);
			cir.setLayoutX((newValue.booleanValue()) ? 15 : 45);
			txName.setX((newValue.booleanValue()) ? 7 : 37);
		});

	}

	// таймер для подачи питания в полюс
	final private AnimationTimer atDcm = new AnimationTimer() {

		long count = 0;

		@Override
		public void handle(long arg0) {
			// отключение питания в полюсе ДЦМ через
			// 0,5 секунд по умолчанию
			if (count >= zadergka) {
				setResistance(obryv);
				stop();
				count = 0;
			}
			count++;
		}

	};

	public void setPlusDcm() {
		setResistance(0);
		atDcm.start();
	}

	public PowerSourceTerminalDCM(String name, String place, String varName) {
		this(name, place);

		this.varName = varName;
	}

	@Override
	public double getEndX() {
		return endX;
	}

	@Override
	public double getEndY() {
		return endY;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(source, cir, txName);
	}

	public long getZadergka() {
		return zadergka;
	}

	public void setZadergka(long zadergka) {
		this.zadergka = zadergka;
	}

}
