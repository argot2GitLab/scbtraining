﻿package com.alex.training.schemElement;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.shape.Line;

public class PowerSourceTerminal extends SchemElement {

	protected Line source;
	protected boolean isMinus;
	protected long indexParent;

	/**
	 * определяет фазу для поюса источника переменного тока 0 - полюс источника
	 * постоянного тока 1 - фаза А 2 - фаза В 3 - фаза С
	 */
	private int faza;

	// сторона отображения полюса
	public BooleanProperty isLeft = new SimpleBooleanProperty();

	public final BooleanProperty isLeftProperty() {
		return isLeft;
	}

	public final void setIsLeft(boolean newValue) {
		isLeft.set(newValue);
	}

	public final boolean getIsLeft() {
		return isLeft.get();
	}

	public PowerSourceTerminal(String name, String place) {
		super(name, place);
		faza = 0;
		setIsLeft(true);
		setMinus(false);
		setIndexParent(-1);
		double centerY = 40;
		endX = 60;
		source = new Line(0, centerY, endX, centerY);

		txName.setX(0);
		txName.setY(centerY - 5);

		endY = centerY; // вывод для подключения следующих элементов

		// установка стороны отображения для графики
		isLeftProperty().addListener(
				(property, oldValue, newValue) -> {
					txName.setX((newValue.booleanValue()) ? 0 : 60 - txName
							.getBoundsInLocal().getWidth());
				});
	}

	public int getFaza() {
		return faza;
	}

	public void setFaza(int faza) {
		this.faza = faza;
	}

	public PowerSourceTerminal(String name, String place, String varName) {
		this(name, place);

		this.varName = varName;
	}

	@Override
	public double getEndX() {
		return endX;
	}

	@Override
	public double getEndY() {
		return endY;
	}

	public long getIndexParent() {
		return indexParent;
	}

	public void setIndexParent(long l) {
		this.indexParent = l;
	}

	public boolean isMinus() {
		return isMinus;
	}

	public void setMinus(boolean isMinus) {
		this.isMinus = isMinus;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(source, txName);
	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
