﻿package com.alex.training.schemElement;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

public class Predochran extends SchemElement {

	private Rectangle rect;
	private Line l;
	protected Text txV1, txV2, txNom;

	public Predochran(String name, String place, String nom) {
		super(name, place);

		rect = new Rectangle(30, 15);
		rect.setX(10);
		rect.setY(32.5);
		rect.setFill(Color.TRANSPARENT);
		rect.setStroke(Color.BLACK);

		l = new Line(0, 40, 50, 40);

		txName.setX(25 - txName.getBoundsInLocal().getWidth() / 2);
		txName.setY(30 - 3);

		txV1 = new Text("1");
		txV1.setX(1);
		txV1.setY(55);

		txV2 = new Text("2");
		txV2.setX(42);
		txV2.setY(55);

		txNom = new Text(nom);
		txNom.setX(25 - txNom.getBoundsInLocal().getWidth() / 2);
		txNom.setY(60);

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(rect, l, txName, txV1, txV2, txNom);

	}

	public Text getTxNom() {
		return txNom;
	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
