﻿package com.alex.training.schemElement;

import java.util.List;

import javafx.animation.AnimationTimer;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class ReleNMSH2_4000 extends Rele {

	public ReleNMSH2_4000() {
		super();
	}

	public ReleNMSH2_4000(boolean isCur) {
		super();
		setIsCurrent(isCur);
	}

	public ReleNMSH2_4000(String name, String place, boolean isCur) {
		super(name, place, isCur);

		double centerY = 40;

		l1 = new Line(0, centerY, 22, centerY);
		l2 = new Line(58, centerY, 80, centerY);

		txV1 = new Text("1");
		txV1.setX(12);
		txV1.setY(centerY + 15);

		txV2 = new Text("4");
		txV2.setX(60);
		txV2.setY(centerY + 15);
		this.getChildren().addAll(cir, txName, txPlace, l1, l2, txV1, txV2);
	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
	}

	@Override
	public void setCurrent(boolean flag) {
		setIsCurrent(flag);
	}

	@Override
	public void setTok(double i) {
		// TODO Auto-generated method stub

	}

	@Override
	public void changeContact(boolean firstLeft) {
		if (firstLeft) {
			txV1.setText("1");
			txV2.setText("4");
		} else {
			txV1.setText("4");
			txV2.setText("1");

		}
	}

}
