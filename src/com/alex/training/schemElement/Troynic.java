﻿package com.alex.training.schemElement;

import javafx.scene.paint.Color;
import javafx.scene.shape.Polyline;
import javafx.scene.text.Text;

public class Troynic extends Contact implements TroynicInterface {

	private TylContact tylTr = null;
	private Polyline jumperTyl;

	public Troynic(Rele rel) {
		super(rel);
	}

	public Troynic(String name, String place, Rele rel, int trNum, boolean isZam) {
		super(name, place, rel, trNum, isZam);

		// фронтовой контакт
		front = new Polyline();
		points = new Double[] { 50., y + 10., 50., y, 80., y };
		front.getPoints().addAll(points);

		fNum = new Text(String.valueOf(getContNum() + 2));
		fNum.setX(60);
		fNum.setY(y + 15);

		// тыловой контакт
		tyl = new Polyline();
		points = new Double[] { 50., y + 20., 50., y + 40, 80., y + 40 };
		tyl.getPoints().addAll(points);

		tNum = new Text(String.valueOf(getContNum() + 3));
		tNum.setX(60);
		tNum.setY(y + 35);

		// перемычка для шунтирования контакта
		points = new Double[] { 0., 40., 0., 20., 80., 20., 80., 40. };
		jumper.getPoints().addAll(points);
		jumper.setStroke(Color.CRIMSON);
		jumper.setStrokeWidth(5);
		// перемычка для шунтирования тылового контакта тройника
		points = new Double[] { 0., 40., 0., 80., 80., 80. };
		jumperTyl = new Polyline();
		jumperTyl.getPoints().addAll(points);
		jumperTyl.setStroke(Color.CRIMSON);
		jumperTyl.setStrokeWidth(5);

		// привязка замкнутости фронтового контакта к целостности цепи
		setIsConductance(isZam);
		isZamProperty().addListener(
				(property, oldValue, newValue) -> isConductance.set(newValue
						.booleanValue()));

		// ДЛЯ РЕДАКТОРА--------------------------------------------
		contNumProperty().addListener((property, oldValue, newValue) -> {
			gNum.setText(String.valueOf(newValue.intValue() + 1));
			fNum.setText(String.valueOf(newValue.intValue() + 2));
			tNum.setText(String.valueOf(newValue.intValue() + 3));
		});

		// для тестирования----------------------------
		txName.setOnMouseClicked((e) -> {
			// if ((e.getClickCount() == 2)
			// && (e.getButton().equals(MouseButton.PRIMARY)))
			// getRele().setIsCurrent(!getRele().getIsCurrent());
		});
		// ------------------------------------------------------
	}

	public TylContact getAsTyl(boolean isZam) {
		if (tylTr == null)
			tylTr = new TylContact(getRele(), isZam);
		tylTr.getTxName().setText(txName.getText());
		tylTr.getTxPlace().setText(txPlace.getText());
		tylTr.setContNum(getContNum());
		return tylTr;
	}

	@Override
	public double getEndX() {
		return 80;
	}

	@Override
	public double getEndY() {
		return 80;
	}

	public TylContact getTylTr() {
		return tylTr;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(general, front, tyl, contact, txName, txPlace,
				gNum, fNum, tNum);
	}

	@Override
	public void setTylTr(TylContact tylTr) {
		this.tylTr = tylTr;

	}

	@Override
	public void setupJumperTyl(boolean flag) {
		if (flag) {
			tylTr.setResistance(0);
			getChildren().addAll(jumperTyl);
			if (tylTr.getIsError())
				tylTr.setIsError(false);
		} else {
			tylTr.setResByZam(tylTr.getIsZam());
			getChildren().remove(jumperTyl);
		}
		tylTr.setIsJumperSet(flag);
	}

	@Override
	public void setupError(boolean flag, int mod) {
		switch (mod) {
		case 0: {
			resError = (flag) ? 2000 : 0;
			if (getIsZam())
				setResistance(resError);
		}
			break;
		case 1: {
			resError = (flag) ? obryv : 0;
			if (getIsZam())
				setResistance(resError);
		}
			break;

		default:
			break;
		}
	}

}
