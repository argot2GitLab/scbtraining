﻿package com.alex.training.schemElement;

import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.text.Text;

public class PolarContact extends Contact implements TroynicInterface {

	protected Line contactObr;
	private TylContact tylTr;

	public PolarContact(Rele rel) {
		super(rel);
	}

	public PolarContact(String name, String place, Rele rel, int trNum,
			boolean isZam) {
		super(name, place, rel, trNum, isZam);

		setIsZam(isZam);
		endYZam = y;

		contact.setEndX((this.isZam.get()) ? endXZam : endXo);
		contact.setEndY((this.isZam.get()) ? endYZam : endYo);

		// обратный контакт
		contactObr = new Line();
		contactObr.setStartX(21);
		contactObr.setStartY(y + 10);
		contactObr.setEndX((!this.isZam.get()) ? endXZam : endXo);
		contactObr.setEndY((!this.isZam.get()) ? endYZam : endYo);

		contactObr.setStrokeWidth(1.25);
		contactObr.getStrokeDashArray().addAll(10., 5.);

		// общий элемент контакта
		points = new Double[] { 0., y + 10, 20., y + 10 };
		general.getPoints().clear();
		general.getPoints().addAll(points);
		gNum.setText(String.valueOf(getContNum() + 1 + 100));
		gNum.setY(y + 25);
		gNum.setX(0);

		// фронтовой контакт
		front = new Polyline();
		points = new Double[] { 50., y, 50., y - 10, 80., y - 10 };
		front.getPoints().addAll(points);

		fNum = new Text(String.valueOf(getContNum() + 2 + 100));
		fNum.setX(60);
		fNum.setY(y + 5);

		// тыловой контакт
		tyl = new Polyline();
		points = new Double[] { 50., y + 20., 50., y + 40, 80., y + 40 };
		tyl.getPoints().addAll(points);

		tNum = new Text(String.valueOf(getContNum() + 3 + 100));
		tNum.setX(60);
		tNum.setY(y + 35);

		txName.setX(30 - txName.getBoundsInLocal().getWidth() / 2);
		txName.setY(y - 20);

		txPlace.setX(30 - txPlace.getBoundsInLocal().getWidth() / 2);
		txPlace.setY(y - 35);

		// привязка замкнутости фронтового контакта к целостности цепи
		setIsConductance(isZam);
		this.isZamProperty().addListener((property, oldValue, newValue) -> {
			isConductance.set(newValue.booleanValue());
			contact.setEndX((newValue) ? endXZam : endXo);
			contact.setEndY((newValue) ? endYZam : endYo);
			contactObr.setEndX((!this.isZam.get()) ? endXZam : endXo);
			contactObr.setEndY((!this.isZam.get()) ? endYZam : endYo);
		});


		// ДЛЯ РЕДАКТОРА--------------------------------------------
		contNumProperty().addListener((property, oldValue, newValue) -> {
			gNum.setText(String.valueOf(newValue.intValue() + 1 + 100));
			fNum.setText(String.valueOf(newValue.intValue() + 2 + 100));
			tNum.setText(String.valueOf(newValue.intValue() + 3 + 100));
		});

		// для тестирования----------------------------
		txName.setOnMouseClicked((e) -> {
			// if ((e.getClickCount() == 2)
			// && (e.getButton().equals(MouseButton.PRIMARY)))
			// getRele().setIsCurrent(!getRele().getIsCurrent());
		});
		// ------------------------------------------------------
	}

	public TylContact getAsTyl(boolean isZam) {
		if (tylTr == null)
			tylTr = new TylContact(getRele(), isZam);
		tylTr.getTxName().setText(txName.getText());
		tylTr.getTxPlace().setText(txPlace.getText());
		tylTr.setContNum(getContNum());
		return tylTr;
	}

	@Override
	public double getEndX() {
		return 80;
	}

	@Override
	public double getEndY() {
		return 80;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(general, front, tyl, contact, txName, txPlace,
				gNum, fNum, tNum, contactObr);
	}

	@Override
	public TylContact getTylTr() {
		return tylTr;
	}

	@Override
	public void setTylTr(TylContact tylTr) {
		this.tylTr = tylTr;
	}

	@Override
	public void setupJumperTyl(boolean flag) {
	}

	@Override
	public void setupError(boolean flag, int mod) {
		switch (mod) {
		case 0: {
			resError = (flag) ? 2000 : 0;
			if (getIsZam())
				setResistance(resError);
		}
			break;
		case 1: {
			resError = (flag) ? obryv : 0;
			if (getIsZam())
				setResistance(resError);
		}
			break;

		default:
			break;
		}
	}

}
