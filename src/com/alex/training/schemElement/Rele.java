﻿package com.alex.training.schemElement;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

import com.alex.training.schem.User;

public abstract class Rele extends SchemElement implements User {

	protected Circle cir;
	protected Line l1, l2, l3, l4;
	protected Text txV1, txV2, txV3, txV4, txType;
	protected boolean flagZaderzgkaRele = true;
	protected List<User> obmotki = new ArrayList<User>();
	protected double resError;

	/**
	 * свойство порядка контактов для графического отображения true - 1 слева
	 */
	public BooleanProperty isFirstLeft = new SimpleBooleanProperty();

	public final BooleanProperty isFirstLeftProperty() {
		return isFirstLeft;
	}

	public final void setIsFirstLeft(boolean newValue) {
		isFirstLeft.set(newValue);
	}

	public final boolean getIsFirstLeft() {
		return isFirstLeft.get();
	}

	// электрические параметры реле

	protected double downV;
	protected double upV;
	protected double dangerV;

	// свойство реле под током
	public BooleanProperty isCurrent = new SimpleBooleanProperty();

	public final BooleanProperty isCurrentProperty() {
		return isCurrent;
	}

	public final void setIsCurrent(boolean newValue) {
		isCurrent.set(newValue);
	}

	public final boolean getIsCurrent() {
		return isCurrent.get();
	}

	// ------------------------------------------------

	public Rele(String name, String place, boolean isCur) {
		super(name, place);
		double centerY = 40;

		cir = new Circle(40, 40, 18, Color.WHITE);
		cir.setStroke(Color.BLACK);

		txName.setX(40 - txName.getBoundsInLocal().getWidth() / 2);
		txName.setY(centerY - 26);

		txType = new Text("");

		txPlace.setX(40 - txPlace.getBoundsInLocal().getWidth() / 2);
		txPlace.setY(centerY - 41);
		setIsCurrent(isCur);
		setIsFirstLeft(true);

		isFirstLeftProperty().addListener((property, oldValue, newValue) -> {
			changeContact(newValue.booleanValue());
		});

	}

	public Rele() {
		super();
		setIsFirstLeft(true);
		setIsCurrent(false);

		isFirstLeftProperty().addListener((property, oldValue, newValue) -> {
			changeContact(newValue.booleanValue());
		});

	}

	public Rele(boolean isCur) {
		super();
		setIsCurrent(isCur);
		setIsFirstLeft(true);
		isFirstLeftProperty().addListener((property, oldValue, newValue) -> {
			changeContact(newValue.booleanValue());
		});

	}

	@Override
	public void setCurrent(boolean flag) {
		isCurrent.set(flag);
	}

	public Text getTxV1() {
		return txV1;
	}

	public Text getTxV2() {
		return txV2;
	}

	public Text getTxV3() {
		return txV3;
	}

	public Text getTxV4() {
		return txV4;
	}

	@Override
	public void setTok(double i) {
		setProvodI(i);
	}

	@Override
	public String toString() {
		return "Реле -" + txName.getText() + ";  тип - " + txType.getText()
				+ ";  реле " + ((getIsCurrent()) ? "под током;" : "без тока;");

	}

	public List<User> getObmotki() {
		return obmotki;
	}

	public abstract void changeContact(boolean firstLeft);

	@Override
	public void setupError(boolean flag, int mod) {
		switch (mod) {
		case 1: {
			setIsConductance(!flag);
			// resError = (flag) ? obryv : 0;
			// setResistance(resError);
		}
			break;

		default:
			break;
		}
	}
}
