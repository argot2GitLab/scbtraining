﻿package com.alex.training.schemElement;

import java.util.List;

import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class ReleDSH2 extends Rele {

	public ReleDSH2() {
		super();
	}

	public ReleDSH2(boolean isCur) {
		super();
		setIsCurrent(isCur);
	}

	public ReleDSH2(String name, String place, boolean isCur) {
		super(name, place, isCur);

		double centerY = 40;

		l1 = new Line(0, centerY - 10, 23, centerY - 10);
		l2 = new Line(57, centerY - 10, 80, centerY - 10);
		l3 = new Line(0, centerY + 10, 23, centerY + 10);
		l4 = new Line(57, centerY + 10, 80, centerY + 10);

		Line lCenter = new Line(22, centerY, 58, centerY);
		Text galka = new Text("~");
		Text galka2 = new Text("~");

		galka.setFont(Font.font("sans", 32));
		galka.setX(40 - galka.getBoundsInLocal().getWidth() / 2);
		galka.setY(centerY);
		galka2.setFont(Font.font("sans", 32));
		galka2.setX(40 - galka.getBoundsInLocal().getWidth() / 2);
		galka2.setY(centerY + 18);

		txV1 = new Text("1");
		txV1.setX(12);
		txV1.setY(centerY - 12);

		txV2 = new Text("2");
		txV2.setX(60);
		txV2.setY(centerY - 12);

		txV3 = new Text("3");
		txV3.setX(12);
		txV3.setY(centerY + 22);

		txV4 = new Text("4");
		txV4.setX(60);
		txV4.setY(centerY + 22);
		this.getChildren().addAll(cir, txName, txPlace, l1, l2, l3, l4, txV1,
				txV2, txV3, txV4, lCenter, galka, galka2);
	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
	}

	@Override
	public void setCurrent(boolean flag) {
		setIsCurrent(flag);
	}

	@Override
	public void setTok(double i) {
		// TODO Auto-generated method stub

	}

	@Override
	public void changeContact(boolean firstLeft) {
		if (firstLeft) {
			txV1.setText("1");
			txV2.setText("4");
		} else {
			txV1.setText("4");
			txV2.setText("1");

		}
	}

}
