﻿package com.alex.training.schemElement;

import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polyline;

public class Clamp extends Group {

	// свойство для направления отсчета от клеммы при подключении в цепь
	// ------------------------------------------------
/*	public BooleanProperty isFirst = new SimpleBooleanProperty();

	public final BooleanProperty isFirstProperty() {
		return isFirst;
	}

	public final void setIsFirst(boolean newValue) {
		isFirst.set(newValue);
	}

	public final boolean getIsFirst() {
		return isFirst.get();
	}
*/
	private Polyline corp;

	public Clamp(boolean isPlus) {
		super();

		Double[] points = new Double[] { 0., 0., 27., 33., 33., 27., 0., 0. };
		corp = new Polyline();
		corp.getPoints().addAll(points);
		corp.setFill((isPlus) ? Color.RED : Color.BLACK);

		getChildren().addAll(corp);
	}

}
