﻿package com.alex.training.schemElement;

import javafx.scene.paint.Color;
import javafx.scene.shape.Polyline;
import javafx.scene.text.Text;

public class TylContact extends Contact {

	public TylContact(Rele rel, boolean isZam) {
		super(rel);
		setIsZam(isZam);
		setResistance((getIsZam()) ? obryv : resError);
		// установка сопротивления при замыкании контакта
		isZamProperty().addListener(
				(property, oldValue, newValue) -> setResByZam(newValue
						.booleanValue()));
	}

	public TylContact(String name, String place, Rele rel, int trNum,
			boolean isZam) {
		super(name, place, rel, trNum, isZam);

		// тыловой контакт
		tyl = new Polyline();
		points = new Double[] { 50., y + 20., 50., y + 40, 80., y + 40 };
		tyl.getPoints().addAll(points);

		tNum = new Text(String.valueOf(getContNum() + 3));
		tNum.setX(60);
		tNum.setY(y + 35);

		// перемычка для шунтирования контакта
		points = new Double[] { 0., 40., 0., 80., 80., 80. };
		jumper.getPoints().addAll(points);
		jumper.setStroke(Color.CRIMSON);
		jumper.setStrokeWidth(5);

		setResistance((getIsZam()) ? obryv : resError);
		isZamProperty().addListener((property, oldValue, newValue) -> {
			if (!getIsJumperSet())
				isConductance.set(!newValue.booleanValue());
			else
				isConductance.set(true);
			setResByZam(newValue.booleanValue());
		});

		// установка сопротивления при изменении ошибки в контакте
		isErrorProperty().addListener((property, oldValue, newValue) -> {
			setupError(newValue.booleanValue(), getErrorMod());
		});

		// ДЛЯ РЕДАКТОРА--------------------------------------------
		contNumProperty().addListener((property, oldValue, newValue) -> {
			gNum.setText(String.valueOf(newValue.intValue() + 1));
			tNum.setText(String.valueOf(newValue.intValue() + 3));
		});

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren()
				.addAll(general, tyl, contact, txName, txPlace, tNum, gNum);
	}

	@Override
	public void setResByZam(boolean f) {
		double res;
		if (!getIsJumperSet())
			res = (f) ? obryv : resError;
		else
			res = 0;
		setResistance(res);
	}

	@Override
	public void setupError(boolean flag, int mod) {
		switch (mod) {
		case 0: {
			resError = (flag) ? (getIsJumperSet()) ? 0 : 2000 : 0;
			if (!getIsZam())
				setResistance(resError);
		}
			break;
		case 1: {
			resError = (flag) ? obryv : 0;
			if (!getIsZam())
				setResistance(resError);
		}
			break;

		default:
			break;
		}
	}

}
