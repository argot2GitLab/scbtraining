﻿package com.alex.training.schemElement;

import java.util.List;

import javafx.animation.AnimationTimer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class ReleNMSHM1_560 extends Rele {

	private Arc zamedlenie;
	private long count;

	private final AnimationTimer atZamedl = new AnimationTimer() {

		@Override
		public void handle(long now) {
			if (count == 12) {
				setIsCurrent(false);
				stop();
			}

			count++;

		}

	};

	public ReleNMSHM1_560() {
		super();
	}

	public ReleNMSHM1_560(boolean isCur) {
		super();
		setIsCurrent(isCur);
	}

	public ReleNMSHM1_560(String name, String place, boolean isCur) {
		super(name, place, isCur);

		double centerY = 40;

		zamedlenie = new Arc(40, 48, 16, 10, 180, 180);
		zamedlenie.setFill(Color.BLACK);

		l1 = new Line(0, centerY, 22, centerY);
		l2 = new Line(58, centerY, 80, centerY);

		txV1 = new Text("1");
		txV1.setX(12);
		txV1.setY(centerY + 15);

		txV2 = new Text("3");
		txV2.setX(60);
		txV2.setY(centerY + 15);

		// установка электрических параметров реле
		downV = 5.7;
		upV = 19;
		dangerV = 45;
		setResistance(1000);

		txType.setText("НМШМ1-560");

		this.getChildren().addAll(cir, txName, txPlace, l1, l2, txV1, txV2,
				zamedlenie);
	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
	}

	@Override
	public void setCurrent(boolean flag) {

		double v = getProvodI() * getResistance();
		// System.out.println(" сопротивление реле - " + getResistance()
		// + " ток -" + getProvodI());
		if (flag) {
			if (v >= upV && v <= dangerV)
				setIsCurrent(flag);
		} else {
			if (v <= downV && getIsCurrent())
				startZamedlenie();
		}

	}

	public void startZamedlenie() {
		count = 0;
		atZamedl.start();
	}

	@Override
	public void changeContact(boolean firstLeft) {
		if (firstLeft) {
			txV1.setText("1");
			txV2.setText("3");
		} else {
			txV1.setText("3");
			txV2.setText("1");

		}
	}

}
