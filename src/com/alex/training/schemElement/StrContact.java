﻿package com.alex.training.schemElement;

import com.alex.training.schemElement.blocks.StrMotor;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class StrContact extends SchemElement {

	private Circle c1, c2;
	private Line cont;
	private Text txNum1, txNum2;

	/**
	 * свойство замкнутости контакта
	 * ------------------------------------------------------------
	 */
	public BooleanProperty isZam = new SimpleBooleanProperty();

	public final BooleanProperty isZamProperty() {
		return isZam;
	}

	public final void setIsZam(boolean newValue) {
		isZam.set(newValue);
	}

	public final boolean getIsZam() {
		return isZam.get();
	}

	// Свойство рабочий контакт или контрольный
	public BooleanProperty isWork = new SimpleBooleanProperty();

	public final BooleanProperty isWorkProperty() {
		return isWork;
	}

	public final void setIsWork(boolean newValue) {
		isWork.set(newValue);
	}

	public final boolean getIsWork() {
		return isWork.get();
	}

	// Свойство направления контакт замкнут когда стрелка в минусе
	public BooleanProperty isMinus = new SimpleBooleanProperty();

	public final BooleanProperty isMinusProperty() {
		return isMinus;
	}

	public final void setIsMinus(boolean newValue) {
		isMinus.set(newValue);
	}

	public final boolean getIsMinus() {
		return isMinus.get();
	}

	public BooleanProperty isTop = new SimpleBooleanProperty();

	public final BooleanProperty isTopProperty() {
		return isTop;
	}

	public final void setIsTop(boolean newValue) {
		isTop.set(newValue);
	}

	public final boolean getIsTop() {
		return isTop.get();
	}

	public IntegerProperty numCont = new SimpleIntegerProperty();

	public final IntegerProperty numContProperty() {
		return numCont;
	}

	public final void setNumCont(int newValue) {
		numCont.set(newValue);
	}

	public final int getNumCont() {
		return numCont.get();
	}

	private StrMotor motor;

	public StrContact(boolean isZam, boolean isTop, int num, StrMotor mot) {
		setIsZam(isZam);
		setNumCont(num);
		setIsTop(isTop);
		setResByZam(isZam);

		this.motor = mot;

		c1 = new Circle(5, 20, 3, Color.TRANSPARENT);
		c1.setStroke(Color.BLACK);

		c2 = new Circle(45, 20, 3, Color.TRANSPARENT);
		c2.setStroke(Color.BLACK);

		cont = new Line();
		cont.setStartX(5);
		cont.setStartY((isTop) ? 15 : 25);
		cont.setEndX(45);
		cont.setEndY((isTop) ? 15 : 25);
		cont.setVisible(isZam);
		cont.setStrokeWidth(3);

		txNum1 = new Text();
		txNum2 = new Text();

		txNum1.setText(String.valueOf(num));
		txNum1.setLayoutX(-3);
		txNum1.setLayoutY((isTop) ? 40 : 5);

		txNum2.setText(String.valueOf(++num));
		txNum2.setLayoutX(45);
		txNum2.setLayoutY((isTop) ? 40 : 5);

		// установка сопротивления при замыкании контакта
		isZamProperty().addListener((property, oldValue, newValue) -> {
			setResByZam(newValue.booleanValue());
			cont.setVisible(newValue.booleanValue());
		});
		isTopProperty().addListener((property, oldValue, newValue) -> {
			cont.setStartY((newValue.booleanValue()) ? 15 : 25);
			cont.setEndY((newValue.booleanValue()) ? 15 : 25);
		});

		numContProperty().addListener((property, oldValue, newValue) -> {
			txNum1.setText(String.valueOf(newValue.intValue()));
			txNum2.setText(String.valueOf(newValue.intValue() + 1));

		});

		// Установка замыкания контактов при работе двигателя--------

		// двигатель работает
		getMotor().isWorkProperty().addListener(
				(property, oldValue, newValue) -> {

				});
		// --------------------------------------------------
		// стрелка замкнута в плюсе
		getMotor().isPlusZamProperty().addListener(
				(property, oldValue, newValue) -> {
					if (newValue.booleanValue()) {
						if (getIsWork()) {
							if (getIsMinus()) {
								setIsZam(false);
							} else {
								setIsZam(true);
							}
						} else {
							if (!getIsMinus()) {
								setIsZam(true);
							}
						}

					} else {
						if (!getIsWork()) {
							if (!getIsMinus()) {
								setIsZam(false);
							}
						}
					}
				});
		// -------------------------------------------------------
		// стрелка замкнута в минусе
		getMotor().isMinusZamProperty().addListener(
				(property, oldValue, newValue) -> {
					if (newValue.booleanValue()) {
						if (getIsWork()) {
							if (!getIsMinus()) {
								setIsZam(false);
							} else {
								setIsZam(true);
							}
						} else {
							if (getIsMinus()) {
								setIsZam(true);
							}
						}

					} else {
						if (!getIsWork()) {
							if (getIsMinus()) {
								setIsZam(false);
							}
						}
					}
				});
		// --------------------------------------------------

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	protected void setResByZam(boolean f) {
		double res = (f) ? 0 : obryv;
		setResistance(res);
	}

	public StrMotor getMotor() {
		return motor;
	}

	public void setMotor(StrMotor motor) {
		this.motor = motor;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(c1, c2, cont, txNum1, txNum2);
	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
