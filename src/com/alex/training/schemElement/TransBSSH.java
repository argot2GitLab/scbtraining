﻿package com.alex.training.schemElement;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class TransBSSH extends Transformer {

	protected Circle circle;
	protected Line l5;
	protected Text txSecond3;

	public TransBSSH(String name, String place) {
		super(name, place);
		setIsConst(false);
		term1.setVarName("вывод 12");
		term2.setVarName("вывод 8");
		terminals.add(0, term1);
		terminals.add(1, term2);
		setEds(26.0);
		setResistance(500);

		// Вывод доп обмотки
		circle = new Circle(59, 80, 3, Color.WHITE);
		circle.setStroke(Color.BLACK);

		l5 = new Line(62, 80, 82, 80);

		txSecond3 = new Text(65, 75, "10");
		txSecond2.setText("8");
		txSecond1.setText("12");

		txFirst1.setText("2");
		txFirst2.setText("1");

	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		this.getChildren().addAll(serd, l1, l2, l3, l4, txName, txPlace,
				txFirst1, txFirst2, txSecond1, txSecond2, circle, l5,
				txSecond3, startRect, startRect1);
	}

	@Override
	public String toString() {
		return getVarName();
	}

	@Override
	public void setCurrent(boolean flag) {
		if (flag)
			term1.setResistance(0);
		else
			term1.setResistance(obryv);
	}

	@Override
	public void setTok(double i) {
		setProvodI(i);
	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
