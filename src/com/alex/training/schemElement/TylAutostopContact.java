﻿package com.alex.training.schemElement;

import javafx.scene.shape.Polyline;
import javafx.scene.text.Text;

public class TylAutostopContact extends RightContact {

	public TylAutostopContact(String name, Rele rel, int trNum, boolean isZam) {
		super(name, "автостоп", rel, trNum, isZam);

		// тыловой контакт
		tyl = new Polyline();
		points = new Double[] { 0., y + 40., 25., y + 40, 25., y + 20 };
		tyl.getPoints().addAll(points);

		tNum = new Text(String.valueOf((trNum > 5) ? 15 : trNum + 1));
		tNum.setX(1);
		tNum.setY(y + 35);
		gNum.setText(String.valueOf((trNum > 5) ? 16 : trNum));

		setResistance((getIsZam()) ? obryv : resError);
		isZamProperty().addListener((property, oldValue, newValue) -> {
			isConductance.set(!newValue.booleanValue());
			setResByZam(newValue.booleanValue());
		});


		// ДЛЯ РЕДАКТОРА--------------------------------------------
		contNumProperty().addListener(
				(property, oldValue, newValue) -> {
					gNum.setText(String.valueOf((newValue.intValue() > 50) ? 16
							: newValue.intValue() / 10));
					tNum.setText(String.valueOf((newValue.intValue() > 50) ? 15
							: newValue.intValue() / 10 + 1));
				});

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren()
				.addAll(general, tyl, contact, txName, txPlace, tNum, gNum);
	}

	@Override
	public void setResByZam(boolean f) {
		double res = (f) ? obryv : resError;
		setResistance(res);
	}

	@Override
	public void setupError(boolean flag, int mod) {
		switch (mod) {
		case 0: {
			resError = (flag) ? 2000 : 0;
			if (!getIsZam())
				setResistance(resError);
		}
			break;
		case 1: {
			resError = (flag) ? obryv : 0;
			if (!getIsZam())
				setResistance(resError);
		}
			break;

		default:
			break;
		}
	}

	// @Override
	// public void setupJumper(boolean flag) {
	// // TODO Auto-generated method stub
	//
	// }

}
