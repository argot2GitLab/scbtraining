﻿/**
 * 
 */
package com.alex.training.schemElement;

import javafx.scene.shape.Polyline;
import javafx.scene.text.Text;

/**
 * @author alex
 *
 */
public class FrontAutostopContact extends RightContact {

	/**
	 * @param name
	 *            название и место реле
	 */
	public FrontAutostopContact(String name, Rele rel, int trNum, boolean isZam) {
		super(name, "автостоп", rel, trNum, isZam);

		// фронтовой контакт
		front = new Polyline();
		points = new Double[] { 0., y, 25., y, 25., y + 10 };
		front.getPoints().addAll(points);

		fNum = new Text(String.valueOf(trNum + 1));
		fNum.setX(1);
		fNum.setY(y + 15);

		gNum.setText(String.valueOf(trNum));

		// ДЛЯ РЕДАКТОРА--------------------------------------------
		contNumProperty().addListener((property, oldValue, newValue) -> {
			gNum.setText(String.valueOf(newValue.intValue() / 10));
			fNum.setText(String.valueOf(newValue.intValue() / 10 + 1));
		});

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(general, front, contact, txName, txPlace, gNum,
				fNum);

	}

	@Override
	public void setupJumper(boolean flag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setupError(boolean flag, int mod) {
		switch (mod) {
		case 0: {
			resError = (flag) ? 2000 : 0;
			if (getIsZam())
				setResistance(resError);
		}
			break;
		case 1: {
			resError = (flag) ? obryv : 0;
			if (getIsZam())
				setResistance(resError);
		}
			break;

		default:
			break;
		}
	}

}
