﻿package com.alex.training.schemElement;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class RelePMPUSH extends Rele {

	private Obmotka obm13, obm42;

	public RelePMPUSH(String name, String place, boolean isCur) {
		super(name, place, isCur);

		double centerY = 40;

		obm13 = new Obmotka(150.);
		obm13.setIndexObm(0);
		obm42 = new Obmotka(150.);
		obm42.setIndexObm(1);

		obmotki.add(0, obm13);
		obmotki.add(1, obm42);

		l1 = new Line(0, centerY - 10, 23, centerY - 10);
		l2 = new Line(57, centerY - 10, 80, centerY - 10);
		l3 = new Line(0, centerY + 10, 23, centerY + 10);
		l4 = new Line(57, centerY + 10, 80, centerY + 10);

		txV1 = new Text("1");
		txV1.setX(12);
		txV1.setY(centerY - 12);

		txV2 = new Text("3");
		txV2.setX(60);
		txV2.setY(centerY - 12);

		txV3 = new Text("4");
		txV3.setX(12);
		txV3.setY(centerY + 22);

		txV4 = new Text("2");
		txV4.setX(60);
		txV4.setY(centerY + 22);
		this.getChildren().addAll(cir, txName, txPlace, l1, l2, l3, l4, txV1,
				txV2, txV3, txV4);

		// минимальное напряжение переброса якоря
		downV = 10;
		dangerV = 36;

		obm13.isCurrentProperty().addListener(
				(property, oldValue, newValue) -> {
					setCurrent(newValue.booleanValue());
				});

		obm42.isCurrentProperty().addListener(
				(property, oldValue, newValue) -> {
					setCurrent(newValue.booleanValue());
				});
	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);

	}

	@Override
	public void setCurrent(boolean flag) {

		double v;
		// стрелка в плюсе замкнуты контакты 11-12
		if (!obm13.getIsCurrent() && obm42.getIsCurrent()) {
			v = obm42.getProvodI() * obm42.getResistance();
			if (v >= downV && v < dangerV)
				setIsCurrent(true);
		}
		// стрелка в минусе замкнуты контакты 11-13
		if (obm13.getIsCurrent() && !obm42.getIsCurrent()) {
			v = obm13.getProvodI() * obm13.getResistance();
			if (v >= downV && v < dangerV)
				setIsCurrent(false);
		}
	}

	@Override
	public void setTok(double i) {
	}

	@Override
	public void changeContact(boolean firstLeft) {
		// TODO Auto-generated method stub
	}

	public Obmotka getObm13() {
		return obm13;
	}

	public Obmotka getObm42() {
		return obm42;
	}

}
