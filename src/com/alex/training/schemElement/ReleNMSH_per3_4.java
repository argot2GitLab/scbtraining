﻿package com.alex.training.schemElement;

import java.util.List;

import javafx.animation.AnimationTimer;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.text.Text;

public class ReleNMSH_per3_4 extends Rele {

	private Polyline l4, l2;

	public ReleNMSH_per3_4() {
		super();
	}

	public ReleNMSH_per3_4(boolean isCur) {
		super();
		setIsCurrent(isCur);
	}

	public ReleNMSH_per3_4(String name, String place, boolean isCur) {
		super(name, place, isCur);

		double centerY = 40;

		l3 = new Line(35, 2, 35, 22);
		l1 = new Line(35, 58, 35, 78);

		l4 = new Polyline(35, 17, 45, 17, 45, 22);
		l2 = new Polyline(35, 63, 45, 63, 45, 58);

		txV1 = new Text("3");
		txV1.setX(20);
		txV1.setY(20);

		txV2 = new Text("4");
		txV2.setX(50);
		txV2.setY(20);

		txV3 = new Text("1");
		txV3.setX(20);
		txV3.setY(70);

		txV4 = new Text("2");
		txV4.setX(50);
		txV4.setY(70);

		txPlace.setX(10 - txPlace.getBoundsInLocal().getWidth());
		txName.setX(txPlace.getX() + txPlace.getBoundsInLocal().getWidth() / 2
				- txName.getBoundsInLocal().getWidth() / 2);
		txName.setY(centerY + 20);
		txPlace.setY(centerY);

		// установка электрических параметров реле
		downV = 5.3;
		upV = 8;
		dangerV = 45;
		setResistance(720);

		txType.setText("НМШ1-1440");

		this.getChildren().addAll(cir, txName, txPlace, l1, l3, l4, l2, txV1,
				txV2, txV3, txV4);
	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
	}

	@Override
	public void setCurrent(boolean flag) {
		// if (!flag)
		// super.setCurrent(flag);
		// isCurrent.set(flag);

		double v = getProvodI() * getResistance();
		// System.out.println(" сопротивление реле - " + getResistance()
		// + " ток -" + getProvodI());
		if (flag) {
			if (v >= upV && v <= dangerV)
				setIsCurrent(flag);
			else
				setIsCurrent(false);
		} else {
			if (v <= downV)
				setIsCurrent(flag);
			else
				setIsCurrent(true);
		}

	}

	@Override
	public void changeContact(boolean firstLeft) {
		if (firstLeft) {
			txV1.setText("1");
			txV2.setText("4");
		} else {
			txV1.setText("4");
			txV2.setText("1");

		}
	}

}
