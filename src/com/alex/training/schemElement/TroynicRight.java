﻿package com.alex.training.schemElement;

import javafx.scene.paint.Color;
import javafx.scene.shape.Polyline;
import javafx.scene.text.Text;

public class TroynicRight extends RightContact implements TroynicInterface {

	private TylContact tylTr;
	private Polyline jumperTyl;

	public TroynicRight(String name, String place, Rele rel, int trNum,
			boolean isZam) {
		super(name, place, rel, trNum, isZam);

		// фронтовой контакт
		front = new Polyline();
		points = new Double[] { 0., y, 25., y, 25., y + 10 };
		front.getPoints().addAll(points);

		fNum = new Text(String.valueOf(getContNum() + 2));
		fNum.setX(1);
		fNum.setY(y + 15);

		// тыловой контакт
		tyl = new Polyline();
		points = new Double[] { 0., y + 40., 25., y + 40, 25., y + 20 };
		tyl.getPoints().addAll(points);

		tNum = new Text(String.valueOf(getContNum() + 3));
		tNum.setX(1);
		tNum.setY(y + 35);

		// перемычка для шунтирования контакта
		points = new Double[] { 0., 40., 0., 20., 80., 20., 80., 40. };
		jumper.getPoints().addAll(points);
		jumper.setStroke(Color.CRIMSON);
		jumper.setStrokeWidth(5);

		// перемычка для шунтирования тылового контакта тройника
		points = new Double[] { 0., 80., 80., 80., 80., 40. };
		jumperTyl = new Polyline();
		jumperTyl.getPoints().addAll(points);
		jumperTyl.setStroke(Color.CRIMSON);
		jumperTyl.setStrokeWidth(5);


		// ДЛЯ РЕДАКТОРА--------------------------------------------
		contNumProperty().addListener((property, oldValue, newValue) -> {
			gNum.setText(String.valueOf(newValue.intValue() + 1));
			fNum.setText(String.valueOf(newValue.intValue() + 2));
			tNum.setText(String.valueOf(newValue.intValue() + 3));
		});

	}

	public TylContact getTylTr() {
		return tylTr;
	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(general, front, tyl, contact, txName, txPlace,
				gNum, fNum, tNum);
	}

	@Override
	public TylContact getAsTyl(boolean isZam) {
		if (tylTr == null)
			tylTr = new TylContact(getRele(), isZam);
		tylTr.getTxName().setText(txName.getText());
		tylTr.getTxPlace().setText(txPlace.getText());
		tylTr.setContNum(getContNum());
		return tylTr;
	}

	@Override
	public void setTylTr(TylContact tylTr) {
		this.tylTr = tylTr;
	}

	public void setupJumperTyl(boolean flag) {
		if (flag) {
			tylTr.setResistance(0);
			getChildren().addAll(jumperTyl);
			if (tylTr.getIsError())
				tylTr.setIsError(false);
		} else {
			tylTr.setResByZam(tylTr.getIsZam());
			getChildren().remove(jumperTyl);
		}
		tylTr.setIsJumperSet(flag);
	}

	@Override
	public void setupError(boolean flag, int mod) {
		switch (mod) {
		case 0: {
			resError = (flag) ? 2000 : 0;
			if (getIsZam())
				setResistance(resError);
		}
			break;
		case 1: {
			resError = (flag) ? obryv : 0;
			if (getIsZam())
				setResistance(resError);
		}
			break;

		default:
			break;
		}
	}
}
