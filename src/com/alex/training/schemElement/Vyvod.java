﻿package com.alex.training.schemElement;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;

public class Vyvod extends SchemElement {

	private Circle cir;

	// сторона отображения номера
	public BooleanProperty isLeft = new SimpleBooleanProperty();

	public final BooleanProperty isLeftProperty() {
		return isLeft;
	}

	public final void setIsLeft(boolean newValue) {
		isLeft.set(newValue);
	}

	public final boolean getIsLeft() {
		return isLeft.get();
	}

	public Vyvod(String name, String place, boolean isLeft) {
		super(name, place);
		setIsLeft(isLeft);

		cir = new Circle(20, 20, 3, Color.WHITE);
		cir.setStroke(Color.BLACK);

		txName.setX((getIsLeft()) ? 5 : 25);
		txName.setY(15);
		txName.setFont(Font.font(12));

		isLeftProperty().addListener((property, oldValue, newValue) -> {
			txName.setX((newValue.booleanValue()) ? 5 : 25);
		});

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(cir, txName);
	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
