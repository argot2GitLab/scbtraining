﻿package com.alex.training.schemElement;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.text.Text;

public abstract class Contact extends SchemElement {

	/**
	 * свойство замкнутости контакта
	 * ------------------------------------------------------------
	 */
	public BooleanProperty isZam = new SimpleBooleanProperty();

	public final BooleanProperty isZamProperty() {
		return isZam;
	}

	public final void setIsZam(boolean newValue) {
		isZam.set(newValue);
	}

	public final boolean getIsZam() {
		return isZam.get();
	}

	/**
	 * свойство что установлена перемычка
	 * ------------------------------------------------------------
	 */
	public BooleanProperty isJumperSet = new SimpleBooleanProperty();

	public final BooleanProperty isJumperSetProperty() {
		return isJumperSet;
	}

	public final void setIsJumperSet(boolean newValue) {
		isJumperSet.set(newValue);
	}

	public final boolean getIsJumperSet() {
		return isJumperSet.get();
	}

	// номер группы контактов
	// ----------------------------------------------------
	public IntegerProperty contNum = new SimpleIntegerProperty();

	public final IntegerProperty contNumProperty() {
		return contNum;
	}

	public final void setContNum(int newValue) {
		contNum.set(newValue);
	}

	public final int getContNum() {
		return contNum.get();
	}

	// --------------------------------------------------------------------
	// для графического отображения----------------------------------
	protected Polyline front;
	protected Polyline tyl;
	protected Polyline jumper;

	protected Polyline general;
	protected Line contact;
	protected Text gNum;
	protected Text fNum;
	protected Text tNum;
	protected Double[] points;
	protected final double endXZam = 55;
	protected final double endXo = 53;
	protected final double y = 40;
	protected double endYZam = y + 10;
	protected final double endYo = y + 22;
	protected final double resObr = 1000000;
	protected double resError;
	// --------------------------------------------------------------------

	// для работы электрических схем----------------------------------

	private Rele rele;

	public Contact(Rele rel) {
		super();
		// замыкание всех контактов при постановке реле под ток
		setRele(rel);
		setIsZam(false);
		// установка сопротивления при замыкании контакта
		isZamProperty().addListener(
				(property, oldValue, newValue) -> setResByZam(newValue
						.booleanValue()));
	}

	public Contact(String name, String place, Rele rel, int trNum, boolean isZam) {
		super(name, place);

		this.isZam.set(isZam);
		// установка сопротивления контакта в зависимости от замкнутости
		resError = 0;
		setIsJumperSet(false);

		setResistance((isZam) ? resError : resObr);

		// общий элемент контакта
		general = new Polyline();
		points = new Double[] { 0., y, 20., y, 20., y + 10 };
		general.getPoints().addAll(points);
		// сам перемещающийся контакт
		// contact = new Line(21, y + 10, 53, y + 22);
		contact = new Line();
		contact.setStartX(21);
		contact.setStartY(y + 10);
		contact.setEndX((this.isZam.get()) ? endXZam : endXo);
		contact.setEndY((this.isZam.get()) ? endYZam : endYo);
		contact.setStrokeWidth(2.5);

		this.isZam.addListener((property, oldValue, newValue) -> {
			contact.setEndX((newValue) ? endXZam : endXo);
			contact.setEndY((newValue) ? endYZam : endYo);
		});

		txName.setX(35 - txName.getBoundsInLocal().getWidth() / 2);
		txName.setY(y - 10);

		txPlace.setX(35 - txPlace.getBoundsInLocal().getWidth() / 2);
		txPlace.setY(y - 25);

		if (trNum < 9 && trNum > 0) {
			setContNum(trNum * 10);
		} else
			contNum.set(10);

		gNum = new Text(String.valueOf(getContNum() + 1));
		gNum.setX(3);
		gNum.setY(y + 15);

		// перемычка для шунтирования контакта
		jumper = new Polyline();

		// подсветка точек подключения тестера
		startRect.setX(-5);
		startRect.setY(y - 5);

		// замыкание всех контактов при постановке реле под ток
		setRele(rel);
		setIsZam(false);
		// установка сопротивления при замыкании контакта
		isZamProperty().addListener(
				(property, oldValue, newValue) -> setResByZam(newValue
						.booleanValue()));

	}

	public Rele getRele() {
		return rele;
	}

	public void setRele(Rele r) {
		this.rele = r;
		// замыкание всех контактов при постановке реле под ток
		rele.isCurrentProperty().addListener(
				(property, oldValue, newValue) -> setIsZam(newValue
						.booleanValue()));
	}

	public void drowElement(double xl, double yl) {
		// TODO Auto-generated method stub

	}

	public void setResByZam(boolean f) {
		double res = (f) ? resError : resObr;
		setResistance(res);
	}

	public void setupJumper(boolean flag) {
		setIsJumperSet(flag);
		if (flag) {
			// errorMod == 0 повышенное сопротивление контакта
			if (getErrorMod() != 1) {
				setResistance(0);
				if (getIsError())
					setIsError(false);
			}
			getChildren().addAll(jumper);

		} else {
			setResByZam(getIsZam());
			getChildren().remove(jumper);
		}
	}
}
