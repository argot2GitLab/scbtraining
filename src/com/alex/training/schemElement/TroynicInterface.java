﻿package com.alex.training.schemElement;

public interface TroynicInterface {

	public TylContact getAsTyl(boolean isZam);

	public TylContact getTylTr();

	public void setTylTr(TylContact tylTr);

	public void setupJumperTyl(boolean flag);

}