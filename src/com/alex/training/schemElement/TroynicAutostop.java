﻿package com.alex.training.schemElement;

import javafx.scene.shape.Polyline;
import javafx.scene.text.Text;

public class TroynicAutostop extends RightContact implements TroynicInterface {

	private TylContact tylTr;

	public TroynicAutostop(String name, Rele rel, int trNum, boolean isZam) {
		super(name, "автостоп", rel, trNum, isZam);

		// фронтовой контакт
		front = new Polyline();
		points = new Double[] { 0., y, 25., y, 25., y + 10 };
		front.getPoints().addAll(points);

		fNum = new Text(String.valueOf((trNum > 1) ? 14 : 11));
		fNum.setX(1);
		fNum.setY(y + 15);

		// тыловой контакт
		tyl = new Polyline();
		points = new Double[] { 0., y + 40., 25., y + 40, 25., y + 20 };
		tyl.getPoints().addAll(points);

		tNum = new Text(String.valueOf((trNum > 1) ? 12 : 9));
		tNum.setX(1);
		tNum.setY(y + 35);

		gNum.setText(String.valueOf((trNum > 1) ? 13 : 10));


		// ДЛЯ РЕДАКТОРА--------------------------------------------
		contNumProperty().addListener((property, oldValue, newValue) -> {
			gNum.setText(String.valueOf((newValue.intValue() > 10) ? 13 : 10));
			fNum.setText(String.valueOf((newValue.intValue() > 10) ? 14 : 11));
			tNum.setText(String.valueOf((newValue.intValue() > 10) ? 12 : 9));
		});

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(general, front, tyl, contact, txName, txPlace,
				gNum, fNum, tNum);
	}

	@Override
	public TylContact getAsTyl(boolean isZam) {
		if (tylTr == null)
			tylTr = new TylContact(getRele(), isZam);
		tylTr.getTxName().setText(txName.getText());
		tylTr.getTxPlace().setText(txPlace.getText());
		tylTr.setContNum(getContNum());
		return tylTr;
	}

	@Override
	public TylContact getTylTr() {
		return tylTr;
	}

	@Override
	public void setTylTr(TylContact tylTr) {
		this.tylTr = tylTr;
	}

	@Override
	public void setupJumperTyl(boolean flag) {
	}

	@Override
	public void setupError(boolean flag, int mod) {
		switch (mod) {
		case 0: {
			resError = (flag) ? 2000 : 0;
			if (getIsZam())
				setResistance(resError);
		}
			break;
		case 1: {
			resError = (flag) ? obryv : 0;
			if (getIsZam())
				setResistance(resError);
		}
			break;

		default:
			break;
		}
	}

}
