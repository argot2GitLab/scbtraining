﻿package com.alex.training.schemElement;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class StrKyrbelContact extends SchemElement {

	private Circle c1, c2;
	private Line cont;
	private Text txNum1, txNum2;

	/**
	 * свойство замкнутости контакта
	 * ------------------------------------------------------------
	 */
	public BooleanProperty isZam = new SimpleBooleanProperty();

	public final BooleanProperty isZamProperty() {
		return isZam;
	}

	public final void setIsZam(boolean newValue) {
		isZam.set(newValue);
	}

	public final boolean getIsZam() {
		return isZam.get();
	}

	public StrKyrbelContact(boolean isZam) {
		setIsZam(isZam);

		c1 = new Circle(20, 20, 5, Color.TRANSPARENT);
		c1.setStroke(Color.BLACK);

		c2 = new Circle(20, 60, 5, Color.TRANSPARENT);
		c2.setStroke(Color.BLACK);

		cont = new Line();
		cont.setStartY(55);
		cont.setStartX(20);
		cont.setEndY(20);
		cont.setEndX((isZam) ? 15 : 5);
		cont.setStrokeWidth(4);

		txNum1 = new Text();

		txNum1.setText("БК");
		txNum1.setLayoutX(30);
		txNum1.setLayoutY(35);

		// установка сопротивления при замыкании контакта
		isZamProperty().addListener((property, oldValue, newValue) -> {
			setResByZam(newValue.booleanValue());
			cont.setEndX((isZam) ? 15 : 5);
		});
	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	protected void setResByZam(boolean f) {
		double res = (f) ? 0 : 100000;
		setResistance(res);
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(c1, c2, cont, txNum1);
	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
