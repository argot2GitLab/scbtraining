﻿package com.alex.training.schemElement;

import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class ReleNMSH extends Rele {

	public ReleNMSH() {
		super();
	}

	public ReleNMSH(boolean isCur) {
		super();
		setIsCurrent(isCur);
	}

	public ReleNMSH(String name, String place, boolean isCur) {
		super(name, place, isCur);

		double centerY = 40;

		l1 = new Line(0, centerY, 22, centerY);
		l2 = new Line(58, centerY, 80, centerY);

		txV1 = new Text("1");
		txV1.setX(12);
		txV1.setY(centerY + 15);

		txV2 = new Text("4");
		txV2.setX(60);
		txV2.setY(centerY + 15);

		// подсветка точек подключения тестера
		startRect.setX(-5);
		startRect.setY(35);

		// установка электрических параметров реле
		downV = 8;
		upV = 14;
		dangerV = 45;
		setResistance(1440);

		txType.setText("НМШ1-1440");

		this.getChildren().addAll(cir, txName, txPlace, l1, l2, txV1, txV2);

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
	}

	@Override
	public void setCurrent(boolean flag) {
		double v = getProvodI() * getResistance();
		// System.out.println(" сопротивление реле - " + getResistance()
		// + " ток -" + getProvodI());
		if (flag) {
//			txName.setFill(Color.RED);
			if (v >= upV && v <= dangerV)
				setIsCurrent(flag);
			else
				setIsCurrent(false);
		} else {
			//			txName.setFill(Color.GREEN);
			if (v <= downV)
				setIsCurrent(flag);
			else
				setIsCurrent(true);
		}

	}

	@Override
	public void changeContact(boolean firstLeft) {
		if (firstLeft) {
			txV1.setText("1");
			txV2.setText("4");
		} else {
			txV1.setText("4");
			txV2.setText("1");

		}
	}

}
