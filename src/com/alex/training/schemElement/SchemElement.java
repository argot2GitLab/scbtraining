﻿package com.alex.training.schemElement;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import javax.xml.bind.annotation.XmlValue;

/**
 * @author Alex Абстрактный класс элемента схемы
 */
public abstract class SchemElement extends Group {

	// переменные для графического отображения

	protected Text txName;
	protected Text txPlace;
	protected double endX;
	protected double endY;
	protected boolean isDrowed;
	protected Paint curColor;
	protected Font curFont;
	protected final double obryv = 100000;
	protected long idElementInSchem;
	protected int errorMod;

	protected String varName;

	// выделение точек подключения тестера (может список?)
	protected Rectangle startRect, startRect1;

	// электрические параметры ------------------------------

	// ток в протекающий в проводнике
	// -------------------------------------------------------
	public DoubleProperty provodI = new SimpleDoubleProperty();

	public final DoubleProperty provodIProperty() {
		return provodI;
	}

	public final void setProvodI(double newValue) {
		provodI.set(newValue);
	}

	public final double getProvodI() {
		return provodI.get();
	}

	// // падение напряжения
	// public DoubleProperty deltaU = new SimpleDoubleProperty();
	// public final DoubleProperty deltaUProperty() {return deltaU;}
	// public final void setDeltaU(double newValue) {deltaU.set(newValue);}
	// public final double getDeltaU() {return deltaU.get();}

	// сопротивление----------------------------------------------------
	public DoubleProperty resistance = new SimpleDoubleProperty();

	public final DoubleProperty resistanceProperty() {
		return resistance;
	}

	public final void setResistance(double newValue) {
		resistance.set(newValue);
	}

	public final double getResistance() {
		return resistance.get();
	}

	// свойство установки ощибки элемента------------------------
	public BooleanProperty isError = new SimpleBooleanProperty();

	public final BooleanProperty isErrorProperty() {
		return isError;
	}

	public final void setIsError(boolean newValue) {
		isError.set(newValue);
	}

	public final boolean getIsError() {
		return isError.get();
	}

	// свойство целостности----------------------------------------------
	public BooleanProperty isConductance = new SimpleBooleanProperty();

	public final BooleanProperty isConductanceProperty() {
		return isConductance;
	}

	public final void setIsConductance(boolean newValue) {
		isConductance.set(newValue);
	}

	public final boolean getIsConductance() {
		return isConductance.get();
	}

	// подключение плюсовой клеммы тестера----------------------------------
	public BooleanProperty plusMultConnect = new SimpleBooleanProperty();

	public final BooleanProperty plusMultConnectProperty() {
		return plusMultConnect;
	}

	public final void setPlusMultConnect(boolean newValue) {
		plusMultConnect.set(newValue);
	}

	public final boolean getPlusMultConnect() {
		return plusMultConnect.get();
	}

	// подключение минусовой клеммы тестера----------------------------------
	public BooleanProperty minusMultConnect = new SimpleBooleanProperty();

	public final BooleanProperty minusMultConnectProperty() {
		return minusMultConnect;
	}

	public final void setMinusMultConnect(boolean newValue) {
		minusMultConnect.set(newValue);
	}

	public final boolean getMinusMultConnect() {
		return minusMultConnect.get();
	}

	public SchemElement() {
		txName = new Text("");
		txPlace = new Text("");
		setIsConductance(true);
		setResistance(0);
		setIsError(false);
		setMinusMultConnect(false);
		setPlusMultConnect(false);

		// точки подключения тестера 2 -если понадобится
		// может сделать список?
		startRect = new Rectangle(10, 10, Color.AQUAMARINE);
		startRect.setVisible(false);
		startRect1 = new Rectangle(10, 10, Color.AQUAMARINE);
		startRect1.setVisible(false);

		isDrowed = true;
		curColor = txName.getFill();

		resistanceProperty().addListener((property, oldValue, newValue) -> setCondByRes(newValue.doubleValue()));

		// установка сопротивления при изменении ошибки в контакте
		isErrorProperty().addListener((property, oldValue, newValue) -> {
			setupError(newValue.booleanValue(), getErrorMod());
		});

	}

	public SchemElement(String name, String place) {
		this();
		if (name != null)
			this.txName.setText(name);
		if (place != null)
			this.txPlace = new Text(place);
		idElementInSchem = -1;
		setErrorMod(-1);

		Font font = Font.font("sans", FontWeight.MEDIUM, 14);
		txName.setFill(Color.BLACK);
		txName.setFont(font);
		txPlace.setFill(Color.BLACK);
		txPlace.setFont(font);
	}

	private void setCondByRes(double res) {
		boolean f = (res >= obryv) ? false : true;
		setIsConductance(f);
	}

	@XmlValue
	public Text getTxName() {
		return txName;
	}

	@XmlValue
	public Text getTxPlace() {
		return txPlace;

	}

	public Rectangle getStartRect() {
		return startRect;
	}

	public Rectangle getStartRect1() {
		return startRect1;
	}

	public void setVisibleCheckPoint(boolean flag) {
		startRect.setVisible(flag);
		startRect1.setVisible(flag);
	}

	// для определения стыковки с другими элементами
	public abstract double getEndX();

	public abstract double getEndY();

	public abstract void setupError(boolean flag, int mod);

	public abstract void drowElement(double xl, double yl);

	/**
	 * @param flag
	 * @param mod  - режим подсветки - 0- нормальный режим 1- подсветка контура 2-
	 *             подсветка соединения 3- подсветка эл. цепи
	 */
	public void podsvetka(int mod) {
		Color color = Color.BLACK;

		switch (mod) {
		case 1:
			color = Color.BLUEVIOLET;
			break;
		case 2:
			color = Color.GREEN;
			break;
		case 3:
			color = Color.YELLOW;
			break;

		default:
			break;
		}

		txName.setFill(color);
	}

	public long getIdElementInSchem() {
		return idElementInSchem;
	}

	public void setIdElementInSchem(long idElementInSchem) {
		this.idElementInSchem = idElementInSchem;
	}

	public void shiftCurrent(boolean flag) {
		if (flag) {
			curColor = txName.getFill();
			curFont = txName.getFont();
			txName.setFill(Color.RED);
			txName.setFont(Font.font(24));
		} else {
			txName.setFill(curColor);
			txName.setFont(curFont);
		}
	}

	// подключена ли какая-нибудь клемма к элементу
	public boolean isAnyClampConnect() {
		return getMinusMultConnect() || getPlusMultConnect();
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	// public Font getFont() {
	// return font;
	// }

	// -----------------------------------------------------------

	public boolean isDrowed() {
		return isDrowed;
	}

	public void setDrowed(boolean isDrowed) {
		this.isDrowed = isDrowed;
	}

	public int getErrorMod() {
		return errorMod;
	}

	public void setErrorMod(int errorMod) {
		this.errorMod = errorMod;
	}

	public double getObryv() {
		return obryv;
	}

}
