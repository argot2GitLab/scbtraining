﻿package com.alex.training.schemElement;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class Tester extends SchemElement {

	private Rectangle tablo, device;
	private AnchorPane pane;
	private Text txTablo, txPlus, txMinus;
	private Circle cirP, cirM, cirP1, cirM1;
	private ToggleButton btnConst, btnExchange, btnV, btnA, btnM;
	private Double initX, initY;
	private Point2D dragAnchor;
	private double dragX;
	private double dragY;
	private double newXPosition;
	private double newYPosition;

	// свойства подключения тестера
	// ------------------------------------------------
	public BooleanProperty isConnect = new SimpleBooleanProperty();

	public final BooleanProperty isConnectProperty() {
		return isConnect;
	}

	public final void setIsConnect(boolean newValue) {
		isConnect.set(newValue);
	}

	public final boolean getIsConnect() {
		return isConnect.get();
	}

	public Tester() {
		super();

		setPlusMultConnect(false);
		setMinusMultConnect(false);
		setIsConnect(false);
		pane = new AnchorPane();
		device = new Rectangle(220, 300, Color.LIGHTGREY);
		device.setArcHeight(30);
		device.setArcWidth(30);
		device.setX(0);
		device.setX(0);
		device.setStroke(Color.BLACK);
		device.setCursor(Cursor.HAND);

		tablo = new Rectangle(160, 40, Color.WHITESMOKE);
		tablo.setX(30);
		tablo.setY(30);
		tablo.setStroke(Color.BLACK);

		txTablo = new Text("0");
		txTablo.setX(190 - 20);
		txTablo.setY(60);
		txTablo.setFill(Color.BLACK);
		txTablo.setFont(Font.font("sans", FontWeight.MEDIUM, 30));
		txTablo.textProperty().addListener(
				(property, oldValue, newValue) -> txTablo.setX(185 - txTablo
						.getBoundsInLocal().getWidth()));

		Font font = Font.font("sans", FontWeight.BOLD, 20);
		// кнопки переключения рода тока
		btnConst = new ToggleButton("=");
		btnConst.setFont(font);
		btnExchange = new ToggleButton("~");
		btnExchange.setFont(font);
		final ToggleGroup tgType = new ToggleGroup();
		btnConst.setToggleGroup(tgType);
		btnExchange.setToggleGroup(tgType);
		btnConst.setLayoutX(30);
		btnConst.setLayoutY(90);
		btnConst.setMaxSize(40, 20);
		btnExchange.setMaxSize(40, 20);
		btnExchange.setLayoutX(70);
		btnExchange.setLayoutY(90);

		// кнопки переключения измеряемых величин
		final ToggleGroup tgVal = new ToggleGroup();
		btnA = new ToggleButton("A");
		btnA.setFont(font);
		btnV = new ToggleButton("V");
		btnV.setFont(font);
		btnM = new ToggleButton("R");
		btnM.setFont(font);
		btnA.setToggleGroup(tgVal);
		btnV.setToggleGroup(tgVal);
		btnM.setToggleGroup(tgVal);
		btnA.setLayoutX(30);
		btnA.setLayoutY(170);
		btnA.setPrefSize(53, 20);
		btnV.setPrefSize(53, 20);
		btnV.setLayoutX(83);
		btnV.setLayoutY(170);
		btnM.setPrefSize(55, 20);
		btnM.setLayoutX(136);
		btnM.setLayoutY(170);

		// контакты клемм
		cirP = new Circle(40, 250, 10, Color.LIGHTGREY);
		cirM = new Circle(180, 250, 10, Color.LIGHTGREY);
		cirP1 = new Circle(40, 250, 5, Color.RED);
		cirM1 = new Circle(180, 250, 5, Color.BLACK);
		final Light.Distant lightDistant = new Light.Distant();
		final Lighting effect = new Lighting();
		effect.setLight(lightDistant);
		cirP.setEffect(effect);
		cirM.setEffect(effect);
		cirP.setCursor(Cursor.HAND);
		cirM.setCursor(Cursor.HAND);

		txPlus = new Text("+");
		txPlus.setX(55);
		txPlus.setY(260);
		txPlus.setFill(Color.BLACK);
		txPlus.setFont(Font.font("sans", FontWeight.MEDIUM, 30));
		txMinus = new Text("-");
		txMinus.setX(150);
		txMinus.setY(260);
		txMinus.setFill(Color.BLACK);
		txMinus.setFont(Font.font("sans", FontWeight.BOLD, 30));

		pane.getChildren().addAll(device, tablo, txTablo, btnConst,
				btnExchange, btnA, btnV, btnM, cirM, cirP, cirM1, cirP1,
				txPlus, txMinus);

		// обработка
		// событий-----------------------------------------------------
		device.setOnMousePressed((me) -> getCursorPosition(me));

		// перетскивание тестера
		setOnMouseDragged((me) -> {
			dragX = me.getSceneX() - dragAnchor.getX();
			dragY = me.getSceneY() - dragAnchor.getY();
			// calculate new position
			newXPosition = initX + dragX;
			newYPosition = initY + dragY;
			setTranslateX(newXPosition);
			setTranslateY(newYPosition);
		});

		setOnMouseReleased((me) -> {
			setLayoutX(getLayoutX() + newXPosition);
			setLayoutY(getLayoutY() + newYPosition);
			setTranslateX(0);
			setTranslateY(0);
			newXPosition = 0.;
			newYPosition = 0.;
		});
		// ---------------------------------------

		// привязка подключения каждой клеммы к подключенному состоянию тестера
		minusMultConnectProperty().addListener(
				(property, oldValue, newValue) -> {
					if (newValue.equals(getPlusMultConnect())
							|| newValue.equals(false))
						setIsConnect(newValue.booleanValue());
				});
		plusMultConnectProperty().addListener(
				(property, oldValue, newValue) -> {
					if (newValue.equals(getMinusMultConnect())
							|| newValue.equals(false))
						setIsConnect(newValue.booleanValue());
				});

		//
		// // привязка нажатия кнопки к измерению
		// btnV.selectedProperty().addListener((property, oldValue, newValue) ->
		// {
		// if (newValue)
		// showVolt(getCircuitForTester());
		// else
		// txTablo.setText("0.");
		//
		// });

	} // конструктор--------------------------------------------------------

	private void getCursorPosition(MouseEvent me) {
		// запись позиции когда нажата кнопка мыши
		initX = getTranslateX();
		initY = getTranslateY();
		dragAnchor = new Point2D(me.getSceneX(), me.getSceneY());
	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().add(pane);
	}

	/**
	 *  отображения на табло тестера
	 * 
	 * @param ec
	 */
	public void showCalculateValue(double value, boolean isConst) {
		if (btnV.isSelected() && (isConst) ? btnConst.isSelected()
				: btnExchange.isSelected()) {
			txTablo.setText(String.format("%.2f", value));
		}
	}

	public Text getTxTablo() {
		return txTablo;
	}

	public ToggleButton getBtnConst() {
		return btnConst;
	}

	public ToggleButton getBtnV() {
		return btnV;
	}

	public ToggleButton getBtnExchange() {
		return btnExchange;
	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
