﻿package com.alex.training.schemElement;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class TransSobs2Au3 extends Transformer {

	protected Circle circle;
	protected Line l5;
	protected Text txSecond3;
	protected PowerSourceTerminal term3;

	public TransSobs2Au3(String name, String place) {
		super(name, place);

		// тип трансформатора
		txType = new Text("СОБС2-АУЗ");
		Font font = Font.font("sans", FontWeight.MEDIUM, 14);

		txType.setFont(font);
		txType.setX(41 - txType.getBoundsInLocal().getWidth() / 2);
		txType.setY(centerY + 80);

		// Вывод доп обмотки
		circle = new Circle(59, 80, 3, Color.WHITE);
		circle.setStroke(Color.BLACK);

		l5 = new Line(62, 80, 82, 80);

		txSecond3 = new Text(65, 75, "V1");
		txSecond2.setText("V2");
		txSecond1.setText("II1");

		txFirst1.setText("I 1,2");
		txFirst2.setText("I 3,4");
		term3 = new PowerSourceTerminal("", "");
		
		term1.setVarName("вывод II1");
		term2.setVarName("вывод V1");
		term3.setVarName("вывод V2");
		terminals.add(0, term1);
		terminals.add(1, term2);
		terminals.add(2, term3);
		setEds(13.0);


	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		this.getChildren().addAll(serd, l1, l2, l3, l4, txName, txPlace,
				txFirst1, txFirst2, txSecond1, txSecond2, txType, circle, l5,
				txSecond3, startRect, startRect1);
	}

	public PowerSourceTerminal getTerm3() {
		return term3;
	}

	@Override
	public void setTok(double i) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setCurrent(boolean flag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
