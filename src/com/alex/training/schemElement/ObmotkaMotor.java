﻿package com.alex.training.schemElement;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

import com.alex.training.schem.User;

public class ObmotkaMotor extends SchemElement implements User {

	// свойство под током
	public BooleanProperty isCurrent = new SimpleBooleanProperty();

	public final BooleanProperty isCurrentProperty() {
		return isCurrent;
	}

	public final void setIsCurrent(boolean newValue) {
		isCurrent.set(newValue);
	}

	public final boolean getIsCurrent() {
		return isCurrent.get();
	}

	// индекс мотора в списки элементов схемы
	private long indexMotor;
	// индекс обмотки двигателя
	private int indexObmotka;

	public ObmotkaMotor(Double res, int index) {
		super();
		setDrowed(false);
		setResistance(res);
		setIndexObmotka(index);
	}

	@Override
	public void setCurrent(boolean flag) {
		setIsCurrent(flag);
	}

	@Override
	public void setTok(double i) {
		// TODO Auto-generated method stub

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		// TODO Auto-generated method stub

	}

	public long getIndexMotor() {
		return indexMotor;
	}

	public void setIndexMotor(long l) {
		this.indexMotor = l;
	}

	public int getIndexObmotka() {
		return indexObmotka;
	}

	public void setIndexObmotka(int indexObmotka) {
		this.indexObmotka = indexObmotka;
	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
