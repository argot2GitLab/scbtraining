﻿package com.alex.training.schemElement;

import java.util.ArrayList;
import java.util.List;

import javafx.animation.AnimationTimer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.shape.Line;
import javafx.scene.transform.Rotate;

import com.alex.training.schem.Source;
import com.alex.training.schem.User;

public class Conder extends SchemElement implements Source, User {

	private Line l1, l2, l3, l4;
	protected List<PowerSourceTerminal> terminals = new ArrayList<PowerSourceTerminal>();
	protected List<PowerSourceTerminal> minusTerminals = new ArrayList<PowerSourceTerminal>();
	private boolean isZarad;
	private long count;
	private long razrad;

	// Свойство горизонтальности расположения
	public BooleanProperty isHorizont = new SimpleBooleanProperty();

	public final BooleanProperty isHorizontProperty() {
		return isHorizont;
	}

	public final void setIsHorizont(boolean newValue) {
		isHorizont.set(newValue);
	}

	public final boolean getIsHorizont() {
		return isHorizont.get();
	}

	// свойство кондер под током
	public BooleanProperty isCurrent = new SimpleBooleanProperty();

	public final BooleanProperty isCurrentProperty() {
		return isCurrent;
	}

	public final void setIsCurrent(boolean newValue) {
		isCurrent.set(newValue);
	}

	public final boolean getIsCurrent() {
		return isCurrent.get();
	}

	final AnimationTimer atRazrad = new AnimationTimer() {

		@Override
		public void handle(long arg0) {
			count++;
			if (count == razrad) {
				if (!getIsCurrent()) {
					terminals.get(0).setResistance(obryv);
					isZarad = false;
				}
				stopRazrad();
			}
		}
	};

	public Conder(boolean isHorizont) {

		setIsHorizont(isHorizont);
		double centerY = 40;

		l1 = new Line(0, centerY, 20, centerY);
		l3 = new Line(20, centerY - 10, 20, centerY + 10);
		l2 = new Line(30, centerY, 50, centerY);
		l4 = new Line(30, centerY - 10, 30, centerY + 10);

		l3.setStrokeWidth(2.5);
		l4.setStrokeWidth(2.5);

		if (!getIsHorizont()) {
			getTransforms().addAll(new Rotate(-90, Rotate.Z_AXIS));
		}

		isHorizontProperty().addListener((property, oldValue, newValue) -> {
			if (newValue.booleanValue())
				getTransforms().addAll(new Rotate(90, Rotate.Z_AXIS));
			else
				getTransforms().addAll(new Rotate(-90, Rotate.Z_AXIS));
		});

		// полюсы для работы эл.цепи
		terminals.add(0, new PowerSourceTerminal("", ""));
		terminals.get(0).setResistance(obryv);
		minusTerminals.add(0, new PowerSourceTerminal("", ""));
		minusTerminals.get(0).setMinus(true);
		// установка конденсатора в разряженное положение
		isZarad = false;
		// установка времени разряда в 1/60 секундах
		razrad = 60;
		setResistance(10000);
	}

	// свойство тип тока постоянный - true
	// ------------------------------------------------
	public BooleanProperty isConst = new SimpleBooleanProperty();

	public BooleanProperty isConstProperty() {
		return isConst;
	}

	public void setIsConst(boolean newValue) {
		isConst.set(newValue);
	}

	public boolean getIsConst() {
		return isConst.get();
	}

	// ЭДС источника
	public DoubleProperty eds = new SimpleDoubleProperty();

	public DoubleProperty edsProperty() {
		return eds;
	}

	public void setEds(double newValue) {
		eds.set(newValue);
	}

	public double getEds() {
		return eds.get();
	}

	// внутреннее сопротивление источника
	public DoubleProperty inR = new SimpleDoubleProperty();

	public DoubleProperty inRProperty() {
		return inR;
	}

	public void setInR(double newValue) {
		inR.set(newValue);
	}

	public double getInR() {
		return inR.get();
	}

	/**
	 * список полюсов источника пиания
	 * 
	 */

	public PowerSourceTerminal getTerminal(int i) {
		return terminals.get(i);
	}

	public List<PowerSourceTerminal> getMinusTerminals() {
		return minusTerminals;
	}

	public void addTerminal(PowerSourceTerminal t) {
		if (t != null
				&& terminals.stream().allMatch((l) -> !l.equals(t))
				&& terminals.stream().allMatch(
						(te) -> !te.getVarName().equals(t.getVarName()))) {
			terminals.add(t);
		}
	}

	public void addMinusTerminal(PowerSourceTerminal t) {
		if (t != null
				&& minusTerminals.stream().allMatch((l) -> !l.equals(t))
				&& minusTerminals.stream().allMatch(
						(te) -> !te.getVarName().equals(t.getVarName())))
			minusTerminals.add(t);
	}

	public List<PowerSourceTerminal> getTerminals() {
		return terminals;
	}

	public boolean isTerminalConteined(String varName) {
		return (getTerminal(varName) != null) ? true : false;
	}

	public SchemElement getTerminal(String varName) {
		SchemElement temp = null;
		for (SchemElement sc : terminals) {
			if (sc.getVarName().equals(varName))
				temp = sc;
		}
		if (temp == null)
			temp = getMinusTerminal(varName);

		return temp;
	}

	public SchemElement getMinusTerminal(String newValue) {
		SchemElement temp = null;
		for (SchemElement sc : minusTerminals) {
			if (sc.getVarName().equals(newValue))
				temp = sc;
		}
		return temp;
	}

	public boolean isTerminalConteined(PowerSourceTerminal term) {
		return terminals.stream().anyMatch((t) -> t.equals(term))
				|| minusTerminals.stream().anyMatch((t) -> t.equals(term));
	}

	public int getIndexTerminal(PowerSourceTerminal term) {
		return (isTerminalConteined(term)) ? (terminals.stream()
				.anyMatch((t) -> t.equals(term))) ? terminals.indexOf(term)
				: minusTerminals.indexOf(term) : -1;
	}

	public int getIndexTerminal(String varName) {
		int i = -1;
		if (getTerminals().stream().anyMatch(
				(t) -> t.getVarName().equals(varName)))
			i = terminals.indexOf(this.getTerminal(varName));
		return i;
	}

	public int getIndexMinusTerminal(String varName) {
		int i = -1;
		if (getMinusTerminals().stream().anyMatch(
				(t) -> t.getVarName().equals(varName)))
			i = minusTerminals.indexOf(this.getTerminal(varName));
		return i;
	}

	public PowerSourceTerminal getMinusTerminal(int i) {
		return minusTerminals.get(i);
	}

	public long getRazrad() {
		return razrad;
	}

	public void setRazrad(long razrad) {
		this.razrad = razrad;
	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(l1, l3, l2, l4);
	}

	@Override
	public void setCurrent(boolean flag) {
		setIsCurrent(flag);
		if (flag) {
			isZarad = true;
			terminals.get(0).setResistance(10);
			count = 0;

		} else {
			if (isZarad) {
				terminals.get(0).setResistance(10);
				startRazrad();
			} else {
				terminals.get(0).setResistance(obryv);
			}
		}
	}

	@Override
	public void setTok(double i) {
		// TODO Auto-generated method stub

	}

	public void startRazrad() {
		atRazrad.start();
	}

	public void stopRazrad() {
		atRazrad.stop();
	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub

	}
}
