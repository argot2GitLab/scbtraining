﻿package com.alex.training.schemElement;

import java.util.List;

import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class ReleOMSHM extends Rele {

	private final double downCur = 0.4;
	private final double upCur = 0.9;
	private final double dangerCur = 3.0;
	private final String error = "Сгорела обмотка реле";
	protected Diod diod;

	public ReleOMSHM(String name, String place, boolean isCur) {
		super(name, place, isCur);
		double centerY = 40;

		l1 = new Line(0, centerY, 22, centerY);
		endX = 80;
		endY = centerY;
		l2 = new Line(58, centerY, endX, centerY);

		txV1 = new Text("4");
		txV1.setX(6);
		txV1.setY(centerY + 15);

		txV2 = new Text("1");
		txV2.setX(60);
		txV2.setY(centerY + 15);

		diod = new Diod(true);
		diod.drowElement(28, 10);

		// подсветка точек подключения тестера
		startRect.setX(-5);
		startRect.setY(35);
		this.getChildren().addAll(cir, txName, txPlace, l1, l2, txV1, txV2,
				diod);

	}

	@Override
	public void setCurrent(boolean flag) {
		if (flag) {
			if (getProvodI() >= upCur && getProvodI() <= dangerCur)
				setIsCurrent(flag);
		} else {
			if (getProvodI() <= downCur)
				setIsCurrent(flag);
		}
	}

	@Override
	public double getEndX() {
		return endX;
	}

	@Override
	public double getEndY() {
		return endY;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
	}

	@Override
	public void changeContact(boolean firstLeft) {
		// TODO Auto-generated method stub

	}

}
