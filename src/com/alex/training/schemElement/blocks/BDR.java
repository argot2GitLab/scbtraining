﻿package com.alex.training.schemElement.blocks;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

import com.alex.training.schem.Source;
import com.alex.training.schem.User;
import com.alex.training.schemElement.Diod;
import com.alex.training.schemElement.PowerSourceTerminal;
import com.alex.training.schemElement.Resistor;
import com.alex.training.schemElement.SchemElement;

public class BDR extends SchemElement implements Source, User {

	private Line l1, l2;
	private Rectangle rect;
	private Diod diod;
	private Resistor res;

	// для электрических цепей
	protected PowerSourceTerminal term1, term2;
	protected List<PowerSourceTerminal> terminals = new ArrayList<PowerSourceTerminal>();
	protected List<PowerSourceTerminal> minusTerminals = new ArrayList<PowerSourceTerminal>();


	public BDR() {

		rect = new Rectangle(20, 20, 40, 100);
		rect.setFill(Color.TRANSPARENT);
		rect.setStroke(Color.BLACK);
		rect.getStrokeDashArray().addAll(15., 8.);

		diod = new Diod(false);
		diod.drowElement(10, 110);

		res = new Resistor("", "", false);
		res.drowElement(0, 90);

		l1 = new Line(40, 5, 40, 40);
		l2 = new Line(40, 110, 40, 135);

		setIsConst(true);
		// выводы вторичной обмотки для работы эл.цепи
		term1 = new PowerSourceTerminal("", "");
		term1.setMinus(false);
		term2 = new PowerSourceTerminal("", "");
		term2.setMinus(true);

		terminals.add(0, term1);
		minusTerminals.add(0, term2);
	}
	
	// свойство тип тока постоянный - true
	// ------------------------------------------------
	public BooleanProperty isConst = new SimpleBooleanProperty();

	public BooleanProperty isConstProperty() {
		return isConst;
	}

	public void setIsConst(boolean newValue) {
		isConst.set(newValue);
	}

	public boolean getIsConst() {
		return isConst.get();
	}

	// ЭДС источника
	public DoubleProperty eds = new SimpleDoubleProperty();

	public DoubleProperty edsProperty() {
		return eds;
	}

	public void setEds(double newValue) {
		eds.set(newValue);
	}

	public double getEds() {
		return eds.get();
	}

	// внутреннее сопротивление источника
	public DoubleProperty inR = new SimpleDoubleProperty();

	public DoubleProperty inRProperty() {
		return inR;
	}

	public void setInR(double newValue) {
		inR.set(newValue);
	}

	public double getInR() {
		return inR.get();
	}

	/**
	 * список полюсов источника пиания
	 * 
	 */


	public PowerSourceTerminal getTerminal(int i) {
		return terminals.get(i);
	}

	public List<PowerSourceTerminal> getMinusTerminals() {
		return minusTerminals;
	}

	public void addTerminal(PowerSourceTerminal t) {
		if (t != null
				&& terminals.stream().allMatch((l) -> !l.equals(t))
				&& terminals.stream().allMatch(
						(te) -> !te.getVarName().equals(t.getVarName())))
			terminals.add(t);

	}

	public void addMinusTerminal(PowerSourceTerminal t) {
		if (t != null
				&& minusTerminals.stream().allMatch((l) -> !l.equals(t))
				&& minusTerminals.stream().allMatch(
						(te) -> !te.getVarName().equals(t.getVarName())))
			minusTerminals.add(t);
	}

	public List<PowerSourceTerminal> getTerminals() {
		return terminals;
	}

	public boolean isTerminalConteined(String varName) {
		return (getTerminal(varName) != null) ? true : false;
	}

	public SchemElement getTerminal(String varName) {
		SchemElement temp = null;
		for (SchemElement sc : terminals) {
			if (sc.getVarName().equals(varName))
				temp = sc;
		}
		if (temp == null)
			temp = getMinusTerminal(varName);

		return temp;
	}

	public SchemElement getMinusTerminal(String newValue) {
		SchemElement temp = null;
		for (SchemElement sc : minusTerminals) {
			if (sc.getVarName().equals(newValue))
				temp = sc;
		}
		return temp;
	}

	public boolean isTerminalConteined(PowerSourceTerminal term) {
		return terminals.stream().anyMatch((t) -> t.equals(term))
				|| minusTerminals.stream().anyMatch((t) -> t.equals(term));
	}

	public int getIndexTerminal(PowerSourceTerminal term) {
		return (isTerminalConteined(term)) ? (terminals.stream()
				.anyMatch((t) -> t.equals(term))) ? terminals.indexOf(term)
				: minusTerminals.indexOf(term) : -1;
	}

	public int getIndexTerminal(String varName) {
		int i = -1;
		if (getTerminals().stream().anyMatch(
				(t) -> t.getVarName().equals(varName)))
			i = terminals.indexOf(this.getTerminal(varName));
		return i;
	}

	public int getIndexMinusTerminal(String varName) {
		int i = -1;
		if (getMinusTerminals().stream().anyMatch(
				(t) -> t.getVarName().equals(varName)))
			i = minusTerminals.indexOf(this.getTerminal(varName));
		return i;
	}

	public PowerSourceTerminal getMinusTerminal(int i) {
		return minusTerminals.get(i);
	}


	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		getChildren().addAll(rect, diod, res, l1, l2);
	}

	@Override
	public void setCurrent(boolean flag) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setTok(double i) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
