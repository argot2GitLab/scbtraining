﻿package com.alex.training.schemElement.blocks;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import com.alex.training.schemElement.SchemElement;
import com.alex.training.schemElement.SvetLamp;

public class SvetGol extends SchemElement {

	protected SvetLamp lamp, lamp1;
//	 *            список ламп светофора 3- зеленый 0- белый 2- КЖ 4- красный
//	 *            - 1 желтый

	protected List<SvetLamp> lamps = new ArrayList<SvetLamp>();
	protected Rectangle boxS;
	private List<Integer> numColor;
	private double y = 40;

	/**
	 * @param name
	 * @param num
	 *            - длина массива -количество показаний светофора элементы цвет
	 *            ламп
	 * @param xl
	 * @param yl
	 */
	public SvetGol(String name, List<Integer> num) {
		numColor = num;
		Font font = Font.font("sans", FontWeight.MEDIUM, 20);
		txName.setFont(font);
		txName.setText(name);
		setResistance(12);
		for (int i = 0; i < 5; i++) {
			lamps.add(i, new SvetLamp());
		}
	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);

		for (int i = 0; i < numColor.size(); i++) {
			lamp = new SvetLamp(numColor.get(i));
			lamp.drowElement(0, y);
			lamp1 = new SvetLamp(numColor.get(i));
			lamp1.drowElement(0, y + 60);
			lamp1.setTxLamp2();
			y += 130;
			getChildren().addAll(lamp, lamp1);

		}

		boxS = new Rectangle(130, y += 50, Color.TRANSPARENT);
		boxS.setX(0);
		boxS.setY(20);
		boxS.setStroke(Color.BLACK);
		boxS.getStrokeDashArray().addAll(20., 10.);
		txName.setX(65 - txName.getBoundsInLocal().getWidth() / 2);
		txName.setY(y - 10);

		getChildren().addAll(boxS, txName);
	}

	public List<Integer> getNumColor() {
		return numColor;
	}

	public void setNumColor(List<Integer> numColor) {
		this.numColor = numColor;
	}

	public List<SvetLamp> getLamps() {
		return lamps;
	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
