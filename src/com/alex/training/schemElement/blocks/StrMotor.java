﻿package com.alex.training.schemElement.blocks;

import java.util.ArrayList;
import java.util.List;

import javafx.animation.AnimationTimer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

import com.alex.training.schem.Motor;
import com.alex.training.schem.User;
import com.alex.training.schemElement.Obmotka;
import com.alex.training.schemElement.ObmotkaMotor;
import com.alex.training.schemElement.SchemElement;

public class StrMotor extends SchemElement implements User, Motor {

	protected Arc[] ar = new Arc[9];
	protected Circle[] cir = new Circle[3];

	protected double radius = 10;
	protected double centerY = 50;
	protected double centerX = 30;
	protected double angle = 60;

	private long count;

	protected Line l1, l2, l3, l4, l5, l6;

	// для электрических цепей
	protected List<User> obmotki = new ArrayList<User>();

	// Свойство двигатель в работе
	public BooleanProperty isWork = new SimpleBooleanProperty();

	public final BooleanProperty isWorkProperty() {
		return isWork;
	}

	public final void setIsWork(boolean newValue) {
		isWork.set(newValue);
	}

	public final boolean getIsWork() {
		return isWork.get();
	}

	// Свойство направление вращения двигателя (true -в плюс), (false - в минус)
	public BooleanProperty inPlus = new SimpleBooleanProperty();

	public final BooleanProperty inPlusProperty() {
		return inPlus;
	}

	public final void setInPlus(boolean newValue) {
		inPlus.set(newValue);
	}

	public final boolean getInPlus() {
		return inPlus.get();
	}

	// Свойство стрелка замкнута в минусе)
	public BooleanProperty isMinusZam = new SimpleBooleanProperty();

	public final BooleanProperty isMinusZamProperty() {
		return isMinusZam;
	}

	public final void setIsMinusZam(boolean newValue) {
		isMinusZam.set(newValue);
	}

	public final boolean getIsMinusZam() {
		return isMinusZam.get();
	}

	// Свойство стрелка замкнута в плюсе)
	public BooleanProperty isPlusZam = new SimpleBooleanProperty();

	public final BooleanProperty isPlusZamProperty() {
		return isPlusZam;
	}

	public final void setIsPlusZam(boolean newValue) {
		isPlusZam.set(newValue);
	}

	public final boolean getIsPlusZam() {
		return isPlusZam.get();
	}

	private final AnimationTimer atStrPerevod = new AnimationTimer() {

		@Override
		public void handle(long now) {
			if (count == 210) {
				stopPerevod(getInPlus());
				// System.out.println("xren");
			}

			count++;

		}

	};

	public StrMotor(String name, String place) {
		super(name, place);

		setIsWork(false);
		setInPlus(false);
		setIsPlusZam(true);
		setIsMinusZam(false);

		// первичная и вторичная обмотки трансформатора
		for (int i = 0; i < ar.length; i++) {
			ar[i] = new Arc(centerX, centerY, radius, radius, angle, 180);
			ar[i].setFill(Color.TRANSPARENT);
			ar[i].setStroke(Color.BLACK);

			getChildren().add(ar[i]);
			if (i <= 2) {
				centerY += radius * 1.7;
				centerX -= radius * 1.2;
			}
			if (i == 2) {
				centerY = 50;
				centerX = 60;
				angle = 300;
			}
			if (i > 2 && i <= 5) {
				centerY += radius * 1.7;
				centerX += radius * 1.2;
			}

			if (i == 5) {
				centerY = 105;
				centerX = 24;
				angle = 360;
			}
			if (i > 5 && i <= 8) {
				centerX += radius * 2;
			}

		}

		// кружки по краям обмоток
		centerX = 46;
		centerY = 28;
		radius = 3;
		for (int i = 0; i < cir.length; i++) {
			cir[i] = new Circle(centerX, centerY, radius, Color.TRANSPARENT);
			cir[i].setStroke(Color.BLACK);
			if (i == 0) {
				centerY = 102;
				centerX = -5;
			}
			if (i == 1) {
				centerY = 102;
				centerX = 95;
			}
			getChildren().add(cir[i]);
		}

		endY = 100; // нижний вывод трансформатора
		endX = 82; // дальний вывод трансформатора

		// отводы от катушек
		l1 = new Line(36, 42, 43, 28);
		l2 = new Line(49, 28, 54, 42);
		l3 = new Line(1, 94, -3, 100);
		l4 = new Line(-2, 105, 14, 105);
		l5 = new Line(74, 105, 93, 105);
		l6 = new Line(88, 94, 93, 99);

		txName.setText("(МСТ-0,25)");
		txName.setX(60);
		txName.setY(30);

		centerX = 23;
		// номера выводов

		// подсветка точек подключения тестера
		startRect.setX(-5);
		startRect.setY(35);
		startRect1.setX(-5);
		startRect1.setY(95);

		// перевод стрелки в минус--------------------------
		obmotki.add(0, new ObmotkaMotor(11.5, 0)); // обмотка АВ при нормальном
													// положении стрелки (+)
		obmotki.add(1, new ObmotkaMotor(11.5, 1)); // обмотка АС при нормальном
													// положении стрелки
		obmotki.add(2, new ObmotkaMotor(11.5, 2)); // обмотка ВС при нормальном
													// положении стрелки

		((ObmotkaMotor) obmotki.get(0)).isCurrentProperty().addListener(
				(property, oldValue, newValue) -> {
					setCurrent(newValue.booleanValue());
				});
		((ObmotkaMotor) obmotki.get(1)).isCurrentProperty().addListener(
				(property, oldValue, newValue) -> {
					setCurrent(newValue.booleanValue());
				});
		((ObmotkaMotor) obmotki.get(2)).isCurrentProperty().addListener(
				(property, oldValue, newValue) -> {
					setCurrent(newValue.booleanValue());
				});
		// ------------------------------------------

		// перевод стрелки в плюс
		obmotki.add(3, new ObmotkaMotor(11.5, 3)); // обмотка ВА при положении
													// стрелки (-)
		obmotki.add(4, new ObmotkaMotor(11.5, 4)); // обмотка ВС при положении
													// стрелки (-)
		obmotki.add(5, new ObmotkaMotor(11.5, 5)); // обмотка АС при положении//
													// стрелки (-)

		((ObmotkaMotor) obmotki.get(3)).isCurrentProperty().addListener(
				(property, oldValue, newValue) -> {
					setCurrent(newValue.booleanValue());
				});
		((ObmotkaMotor) obmotki.get(4)).isCurrentProperty().addListener(
				(property, oldValue, newValue) -> {
					setCurrent(newValue.booleanValue());
				});
		((ObmotkaMotor) obmotki.get(5)).isCurrentProperty().addListener(
				(property, oldValue, newValue) -> {
					setCurrent(newValue.booleanValue());
				});

	}

	public double getEndX() {
		return endX;
	}

	public double getEndY() {
		return endY;
	}

	public double getTopY() {
		return centerY;
	}

	public void stopPerevod(boolean inPlus) {
		atStrPerevod.stop();
		setIsWork(false);
		if (inPlus) {
			setIsPlusZam(true);
		} else {
			setIsMinusZam(true);

		}

	}

	/**
	 * inPlus - направление вращения двигателя
	 */
	public void startPerevod(boolean inPlus) {
		count = 0;
		atStrPerevod.start();
		setIsWork(true);
		if (inPlus) {
			setIsMinusZam(false);
		} else {
			setIsPlusZam(false);
		}
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		this.getChildren().addAll(l1, l2, l3, l4, l5, l6, txName, txPlace,
				startRect, startRect1);
	}

	public List<User> getObmotki() {
		return obmotki;
	}

	@Override
	public void setCurrent(boolean flag) {
		if (flag) {
			if (((ObmotkaMotor) obmotki.get(0)).getIsCurrent()
					&& ((ObmotkaMotor) obmotki.get(1)).getIsCurrent()
					&& ((ObmotkaMotor) obmotki.get(2)).getIsCurrent()) {
				setInPlus(false);
				startPerevod(getInPlus());
			}
			if (((ObmotkaMotor) obmotki.get(3)).getIsCurrent()
					&& ((ObmotkaMotor) obmotki.get(4)).getIsCurrent()
					&& ((ObmotkaMotor) obmotki.get(5)).getIsCurrent()) {
				setInPlus(true);
				startPerevod(getInPlus());
			}
		}
	}

	@Override
	public void setTok(double i) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
