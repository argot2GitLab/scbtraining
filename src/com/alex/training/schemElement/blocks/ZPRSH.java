﻿package com.alex.training.schemElement.blocks;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import com.alex.training.schemElement.SchemElement;

public class ZPRSH extends SchemElement {

	private Rectangle rect;
	private Circle v1, v2, v3, v4;
	private Text t1, t2, t3, t4;

	public ZPRSH(String place) {
		super("", place);

		rect = new Rectangle(160, 60, Color.WHITE);
		rect.setX(20);
		rect.setY(20);
		rect.setStroke(Color.BLACK);

		v4 = new Circle(40, 20, 5, Color.WHITE);
		v4.setStroke(Color.BLACK);
		v3 = new Circle(160, 20, 5, Color.WHITE);
		v3.setStroke(Color.BLACK);
		v2 = new Circle(40, 80, 5, Color.WHITE);
		v2.setStroke(Color.BLACK);
		v1 = new Circle(160, 80, 5, Color.WHITE);
		v1.setStroke(Color.BLACK);

		t1 = new Text("1");
		t1.setLayoutX(150);
		t1.setLayoutY(70);

		t2 = new Text("2");
		t2.setLayoutX(40);
		t2.setLayoutY(70);

		t3 = new Text("3");
		t3.setLayoutX(150);
		t3.setLayoutY(40);

		t4 = new Text("4");
		t4.setLayoutX(40);
		t4.setLayoutY(40);

		txName.setText("зПРШ-2");
		txName.setFont(Font.font("sans", FontWeight.MEDIUM, 20));
		txName.setLayoutX(100 - txName.getBoundsInLocal().getWidth() / 2);
		txName.setLayoutY(60);
		txPlace.setLayoutX(100 - txName.getBoundsInLocal().getWidth() / 2);
		txPlace.setLayoutY(15);

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		this.getChildren().addAll(rect, txName, txPlace, v4, v3, v2, v1, t1,
				t2, t3, t4);
	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
