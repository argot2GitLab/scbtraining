﻿package com.alex.training.schemElement.blocks;

import java.util.HashMap;
import java.util.Map;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import com.alex.training.schemElement.SchemElement;

public class BSSH extends SchemElement {

	private Map<Integer, Circle> mapCont = new HashMap<>();
	private Map<Integer, Text> mapTxCont = new HashMap<>();

	protected Rectangle box;
	protected final String[] cont = new String[] { "71", "51", "52", "33",
			"13", "62", "82", "72", "31", "32", "11", "12", "22", "42", "73",
			"53" };

	public BSSH(String name, String place) {
		super(name, place);

		box = new Rectangle(150, 800);
		box.setFill(Color.TRANSPARENT);
		box.setStroke(Color.BLACK);
		box.getStrokeDashArray().addAll(20., 10.);
		box.setX(10);
		box.setY(40);

		txName.setX(85 - txName.getBoundsInLocal().getWidth() / 2);
		txName.setY(35);

		txPlace.setX(85 - txPlace.getBoundsInLocal().getWidth() / 2);
		txPlace.setY(17);

	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);

		getChildren().addAll(box, txName, txPlace);

		double x = 10;
		double y = 80;
		double xD = 10;
		for (int i = 0; i < cont.length; i++) {
			if (i == 7) {
				x = 160;
				y = 80;
				xD = -25;
			}
			mapCont.put(i, new Circle(x, y, 4, Color.WHITE));
			mapCont.get(i).setStroke(Color.BLACK);
			mapTxCont.put(i, new Text(x + xD, y, cont[i]));
			getChildren().addAll(mapCont.get(i), mapTxCont.get(i));
			y += 120;
		}

		mapCont.get(2).setCenterY(120);
		mapTxCont.get(2).setY(120);

	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
