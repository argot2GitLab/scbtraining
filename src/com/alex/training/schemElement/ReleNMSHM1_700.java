﻿package com.alex.training.schemElement;

import java.util.List;

import javafx.scene.paint.Color;
import javafx.scene.shape.Arc;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

public class ReleNMSHM1_700 extends Rele {

	private Arc zamedlenie;

	public ReleNMSHM1_700() {
		super();
	}

	public ReleNMSHM1_700(boolean isCur) {
		super();
		setIsCurrent(isCur);
	}

	public ReleNMSHM1_700(String name, String place, boolean isCur) {
		super(name, place, isCur);

		double centerY = 40;

		zamedlenie = new Arc(40, 48, 16, 10, 180, 180);
		zamedlenie.setFill(Color.BLACK);

		l1 = new Line(0, centerY, 22, centerY);
		l2 = new Line(58, centerY, 80, centerY);

		txV1 = new Text("1");
		txV1.setX(12);
		txV1.setY(centerY + 15);

		txV2 = new Text("3");
		txV2.setX(60);
		txV2.setY(centerY + 15);
		this.getChildren().addAll(cir, txName, txPlace, l1, l2, txV1, txV2,
				zamedlenie);
	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
	}

	@Override
	public void setCurrent(boolean flag) {
		if (!flag)
			super.setCurrent(flag);
		isCurrent.set(flag);
	}

	@Override
	public void setTok(double i) {
		// TODO Auto-generated method stub

	}

	@Override
	public void changeContact(boolean firstLeft) {
		if (firstLeft) {
			txV1.setText("1");
			txV2.setText("3");
		} else {
			txV1.setText("3");
			txV2.setText("1");

		}
	}

}
