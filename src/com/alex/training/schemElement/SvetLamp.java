﻿package com.alex.training.schemElement;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class SvetLamp extends SchemElement {

	protected Circle circle;
	protected Line lPr;
	protected Polyline lObr;
	protected Text txPr, txObr;
	private Circle cInner = null;
	private long indexParent;
	private int indexLamp;
	private List<Line> go = new ArrayList<Line>();

	/**
	 * @param color
	 *            показание светофора 3- зеленый 0- белый 2- КЖ 4- красный - 1
	 *            желтый
	 * @param xl
	 * @param yl
	 */

	public SvetLamp() {
		super();
		setResistance(12);
	}

	public SvetLamp(int color) {

		circle = new Circle(60, 20, 15);
		circle.setStroke(Color.BLACK);

		// штриховка для желтого
		go.add(new Line(46, 20, 60, 6));
		go.add(new Line(47, 25, 65, 7));
		go.add(new Line(50, 30, 70, 10));
		go.add(new Line(55, 33, 73, 14));
		go.add(new Line(60, 34, 74, 20));

		go.forEach((l) -> {
			l.setVisible(false);
		});

		// прямой провод
		lPr = new Line(0, 20, 45, 20);

		String provod = "О";
		String col = "";
		// обратный провод
		Double[] points = new Double[] { 75., 20., 90., 20., 90., 55., 0., 55. };
		lObr = new Polyline();
		lObr.getPoints().addAll(points);

		// обозначение проводов

		txPr = new Text(10, 17, "");
		txObr = new Text(10, 52, "");

		// подсветка точек подключения тестера
		startRect.setX(-5);
		startRect.setY(15);
		startRect1.setX(-5);
		startRect1.setY(50);

		switch (color) {
		case 3: {
			circle.setFill(Color.WHITE);
			col = "З";
		}
			break;

		case 0: {
			circle.setFill(Color.WHITE);
			cInner = new Circle(60, 20, 7, Color.WHITE);
			cInner.setStroke(Color.BLACK);
			col = "Б";
		}
			break;

		case 4: {
			circle.setFill(Color.BLACK);
			col = "К";
		}
			break;
		case 2: {
			circle.setFill(Color.TRANSPARENT);
			col = "КЖ";
			go.forEach((l) -> {
				l.setVisible(true);
			});
		}
			break;
		case 1: {
			circle.setFill(Color.TRANSPARENT);
			col = "Ж";
			go.forEach((l) -> {
				l.setVisible(true);
			});
		}
			break;

		default:
			break;
		}
		Font f = Font.font("sans", FontWeight.MEDIUM, 18);
		txPr.setText(col);
		txPr.setFont(f);
		txObr.setText(provod + col);
		txObr.setFont(f);

	}

	public void setTxLamp2() {
		String temp = txPr.getText() + "1";
		txPr.setText(temp);
		temp = txObr.getText() + "1";
		txObr.setText(temp);
	}

	@Override
	public double getEndX() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getEndY() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void drowElement(double xl, double yl) {
		this.setLayoutX(xl);
		this.setLayoutY(yl);
		go.forEach((l) -> {
			getChildren().add(l);
		});
		getChildren().addAll(circle, lPr, lObr, txPr, txObr, startRect,
				startRect1);
		if (cInner != null)
			getChildren().add(cInner);
	}

	public long getIndexParent() {
		return indexParent;
	}

	public void setIndexParent(long l) {
		this.indexParent = l;
	}

	public int getIndexLamp() {
		return indexLamp;
	}

	public void setIndexLamp(int indexLamp) {
		this.indexLamp = indexLamp;
	}

	@Override
	public void setupError(boolean flag, int mod) {
		// TODO Auto-generated method stub
		
	}

}
